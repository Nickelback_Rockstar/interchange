package spoon.bot.balance.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QPolygonBalance2 is a Querydsl query type for PolygonBalance2
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QPolygonBalance2 extends EntityPathBase<PolygonBalance2> {

    private static final long serialVersionUID = 1954411611L;

    public static final QPolygonBalance2 polygonBalance2 = new QPolygonBalance2("polygonBalance2");

    public final StringPath betType = createString("betType");

    public final StringPath game = createString("game");

    public final StringPath gameDate = createString("gameDate");

    public final StringPath gameType = createString("gameType");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath message = createString("message");

    public final NumberPath<Long> price = createNumber("price", Long.class);

    public final NumberPath<Long> realPrice = createNumber("realPrice", Long.class);

    public final DateTimePath<java.util.Date> regDate = createDateTime("regDate", java.util.Date.class);

    public final StringPath round = createString("round");

    public QPolygonBalance2(String variable) {
        super(PolygonBalance2.class, forVariable(variable));
    }

    public QPolygonBalance2(Path<? extends PolygonBalance2> path) {
        super(path.getType(), path.getMetadata());
    }

    public QPolygonBalance2(PathMetadata metadata) {
        super(PolygonBalance2.class, metadata);
    }

}

