package spoon.bot.balance.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QPolygonBalance1 is a Querydsl query type for PolygonBalance1
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QPolygonBalance1 extends EntityPathBase<PolygonBalance1> {

    private static final long serialVersionUID = 1954411610L;

    public static final QPolygonBalance1 polygonBalance1 = new QPolygonBalance1("polygonBalance1");

    public final StringPath betType = createString("betType");

    public final StringPath game = createString("game");

    public final StringPath gameDate = createString("gameDate");

    public final StringPath gameType = createString("gameType");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath message = createString("message");

    public final NumberPath<Long> price = createNumber("price", Long.class);

    public final NumberPath<Long> realPrice = createNumber("realPrice", Long.class);

    public final DateTimePath<java.util.Date> regDate = createDateTime("regDate", java.util.Date.class);

    public final StringPath round = createString("round");

    public QPolygonBalance1(String variable) {
        super(PolygonBalance1.class, forVariable(variable));
    }

    public QPolygonBalance1(Path<? extends PolygonBalance1> path) {
        super(path.getType(), path.getMetadata());
    }

    public QPolygonBalance1(PathMetadata metadata) {
        super(PolygonBalance1.class, metadata);
    }

}

