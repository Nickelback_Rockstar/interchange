package spoon.sale.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QPowerLottoSaleItem is a Querydsl query type for PowerLottoSaleItem
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QPowerLottoSaleItem extends EntityPathBase<PowerLottoSaleItem> {

    private static final long serialVersionUID = -943862718L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QPowerLottoSaleItem powerLottoSaleItem = new QPowerLottoSaleItem("powerLottoSaleItem");

    public final StringPath agency1 = createString("agency1");

    public final StringPath agency2 = createString("agency2");

    public final StringPath agency3 = createString("agency3");

    public final StringPath agency4 = createString("agency4");

    public final StringPath agency5 = createString("agency5");

    public final StringPath agency6 = createString("agency6");

    public final StringPath agency7 = createString("agency7");

    public final NumberPath<Integer> agencyDepth = createNumber("agencyDepth", Integer.class);

    public final NumberPath<Long> betPowerLotto = createNumber("betPowerLotto", Long.class);

    public final NumberPath<Long> betSports = createNumber("betSports", Long.class);

    public final NumberPath<Long> betZone = createNumber("betZone", Long.class);

    public final NumberPath<Long> betZone1 = createNumber("betZone1", Long.class);

    public final NumberPath<Long> betZone2 = createNumber("betZone2", Long.class);

    public final BooleanPath closing = createBoolean("closing");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final NumberPath<Long> inMoney = createNumber("inMoney", Long.class);

    public final NumberPath<Long> lastMoney = createNumber("lastMoney", Long.class);

    public final StringPath nickname = createString("nickname");

    public final NumberPath<Long> outMoney = createNumber("outMoney", Long.class);

    public final QPowerLottoSale powerLottoSale;

    public final StringPath rateCode = createString("rateCode");

    public final NumberPath<Double> ratePowerLotto = createNumber("ratePowerLotto", Double.class);

    public final NumberPath<Double> rateShare = createNumber("rateShare", Double.class);

    public final NumberPath<Double> rateSports = createNumber("rateSports", Double.class);

    public final NumberPath<Double> rateZone = createNumber("rateZone", Double.class);

    public final NumberPath<Double> rateZone1 = createNumber("rateZone1", Double.class);

    public final NumberPath<Double> rateZone2 = createNumber("rateZone2", Double.class);

    public final DateTimePath<java.util.Date> regDate = createDateTime("regDate", java.util.Date.class);

    public final EnumPath<spoon.member.domain.Role> role = createEnum("role", spoon.member.domain.Role.class);

    public final NumberPath<Long> tmpMoney1 = createNumber("tmpMoney1", Long.class);

    public final NumberPath<Long> tmpMoney2 = createNumber("tmpMoney2", Long.class);

    public final NumberPath<Long> tmpMoney3 = createNumber("tmpMoney3", Long.class);

    public final NumberPath<Long> tmpMoney4 = createNumber("tmpMoney4", Long.class);

    public final NumberPath<Long> tmpMoney5 = createNumber("tmpMoney5", Long.class);

    public final NumberPath<Long> tmpMoney6 = createNumber("tmpMoney6", Long.class);

    public final NumberPath<Long> tmpMoney7 = createNumber("tmpMoney7", Long.class);

    public final NumberPath<Long> totalMoney = createNumber("totalMoney", Long.class);

    public final StringPath userid = createString("userid");

    public final NumberPath<Long> users = createNumber("users", Long.class);

    public QPowerLottoSaleItem(String variable) {
        this(PowerLottoSaleItem.class, forVariable(variable), INITS);
    }

    public QPowerLottoSaleItem(Path<? extends PowerLottoSaleItem> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QPowerLottoSaleItem(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QPowerLottoSaleItem(PathMetadata metadata, PathInits inits) {
        this(PowerLottoSaleItem.class, metadata, inits);
    }

    public QPowerLottoSaleItem(Class<? extends PowerLottoSaleItem> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.powerLottoSale = inits.isInitialized("powerLottoSale") ? new QPowerLottoSale(forProperty("powerLottoSale")) : null;
    }

}

