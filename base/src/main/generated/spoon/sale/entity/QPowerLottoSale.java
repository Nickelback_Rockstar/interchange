package spoon.sale.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QPowerLottoSale is a Querydsl query type for PowerLottoSale
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QPowerLottoSale extends EntityPathBase<PowerLottoSale> {

    private static final long serialVersionUID = -1963344753L;

    public static final QPowerLottoSale powerLottoSale = new QPowerLottoSale("powerLottoSale");

    public final StringPath agency1 = createString("agency1");

    public final StringPath agency2 = createString("agency2");

    public final StringPath agency3 = createString("agency3");

    public final StringPath agency4 = createString("agency4");

    public final StringPath agency5 = createString("agency5");

    public final StringPath agency6 = createString("agency6");

    public final StringPath agency7 = createString("agency7");

    public final BooleanPath closing = createBoolean("closing");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final ListPath<PowerLottoSaleItem, QPowerLottoSaleItem> powerLottoSaleItems = this.<PowerLottoSaleItem, QPowerLottoSaleItem>createList("powerLottoSaleItems", PowerLottoSaleItem.class, QPowerLottoSaleItem.class, PathInits.DIRECT2);

    public final DateTimePath<java.util.Date> regDate = createDateTime("regDate", java.util.Date.class);

    public final EnumPath<spoon.member.domain.Role> role = createEnum("role", spoon.member.domain.Role.class);

    public final StringPath userid = createString("userid");

    public QPowerLottoSale(String variable) {
        super(PowerLottoSale.class, forVariable(variable));
    }

    public QPowerLottoSale(Path<? extends PowerLottoSale> path) {
        super(path.getType(), path.getMetadata());
    }

    public QPowerLottoSale(PathMetadata metadata) {
        super(PowerLottoSale.class, metadata);
    }

}

