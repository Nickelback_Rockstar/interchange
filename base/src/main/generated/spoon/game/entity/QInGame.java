package spoon.game.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QInGame is a Querydsl query type for InGame
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QInGame extends EntityPathBase<InGame> {

    private static final long serialVersionUID = 180996163L;

    public static final QInGame inGame = new QInGame("inGame");

    public final StringPath closeGameInfo = createString("closeGameInfo");

    public final BooleanPath closing = createBoolean("closing");

    public final StringPath code = createString("code");

    public final StringPath gameCode = createString("gameCode");

    public final StringPath history = createString("history");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath league = createString("league");

    public final DateTimePath<java.util.Date> regDate = createDateTime("regDate", java.util.Date.class);

    public final NumberPath<Integer> score1 = createNumber("score1", Integer.class);

    public final NumberPath<Integer> score2 = createNumber("score2", Integer.class);

    public final StringPath sports = createString("sports");

    public final StringPath status = createString("status");

    public final StringPath teamName1 = createString("teamName1");

    public final StringPath temaName2 = createString("temaName2");

    public QInGame(String variable) {
        super(InGame.class, forVariable(variable));
    }

    public QInGame(Path<? extends InGame> path) {
        super(path.getType(), path.getMetadata());
    }

    public QInGame(PathMetadata metadata) {
        super(InGame.class, metadata);
    }

}

