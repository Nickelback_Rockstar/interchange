package spoon.event.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QLogger is a Querydsl query type for Logger
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QLogger extends EntityPathBase<Logger> {

    private static final long serialVersionUID = 1521393514L;

    public static final QLogger logger = new QLogger("logger");

    public final StringPath code = createString("code");

    public final StringPath data = createString("data");

    public final StringPath etc1 = createString("etc1");

    public final StringPath etc2 = createString("etc2");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final DateTimePath<java.util.Date> regDate = createDateTime("regDate", java.util.Date.class);

    public QLogger(String variable) {
        super(Logger.class, forVariable(variable));
    }

    public QLogger(Path<? extends Logger> path) {
        super(path.getType(), path.getMetadata());
    }

    public QLogger(PathMetadata metadata) {
        super(Logger.class, metadata);
    }

}

