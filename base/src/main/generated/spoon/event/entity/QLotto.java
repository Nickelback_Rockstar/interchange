package spoon.event.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QLotto is a Querydsl query type for Lotto
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QLotto extends EntityPathBase<Lotto> {

    private static final long serialVersionUID = 1711658098L;

    public static final QLotto lotto = new QLotto("lotto");

    public final BooleanPath cancel = createBoolean("cancel");

    public final BooleanPath closing = createBoolean("closing");

    public final DateTimePath<java.util.Date> closingDate = createDateTime("closingDate", java.util.Date.class);

    public final EnumPath<spoon.event.domain.EventCode> eventCode = createEnum("eventCode", spoon.event.domain.EventCode.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath memo = createString("memo");

    public final NumberPath<Long> point = createNumber("point", Long.class);

    public final DateTimePath<java.util.Date> regDate = createDateTime("regDate", java.util.Date.class);

    public final BooleanPath startEnd = createBoolean("startEnd");

    public final StringPath userid = createString("userid");

    public final StringPath worker = createString("worker");

    public QLotto(String variable) {
        super(Lotto.class, forVariable(variable));
    }

    public QLotto(Path<? extends Lotto> path) {
        super(path.getType(), path.getMetadata());
    }

    public QLotto(PathMetadata metadata) {
        super(Lotto.class, metadata);
    }

}

