package spoon.gameZone.fxGame5;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QFxGame5 is a Querydsl query type for FxGame5
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QFxGame5 extends EntityPathBase<FxGame5> {

    private static final long serialVersionUID = -2003474015L;

    public static final QFxGame5 fxGame5 = new QFxGame5("fxGame5");

    public final ArrayPath<long[], Long> amount = createArray("amount", long[].class);

    public final BooleanPath cancel = createBoolean("cancel");

    public final BooleanPath closing = createBoolean("closing");

    public final StringPath fxResult = createString("fxResult");

    public final DateTimePath<java.util.Date> gameDate = createDateTime("gameDate", java.util.Date.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath oddeven = createString("oddeven");

    public final ArrayPath<double[], Double> odds = createArray("odds", double[].class);

    public final StringPath overunder = createString("overunder");

    public final NumberPath<Integer> round = createNumber("round", Integer.class);

    public final StringPath sdate = createString("sdate");

    public QFxGame5(String variable) {
        super(FxGame5.class, forVariable(variable));
    }

    public QFxGame5(Path<? extends FxGame5> path) {
        super(path.getType(), path.getMetadata());
    }

    public QFxGame5(PathMetadata metadata) {
        super(FxGame5.class, metadata);
    }

}

