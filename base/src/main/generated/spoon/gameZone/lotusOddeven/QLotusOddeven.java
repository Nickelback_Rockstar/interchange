package spoon.gameZone.lotusOddeven;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QLotusOddeven is a Querydsl query type for LotusOddeven
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QLotusOddeven extends EntityPathBase<LotusOddeven> {

    private static final long serialVersionUID = -685023653L;

    public static final QLotusOddeven lotusOddeven = new QLotusOddeven("lotusOddeven");

    public final ArrayPath<long[], Long> amount = createArray("amount", long[].class);

    public final BooleanPath cancel = createBoolean("cancel");

    public final StringPath card1 = createString("card1");

    public final StringPath card2 = createString("card2");

    public final BooleanPath closing = createBoolean("closing");

    public final DateTimePath<java.util.Date> gameDate = createDateTime("gameDate", java.util.Date.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath oddeven = createString("oddeven");

    public final ArrayPath<double[], Double> odds = createArray("odds", double[].class);

    public final StringPath overunder = createString("overunder");

    public final StringPath pattern = createString("pattern");

    public final StringPath redblack = createString("redblack");

    public final NumberPath<Integer> round = createNumber("round", Integer.class);

    public final StringPath sdate = createString("sdate");

    public QLotusOddeven(String variable) {
        super(LotusOddeven.class, forVariable(variable));
    }

    public QLotusOddeven(Path<? extends LotusOddeven> path) {
        super(path.getType(), path.getMetadata());
    }

    public QLotusOddeven(PathMetadata metadata) {
        super(LotusOddeven.class, metadata);
    }

}

