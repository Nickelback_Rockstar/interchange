package spoon.gameZone.powerFreeKick;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QPowerFreeKick is a Querydsl query type for PowerFreeKick
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QPowerFreeKick extends EntityPathBase<PowerFreeKick> {

    private static final long serialVersionUID = 1054024109L;

    public static final QPowerFreeKick powerFreeKick = new QPowerFreeKick("powerFreeKick");

    public final ArrayPath<long[], Long> amount = createArray("amount", long[].class);

    public final BooleanPath cancel = createBoolean("cancel");

    public final BooleanPath closing = createBoolean("closing");

    public final StringPath direction = createString("direction");

    public final DateTimePath<java.util.Date> gameDate = createDateTime("gameDate", java.util.Date.class);

    public final StringPath goal = createString("goal");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath lastBall = createString("lastBall");

    public final ArrayPath<double[], Double> odds = createArray("odds", double[].class);

    public final StringPath player = createString("player");

    public final NumberPath<Integer> round = createNumber("round", Integer.class);

    public final StringPath sdate = createString("sdate");

    public final NumberPath<Integer> times = createNumber("times", Integer.class);

    public QPowerFreeKick(String variable) {
        super(PowerFreeKick.class, forVariable(variable));
    }

    public QPowerFreeKick(Path<? extends PowerFreeKick> path) {
        super(path.getType(), path.getMetadata());
    }

    public QPowerFreeKick(PathMetadata metadata) {
        super(PowerFreeKick.class, metadata);
    }

}

