package spoon.gameZone.speedHomeRun;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QSpeedHomeRun is a Querydsl query type for SpeedHomeRun
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QSpeedHomeRun extends EntityPathBase<SpeedHomeRun> {

    private static final long serialVersionUID = 40943675L;

    public static final QSpeedHomeRun speedHomeRun = new QSpeedHomeRun("speedHomeRun");

    public final ArrayPath<long[], Long> amount = createArray("amount", long[].class);

    public final BooleanPath cancel = createBoolean("cancel");

    public final BooleanPath closing = createBoolean("closing");

    public final StringPath direction = createString("direction");

    public final DateTimePath<java.util.Date> gameDate = createDateTime("gameDate", java.util.Date.class);

    public final StringPath homeRun = createString("homeRun");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath lastBall = createString("lastBall");

    public final ArrayPath<double[], Double> odds = createArray("odds", double[].class);

    public final StringPath player = createString("player");

    public final NumberPath<Integer> round = createNumber("round", Integer.class);

    public final StringPath sdate = createString("sdate");

    public QSpeedHomeRun(String variable) {
        super(SpeedHomeRun.class, forVariable(variable));
    }

    public QSpeedHomeRun(Path<? extends SpeedHomeRun> path) {
        super(path.getType(), path.getMetadata());
    }

    public QSpeedHomeRun(PathMetadata metadata) {
        super(SpeedHomeRun.class, metadata);
    }

}

