package spoon.gameZone.lotusBaccarat2;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QLotusBaccarat2 is a Querydsl query type for LotusBaccarat2
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QLotusBaccarat2 extends EntityPathBase<LotusBaccarat2> {

    private static final long serialVersionUID = 1248646683L;

    public static final QLotusBaccarat2 lotusBaccarat2 = new QLotusBaccarat2("lotusBaccarat2");

    public final ArrayPath<long[], Long> amount = createArray("amount", long[].class);

    public final StringPath b1 = createString("b1");

    public final StringPath b2 = createString("b2");

    public final StringPath b3 = createString("b3");

    public final BooleanPath bp = createBoolean("bp");

    public final BooleanPath cancel = createBoolean("cancel");

    public final BooleanPath closing = createBoolean("closing");

    public final DateTimePath<java.util.Date> gameDate = createDateTime("gameDate", java.util.Date.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final ArrayPath<double[], Double> odds = createArray("odds", double[].class);

    public final StringPath p1 = createString("p1");

    public final StringPath p2 = createString("p2");

    public final StringPath p3 = createString("p3");

    public final BooleanPath pp = createBoolean("pp");

    public final StringPath result = createString("result");

    public final NumberPath<Integer> round = createNumber("round", Integer.class);

    public final StringPath sdate = createString("sdate");

    public QLotusBaccarat2(String variable) {
        super(LotusBaccarat2.class, forVariable(variable));
    }

    public QLotusBaccarat2(Path<? extends LotusBaccarat2> path) {
        super(path.getType(), path.getMetadata());
    }

    public QLotusBaccarat2(PathMetadata metadata) {
        super(LotusBaccarat2.class, metadata);
    }

}

