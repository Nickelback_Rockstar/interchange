package spoon.gameZone.fxGame4;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QFxGame4 is a Querydsl query type for FxGame4
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QFxGame4 extends EntityPathBase<FxGame4> {

    private static final long serialVersionUID = -196019553L;

    public static final QFxGame4 fxGame4 = new QFxGame4("fxGame4");

    public final ArrayPath<long[], Long> amount = createArray("amount", long[].class);

    public final BooleanPath cancel = createBoolean("cancel");

    public final BooleanPath closing = createBoolean("closing");

    public final StringPath fxResult = createString("fxResult");

    public final DateTimePath<java.util.Date> gameDate = createDateTime("gameDate", java.util.Date.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath oddeven = createString("oddeven");

    public final ArrayPath<double[], Double> odds = createArray("odds", double[].class);

    public final StringPath overunder = createString("overunder");

    public final NumberPath<Integer> round = createNumber("round", Integer.class);

    public final StringPath sdate = createString("sdate");

    public QFxGame4(String variable) {
        super(FxGame4.class, forVariable(variable));
    }

    public QFxGame4(Path<? extends FxGame4> path) {
        super(path.getType(), path.getMetadata());
    }

    public QFxGame4(PathMetadata metadata) {
        super(FxGame4.class, metadata);
    }

}

