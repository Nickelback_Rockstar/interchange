package spoon.gameZone.fxGame3;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QFxGame3 is a Querydsl query type for FxGame3
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QFxGame3 extends EntityPathBase<FxGame3> {

    private static final long serialVersionUID = 1611434909L;

    public static final QFxGame3 fxGame3 = new QFxGame3("fxGame3");

    public final ArrayPath<long[], Long> amount = createArray("amount", long[].class);

    public final BooleanPath cancel = createBoolean("cancel");

    public final BooleanPath closing = createBoolean("closing");

    public final StringPath fxResult = createString("fxResult");

    public final DateTimePath<java.util.Date> gameDate = createDateTime("gameDate", java.util.Date.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath oddeven = createString("oddeven");

    public final ArrayPath<double[], Double> odds = createArray("odds", double[].class);

    public final StringPath overunder = createString("overunder");

    public final NumberPath<Integer> round = createNumber("round", Integer.class);

    public final StringPath sdate = createString("sdate");

    public QFxGame3(String variable) {
        super(FxGame3.class, forVariable(variable));
    }

    public QFxGame3(Path<? extends FxGame3> path) {
        super(path.getType(), path.getMetadata());
    }

    public QFxGame3(PathMetadata metadata) {
        super(FxGame3.class, metadata);
    }

}

