package spoon.gameZone.fxGame1;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QFxGame1 is a Querydsl query type for FxGame1
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QFxGame1 extends EntityPathBase<FxGame1> {

    private static final long serialVersionUID = 931376537L;

    public static final QFxGame1 fxGame1 = new QFxGame1("fxGame1");

    public final ArrayPath<long[], Long> amount = createArray("amount", long[].class);

    public final BooleanPath cancel = createBoolean("cancel");

    public final BooleanPath closing = createBoolean("closing");

    public final StringPath fxResult = createString("fxResult");

    public final DateTimePath<java.util.Date> gameDate = createDateTime("gameDate", java.util.Date.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath oddeven = createString("oddeven");

    public final ArrayPath<double[], Double> odds = createArray("odds", double[].class);

    public final StringPath overunder = createString("overunder");

    public final NumberPath<Integer> round = createNumber("round", Integer.class);

    public final StringPath sdate = createString("sdate");

    public QFxGame1(String variable) {
        super(FxGame1.class, forVariable(variable));
    }

    public QFxGame1(Path<? extends FxGame1> path) {
        super(path.getType(), path.getMetadata());
    }

    public QFxGame1(PathMetadata metadata) {
        super(FxGame1.class, metadata);
    }

}

