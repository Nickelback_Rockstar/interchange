package spoon.gameZone.kenoSpeed;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QKenoSpeed is a Querydsl query type for KenoSpeed
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QKenoSpeed extends EntityPathBase<KenoSpeed> {

    private static final long serialVersionUID = 367392727L;

    public static final QKenoSpeed kenoSpeed = new QKenoSpeed("kenoSpeed");

    public final ArrayPath<long[], Long> amount = createArray("amount", long[].class);

    public final BooleanPath cancel = createBoolean("cancel");

    public final BooleanPath closing = createBoolean("closing");

    public final DateTimePath<java.util.Date> gameDate = createDateTime("gameDate", java.util.Date.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath num0 = createString("num0");

    public final StringPath num1 = createString("num1");

    public final StringPath num2 = createString("num2");

    public final StringPath num3 = createString("num3");

    public final StringPath num4 = createString("num4");

    public final StringPath num5 = createString("num5");

    public final StringPath num6 = createString("num6");

    public final StringPath num7 = createString("num7");

    public final StringPath num8 = createString("num8");

    public final StringPath num9 = createString("num9");

    public final StringPath oddeven = createString("oddeven");

    public final ArrayPath<double[], Double> odds = createArray("odds", double[].class);

    public final StringPath overUnder = createString("overUnder");

    public final NumberPath<Integer> round = createNumber("round", Integer.class);

    public final StringPath sdate = createString("sdate");

    public QKenoSpeed(String variable) {
        super(KenoSpeed.class, forVariable(variable));
    }

    public QKenoSpeed(Path<? extends KenoSpeed> path) {
        super(path.getType(), path.getMetadata());
    }

    public QKenoSpeed(PathMetadata metadata) {
        super(KenoSpeed.class, metadata);
    }

}

