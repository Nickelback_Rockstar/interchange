package spoon.member.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QMember is a Querydsl query type for Member
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QMember extends EntityPathBase<Member> {

    private static final long serialVersionUID = 2130808206L;

    public static final QMember member = new QMember("member1");

    public final StringPath account = createString("account");

    public final StringPath agency1 = createString("agency1");

    public final StringPath agency2 = createString("agency2");

    public final StringPath agency3 = createString("agency3");

    public final StringPath agency4 = createString("agency4");

    public final StringPath agency5 = createString("agency5");

    public final StringPath agency6 = createString("agency6");

    public final StringPath agency7 = createString("agency7");

    public final NumberPath<Integer> agencyDepth = createNumber("agencyDepth", Integer.class);

    public final BooleanPath alarm = createBoolean("alarm");

    public final BooleanPath balanceAladdin = createBoolean("balanceAladdin");

    public final BooleanPath balanceDari = createBoolean("balanceDari");

    public final BooleanPath balanceDariLeak = createBoolean("balanceDariLeak");

    public final BooleanPath balanceDariReverse = createBoolean("balanceDariReverse");

    public final BooleanPath balanceLadder = createBoolean("balanceLadder");

    public final BooleanPath balanceLadderLeak = createBoolean("balanceLadderLeak");

    public final BooleanPath balanceLadderReverse = createBoolean("balanceLadderReverse");

    public final BooleanPath balanceLeak = createBoolean("balanceLeak");

    public final BooleanPath balanceLowhi = createBoolean("balanceLowhi");

    public final BooleanPath balancePower = createBoolean("balancePower");

    public final BooleanPath balancePowerLeak = createBoolean("balancePowerLeak");

    public final BooleanPath balancePowerReverse = createBoolean("balancePowerReverse");

    public final StringPath bank = createString("bank");

    public final StringPath bankPassword = createString("bankPassword");

    public final NumberPath<Long> betCntTotal = createNumber("betCntTotal", Long.class);

    public final DateTimePath<java.util.Date> betDate = createDateTime("betDate", java.util.Date.class);

    public final NumberPath<Long> betSports = createNumber("betSports", Long.class);

    public final NumberPath<Long> betSportsCnt = createNumber("betSportsCnt", Long.class);

    public final NumberPath<Long> betTotal = createNumber("betTotal", Long.class);

    public final NumberPath<Long> betZone = createNumber("betZone", Long.class);

    public final NumberPath<Long> betZoneCnt = createNumber("betZoneCnt", Long.class);

    public final BooleanPath binanceCoin1Block = createBoolean("binanceCoin1Block");

    public final BooleanPath binanceCoin3Block = createBoolean("binanceCoin3Block");

    public final BooleanPath black = createBoolean("black");

    public final BooleanPath block = createBoolean("block");

    public final StringPath casinoId = createString("casinoId");

    public final StringPath casinoNick = createString("casinoNick");

    public final NumberPath<Long> change = createNumber("change", Long.class);

    public final BooleanPath dariBlock = createBoolean("dariBlock");

    public final NumberPath<Long> deposit = createNumber("deposit", Long.class);

    public final StringPath depositor = createString("depositor");

    public final BooleanPath enabled = createBoolean("enabled");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath joinCode = createString("joinCode");

    public final DateTimePath<java.util.Date> joinDate = createDateTime("joinDate", java.util.Date.class);

    public final StringPath joinDomain = createString("joinDomain");

    public final StringPath joinIp = createString("joinIp");

    public final BooleanPath kenoLadderBlock = createBoolean("kenoLadderBlock");

    public final BooleanPath kenoSpeedBlock = createBoolean("kenoSpeedBlock");

    public final BooleanPath ladderBlock = createBoolean("ladderBlock");

    public final NumberPath<Integer> level = createNumber("level", Integer.class);

    public final NumberPath<Long> loginCnt = createNumber("loginCnt", Long.class);

    public final DateTimePath<java.util.Date> loginDate = createDateTime("loginDate", java.util.Date.class);

    public final StringPath loginDevice = createString("loginDevice");

    public final StringPath loginDomain = createString("loginDomain");

    public final StringPath loginIp = createString("loginIp");

    public final BooleanPath lotusBaccarat2Block = createBoolean("lotusBaccarat2Block");

    public final BooleanPath lotusBaccaratBlock = createBoolean("lotusBaccaratBlock");

    public final BooleanPath lotusOddEvenBlock = createBoolean("lotusOddEvenBlock");

    public final StringPath memo = createString("memo");

    public final NumberPath<Long> money = createNumber("money", Long.class);

    public final BooleanPath newSnailBlock = createBoolean("newSnailBlock");

    public final StringPath nickname = createString("nickname");

    public final BooleanPath oneFolderBlock = createBoolean("oneFolderBlock");

    public final StringPath passKey = createString("passKey");

    public final StringPath password = createString("password");

    public final StringPath phone = createString("phone");

    public final StringPath phoneBak = createString("phoneBak");

    public final NumberPath<Long> point = createNumber("point", Long.class);

    public final BooleanPath powerBlock = createBoolean("powerBlock");

    public final BooleanPath powerFreeKickBlock = createBoolean("powerFreeKickBlock");

    public final BooleanPath powerLadderBlock = createBoolean("powerLadderBlock");

    public final BooleanPath powerLotto = createBoolean("powerLotto");

    public final StringPath rateCode = createString("rateCode");

    public final NumberPath<Double> ratePowerLotto = createNumber("ratePowerLotto", Double.class);

    public final NumberPath<Double> rateShare = createNumber("rateShare", Double.class);

    public final NumberPath<Double> rateSports = createNumber("rateSports", Double.class);

    public final NumberPath<Double> rateZone = createNumber("rateZone", Double.class);

    public final NumberPath<Double> rateZone1 = createNumber("rateZone1", Double.class);

    public final NumberPath<Double> rateZone2 = createNumber("rateZone2", Double.class);

    public final StringPath recommender = createString("recommender");

    public final EnumPath<spoon.member.domain.Role> role = createEnum("role", spoon.member.domain.Role.class);

    public final BooleanPath secession = createBoolean("secession");

    public final BooleanPath speedHomeRunBlock = createBoolean("speedHomeRunBlock");

    public final StringPath tokenid = createString("tokenid");

    public final BooleanPath twoFolderBlock = createBoolean("twoFolderBlock");

    public final StringPath userid = createString("userid");

    public final StringPath virtualId = createString("virtualId");

    public final NumberPath<Long> withdraw = createNumber("withdraw", Long.class);

    public QMember(String variable) {
        super(Member.class, forVariable(variable));
    }

    public QMember(Path<? extends Member> path) {
        super(path.getType(), path.getMetadata());
    }

    public QMember(PathMetadata metadata) {
        super(Member.class, metadata);
    }

}

