package spoon.member.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QMvInfo is a Querydsl query type for MvInfo
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QMvInfo extends EntityPathBase<MvInfo> {

    private static final long serialVersionUID = 2145447147L;

    public static final QMvInfo mvInfo = new QMvInfo("mvInfo");

    public final StringPath code = createString("code");

    public final StringPath crypto1 = createString("crypto1");

    public final StringPath crypto2 = createString("crypto2");

    public final StringPath crypto3 = createString("crypto3");

    public final StringPath crypto4 = createString("crypto4");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath json = createString("json");

    public final DateTimePath<java.util.Date> regDate = createDateTime("regDate", java.util.Date.class);

    public QMvInfo(String variable) {
        super(MvInfo.class, forVariable(variable));
    }

    public QMvInfo(Path<? extends MvInfo> path) {
        super(path.getType(), path.getMetadata());
    }

    public QMvInfo(PathMetadata metadata) {
        super(MvInfo.class, metadata);
    }

}

