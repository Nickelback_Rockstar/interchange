package spoon.web.site.game;

import lombok.AllArgsConstructor;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import spoon.board.service.NoticeService;
import spoon.common.net.HttpParsing;
import spoon.common.utils.JsonUtils;
import spoon.config.domain.Config;
import spoon.config.service.GameConfigService;
import spoon.game.domain.GameDto;
import spoon.game.domain.MenuCode;
import spoon.game.service.GameListService;

import java.util.Date;
import java.util.List;

@AllArgsConstructor
@Controller
@RequestMapping("#{config.pathSite}")
public class GameListController {

    private GameListService gameListService;

    private GameConfigService gameConfigService;

    private NoticeService noticeService;

    /**
     * 스포츠 사용자 List
     */
    @RequestMapping(value = "game/{menu:cross|special|live|inGame}", method = RequestMethod.GET)
    public String list(ModelMap map, @PathVariable("menu") String menu, String sports, String inGameSports) {
        List<GameDto.List> lists = gameListService.gameList(menu, sports);
        List<GameDto.League> leagues = gameListService.gameLeague(menu, sports);
        long total = leagues.stream().mapToLong(GameDto.League::getCnt).sum();

        MenuCode menuCode = MenuCode.valueOf(menu.toUpperCase());
        map.addAttribute("config", JsonUtils.toString(gameConfigService.gameConfig(menuCode)));
        map.addAttribute("gameCnt", total);
        map.addAttribute("gameList", JsonUtils.toString(lists));
        map.addAttribute("league", JsonUtils.toString(leagues));
        map.addAttribute("sports", sports);
        map.addAttribute("menu", menu);
        map.addAttribute("notice", noticeService.siteList());
        map.addAttribute("title", menuCode.getName());

        if ("inGame".equals(menu)) {
            if ("baseball".equals(inGameSports)) {
                return "site/game/inGameBaseballList";
            } else {
                return "site/game/inGameList";
            }
        } else {
            return "site/game/list";
        }

    }

    /**
     * 스포츠 사용자 List
     */
    @RequestMapping(value = "game/inGameDetail", method = RequestMethod.GET)
    public String inGameDetail(ModelMap map, @RequestParam("idx") String idx) {

        MenuCode menuCode = MenuCode.valueOf("INGAME");
        map.addAttribute("config", JsonUtils.toString(gameConfigService.gameConfig(menuCode)));
        map.addAttribute("notice", noticeService.siteList());
        map.addAttribute("title", menuCode.getName());
        map.addAttribute("idx", idx);

        return "site/game/inGameDetail";

    }

    /**
     * 스포츠 사용자 List
     */
    @RequestMapping(value = "game/inGameFrame", method = RequestMethod.GET)
    public String inGameFrame(ModelMap map) {
        return "site/game/inGameFrame";
    }

    /**
     * 스포츠 사용자 List Ajax
     */
    @ResponseBody
    @RequestMapping(value = "game/{menu:cross|special|live|inGame}", method = RequestMethod.POST)
    public List<GameDto.List> lists(@PathVariable("menu") String menu, String sports) {
        return gameListService.gameList(menu, sports);
    }

    @ResponseBody
    @RequestMapping(value = "game/inGameJson", method = RequestMethod.GET)
    public JSONArray inGame() {
        JSONArray jsonArr = new JSONArray();
        String json = HttpParsing.getJson(Config.getSysConfig().getSports().getInGameApi() + "/api/game?sts=2&cp=paris&sdate=" + new Date());
        if (json == null) return jsonArr;
//        System.out.println("인겜정보 = "+json);
        JSONParser parser = new JSONParser();

        try {
            jsonArr = (JSONArray) parser.parse(json);
            System.out.println(jsonArr.toJSONString());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return jsonArr;
    }

    @ResponseBody
    @RequestMapping(value = "game/inGameDetailJson", method = RequestMethod.GET)
    public JSONObject inGameDetail(@RequestParam("idx") String idx) {
        JSONObject obj = new JSONObject();
        String json = HttpParsing.getJson(Config.getSysConfig().getSports().getInGameApi() + "/api/gameDetail?idx=" + idx + "&cp=paris&sdate=" + new Date());
        if (json == null) return obj;
        JSONParser parser = new JSONParser();

        try {
            obj = (JSONObject) parser.parse(json);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return obj;
    }


    @RequestMapping(value = "game/inGameTotalJson", method = RequestMethod.GET)
    public @ResponseBody
    String inGameTotal() {
        return HttpParsing.getJson(Config.getSysConfig().getSports().getInGameApi() + "/totGame.json?sdate=" + new Date());
    }
}
