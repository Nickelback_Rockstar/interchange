package spoon.web.site.event;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import spoon.common.utils.JsonUtils;
import spoon.common.utils.WebUtils;
import spoon.config.domain.Config;
import spoon.event.domain.DailyDto;
import spoon.event.entity.Lotto;
import spoon.event.service.LottoService;

@Slf4j
@AllArgsConstructor
@Controller
@RequestMapping(value = "#{config.pathSite}")
public class LottoController {

    private LottoService lottoService;

    @RequestMapping(value = "event/lotto", method = RequestMethod.GET)
    public String lotto(ModelMap map, @ModelAttribute DailyDto.Command command,
                        @PageableDefault(size = 10, direction = Sort.Direction.DESC, sort = {"id"}) Pageable pageable) {

        long lottoCnt = lottoService.getLottoCount(WebUtils.userid());
        command.setUserid(WebUtils.userid());
        map.addAttribute("lottoCnt", lottoCnt);
        map.addAttribute("config", Config.getLottoConfig());
        map.addAttribute("page", lottoService.lottoPage(command, pageable));

        return "site/event/lotto";
    }

    @ResponseBody
    @RequestMapping(value = "event/lottoList", method = RequestMethod.GET)
    public String lottoList(@ModelAttribute DailyDto.Command command,
                        @PageableDefault(size = 10, direction = Sort.Direction.DESC, sort = {"id"}) Pageable pageable) {
        command.setUserid(WebUtils.userid());
        Page<Lotto> page = lottoService.lottoPage(command, pageable);
        return JsonUtils.toString(page);
    }

    @ResponseBody
    @RequestMapping(value = "event/lottoRolling", method = RequestMethod.POST)
    public long lottoRolling() {
        long lottoCnt = lottoService.getLottoCount(WebUtils.userid());
        if(Config.getSysConfig().getEvent().isLotto() && lottoCnt > 0){
            return lottoService.lottoRolling(WebUtils.userid());
        }else{
            return 0;
        }
    }



}
