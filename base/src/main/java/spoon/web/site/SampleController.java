package spoon.web.site;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Slf4j
@AllArgsConstructor
@Controller
public class SampleController {

    @RequestMapping(value = "/sample/login1", method = RequestMethod.GET)
    public String login1() {
        return "/site/login1";
    }

    @RequestMapping(value = "/sample/login2", method = RequestMethod.GET)
    public String login2() {
        return "/site/login2";
    }

    @RequestMapping(value = "/sample/login3", method = RequestMethod.GET)
    public String login3() {
        return "/site/login3";
    }

    @RequestMapping(value = "/sample/login4", method = RequestMethod.GET)
    public String login4() {
        return "/site/login4";
    }

    @RequestMapping(value = "/sample/login5", method = RequestMethod.GET)
    public String login5() {
        return "/site/login5";
    }

    @RequestMapping(value = "/sample/sample1", method = RequestMethod.GET)
    public String sample1() {
        return "/site/sample1";
    }

    @RequestMapping(value = "/sample/sample2", method = RequestMethod.GET)
    public String sample2() {
        return "/site/sample2";
    }

    @RequestMapping(value = "/ex/login1", method = RequestMethod.GET)
    public String exlogin1() {
        return "/ex/login1";
    }

    @RequestMapping(value = "/ex/login2", method = RequestMethod.GET)
    public String exlogin2() {
        return "/ex/login2";
    }

    @RequestMapping(value = "/ex/login3", method = RequestMethod.GET)
    public String exlogin3() {
        return "/ex/login3";
    }

    @RequestMapping(value = "/ex/login4", method = RequestMethod.GET)
    public String exlogin4() {
        return "/ex/login4";
    }

    @RequestMapping(value = "/ex/login5", method = RequestMethod.GET)
    public String lexogin5() {
        return "/ex/login5";
    }

    @RequestMapping(value = "/ex/login6", method = RequestMethod.GET)
    public String lexogin6() {
        return "/ex/login6";
    }

}
