package spoon.web.site;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import spoon.member.entity.Member;
import spoon.member.service.MemberService;
import spoon.payment.domain.PaymentDto;
import spoon.payment.service.PaymentService;
import spoon.support.security.LoginUser;
import spoon.support.web.AjaxResult;

@Slf4j
@AllArgsConstructor
@Controller
public class BustabitController {

    private SessionRegistry sessionRegistry;

    private MemberService memberService;

    private PaymentService paymentService;

    @ResponseBody
    @RequestMapping(value = "/block/dbwjcpzmdlqslekd")
    public String dbwjcpzmdlqslekd(
            @RequestParam(value = "uid", required = false) String uid,
            @RequestParam(value = "uip", required = false) String uip
    ) {

        String result = "false";
        System.out.println("체크 들어옴");
        System.out.println("uid");
        System.out.println("uip");

        for (Object principal : sessionRegistry.getAllPrincipals()) {
            if (principal instanceof LoginUser) {
                LoginUser user = (LoginUser) principal;

                System.out.println("id web ="+user.getUser().getUserid());
                System.out.println("ip web ="+user.getUser().getLoginIp());
                System.out.println("id uid ="+uid);
                System.out.println("ip uip ="+uip);
                if(user.getUser().getUserid().equals(uid)){
                    System.out.println("user true");
                    result = "true";
                    break;
                }
            }
        }
        return result;
    }

    @ResponseBody
    @RequestMapping(value = "/block/cndwjsdlqslekd")
    public String cndwjsdlqslekd(
            @RequestHeader(value="bustabitKey") String bustabitKey,
            @RequestParam(value = "uid", required = false) String uid,
            @RequestParam(value = "uip", required = false) String uip,
            @RequestParam(value = "inMoney", required = false) long inMoney
    ) {

        String result = "false";

        if("ameame4321".equals(bustabitKey) && inMoney > 1000) {
            Member member = memberService.getMember(uid);

            System.out.println("in chk");
            System.out.println("bustabitKey=" + bustabitKey);
            System.out.println("inMoney:" + inMoney);
            System.out.println("member.getMoney():" + member.getMoney());

            for (Object principal : sessionRegistry.getAllPrincipals()) {
                if (principal instanceof LoginUser) {
                    LoginUser user = (LoginUser) principal;
                    //if (user.getUser().getUserid().equals(uid) && user.getUser().getLoginIp().equals(uip)) {
                    if (user.getUser().getUserid().equals(uid)) {
                        if(member.getMoney() > inMoney){
                            result = "true";
                            System.out.println("cndwjsdlqslekd true");
                            break;
                        }else{
                            System.out.println("cndwjsdlqslekd false");
                        }
                    }
                }
            }
        }

        return result;
    }

    @ResponseBody
    @RequestMapping(value = "/block/cndwjsdlqslekd2")
    public String cndwjsdlqslekd2(
            @RequestHeader(value="bustabitKey") String bustabitKey,
            @RequestParam(value = "uid", required = false) String uid,
            @RequestParam(value = "uip", required = false) String uip,
            @RequestParam(value = "inMoney", required = false) long inMoney,
            @RequestParam(value = "pass", required = false) String pass
    ) {

        String result = "false";

        if("ameame4321".equals(bustabitKey) && inMoney > 1000) {
            Member member = memberService.getMember(uid);
            if(!pass.equals(member.getBankPassword())){
                return result;
            }

            System.out.println("in chk");
            System.out.println("bustabitKey=" + bustabitKey);
            System.out.println("inMoney:" + inMoney);
            System.out.println("member.getMoney():" + member.getMoney());

            for (Object principal : sessionRegistry.getAllPrincipals()) {
                if (principal instanceof LoginUser) {
                    LoginUser user = (LoginUser) principal;
                    //if (user.getUser().getUserid().equals(uid) && user.getUser().getLoginIp().equals(uip)) {
                    if (user.getUser().getUserid().equals(uid)) {
                        if(member.getMoney() > inMoney){
                            result = "true";
                            System.out.println("cndwjsdlqslekd2 true");
                            PaymentDto.Add add = new PaymentDto.Add();
                            add.setUserid(member.getUserid());
                            add.setMemo("부스타빗 충전용 - 금액 차감");
                            add.setPlus(false);
                            add.setAmount(inMoney);
                            add.setType("GRAPH");
                            AjaxResult ar = paymentService.addMoney(add);
                            System.out.println(ar.toString());
                            System.out.println("차감 완료");
                            break;
                        }else{
                            System.out.println("cndwjsdlqslekd2 false");
                        }
                    }
                }
            }
        }

        return result;
    }

    @ResponseBody
    @RequestMapping(value = "/block/cndwjsdlqslekd3")
    public String cndwjsdlqslekd3(
            @RequestHeader(value="bustabitKey") String bustabitKey,
            @RequestParam(value = "uid", required = false) String uid,
            @RequestParam(value = "uip", required = false) String uip,
            @RequestParam(value = "pass", required = false) String pass
    ) {

        String result = "false";

        if("ameame4321".equals(bustabitKey)) {
            Member member = memberService.getMember(uid);
            if(!pass.equals(member.getBankPassword())){
                return result;
            }

            System.out.println("in cndwjsdlqslekd3");

            for (Object principal : sessionRegistry.getAllPrincipals()) {
                if (principal instanceof LoginUser) {
                    LoginUser user = (LoginUser) principal;
                    //if (user.getUser().getUserid().equals(uid) && user.getUser().getLoginIp().equals(uip)) {
                    if (user.getUser().getUserid().equals(uid)) {
                        result = "true";
                    }
                }
            }
        }

        return result;
    }

    @ResponseBody
    @RequestMapping(value = "/block/ghkswjsgkqslekdyd")
    public String ghkswjsgkqslekdyd(
            @RequestHeader(value="bustabitKey") String bustabitKey,
            @RequestParam(value = "uid", required = false) String uid,
            @RequestParam(value = "uip", required = false) String uip,
            @RequestParam(value = "outMoney", required = false) long outMoney,
            @RequestParam(value = "pass", required = false) String pass

    ) {

        String result = "false";

        if("ameame4321".equals(bustabitKey)) {
            Member member = memberService.getMember(uid);

            if(!pass.equals(member.getBankPassword())){
                return result;
            }

            System.out.println("체크 들어옴3");
            System.out.println("bustabitKey=" + bustabitKey);
            System.out.println("환전금액:" + outMoney);
            System.out.println("유저금액:" + member.getMoney());

            for (Object principal : sessionRegistry.getAllPrincipals()) {
                if (principal instanceof LoginUser) {
                    LoginUser user = (LoginUser) principal;
                    //if (user.getUser().getUserid().equals(uid) && user.getUser().getLoginIp().equals(uip)) {
                    if (user.getUser().getUserid().equals(uid)) {
                        result = "true";
                        System.out.println("정상 완료2");
                        PaymentDto.Add add = new PaymentDto.Add();
                        add.setUserid(member.getUserid());
                        add.setMemo("부스타빗 환전용 - 금액 증액");
                        add.setType("GRAPH");
                        add.setPlus(true);
                        add.setAmount(outMoney);
                        AjaxResult ar = paymentService.addMoney(add);
                        System.out.println(ar.toString());
                        System.out.println("증액 완료");
                        break;

                    }
                }
            }
        }

        return result;
    }

    @ResponseBody
    @RequestMapping(value = "/block/userLoginChk")
    public JSONObject userLoginChk(@RequestParam(value = "uid", required = false) String uid) {
        JSONObject json = new JSONObject();
        json.put("result",false);
        try {
            Member member = memberService.getMember(uid);
            if (member.getUserid().equals(uid)) {
                for (Object principal : sessionRegistry.getAllPrincipals()) {
                    if (principal instanceof LoginUser) {
                        LoginUser user = (LoginUser) principal;
                        if (user.getUser().getUserid().equals(uid)) {
                            json.put("result",true);
                        }
                    }
                }
            }
        }catch ( Exception e){
            e.printStackTrace();
            return json;
        }

        return json;
    }

    @ResponseBody
    @RequestMapping(value = "/block/userBetting")
    public JSONObject userBetting(@RequestParam(value = "uid", required = false) String uid,@RequestParam(value = "uid", required = false) String betMoney) {
        JSONObject json = new JSONObject();
        json.put("result",false);
        try {
            Member member = memberService.getMember(uid);
            if (member.getUserid().equals(uid)) {
                for (Object principal : sessionRegistry.getAllPrincipals()) {
                    if (principal instanceof LoginUser) {
                        LoginUser user = (LoginUser) principal;
                        if (user.getUser().getUserid().equals(uid)) {
                            json.put("result",true);
                        }
                    }
                }
            }
        }catch ( Exception e){
            e.printStackTrace();
            return json;
        }

        return json;
    }

}
