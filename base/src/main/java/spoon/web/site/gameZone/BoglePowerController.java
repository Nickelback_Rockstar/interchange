package spoon.web.site.gameZone;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import spoon.bet.domain.BetDto;
import spoon.bet.service.BetListService;
import spoon.common.utils.JsonUtils;
import spoon.common.utils.WebUtils;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.boglePower.BoglePowerDto;
import spoon.gameZone.boglePower.service.BoglePowerGameService;
import spoon.gameZone.boglePower.service.BoglePowerService;
import spoon.support.web.AjaxResult;

@Slf4j
@AllArgsConstructor
@Controller
@RequestMapping(value = "#{config.pathSite}")
public class BoglePowerController {

    private BoglePowerService boglePowerService;

    private BoglePowerGameService boglePowerGameService;

    private BetListService betListService;

    @RequestMapping(value = "zone/boglePower", method = RequestMethod.GET)
    public String zone(ModelMap map, BetDto.UserCommand command, @PageableDefault(size = 10, direction = Sort.Direction.DESC, sort = "sdate") Pageable pageable) {
        String userid = WebUtils.userid();
        command.setUserid(userid);
        map.addAttribute("powerBetList", betListService.userBoglePowerBet(command, pageable));
        map.addAttribute("config", JsonUtils.toString(boglePowerService.gameConfig()));
        return "site/zone/boglePower";
    }


    @ResponseBody
    @RequestMapping(value = "zone/boglePower/config", method = RequestMethod.POST)
    public BoglePowerDto.Config config() {
        return boglePowerService.gameConfig();
    }

    @RequestMapping(value = "zone/boglePower/score", method = RequestMethod.GET)
    public String score(ModelMap map, @ModelAttribute("command") ZoneDto.Command command,
                        @PageableDefault(size = 20, direction = Sort.Direction.DESC, sort = "sdate") Pageable pageable) {
        map.addAttribute("page", boglePowerService.getClosing(command, pageable));
        return "site/score/boglePower";
    }

    @ResponseBody
    @RequestMapping(value = "zone/boglePower/betting", method = RequestMethod.POST)
    public AjaxResult betting(@RequestHeader(value = "AJAX") boolean ajax, @RequestBody ZoneDto.Bet bet) {
        if (!ajax) {
            return new AjaxResult(false, "페이지를 찾을 수 없습니다.");
        }
        return boglePowerGameService.betting(bet);
    }

}
