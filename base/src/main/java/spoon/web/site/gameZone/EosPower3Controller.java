package spoon.web.site.gameZone;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import spoon.bet.domain.BetDto;
import spoon.bet.service.BetListService;
import spoon.common.utils.JsonUtils;
import spoon.common.utils.WebUtils;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.eosPower3.EosPower3Dto;
import spoon.gameZone.eosPower3.service.EosPower3GameService;
import spoon.gameZone.eosPower3.service.EosPower3Service;
import spoon.support.web.AjaxResult;

@Slf4j
@AllArgsConstructor
@Controller
@RequestMapping(value = "#{config.pathSite}")
public class EosPower3Controller {

    private EosPower3Service eosPower3Service;

    private EosPower3GameService eosPower3GameService;

    private BetListService betListService;

    @RequestMapping(value = "zone/eosPower3", method = RequestMethod.GET)
    public String zone(ModelMap map, BetDto.UserCommand command, @PageableDefault(size = 10, direction = Sort.Direction.DESC, sort = "sdate") Pageable pageable) {
        String userid = WebUtils.userid();
        command.setUserid(userid);
        map.addAttribute("powerBetList", betListService.userEosPower3Bet(command, pageable));
        map.addAttribute("config", JsonUtils.toString(eosPower3Service.gameConfig()));
        return "site/zone/eosPower3";
    }

    @RequestMapping(value = "zone/eosPower3/score", method = RequestMethod.GET)
    public String score(ModelMap map, @ModelAttribute("command") ZoneDto.Command command,
                        @PageableDefault(size = 20, direction = Sort.Direction.DESC, sort = "sdate") Pageable pageable) {
        map.addAttribute("page", eosPower3Service.getClosing(command, pageable));
        return "site/score/eosPower3";
    }

    @ResponseBody
    @RequestMapping(value = "zone/eosPower3/config", method = RequestMethod.POST)
    public EosPower3Dto.Config config() {
        return eosPower3Service.gameConfig();
    }

    @ResponseBody
    @RequestMapping(value = "zone/eosPower3/betting", method = RequestMethod.POST)
    public AjaxResult betting(@RequestHeader(value = "AJAX") boolean ajax, @RequestBody ZoneDto.Bet bet) {
        if (!ajax) {
            return new AjaxResult(false, "페이지를 찾을 수 없습니다.");
        }
        return eosPower3GameService.betting(bet);
    }
}
