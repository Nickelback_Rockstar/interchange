package spoon.web.site.gameZone;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import spoon.bet.domain.BetDto;
import spoon.bet.service.BetListService;
import spoon.common.utils.JsonUtils;
import spoon.common.utils.WebUtils;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.eosPower5.EosPower5Dto;
import spoon.gameZone.eosPower5.service.EosPower5GameService;
import spoon.gameZone.eosPower5.service.EosPower5Service;
import spoon.support.web.AjaxResult;

@Slf4j
@AllArgsConstructor
@Controller
@RequestMapping(value = "#{config.pathSite}")
public class EosPower5Controller {

    private EosPower5Service eosPower5Service;

    private EosPower5GameService eosPower5GameService;

    private BetListService betListService;

    @RequestMapping(value = "zone/eosPower5", method = RequestMethod.GET)
    public String zone(ModelMap map, BetDto.UserCommand command, @PageableDefault(size = 10, direction = Sort.Direction.DESC, sort = "sdate") Pageable pageable) {
        String userid = WebUtils.userid();
        command.setUserid(userid);
        map.addAttribute("powerBetList", betListService.userEosPower5Bet(command, pageable));
        map.addAttribute("config", JsonUtils.toString(eosPower5Service.gameConfig()));
        return "site/zone/eosPower5";
    }

    @RequestMapping(value = "zone/eosPower5/score", method = RequestMethod.GET)
    public String score(ModelMap map, @ModelAttribute("command") ZoneDto.Command command,
                        @PageableDefault(size = 20, direction = Sort.Direction.DESC, sort = "sdate") Pageable pageable) {
        map.addAttribute("page", eosPower5Service.getClosing(command, pageable));
        return "site/score/eosPower5";
    }

    @ResponseBody
    @RequestMapping(value = "zone/eosPower5/config", method = RequestMethod.POST)
    public EosPower5Dto.Config config() {
        return eosPower5Service.gameConfig();
    }

    @ResponseBody
    @RequestMapping(value = "zone/eosPower5/betting", method = RequestMethod.POST)
    public AjaxResult betting(@RequestHeader(value = "AJAX") boolean ajax, @RequestBody ZoneDto.Bet bet) {
        if (!ajax) {
            return new AjaxResult(false, "페이지를 찾을 수 없습니다.");
        }
        return eosPower5GameService.betting(bet);
    }
}
