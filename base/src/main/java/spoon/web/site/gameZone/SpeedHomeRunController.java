package spoon.web.site.gameZone;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import spoon.common.utils.JsonUtils;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.speedHomeRun.SpeedHomeRunDto;
import spoon.gameZone.speedHomeRun.service.SpeedHomeRunGameService;
import spoon.gameZone.speedHomeRun.service.SpeedHomeRunService;
import spoon.support.web.AjaxResult;

@Slf4j
@AllArgsConstructor
@Controller
@RequestMapping(value = "#{config.pathSite}")
public class SpeedHomeRunController {

    private SpeedHomeRunService speedHomeRunService;

    private SpeedHomeRunGameService speedHomeRunGameService;

    @RequestMapping(value = "zone/speedHomeRun", method = RequestMethod.GET)
    public String zone(ModelMap map) {
        map.addAttribute("config", JsonUtils.toString(speedHomeRunService.gameConfig()));
        return "site/zone/speedHomeRun";
    }

    @RequestMapping(value = "zone/speedHomeRun/score", method = RequestMethod.GET)
    public String score(ModelMap map, @ModelAttribute("command") ZoneDto.Command command,
                        @PageableDefault(size = 20, direction = Sort.Direction.DESC, sort = "sdate") Pageable pageable) {
        map.addAttribute("page", speedHomeRunService.getClosing(command, pageable));
        return "site/score/speedHomeRun";
    }

    @ResponseBody
    @RequestMapping(value = "zone/speedHomeRun/config", method = RequestMethod.POST)
    public SpeedHomeRunDto.Config config() {
        return speedHomeRunService.gameConfig();
    }

    @ResponseBody
    @RequestMapping(value = "zone/speedHomeRun/betting", method = RequestMethod.POST)
    public AjaxResult betting(@RequestHeader(value = "AJAX") boolean ajax, @RequestBody ZoneDto.Bet bet) {
        if (!ajax) {
            return new AjaxResult(false, "페이지를 찾을 수 없습니다.");
        }
        return speedHomeRunGameService.betting(bet);
    }
}
