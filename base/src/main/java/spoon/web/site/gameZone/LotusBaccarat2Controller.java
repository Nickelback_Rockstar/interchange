package spoon.web.site.gameZone;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import spoon.common.utils.JsonUtils;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.lotusBaccarat2.LotusBaccarat2Dto;
import spoon.gameZone.lotusBaccarat2.service.LotusBaccarat2GameService;
import spoon.gameZone.lotusBaccarat2.service.LotusBaccarat2Service;
import spoon.support.web.AjaxResult;

@Slf4j
@AllArgsConstructor
@Controller
@RequestMapping(value = "#{config.pathSite}")
public class LotusBaccarat2Controller {

    private LotusBaccarat2Service lotusBaccarat2Service;

    private LotusBaccarat2GameService lotusBaccarat2GameService;

    @RequestMapping(value = "zone/lotusBaccarat2", method = RequestMethod.GET)
    public String zone(ModelMap map) {
        map.addAttribute("config", JsonUtils.toString(lotusBaccarat2Service.gameConfig()));
        return "site/zone/lotusBaccarat2";
    }

    @RequestMapping(value = "zone/lotusBaccarat2/score", method = RequestMethod.GET)
    public String score(ModelMap map, @ModelAttribute("command") ZoneDto.Command command,
                        @PageableDefault(size = 20, direction = Sort.Direction.DESC, sort = "sdate") Pageable pageable) {
        map.addAttribute("page", lotusBaccarat2Service.getClosing(command, pageable));
        return "site/score/lotusBaccarat2";
    }

    @ResponseBody
    @RequestMapping(value = "zone/lotusBaccarat2/config", method = RequestMethod.POST)
    public LotusBaccarat2Dto.Config config() {
        return lotusBaccarat2Service.gameConfig();
    }

    @ResponseBody
    @RequestMapping(value = "zone/lotusBaccarat2/betting", method = RequestMethod.POST)
    public AjaxResult betting(@RequestHeader(value = "AJAX") boolean ajax, @RequestBody ZoneDto.Bet bet) {
        if (!ajax) {
            return new AjaxResult(false, "페이지를 찾을 수 없습니다.");
        }
        return lotusBaccarat2GameService.betting(bet);
    }
}
