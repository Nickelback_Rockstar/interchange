package spoon.web.site.gameZone;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import spoon.bet.domain.BetDto;
import spoon.bet.service.BetListService;
import spoon.common.utils.JsonUtils;
import spoon.common.utils.WebUtils;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.eosPower1.EosPower1Dto;
import spoon.gameZone.eosPower1.service.EosPower1GameService;
import spoon.gameZone.eosPower1.service.EosPower1Service;
import spoon.support.web.AjaxResult;

@Slf4j
@AllArgsConstructor
@Controller
@RequestMapping(value = "#{config.pathSite}")
public class EosPower1Controller {

    private EosPower1Service eosPower1Service;

    private EosPower1GameService eosPower1GameService;

    private BetListService betListService;

    @RequestMapping(value = "zone/eosPower1", method = RequestMethod.GET)
    public String zone(ModelMap map, BetDto.UserCommand command, @PageableDefault(size = 10, direction = Sort.Direction.DESC, sort = "sdate") Pageable pageable) {
        String userid = WebUtils.userid();
        command.setUserid(userid);
        map.addAttribute("powerBetList", betListService.userEosPower1Bet(command, pageable));
        map.addAttribute("config", JsonUtils.toString(eosPower1Service.gameConfig()));
        return "site/zone/eosPower1";
    }

    @RequestMapping(value = "zone/eosPower1/score", method = RequestMethod.GET)
    public String score(ModelMap map, @ModelAttribute("command") ZoneDto.Command command,
                        @PageableDefault(size = 20, direction = Sort.Direction.DESC, sort = "sdate") Pageable pageable) {
        map.addAttribute("page", eosPower1Service.getClosing(command, pageable));
        return "site/score/eosPower1";
    }

    @ResponseBody
    @RequestMapping(value = "zone/eosPower1/config", method = RequestMethod.POST)
    public EosPower1Dto.Config config() {
        return eosPower1Service.gameConfig();
    }

    @ResponseBody
    @RequestMapping(value = "zone/eosPower1/betting", method = RequestMethod.POST)
    public AjaxResult betting(@RequestHeader(value = "AJAX") boolean ajax, @RequestBody ZoneDto.Bet bet) {
        if (!ajax) {
            return new AjaxResult(false, "페이지를 찾을 수 없습니다.");
        }
        return eosPower1GameService.betting(bet);
    }
}
