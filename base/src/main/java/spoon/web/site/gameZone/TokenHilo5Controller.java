package spoon.web.site.gameZone;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import spoon.common.net.HttpParsing;
import spoon.common.utils.StringUtils;
import spoon.common.utils.TokenUtils;
import spoon.common.utils.WebUtils;
import spoon.member.entity.Member;
import spoon.member.service.MemberService;

@Slf4j
@AllArgsConstructor
@Controller
@RequestMapping(value = "#{config.pathSite}")
public class TokenHilo5Controller {

    private MemberService memberService;

    @RequestMapping(value = "zone/hilo5", method = RequestMethod.GET)
    public String zone(ModelMap map) {
        String userid = WebUtils.userid();
        Member m = memberService.getMember(WebUtils.userid());
        String hash = "";
        if(StringUtils.empty(m.getTokenid())){
            String json = HttpParsing.getJson(TokenUtils.SITE_DOMAIN+"/ex/userCheck?userId="+userid);
            System.out.println("json="+json);
            JSONParser parser = new JSONParser();
            try {
                JSONObject jsonObj = (JSONObject) parser.parse(json);
                System.out.println("jsonObj = "+jsonObj.toJSONString());
                if("0".equals(jsonObj.get("statuscode").toString())){
                    m.setTokenid(jsonObj.get("uid").toString());
                    memberService.update(m);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        map.addAttribute("uid", m.getTokenid());
        map.addAttribute("hash", StringUtils.cvMD5(TokenUtils.AGENT_ID+"|"+ TokenUtils.API_KEY+"|"+m.getTokenid()));

        return "site/zone/hilo5";
    }

}
