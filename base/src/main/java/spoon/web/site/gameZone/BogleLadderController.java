package spoon.web.site.gameZone;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import spoon.bet.domain.BetDto;
import spoon.bet.service.BetListService;
import spoon.common.utils.JsonUtils;
import spoon.common.utils.StringUtils;
import spoon.common.utils.WebUtils;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.bogleLadder.BogleLadderDto;
import spoon.gameZone.bogleLadder.service.BogleLadderGameService;
import spoon.gameZone.bogleLadder.service.BogleLadderService;
import spoon.support.web.AjaxResult;

@Slf4j
@AllArgsConstructor
@Controller
@RequestMapping(value = "#{config.pathSite}")
public class BogleLadderController {

    private BogleLadderService bogleLadderService;

    private BogleLadderGameService bogleLadderGameService;

    private BetListService betListService;

    @RequestMapping(value = "zone/bogleLadder", method = RequestMethod.GET)
    public String zone(ModelMap map, BetDto.UserCommand command, @PageableDefault(size = 10, direction = Sort.Direction.DESC, sort = "sdate") Pageable pageable) {
        String userid = WebUtils.userid();
        command.setUserid(userid);
        map.addAttribute("powerBetList", betListService.userBogleLadderBet(command, pageable));
        map.addAttribute("config", JsonUtils.toString(bogleLadderService.gameConfig()));
        return "site/zone/bogleLadder";
    }


    @ResponseBody
    @RequestMapping(value = "zone/bogleLadder/config", method = RequestMethod.POST)
    public BogleLadderDto.Config config() {
        return bogleLadderService.gameConfig();
    }

    @RequestMapping(value = "zone/bogleLadder/score", method = RequestMethod.GET)
    public String score(ModelMap map, @ModelAttribute("command") ZoneDto.Command command,
                        @PageableDefault(size = 20, direction = Sort.Direction.DESC, sort = "sdate") Pageable pageable) {
        map.addAttribute("page", bogleLadderService.getClosing(command, pageable));
        return "site/score/bogleLadder";
    }

    @ResponseBody
    @RequestMapping(value = "zone/bogleLadder/betting", method = RequestMethod.POST)
    public AjaxResult betting(@RequestHeader(value = "AJAX") boolean ajax, @RequestBody ZoneDto.Bet bet) {
        if (!ajax) {
            return new AjaxResult(false, "페이지를 찾을 수 없습니다.");
        }
        return bogleLadderGameService.betting(bet);
    }

}
