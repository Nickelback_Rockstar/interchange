package spoon.web.site.gameZone;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import spoon.common.utils.JsonUtils;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.lotusOddeven.LotusOddevenDto;
import spoon.gameZone.lotusOddeven.service.LotusOddevenGameService;
import spoon.gameZone.lotusOddeven.service.LotusOddevenService;
import spoon.support.web.AjaxResult;

@Slf4j
@AllArgsConstructor
@Controller
@RequestMapping(value = "#{config.pathSite}")
public class LotusOddevenController {

    private LotusOddevenService lotusOddevenService;

    private LotusOddevenGameService lotusOddevenGameService;

    @RequestMapping(value = "zone/lotusOddeven", method = RequestMethod.GET)
    public String zone(ModelMap map) {
        map.addAttribute("config", JsonUtils.toString(lotusOddevenService.gameConfig()));
        return "site/zone/lotusOddeven";
    }

    @RequestMapping(value = "zone/lotusOddeven/score", method = RequestMethod.GET)
    public String score(ModelMap map, @ModelAttribute("command") ZoneDto.Command command,
                        @PageableDefault(size = 20, direction = Sort.Direction.DESC, sort = "sdate") Pageable pageable) {
        map.addAttribute("page", lotusOddevenService.getClosing(command, pageable));
        return "site/score/lotusOddeven";
    }

    @ResponseBody
    @RequestMapping(value = "zone/lotusOddeven/config", method = RequestMethod.POST)
    public LotusOddevenDto.Config config() {
        return lotusOddevenService.gameConfig();
    }

    @ResponseBody
    @RequestMapping(value = "zone/lotusOddeven/betting", method = RequestMethod.POST)
    public AjaxResult betting(@RequestHeader(value = "AJAX") boolean ajax, @RequestBody ZoneDto.Bet bet) {
        if (!ajax) {
            return new AjaxResult(false, "페이지를 찾을 수 없습니다.");
        }
        return lotusOddevenGameService.betting(bet);
    }
}
