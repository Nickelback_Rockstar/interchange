package spoon.web.site.gameZone;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import spoon.bet.domain.BetDto;
import spoon.bet.service.BetListService;
import spoon.common.utils.JsonUtils;
import spoon.common.utils.StringUtils;
import spoon.common.utils.WebUtils;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.fxGame4.FxGame4Dto;
import spoon.gameZone.fxGame4.service.FxGame4GameService;
import spoon.gameZone.fxGame4.service.FxGame4Service;
import spoon.support.web.AjaxResult;

@Slf4j
@AllArgsConstructor
@Controller
@RequestMapping(value = "#{config.pathSite}")
public class FxGame4Controller {

    private FxGame4Service fxGame4Service;

    private FxGame4GameService fxGame4GameService;

    private BetListService betListService;

    @RequestMapping(value = "zone/fxGame4", method = RequestMethod.GET)
    public String zone(ModelMap map, @RequestParam(value = "round", required = false) String round, BetDto.UserCommand command, @PageableDefault(size = 10, direction = Sort.Direction.DESC, sort = "sdate") Pageable pageable) {
        String userid = WebUtils.userid();
        command.setUserid(userid);
        FxGame4Dto.Config config = fxGame4Service.gameConfig(getNextRound(round));
        map.addAttribute("config", JsonUtils.toString(config));
        return "site/zone/fxGame4";
    }

    @ResponseBody
    @RequestMapping(value = "zone/fxGame4/config", method = RequestMethod.POST)
    public FxGame4Dto.Config config(@RequestParam(value = "round", required = false) String round) {
        return fxGame4Service.gameConfig(getNextRound(round));
    }

    public int getNextRound(String round){
        int nextRound = 2;
        try {
            if (StringUtils.notEmpty(round)) {
                nextRound = Integer.parseInt(round);
                if (nextRound != 2 && nextRound != 4) {
                    nextRound = 2;
                }
            }
        }catch (Exception e){
        }finally {
            return nextRound;
        }
    }

    @RequestMapping(value = "zone/fxGame4/score", method = RequestMethod.GET)
    public String score(ModelMap map, @ModelAttribute("command") ZoneDto.Command command,
                        @PageableDefault(size = 20, direction = Sort.Direction.DESC, sort = "sdate") Pageable pageable) {
        map.addAttribute("page", fxGame4Service.getClosing(command, pageable));
        return "site/score/fxGame4";
    }

    @ResponseBody
    @RequestMapping(value = "zone/fxGame4/betting", method = RequestMethod.POST)
    public AjaxResult betting(@RequestHeader(value = "AJAX") boolean ajax, @RequestBody ZoneDto.Bet bet) {
        if (!ajax) {
            return new AjaxResult(false, "페이지를 찾을 수 없습니다.");
        }
        return fxGame4GameService.betting(bet);
    }

}
