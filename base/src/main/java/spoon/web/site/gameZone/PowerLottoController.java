package spoon.web.site.gameZone;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import spoon.common.net.HttpParsing;
import spoon.common.utils.WebUtils;
import spoon.config.domain.Config;
import spoon.member.domain.MemberDto;
import spoon.member.entity.Member;
import spoon.member.service.MemberService;
import spoon.member.service.MemberUpdateService;
import spoon.support.web.AjaxResult;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@AllArgsConstructor
@Controller
@RequestMapping(value = "#{config.pathSite}")
public class PowerLottoController {

    private MemberService memberService;
    private MemberUpdateService memberUpdateService;

    @RequestMapping(value = "zone/powerLotto", method = RequestMethod.GET)
    public String score(ModelMap map, HttpServletRequest request) {
        Member member = memberService.getMember(WebUtils.userid());
        map.addAttribute("powerLotto", member.isPowerLotto());
        map.addAttribute("domain", request.getServerName());
        map.addAttribute("code", Config.getSysConfig().getZone().getPowerLottoCode());
        map.addAttribute("aid", WebUtils.userid());

        return "site/zone/powerLotto";
    }

    @ResponseBody
    @RequestMapping(value = "zone/powerLottoJoin", method = RequestMethod.POST)
    public AjaxResult powerLottoJoin(MemberDto.Add add) {

        if(add.getJoinKey().length() == 4 && Integer.parseInt(add.getJoinKey()) >= 0) {

            String userid = WebUtils.userid();
            String nickname = WebUtils.user().getNickname();
            String code = Config.getSysConfig().getZone().getPowerLottoCode();
            String url = Config.getSysConfig().getZone().getPowerLottoUrl();

            String param = ""
                    + "?code=" + code
                    + "&userid=" + userid
                    + "&nickname=" + nickname
                    + "&password=" + add.getJoinKey();
            String json = HttpParsing.getJson(url + "/ap/join" + param);
            JSONParser parser = new JSONParser();
            try {
                JSONObject jsonObj = (JSONObject) parser.parse(json);
                if (jsonObj.containsKey("success") && (Boolean) jsonObj.get("success")) {
                    MemberDto.Update update = new MemberDto.Update();
                    update.setUserid(userid);
                    AjaxResult ar = memberUpdateService.powerLottoUpdate(update);
                    return new AjaxResult(true, "가입되었습니다.");
                } else {
                    return new AjaxResult(false, jsonObj.containsKey("message") ? jsonObj.get("message").toString() : "페이지를 찾을 수 없습니다.[1]");
                }
            } catch (ParseException e) {
                return new AjaxResult(false, "페이지를 찾을 수 없습니다.[2]");
            }
        }else{
            return new AjaxResult(false, "숫자 4자리로 입력하세요.[3]");
        }
    }
}
