package spoon.web.site;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import spoon.common.net.HttpParsing;
import spoon.common.utils.JsonUtils;
import spoon.common.utils.StringUtils;
import spoon.common.utils.TokenUtils;
import spoon.gameZone.tokenBaccarat.TkBaccaratBetDto;
import spoon.gameZone.tokenBaccarat.TkBaccaratBetInfoDto;
import spoon.gameZone.tokenBaccarat.TkBaccaratResultDto;
import spoon.gameZone.tokenBaccarat.service.TkBaccaratGameService;
import spoon.gameZone.tokenHilo.HiloBetDto;
import spoon.gameZone.tokenHilo.HiloBetInfoDto;
import spoon.gameZone.tokenHilo.HiloResultDto;
import spoon.gameZone.tokenHilo.service.HiloGameService;
import spoon.gameZone.tokenHilo5.Hilo5BetDto;
import spoon.gameZone.tokenHilo5.Hilo5BetInfoDto;
import spoon.gameZone.tokenHilo5.Hilo5ResultDto;
import spoon.gameZone.tokenHilo5.service.Hilo5GameService;
import spoon.gameZone.tokenRoulette.TkRouletteBetDto;
import spoon.gameZone.tokenRoulette.TkRouletteBetInfoDto;
import spoon.gameZone.tokenRoulette.TkRouletteResultDto;
import spoon.gameZone.tokenRoulette.service.TkRouletteGameService;
import spoon.member.entity.Member;
import spoon.member.service.MemberService;
import spoon.support.web.AjaxResult;

import java.util.Map;

@Slf4j
@AllArgsConstructor
@Controller
public class TokenGameController {

    private MemberService memberService;

    private HiloGameService hiloGameService;
    private Hilo5GameService hilo5GameService;
    private TkBaccaratGameService baccaratGameService;
    private TkRouletteGameService rouletteGameService;

    @ResponseBody
    @RequestMapping(value = "/ex/userCheck", method = RequestMethod.GET)
    public String usersave(@RequestParam("userId") String userId) {
        String json = HttpParsing.getJson(TokenUtils.TOKEN_DOMAIN + "/api/auth?agent_id=" + TokenUtils.AGENT_ID + "&user_id=" + userId + "&hash=" + StringUtils.cvMD5(TokenUtils.AGENT_ID + "|" + TokenUtils.API_KEY));
        return json;
    }

    @ResponseBody
    @RequestMapping(value = "/balance", method = RequestMethod.POST)
    public String balance1(@RequestParam Map<String, String> body) {
        for (String key : body.keySet()) {
            System.out.println("key : " + key + " / value : " + body.get(key));
        }
        System.out.println("balance 호출됨1");
        return memberService.getTokenMember(body);
    }

    @ResponseBody
    @RequestMapping(value = "/bet", method = RequestMethod.POST)
    public String bet(@RequestParam Map<String, String> body) {
        System.out.println("bet 호출됨");
        for (String key : body.keySet()) {
            System.out.println("key : " + key + " / value : " + body.get(key));
        }

        String jArr = body.get("betData");

        System.out.println("jArr= " + jArr);

        JSONParser parser = new JSONParser();
        JSONArray jsonArr = new JSONArray();
        JSONObject j1 = new JSONObject();
        JSONObject j2 = new JSONObject();
        JSONObject j3 = new JSONObject();


        try {
            jsonArr = (JSONArray) parser.parse(jArr);
            for (int i = 0; i < jsonArr.size(); i++) {
                j1 = (JSONObject) jsonArr.get(i);
                j2 = (JSONObject) j1.get("bet");

                //하이로우 10초
                if ("hilo".equals(j1.get("gtype").toString())) {
                    HiloBetInfoDto betinfo;
                    HiloBetDto bet;
                    HiloResultDto result = null;

                    betinfo = JsonUtils.toModel(j1.toString(), HiloBetInfoDto.class);
                    bet = JsonUtils.toModel(j2.toString(), HiloBetDto.class);

                    if (j1.containsKey("result")) {
                        j3 = (JSONObject) j1.get("result");
                        result = JsonUtils.toModel(j3.toString(), HiloResultDto.class);
                    }

                    //에이전트 아이디 확인
                    if (!betinfo.getAgent_id().equals(TokenUtils.AGENT_ID)) {
                        return null;
                    }

                    JSONObject resultJson = new JSONObject();
                    if ("bet".equals(betinfo.getDiv())) {
                        System.out.println("bet 호출됨 bet");
                        AjaxResult ajax = new AjaxResult();
                        ajax = hiloGameService.betting(betinfo, bet);
                        System.out.println("ajax = " + ajax);
                        if (ajax.isSuccess()) {
                            Member m = memberService.getMember(betinfo.getUser_id());
                            resultJson.put("statuscode", 0);
                            resultJson.put("cash", m.getMoney());
                        } else {
                            resultJson.put("statuscode", 1);
                            resultJson.put("cash", 0);
                        }
                        return resultJson.toJSONString();
                    } else if ("result".equals(betinfo.getDiv()) || "cancel".equals(betinfo.getDiv())) {
                        System.out.println("bet 호출됨" + betinfo.getDiv());
                        hiloGameService.closingBetting(betinfo, bet, result);
                        if (i == jsonArr.size() - 1) { //마지막 for문 리턴
                            Member m = memberService.getMember(betinfo.getUser_id());
                            resultJson.put("statuscode", 0);
                            resultJson.put("cash", m.getMoney());
                            return resultJson.toJSONString();
                        }
                    }

                    //하이로우 5초
                } else if ("hilo_5s".equals(j1.get("gtype").toString())) {
                    Hilo5BetInfoDto betinfo;
                    Hilo5BetDto bet;
                    Hilo5ResultDto result = null;

                    betinfo = JsonUtils.toModel(j1.toString(), Hilo5BetInfoDto.class);
                    bet = JsonUtils.toModel(j2.toString(), Hilo5BetDto.class);

                    if (j1.containsKey("result")) {
                        j3 = (JSONObject) j1.get("result");
                        result = JsonUtils.toModel(j3.toString(), Hilo5ResultDto.class);
                    }

                    //에이전트 아이디 확인
                    if (!betinfo.getAgent_id().equals(TokenUtils.AGENT_ID)) {
                        return null;
                    }

                    JSONObject resultJson = new JSONObject();
                    if ("bet".equals(betinfo.getDiv())) {
                        System.out.println("bet 호출됨 bet");
                        AjaxResult ajax = new AjaxResult();
                        ajax = hilo5GameService.betting(betinfo, bet);
                        System.out.println("ajax = " + ajax);
                        if (ajax.isSuccess()) {
                            Member m = memberService.getMember(betinfo.getUser_id());
                            resultJson.put("statuscode", 0);
                            resultJson.put("cash", m.getMoney());
                        } else {
                            resultJson.put("statuscode", 1);
                            resultJson.put("cash", 0);
                        }
                        return resultJson.toJSONString();
                    } else if ("result".equals(betinfo.getDiv()) || "cancel".equals(betinfo.getDiv())) {
                        System.out.println("bet 호출됨" + betinfo.getDiv());
                        hilo5GameService.closingBetting(betinfo, bet, result);
                        if (i == jsonArr.size() - 1) { //마지막 for문 리턴
                            Member m = memberService.getMember(betinfo.getUser_id());
                            resultJson.put("statuscode", 0);
                            resultJson.put("cash", m.getMoney());
                            return resultJson.toJSONString();
                        }
                    }

                    //바카라
                } else if (j1.get("gtype").toString().startsWith("baccarat")) {
                    TkBaccaratBetInfoDto betinfo;
                    TkBaccaratBetDto bet;
                    TkBaccaratResultDto result = null;

                    betinfo = JsonUtils.toModel(j1.toString(), TkBaccaratBetInfoDto.class);
                    bet = JsonUtils.toModel(j2.toString(), TkBaccaratBetDto.class);

                    if (j1.containsKey("result")) {
                        j3 = (JSONObject) j1.get("result");
                        result = JsonUtils.toModel(j3.toString(), TkBaccaratResultDto.class);
                    }

                    //에이전트 아이디 확인
                    if (!betinfo.getAgent_id().equals(TokenUtils.AGENT_ID)) {
                        return null;
                    }

                    JSONObject resultJson = new JSONObject();
                    if ("bet".equals(betinfo.getDiv())) {
                        System.out.println("bet 호출됨 bet");
                        AjaxResult ajax = new AjaxResult();
                        ajax = baccaratGameService.betting(betinfo, bet);
                        System.out.println("ajax = " + ajax);
                        if (ajax.isSuccess()) {
                            Member m = memberService.getMember(betinfo.getUser_id());
                            resultJson.put("statuscode", 0);
                            resultJson.put("cash", m.getMoney());
                        } else {
                            resultJson.put("statuscode", 1);
                            resultJson.put("cash", 0);
                        }
                        return resultJson.toJSONString();
                    } else if ("result".equals(betinfo.getDiv()) || "cancel".equals(betinfo.getDiv())) {
                        System.out.println("bet 호출됨" + betinfo.getDiv());
                        baccaratGameService.closingBetting(betinfo, bet, result);
                        if (i == jsonArr.size() - 1) { //마지막 for문 리턴
                            Member m = memberService.getMember(betinfo.getUser_id());
                            resultJson.put("statuscode", 0);
                            resultJson.put("cash", m.getMoney());
                            return resultJson.toJSONString();
                        }
                    }

                    //룰렛
                } else if ("roulette".equals(j1.get("gtype").toString())) {
                    TkRouletteBetInfoDto betinfo;
                    TkRouletteBetDto bet;
                    TkRouletteResultDto result = null;

                    betinfo = JsonUtils.toModel(j1.toString(), TkRouletteBetInfoDto.class);
                    bet = JsonUtils.toModel(j2.toString(), TkRouletteBetDto.class);

                    if (j1.containsKey("result")) {
                        j3 = (JSONObject) j1.get("result");
                        result = JsonUtils.toModel(j3.toString(), TkRouletteResultDto.class);
                    }

                    //에이전트 아이디 확인
                    if (!betinfo.getAgent_id().equals(TokenUtils.AGENT_ID)) {
                        return null;
                    }

                    JSONObject resultJson = new JSONObject();
                    if ("bet".equals(betinfo.getDiv())) {
                        System.out.println("bet 호출됨 bet");
                        AjaxResult ajax = new AjaxResult();
                        ajax = rouletteGameService.betting(betinfo, bet);
                        System.out.println("ajax = " + ajax);
                        if (ajax.isSuccess()) {
                            Member m = memberService.getMember(betinfo.getUser_id());
                            resultJson.put("statuscode", 0);
                            resultJson.put("cash", m.getMoney());
                        } else {
                            resultJson.put("statuscode", 1);
                            resultJson.put("cash", 0);
                        }
                        return resultJson.toJSONString();
                    } else if ("result".equals(betinfo.getDiv()) || "cancel".equals(betinfo.getDiv())) {
                        System.out.println("bet 호출됨" + betinfo.getDiv());
                        rouletteGameService.closingBetting(betinfo, bet, result);
                        if (i == jsonArr.size() - 1) { //마지막 for문 리턴
                            Member m = memberService.getMember(betinfo.getUser_id());
                            resultJson.put("statuscode", 0);
                            resultJson.put("cash", m.getMoney());
                            return resultJson.toJSONString();
                        }
                    }
                }

            }
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }

        return null;
    }

    @ResponseBody
    @RequestMapping(value = "/newbet", method = RequestMethod.POST)
    public String newbet(@RequestParam Map<String, String> body) {
        System.out.println("newbet 호출됨");
        for (String key : body.keySet()) {
            System.out.println("key : " + key + " / value : " + body.get(key));
        }
        return null;
    }


}
