package spoon.web.site.customer;

import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import spoon.common.utils.WebUtils;
import spoon.member.domain.User;
import spoon.member.service.MemberListService;
import spoon.payment.domain.PaymentDto;
import spoon.payment.service.PaymentListService;

import java.util.List;

@AllArgsConstructor
@Controller
@RequestMapping("#{config.pathSite}")
public class CustomerController {

    private PaymentListService paymentListService;

    private MemberListService memberListService;

    @RequestMapping(value = "customer/guide/{menu}", method = RequestMethod.GET)
    public String guide(@PathVariable("menu") String menu) {
        return "site/customer/" + menu;
    }

    @RequestMapping(value = "recommender", method = RequestMethod.GET)
    public String recommender(ModelMap map, PaymentDto.Command command, @PageableDefault(size = 30, direction = Sort.Direction.DESC, sort = "regDate") Pageable pageable) {

        String userid = WebUtils.userid();
        List<User> recommList = memberListService.userRecommList(userid); //추천인 리스트
        command.setUserid(userid);
        map.addAttribute("recommender", recommList);
        map.addAttribute("page", paymentListService.pointPage(command, pageable));
        return "site/customer/recommender";
    }

}
