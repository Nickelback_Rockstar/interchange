package spoon.web.seller.agency;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import spoon.common.utils.JsonUtils;
import spoon.common.utils.WebUtils;
import spoon.member.domain.CurrentUser;
import spoon.member.domain.MemberDto;
import spoon.member.service.MemberService;
import spoon.seller.domain.JoinCodeDto;
import spoon.seller.service.JoinCodeService;
import spoon.support.web.AjaxResult;

@Slf4j
@AllArgsConstructor
@Controller("seller.joinCodeController")
@RequestMapping("#{config.pathSeller}")
public class JoinCodeController {

    private JoinCodeService joinCodeService;

    private MemberService memberService;

    /**
     * 가입코드 페이지
     */
    @RequestMapping(value = "agency/joinCode", method = RequestMethod.GET)
    public String list(ModelMap map, JoinCodeDto.Partner command) {
        CurrentUser user = WebUtils.user();
        command.setAgency1(user.getAgency1());
        command.setAgency2(user.getAgency2());
        command.setAgency3(user.getAgency3());
        command.setAgency4(user.getAgency4());
        command.setAgency5(user.getAgency5());
        command.setAgency6(user.getAgency6());
        command.setAgency7(user.getAgency7());

        map.addAttribute("list", joinCodeService.getPartner(command));
        return "seller/agency/joinCode";
    }

    @ResponseBody
    @RequestMapping(value = "seller/joinCode/add", method = RequestMethod.POST)
    public AjaxResult add(JoinCodeDto.Add add) {
        String userid = WebUtils.userid();
        add.setAgency7(userid);
        return joinCodeService.add(add);
    }

    @ResponseBody
    @RequestMapping(value = "seller/joinCode/update", method = RequestMethod.POST)
    public AjaxResult update(JoinCodeDto.Update update) {
        return joinCodeService.update(update);
    }

    @ResponseBody
    @RequestMapping(value = "seller/joinCode/delete", method = RequestMethod.POST)
    public AjaxResult delete(Long id) {
        return joinCodeService.delete(id);
    }

}
