package spoon.web.seller.sale;

import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import spoon.banking.domain.BankingDto;
import spoon.banking.service.BankingListService;
import spoon.common.utils.WebUtils;
import spoon.member.domain.CurrentUser;
import spoon.member.domain.MemberDto;
import spoon.member.service.MemberService;

import java.util.List;

@AllArgsConstructor
@Controller("seller.bankController")
@RequestMapping("#{config.pathSeller}")
public class BankController {

    private BankingListService bankingListService;

    private MemberService memberService;

    @RequestMapping(value = "sale/banking", method = RequestMethod.GET)
    public String banking(ModelMap map, BankingDto.Seller command,
                          @PageableDefault(size = 20, direction = Sort.Direction.DESC, sort = "regDate") Pageable pageable) {
        CurrentUser user = WebUtils.user();
        command.setAgency1(user.getAgency1());
        command.setAgency2(user.getAgency2());
        command.setAgency3(user.getAgency3());
        command.setAgency4(user.getAgency4());
        command.setAgency5(user.getAgency5());
        command.setAgency6(user.getAgency6());
        command.setAgency7(user.getAgency7());

        MemberDto.Seller seller = new MemberDto.Seller();
        seller.setAgency(user.getUserid());
        seller.setAgencyDepth(user.getAgencyDepth());
        List<MemberDto.Agency> agencyList = memberService.getAgencyMemberList2(seller); //상위총판을 가져옴
        map.addAttribute("agencyList", agencyList);
        map.addAttribute("page", bankingListService.bankingPage(command, pageable));
        map.addAttribute("command", command);

        return "seller/sale/banking";
    }

}
