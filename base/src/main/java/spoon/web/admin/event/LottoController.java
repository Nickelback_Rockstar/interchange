package spoon.web.admin.event;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import spoon.config.domain.Config;
import spoon.config.domain.LottoConfig;
import spoon.event.domain.DailyDto;
import spoon.event.service.LottoService;
import spoon.member.service.MemberService;
import spoon.support.web.AjaxResult;

@Slf4j
@AllArgsConstructor
@Controller("admin.lottoController")
@RequestMapping(value = "#{config.pathAdmin}")
public class LottoController {

    private LottoService lottoService;

    private MemberService memberService;

    @RequestMapping(value = "event/lotto/config", method = RequestMethod.GET)
    public String config(ModelMap map) {
        System.out.printf("=lottoConfig="+Config.getLottoConfig().toString());
        map.addAttribute("lottoConfig", Config.getLottoConfig());
        return "admin/event/lotto/config";
    }

    @RequestMapping(value = "event/lotto/config", method = RequestMethod.POST)
    public String config(LottoConfig lottoConfig, RedirectAttributes ra) {
        boolean success = lottoService.updateConfig(lottoConfig);
        if (success) {
            ra.addFlashAttribute("message", "출석체크 설정을 변경하였습니다.");
        } else {
            ra.addFlashAttribute("message", "출석체크 설정 변경시 오류가 발생하였습니다.");
        }
        return "redirect:" + Config.getPathAdmin() + "/event/lotto/config";
    }

    /**
     * 회원별 복권 변동 내역
     */
    @RequestMapping(value = "event/lotto/{userid}", method = RequestMethod.GET)
    public String lottoPopup(ModelMap map, @PathVariable("userid") String userid, DailyDto.Command command,
                             @PageableDefault(size = 30, direction = Sort.Direction.DESC, sort = "regDate") Pageable pageable) {
        command.setUserid(userid);
        map.addAttribute("page", lottoService.lottoPage(command, pageable));
        map.addAttribute("member", memberService.getUser(userid));
        return "admin/event/lotto/popup/lotto";
    }

    /**
     * 회원별 복권 변동 내역
     */
    @RequestMapping(value = "event/lotto/list", method = RequestMethod.GET)
    public String list(ModelMap map, DailyDto.Command command,
                             @PageableDefault(size = 30, direction = Sort.Direction.DESC, sort = "regDate") Pageable pageable) {
        map.addAttribute("page", lottoService.lottoListPage(command, pageable));
        return "admin/event/lotto/list";
    }



    /**
     * 회원별 복권 증정
     */
    @ResponseBody
    @RequestMapping(value = "event/lotto/add", method = RequestMethod.POST)
    public AjaxResult moneyAdd(DailyDto.Add add) {
        return lottoService.addLotto(add);
    }

    /**
     * 회원별 복권 증정
     */
    @ResponseBody
    @RequestMapping(value = "event/lotto/addTotal", method = RequestMethod.POST)
    public AjaxResult addTotal(DailyDto.Add add) {
        return lottoService.addTotalLotto(add);
    }

    @ResponseBody
    @RequestMapping(value = "event/lotto/change", method = RequestMethod.POST)
    public AjaxResult changeCancel(DailyDto.Add add) {
        return lottoService.changeCancel(add);
    }


}
