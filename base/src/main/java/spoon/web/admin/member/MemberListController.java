package spoon.web.admin.member;

import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import spoon.common.utils.JsonUtils;
import spoon.common.utils.StringUtils;
import spoon.member.domain.MemberDto;
import spoon.member.service.MemberListService;
import spoon.member.service.MemberService;

@AllArgsConstructor
@Controller("admin.memberListController")
@RequestMapping("#{config.pathAdmin}")
public class MemberListController {

    private MemberListService memberListService;

    private MemberService memberService;

    @RequestMapping(value = "member/list", method = RequestMethod.GET)
    public String list(ModelMap map, MemberDto.Command command, MemberDto.Seller command2,
                       @PageableDefault(size = 50) Pageable pageable) {
        map.addAttribute("page", memberListService.list(command, pageable));
        map.addAttribute("lnb", memberListLnb(command));
        map.addAttribute("command", command);
        map.addAttribute("agencyList", memberService.getAgency7List2(command2));


        return "admin/member/list";
    }

    private String memberListLnb(MemberDto.Command param) {
        if (StringUtils.notEmpty(param.getSearchValue())) {
            return "list";
        }
        switch (param.getMode()) {
            case "disabled":
                return "disabled";
            case "secession":
                return "secession";
            case "dummy":
                return "dummy";
            default:
                return "list";
        }
    }
}
