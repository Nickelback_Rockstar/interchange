package spoon.web.admin.gameZone;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import spoon.config.domain.Config;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.binanceCoin1.BinanceCoin1Config;
import spoon.gameZone.binanceCoin1.BinanceCoin1Dto;
import spoon.gameZone.binanceCoin1.service.BinanceCoin1Service;
import spoon.support.web.AjaxResult;

@Slf4j
@AllArgsConstructor
@Controller("admin.binanceCoin1Controller")
@RequestMapping(value = "#{config.pathAdmin}")
public class BinanceCoin1Controller {

    private BinanceCoin1Service binanceCoin1Service;

    /**
     * 바이낸스코인1 설정
     */
    @RequestMapping(value = "zone/binanceCoin1/config", method = RequestMethod.GET)
    public String config(ModelMap map) {
        map.addAttribute("config", ZoneConfig.getBinanceCoin1());
        return "admin/zone/binanceCoin1/config";
    }

    /**
     * 바이낸스코인1 설정 변경
     */
    @RequestMapping(value = "zone/binanceCoin1/config", method = RequestMethod.POST)
    public String config(BinanceCoin1Config binanceCoin1Config, RedirectAttributes ra) {
        boolean success = binanceCoin1Service.updateConfig(binanceCoin1Config);
        if (success) {
            ra.addFlashAttribute("message", "비트코인1 게임설정을 변경하였습니다.");
        } else {
            ra.addFlashAttribute("message", "비트코인1 게임설정 변경에 실패하였습니다.");
        }
        return "redirect:" + Config.getPathAdmin() + "/zone/binanceCoin1/config";
    }

    /**
     * 바이낸스코인1 진행
     */
    @RequestMapping(value = "zone/binanceCoin1/complete", method = RequestMethod.GET)
    public String complete(ModelMap map) {
        map.addAttribute("list", binanceCoin1Service.getComplete());
        map.addAttribute("config", ZoneConfig.getBinanceCoin1());
        return "admin/zone/binanceCoin1/complete";
    }

    /**
     * 바이낸스코인1 완료
     */
    @RequestMapping(value = "zone/binanceCoin1/closing", method = RequestMethod.GET)
    public String closing(ModelMap map, @ModelAttribute("command") ZoneDto.Command command,
                          @PageableDefault(size = 20, direction = Sort.Direction.DESC, sort = "gameDate") Pageable pageable) {
        map.addAttribute("page", binanceCoin1Service.getClosing(command, pageable));
        map.addAttribute("config", ZoneConfig.getBinanceCoin1());
        return "admin/zone/binanceCoin1/closing";
    }

    /**
     * 바이낸스코인1 스코어 입력 폼
     */
    @RequestMapping(value = "zone/binanceCoin1/score", method = RequestMethod.GET)
    public String score(ModelMap map, Long id) {
        map.addAttribute("score", binanceCoin1Service.findScore(id));
        return "admin/zone/binanceCoin1/score";
    }

    /**
     * 바이낸스코인1 결과처리
     */
    @RequestMapping(value = "zone/binanceCoin1/score", method = RequestMethod.POST)
    public String score(BinanceCoin1Dto.Score score, RedirectAttributes ra) {
        boolean success = binanceCoin1Service.closingGame(score);

        if (success) {
            ra.addFlashAttribute("message", "달팽이 결과처리를 완료 하였습니다.");
        } else {
            ra.addFlashAttribute("message", "달팽이 결과처리에 실패 하였습니다.");
        }
        ra.addFlashAttribute("popup", "closing");

        return "redirect:" + Config.getPathAdmin() + "/zone/binanceCoin1/score?id=" + score.getId();
    }

    /**
     * 바이낸스코인1 베팅 없는경기 모두 결과처리
     */
    @ResponseBody
    @RequestMapping(value = "zone/binanceCoin1/closing", method = RequestMethod.POST)
    public AjaxResult closing() {
        return binanceCoin1Service.closingAllGame();
    }

}
