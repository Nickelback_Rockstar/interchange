package spoon.web.admin.gameZone;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import spoon.config.domain.Config;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.fxGame3.FxGame3Config;
import spoon.gameZone.fxGame3.FxGame3Dto;
import spoon.gameZone.fxGame3.service.FxGame3Service;
import spoon.support.web.AjaxResult;

@Slf4j
@AllArgsConstructor
@Controller("admin.fxGame3Controller")
@RequestMapping(value = "#{config.pathAdmin}")
public class FxGame3Controller {

    private FxGame3Service fxGame3Service;

    /**
     * FxGame3 설정
     */
    @RequestMapping(value = "zone/fxGame3/config", method = RequestMethod.GET)
    public String config(ModelMap map) {
        map.addAttribute("config", ZoneConfig.getFxGame3());
        return "admin/zone/fxGame3/config";
    }

    /**
     * FxGame3 설정 변경
     */
    @RequestMapping(value = "zone/fxGame3/config", method = RequestMethod.POST)
    public String config(FxGame3Config fxGame3Config, RedirectAttributes ra) {
        boolean success = fxGame3Service.updateConfig(fxGame3Config);
        if (success) {
            ra.addFlashAttribute("message", "FxGame3게임 설정을 변경하였습니다.");
        } else {
            ra.addFlashAttribute("message", "FxGame3게임 설정변경에 실패하였습니다.");
        }
        return "redirect:" + Config.getPathAdmin() + "/zone/fxGame3/config";
    }

    /**
     * FxGame3 진행
     */
    @RequestMapping(value = "zone/fxGame3/complete", method = RequestMethod.GET)
    public String complete(ModelMap map) {
        map.addAttribute("list", fxGame3Service.getComplete());
        map.addAttribute("config", ZoneConfig.getFxGame3());
        return "admin/zone/fxGame3/complete";
    }

    /**
     * FxGame3 완료
     */
    @RequestMapping(value = "zone/fxGame3/closing", method = RequestMethod.GET)
    public String closing(ModelMap map, @ModelAttribute("command") ZoneDto.Command command,
                          @PageableDefault(size = 20, direction = Sort.Direction.DESC, sort = "sdate") Pageable pageable) {
        map.addAttribute("page", fxGame3Service.getClosing(command, pageable));
        map.addAttribute("config", ZoneConfig.getFxGame3());
        return "admin/zone/fxGame3/closing";
    }

    /**
     * FxGame3 스코어 입력 폼
     */
    @RequestMapping(value = "zone/fxGame3/score", method = RequestMethod.GET)
    public String score(ModelMap map, Long id) {
        map.addAttribute("score", fxGame3Service.findScore(id));
        return "admin/zone/fxGame3/score";
    }

    /**
     * FxGame3 스코어 결과처리
     */
    @RequestMapping(value = "zone/fxGame3/score", method = RequestMethod.POST)
    public String score(FxGame3Dto.Score score, RedirectAttributes ra) {
        boolean success = fxGame3Service.closingGame(score);

        if (success) {
            ra.addFlashAttribute("message", "FxGame3 결과처리를 완료 하였습니다.");
        } else {
            ra.addFlashAttribute("message", "FxGame3 결과처리에 실패 하였습니다.");
        }
        ra.addFlashAttribute("popup", "closing");

        return "redirect:" + Config.getPathAdmin() + "/zone/fxGame3/score?id=" + score.getId();
    }

    /**
     * FxGame3 베팅 없는경기 모두 결과처리
     */
    @ResponseBody
    @RequestMapping(value = "zone/fxGame3/closing", method = RequestMethod.POST)
    public AjaxResult closing() {
        return fxGame3Service.closingAllGame();
    }

}
