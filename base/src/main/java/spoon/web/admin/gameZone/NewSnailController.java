package spoon.web.admin.gameZone;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import spoon.config.domain.Config;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.newSnail.NewSnailConfig;
import spoon.gameZone.newSnail.NewSnailDto;
import spoon.gameZone.newSnail.service.NewSnailService;
import spoon.support.web.AjaxResult;

@Slf4j
@AllArgsConstructor
@Controller("admin.newSnailController")
@RequestMapping(value = "#{config.pathAdmin}")
public class NewSnailController {

    private NewSnailService newSnailService;

    /**
     * NEW달팽이 설정
     */
    @RequestMapping(value = "zone/newSnail/config", method = RequestMethod.GET)
    public String config(ModelMap map) {
        map.addAttribute("config", ZoneConfig.getNewSnail());
        return "admin/zone/newSnail/config";
    }

    /**
     * NEW달팽이 설정 변경
     */
    @RequestMapping(value = "zone/newSnail/config", method = RequestMethod.POST)
    public String config(NewSnailConfig newSnailConfig, RedirectAttributes ra) {
        //순위,복승식,쌍승식 뷰단에서 한개만 설정했으니 나머지도 세팅해줌.
        double[] odds = newSnailConfig.getOdds();
        //
        odds[6] = odds[5];//순위 배당
        odds[7] = odds[5];//순위 배당
        odds[8] = odds[5];//순위 배당
        //
        odds[10] = odds[9];//복승식 배당
        odds[11] = odds[9];//복승식 배당
        odds[12] = odds[9];//복승식 배당
        odds[13] = odds[9];//복승식 배당
        odds[14] = odds[9];//복승식 배당
        //
        odds[16] = odds[15];//쌍승식 배당
        odds[17] = odds[15];//쌍승식 배당
        odds[18] = odds[15];//쌍승식 배당
        odds[19] = odds[15];//쌍승식 배당
        odds[20] = odds[15];//쌍승식 배당
        odds[21] = odds[15];//쌍승식 배당
        odds[22] = odds[15];//쌍승식 배당
        odds[23] = odds[15];//쌍승식 배당
        odds[24] = odds[15];//쌍승식 배당
        odds[25] = odds[15];//쌍승식 배당
        odds[26] = odds[15];//쌍승식 배당
        //
        newSnailConfig.setOdds(odds);
        boolean success = newSnailService.updateConfig(newSnailConfig);
        if (success) {
            ra.addFlashAttribute("message", "NEW달팽이 게임설정을 변경하였습니다.");
        } else {
            ra.addFlashAttribute("message", "NEW달팽이 게임설정 변경에 실패하였습니다.");
        }
        return "redirect:" + Config.getPathAdmin() + "/zone/newSnail/config";
    }

    /**
     * NEW달팽이 진행
     */
    @RequestMapping(value = "zone/newSnail/complete", method = RequestMethod.GET)
    public String complete(ModelMap map) {
        map.addAttribute("list", newSnailService.getComplete());
        map.addAttribute("config", ZoneConfig.getNewSnail());
        return "admin/zone/newSnail/complete";
    }

    /**
     * NEW달팽이 완료
     */
    @RequestMapping(value = "zone/newSnail/closing", method = RequestMethod.GET)
    public String closing(ModelMap map, @ModelAttribute("command") ZoneDto.Command command,
                          @PageableDefault(size = 20, direction = Sort.Direction.DESC, sort = "gameDate") Pageable pageable) {
        map.addAttribute("page", newSnailService.getClosing(command, pageable));
        map.addAttribute("config", ZoneConfig.getNewSnail());
        return "admin/zone/newSnail/closing";
    }

    /**
     * NEW달팽이 스코어 입력 폼
     */
    @RequestMapping(value = "zone/newSnail/score", method = RequestMethod.GET)
    public String score(ModelMap map, Long id) {
        map.addAttribute("score", newSnailService.findScore(id));
        return "admin/zone/newSnail/score";
    }

    /**
     * NEW달팽이 결과처리
     */
    @RequestMapping(value = "zone/newSnail/score", method = RequestMethod.POST)
    public String score(NewSnailDto.Score score, RedirectAttributes ra) {
        boolean success = newSnailService.closingGame(score);

        if (success) {
            ra.addFlashAttribute("message", "달팽이 결과처리를 완료 하였습니다.");
        } else {
            ra.addFlashAttribute("message", "달팽이 결과처리에 실패 하였습니다.");
        }
        ra.addFlashAttribute("popup", "closing");

        return "redirect:" + Config.getPathAdmin() + "/zone/newSnail/score?id=" + score.getId();
    }

    /**
     * NEW달팽이 베팅 없는경기 모두 결과처리
     */
    @ResponseBody
    @RequestMapping(value = "zone/newSnail/closing", method = RequestMethod.POST)
    public AjaxResult closing() {
        return newSnailService.closingAllGame();
    }

}
