package spoon.web.admin.gameZone;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import spoon.config.domain.Config;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.fxGame4.FxGame4Config;
import spoon.gameZone.fxGame4.FxGame4Dto;
import spoon.gameZone.fxGame4.service.FxGame4Service;
import spoon.support.web.AjaxResult;

@Slf4j
@AllArgsConstructor
@Controller("admin.fxGame4Controller")
@RequestMapping(value = "#{config.pathAdmin}")
public class FxGame4Controller {

    private FxGame4Service fxGame4Service;

    /**
     * FxGame4 설정
     */
    @RequestMapping(value = "zone/fxGame4/config", method = RequestMethod.GET)
    public String config(ModelMap map) {
        map.addAttribute("config", ZoneConfig.getFxGame4());
        return "admin/zone/fxGame4/config";
    }

    /**
     * FxGame4 설정 변경
     */
    @RequestMapping(value = "zone/fxGame4/config", method = RequestMethod.POST)
    public String config(FxGame4Config fxGame4Config, RedirectAttributes ra) {
        boolean success = fxGame4Service.updateConfig(fxGame4Config);
        if (success) {
            ra.addFlashAttribute("message", "FxGame4게임 설정을 변경하였습니다.");
        } else {
            ra.addFlashAttribute("message", "FxGame4게임 설정변경에 실패하였습니다.");
        }
        return "redirect:" + Config.getPathAdmin() + "/zone/fxGame4/config";
    }

    /**
     * FxGame4 진행
     */
    @RequestMapping(value = "zone/fxGame4/complete", method = RequestMethod.GET)
    public String complete(ModelMap map) {
        map.addAttribute("list", fxGame4Service.getComplete());
        map.addAttribute("config", ZoneConfig.getFxGame4());
        return "admin/zone/fxGame4/complete";
    }

    /**
     * FxGame4 완료
     */
    @RequestMapping(value = "zone/fxGame4/closing", method = RequestMethod.GET)
    public String closing(ModelMap map, @ModelAttribute("command") ZoneDto.Command command,
                          @PageableDefault(size = 20, direction = Sort.Direction.DESC, sort = "sdate") Pageable pageable) {
        map.addAttribute("page", fxGame4Service.getClosing(command, pageable));
        map.addAttribute("config", ZoneConfig.getFxGame4());
        return "admin/zone/fxGame4/closing";
    }

    /**
     * FxGame4 스코어 입력 폼
     */
    @RequestMapping(value = "zone/fxGame4/score", method = RequestMethod.GET)
    public String score(ModelMap map, Long id) {
        map.addAttribute("score", fxGame4Service.findScore(id));
        return "admin/zone/fxGame4/score";
    }

    /**
     * FxGame4 스코어 결과처리
     */
    @RequestMapping(value = "zone/fxGame4/score", method = RequestMethod.POST)
    public String score(FxGame4Dto.Score score, RedirectAttributes ra) {
        boolean success = fxGame4Service.closingGame(score);

        if (success) {
            ra.addFlashAttribute("message", "FxGame4 결과처리를 완료 하였습니다.");
        } else {
            ra.addFlashAttribute("message", "FxGame4 결과처리에 실패 하였습니다.");
        }
        ra.addFlashAttribute("popup", "closing");

        return "redirect:" + Config.getPathAdmin() + "/zone/fxGame4/score?id=" + score.getId();
    }

    /**
     * FxGame4 베팅 없는경기 모두 결과처리
     */
    @ResponseBody
    @RequestMapping(value = "zone/fxGame4/closing", method = RequestMethod.POST)
    public AjaxResult closing() {
        return fxGame4Service.closingAllGame();
    }

}
