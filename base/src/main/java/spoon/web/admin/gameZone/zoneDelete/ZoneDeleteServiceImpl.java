package spoon.web.admin.gameZone.zoneDelete;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import spoon.mapper.GameMapper;
import spoon.support.web.AjaxResult;

@AllArgsConstructor
@Service
public class ZoneDeleteServiceImpl implements ZoneDeleteService {

    private GameMapper gameMapper;

    @Transactional
    @Override
    public AjaxResult noBetDelete() {
        gameMapper.noBetDelete();
        return new AjaxResult(true, "베팅, 결과없는 경기 삭제처리 했습니다.");
    }
}
