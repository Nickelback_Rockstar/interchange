package spoon.web.admin.gameZone;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import spoon.config.domain.Config;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.powerFreeKick.PowerFreeKickConfig;
import spoon.gameZone.powerFreeKick.PowerFreeKickDto;
import spoon.gameZone.powerFreeKick.service.PowerFreeKickService;
import spoon.support.web.AjaxResult;

@Slf4j
@AllArgsConstructor
@Controller("admin.PowerFreeKickController")
@RequestMapping(value = "#{config.pathAdmin}")
public class PowerFreeKickController {

    private PowerFreeKickService powerFreeKickService;

    /**
     * 파워프리킥 설정
     */
    @RequestMapping(value = "zone/powerFreeKick/config", method = RequestMethod.GET)
    public String config(ModelMap map) {
        map.addAttribute("config", ZoneConfig.getPowerFreeKick());
        return "admin/zone/powerFreeKick/config";
    }

    /**
     * 파워프리킥 설정 변경
     */
    @RequestMapping(value = "zone/powerFreeKick/config", method = RequestMethod.POST)
    public String config(PowerFreeKickConfig powerFreeKickConfig, RedirectAttributes ra) {
        boolean success = powerFreeKickService.updateConfig(powerFreeKickConfig);
        if (success) {
            ra.addFlashAttribute("message", "파워볼 설정을 변경하였습니다.");
        } else {
            ra.addFlashAttribute("message", "파워볼 설정변경에 실패하였습니다.");
        }

        return "redirect:" + Config.getPathAdmin() + "/zone/powerFreeKick/config";
    }

    /**
     * 파워프리킥 진행
     */
    @RequestMapping(value = "zone/powerFreeKick/complete", method = RequestMethod.GET)
    public String complete(ModelMap map) {
        map.addAttribute("list", powerFreeKickService.getComplete());
        map.addAttribute("config", ZoneConfig.getPowerFreeKick());
        return "admin/zone/powerFreeKick/complete";
    }

    /**
     * 파워프리킥 완료
     */
    @RequestMapping(value = "zone/powerFreeKick/closing", method = RequestMethod.GET)
    public String closing(ModelMap map, @ModelAttribute("command") ZoneDto.Command command,
                          @PageableDefault(size = 20, direction = Sort.Direction.DESC, sort = "gameDate") Pageable pageable) {
        map.addAttribute("page", powerFreeKickService.getClosing(command, pageable));
        map.addAttribute("config", ZoneConfig.getPowerFreeKick());
        return "admin/zone/powerFreeKick/closing";
    }

    /**
     * 파워프리킥 스코어 입력 폼
     */
    @RequestMapping(value = "zone/powerFreeKick/score", method = RequestMethod.GET)
    public String score(ModelMap map, Long id) {
        map.addAttribute("score", powerFreeKickService.findScore(id));
        return "admin/zone/powerFreeKick/score";
    }

    /**
     * 파워프리킥 결과처리
     */
    @RequestMapping(value = "zone/powerFreeKick/score", method = RequestMethod.POST)
    public String score(PowerFreeKickDto.Score score, RedirectAttributes ra) {
        boolean success = powerFreeKickService.closingGame(score);

        if (success) {
            ra.addFlashAttribute("message", "파워볼 결과처리를 완료 하였습니다.");
        } else {
            ra.addFlashAttribute("message", "파워볼 결과처리에 실패 하였습니다.");
        }
        ra.addFlashAttribute("popup", "closing");

        return "redirect:" + Config.getPathAdmin() + "/zone/powerFreeKick/score?id=" + score.getId();
    }

    /**
     * 파워프리킥 베팅 없는경기 모두 결과처리
     */
    @ResponseBody
    @RequestMapping(value = "zone/powerFreeKick/closing", method = RequestMethod.POST)
    public AjaxResult closing() {
        return powerFreeKickService.closingAllGame();
    }

}
