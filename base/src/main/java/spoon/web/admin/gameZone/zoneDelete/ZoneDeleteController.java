package spoon.web.admin.gameZone.zoneDelete;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import spoon.support.web.AjaxResult;

@AllArgsConstructor
@Controller("admin.zoneDeleteController")
@RequestMapping(value = "#{config.pathAdmin}")
public class ZoneDeleteController {

    private ZoneDeleteService zoneDeleteService;

    @ResponseBody
    @RequestMapping(value = "zoneGame/delete", method = RequestMethod.POST)
    public AjaxResult zoneGameDelete() {
        return zoneDeleteService.noBetDelete();
    }
}
