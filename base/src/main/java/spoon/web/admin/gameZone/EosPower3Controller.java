package spoon.web.admin.gameZone;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import spoon.config.domain.Config;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.eosPower3.EosPower3Config;
import spoon.gameZone.eosPower3.EosPower3Dto;
import spoon.gameZone.eosPower3.service.EosPower3Service;
import spoon.support.web.AjaxResult;

@Slf4j
@AllArgsConstructor
@Controller("admin.eosPower3Controller")
@RequestMapping(value = "#{config.pathAdmin}")
public class EosPower3Controller {

    private EosPower3Service eosPower3Service;

    /**
     * 파워볼 설정
     */
    @RequestMapping(value = "zone/eosPower3/config", method = RequestMethod.GET)
    public String config(ModelMap map) {
        map.addAttribute("config", ZoneConfig.getEosPower3());
        return "admin/zone/eosPower3/config";
    }

    /**
     * 파워볼 설정 변경
     */
    @RequestMapping(value = "zone/eosPower3/config", method = RequestMethod.POST)
    public String config(EosPower3Config powerConfig, RedirectAttributes ra) {
        boolean success = eosPower3Service.updateConfig(powerConfig);
        if (success) {
            ra.addFlashAttribute("message", "EOS파워볼 3분 설정을 변경하였습니다.");
        } else {
            ra.addFlashAttribute("message", "EOS파워볼 3분 설정변경에 실패하였습니다.");
        }

        return "redirect:" + Config.getPathAdmin() + "/zone/eosPower3/config";
    }

    /**
     * 파워볼 진행
     */
    @RequestMapping(value = "zone/eosPower3/complete", method = RequestMethod.GET)
    public String complete(ModelMap map) {
        map.addAttribute("list", eosPower3Service.getComplete());
        map.addAttribute("config", ZoneConfig.getEosPower3());
        return "admin/zone/eosPower3/complete";
    }

    /**
     * 파워볼 완료
     */
    @RequestMapping(value = "zone/eosPower3/closing", method = RequestMethod.GET)
    public String closing(ModelMap map, @ModelAttribute("command") ZoneDto.Command command,
                          @PageableDefault(size = 20, direction = Sort.Direction.DESC, sort = "gameDate") Pageable pageable) {
        map.addAttribute("page", eosPower3Service.getClosing(command, pageable));
        map.addAttribute("config", ZoneConfig.getEosPower3());
        return "admin/zone/eosPower3/closing";
    }

    /**
     * 파워볼 스코어 입력 폼
     */
    @RequestMapping(value = "zone/eosPower3/score", method = RequestMethod.GET)
    public String score(ModelMap map, Long id) {
        map.addAttribute("score", eosPower3Service.findScore(id));
        return "admin/zone/eosPower3/score";
    }

    /**
     * 파워볼 결과처리
     */
    @RequestMapping(value = "zone/eosPower3/score", method = RequestMethod.POST)
    public String score(EosPower3Dto.Score score, RedirectAttributes ra) {
        boolean success = eosPower3Service.closingGame(score);

        if (success) {
            ra.addFlashAttribute("message", "EOS파워볼 3분 결과처리를 완료 하였습니다.");
        } else {
            ra.addFlashAttribute("message", "EOS파워볼 3분  결과처리에 실패 하였습니다.");
        }
        ra.addFlashAttribute("popup", "closing");

        return "redirect:" + Config.getPathAdmin() + "/zone/eosPower3/score?id=" + score.getId();
    }

    /**
     * 파워볼 베팅 없는경기 모두 결과처리
     */
    @ResponseBody
    @RequestMapping(value = "zone/eosPower3/closing", method = RequestMethod.POST)
    public AjaxResult closing() {
        return eosPower3Service.closingAllGame();
    }

}
