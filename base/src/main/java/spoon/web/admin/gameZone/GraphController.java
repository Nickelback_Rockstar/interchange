package spoon.web.admin.gameZone;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import spoon.payment.domain.PaymentDto;
import spoon.payment.service.PaymentListService;

@Slf4j
@AllArgsConstructor
@Controller("admin.graphController")
@RequestMapping(value = "#{config.pathAdmin}")
public class GraphController {

    private PaymentListService paymentListService;

    /**
     * 그래프
     */
    @RequestMapping(value = "zone/graph/config", method = RequestMethod.GET)
    public String config(ModelMap map, PaymentDto.Command command, @PageableDefault(size = 30, direction = Sort.Direction.DESC, sort = "regDate") Pageable pageable) {
        command.setCode("그래프");
        map.addAttribute("page", paymentListService.moneyPage(command, pageable));
        return "admin/zone/graph/config";
    }

    @RequestMapping(value = "graph/userInfo/{userid}", method = RequestMethod.GET)
    public String userInfo(ModelMap map, @PathVariable("userid") String userid) {
        map.addAttribute("userid", userid);
        return "admin/zone/graph/popup/user";
    }

    @RequestMapping(value = "graph/total", method = RequestMethod.GET)
    public String total(ModelMap map) {
        return "admin/zone/graph/popup/total";
    }

    @RequestMapping(value = "graph/account", method = RequestMethod.GET)
    public String acount(ModelMap map) {
        return "admin/zone/graph/popup/accounting";
    }

}
