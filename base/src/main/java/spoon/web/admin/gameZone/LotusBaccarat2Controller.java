package spoon.web.admin.gameZone;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import spoon.config.domain.Config;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.lotusBaccarat2.LotusBaccarat2Config;
import spoon.gameZone.lotusBaccarat2.LotusBaccarat2Dto;
import spoon.gameZone.lotusBaccarat2.service.LotusBaccarat2Service;
import spoon.support.web.AjaxResult;

@Slf4j
@AllArgsConstructor
@Controller("admin.lotusBaccarat2Controller")
@RequestMapping(value = "#{config.pathAdmin}")
public class LotusBaccarat2Controller {

    private LotusBaccarat2Service lotusBaccarat2Service;

    /**
     * 바카라 설정
     */
    @RequestMapping(value = "zone/lotusBaccarat2/config", method = RequestMethod.GET)
    public String config(ModelMap map) {
        map.addAttribute("config", ZoneConfig.getLotusBaccarat2());
        return "admin/zone/lotusBaccarat2/config";
    }

    /**
     * 바카라 설정 변경
     */
    @RequestMapping(value = "zone/lotusBaccarat2/config", method = RequestMethod.POST)
    public String config(LotusBaccarat2Config lotusBaccarat2Config, RedirectAttributes ra) {

        //pair 게임 lose 배당은 무조건 0으로 셋팅해준다.
        double[] odds = lotusBaccarat2Config.getOdds();
        odds[4] = 0;
        odds[6] = 0;
        lotusBaccarat2Config.setOdds(odds);

        boolean success = lotusBaccarat2Service.updateConfig(lotusBaccarat2Config);
        if (success) {
            ra.addFlashAttribute("message", "바카라 설정을 변경하였습니다.");
        } else {
            ra.addFlashAttribute("message", "바카라 설정변경에 실패하였습니다.");
        }
        return "redirect:" + Config.getPathAdmin() + "/zone/lotusBaccarat2/config";
    }

    /**
     * 바카라 진행
     */
    @RequestMapping(value = "zone/lotusBaccarat2/complete", method = RequestMethod.GET)
    public String complete(ModelMap map) {
        map.addAttribute("list", lotusBaccarat2Service.getComplete());
        map.addAttribute("config", ZoneConfig.getLotusBaccarat2());
        return "admin/zone/lotusBaccarat2/complete";
    }

    /**
     * 바카라 완료
     */
    @RequestMapping(value = "zone/lotusBaccarat2/closing", method = RequestMethod.GET)
    public String closing(ModelMap map, @ModelAttribute("command") ZoneDto.Command command,
                          @PageableDefault(size = 20, direction = Sort.Direction.DESC, sort = "gameDate") Pageable pageable) {
        map.addAttribute("page", lotusBaccarat2Service.getClosing(command, pageable));
        map.addAttribute("config", ZoneConfig.getLotusBaccarat2());
        return "admin/zone/lotusBaccarat2/closing";
    }

    /**
     * 바카라 스코어 입력 폼
     */
    @RequestMapping(value = "zone/lotusBaccarat2/score", method = RequestMethod.GET)
    public String score(ModelMap map, Long id) {
        map.addAttribute("score", lotusBaccarat2Service.findScore(id));
        return "admin/zone/lotusBaccarat2/score";
    }

    /**
     * 바카라 결과처리
     */
    @RequestMapping(value = "zone/lotusBaccarat2/score", method = RequestMethod.POST)
    public String score(LotusBaccarat2Dto.Score score, RedirectAttributes ra) {
        boolean success = lotusBaccarat2Service.closingGame(score);

        if (success) {
            ra.addFlashAttribute("message", "바카라 결과처리를 완료 하였습니다.");
        } else {
            ra.addFlashAttribute("message", "바카라 결과처리에 실패 하였습니다.");
        }
        ra.addFlashAttribute("popup", "closing");

        return "redirect:" + Config.getPathAdmin() + "/zone/lotusBaccarat2/score?id=" + score.getId();
    }

    /**
     * 바카라 베팅 없는경기 모두 결과처리
     */
    @ResponseBody
    @RequestMapping(value = "zone/lotusBaccarat2/closing", method = RequestMethod.POST)
    public AjaxResult closing() {
        return lotusBaccarat2Service.closingAllGame();
    }

    /**
     * 바카라 베팅 없는경기 모두 삭제처리
     */
    @ResponseBody
    @RequestMapping(value = "zone/lotusBaccarat2/delete", method = RequestMethod.POST)
    public AjaxResult delete() {
        return lotusBaccarat2Service.deleteAllGame();
    }

}
