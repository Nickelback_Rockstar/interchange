package spoon.web.admin.gameZone.zoneDelete;

import spoon.support.web.AjaxResult;

public interface ZoneDeleteService {
    /**
     * 미처리된 경기 삭제처리
     */
    AjaxResult noBetDelete();

}
