package spoon.web.admin.gameZone;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import spoon.config.domain.Config;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.eosPower1.EosPower1Config;
import spoon.gameZone.eosPower1.EosPower1Dto;
import spoon.gameZone.eosPower1.service.EosPower1Service;
import spoon.support.web.AjaxResult;

@Slf4j
@AllArgsConstructor
@Controller("admin.eosPower1Controller")
@RequestMapping(value = "#{config.pathAdmin}")
public class EosPower1Controller {

    private EosPower1Service eosPower1Service;

    /**
     * 파워볼 설정
     */
    @RequestMapping(value = "zone/eosPower1/config", method = RequestMethod.GET)
    public String config(ModelMap map) {
        map.addAttribute("config", ZoneConfig.getEosPower1());
        return "admin/zone/eosPower1/config";
    }

    /**
     * 파워볼 설정 변경
     */
    @RequestMapping(value = "zone/eosPower1/config", method = RequestMethod.POST)
    public String config(EosPower1Config powerConfig, RedirectAttributes ra) {
        boolean success = eosPower1Service.updateConfig(powerConfig);
        if (success) {
            ra.addFlashAttribute("message", "EOS파워볼 1분 설정을 변경하였습니다.");
        } else {
            ra.addFlashAttribute("message", "EOS파워볼 1분 설정변경에 실패하였습니다.");
        }

        return "redirect:" + Config.getPathAdmin() + "/zone/eosPower1/config";
    }

    /**
     * 파워볼 진행
     */
    @RequestMapping(value = "zone/eosPower1/complete", method = RequestMethod.GET)
    public String complete(ModelMap map) {
        map.addAttribute("list", eosPower1Service.getComplete());
        map.addAttribute("config", ZoneConfig.getEosPower1());
        return "admin/zone/eosPower1/complete";
    }

    /**
     * 파워볼 완료
     */
    @RequestMapping(value = "zone/eosPower1/closing", method = RequestMethod.GET)
    public String closing(ModelMap map, @ModelAttribute("command") ZoneDto.Command command,
                          @PageableDefault(size = 20, direction = Sort.Direction.DESC, sort = "gameDate") Pageable pageable) {
        map.addAttribute("page", eosPower1Service.getClosing(command, pageable));
        map.addAttribute("config", ZoneConfig.getEosPower1());
        return "admin/zone/eosPower1/closing";
    }

    /**
     * 파워볼 스코어 입력 폼
     */
    @RequestMapping(value = "zone/eosPower1/score", method = RequestMethod.GET)
    public String score(ModelMap map, Long id) {
        map.addAttribute("score", eosPower1Service.findScore(id));
        return "admin/zone/eosPower1/score";
    }

    /**
     * 파워볼 결과처리
     */
    @RequestMapping(value = "zone/eosPower1/score", method = RequestMethod.POST)
    public String score(EosPower1Dto.Score score, RedirectAttributes ra) {
        boolean success = eosPower1Service.closingGame(score);

        if (success) {
            ra.addFlashAttribute("message", "EOS파워볼 1분 결과처리를 완료 하였습니다.");
        } else {
            ra.addFlashAttribute("message", "EOS파워볼 1분  결과처리에 실패 하였습니다.");
        }
        ra.addFlashAttribute("popup", "closing");

        return "redirect:" + Config.getPathAdmin() + "/zone/eosPower1/score?id=" + score.getId();
    }

    /**
     * 파워볼 베팅 없는경기 모두 결과처리
     */
    @ResponseBody
    @RequestMapping(value = "zone/eosPower1/closing", method = RequestMethod.POST)
    public AjaxResult closing() {
        return eosPower1Service.closingAllGame();
    }

}
