package spoon.web.admin.gameZone;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import spoon.config.domain.Config;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.boglePower.BoglePowerConfig;
import spoon.gameZone.boglePower.BoglePowerDto;
import spoon.gameZone.boglePower.service.BoglePowerService;
import spoon.support.web.AjaxResult;

@Slf4j
@AllArgsConstructor
@Controller("admin.boglePowerController")
@RequestMapping(value = "#{config.pathAdmin}")
public class BoglePowerController {

    private BoglePowerService boglePowerService;

    /**
     * 보글파워볼 설정
     */
    @RequestMapping(value = "zone/boglePower/config", method = RequestMethod.GET)
    public String config(ModelMap map) {
        map.addAttribute("config", ZoneConfig.getBoglePower());
        return "admin/zone/boglePower/config";
    }

    /**
     * 보글파워볼 설정 변경
     */
    @RequestMapping(value = "zone/boglePower/config", method = RequestMethod.POST)
    public String config(BoglePowerConfig boglePowerConfig, RedirectAttributes ra) {
        boolean success = boglePowerService.updateConfig(boglePowerConfig);
        if (success) {
            ra.addFlashAttribute("message", "보글파워볼 게임설정을 변경하였습니다.");
        } else {
            ra.addFlashAttribute("message", "보글파워볼 게임설정변경에 실패하였습니다.");
        }
        return "redirect:" + Config.getPathAdmin() + "/zone/boglePower/config";
    }

    /**
     * 보글파워볼 진행
     */
    @RequestMapping(value = "zone/boglePower/complete", method = RequestMethod.GET)
    public String complete(ModelMap map) {
        map.addAttribute("list", boglePowerService.getComplete());
        map.addAttribute("config", ZoneConfig.getBoglePower());
        return "admin/zone/boglePower/complete";
    }

    /**
     * 보글파워볼 완료
     */
    @RequestMapping(value = "zone/boglePower/closing", method = RequestMethod.GET)
    public String closing(ModelMap map, @ModelAttribute("command") ZoneDto.Command command,
                          @PageableDefault(size = 20, direction = Sort.Direction.DESC, sort = "sdate") Pageable pageable) {
        map.addAttribute("page", boglePowerService.getClosing(command, pageable));
        map.addAttribute("config", ZoneConfig.getBoglePower());
        return "admin/zone/boglePower/closing";
    }

    /**
     * 보글파워볼 스코어 입력 폼
     */
    @RequestMapping(value = "zone/boglePower/score", method = RequestMethod.GET)
    public String score(ModelMap map, Long id) {
        map.addAttribute("score", boglePowerService.findScore(id));
        return "admin/zone/boglePower/score";
    }

    /**
     * 보글파워볼 스코어 결과처리
     */
    @RequestMapping(value = "zone/boglePower/score", method = RequestMethod.POST)
    public String score(BoglePowerDto.Score score, RedirectAttributes ra) {
        boolean success = boglePowerService.closingGame(score);

        if (success) {
            ra.addFlashAttribute("message", "보글파워볼 결과처리를 완료 하였습니다.");
        } else {
            ra.addFlashAttribute("message", "보글파워볼 결과처리에 실패 하였습니다.");
        }
        ra.addFlashAttribute("popup", "closing");

        return "redirect:" + Config.getPathAdmin() + "/zone/boglePower/score?id=" + score.getId();
    }

    /**
     * 보글파워볼 베팅 없는경기 모두 결과처리
     */
    @ResponseBody
    @RequestMapping(value = "zone/boglePower/closing", method = RequestMethod.POST)
    public AjaxResult closing() {
        return boglePowerService.closingAllGame();
    }

}
