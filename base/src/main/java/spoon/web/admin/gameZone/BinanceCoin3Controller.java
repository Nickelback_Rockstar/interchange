package spoon.web.admin.gameZone;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import spoon.config.domain.Config;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.binanceCoin3.BinanceCoin3Config;
import spoon.gameZone.binanceCoin3.BinanceCoin3Dto;
import spoon.gameZone.binanceCoin3.service.BinanceCoin3Service;
import spoon.support.web.AjaxResult;

@Slf4j
@AllArgsConstructor
@Controller("admin.binanceCoin3Controller")
@RequestMapping(value = "#{config.pathAdmin}")
public class BinanceCoin3Controller {

    private BinanceCoin3Service binanceCoin3Service;

    /**
     * 바이낸스코인3 설정
     */
    @RequestMapping(value = "zone/binanceCoin3/config", method = RequestMethod.GET)
    public String config(ModelMap map) {
        map.addAttribute("config", ZoneConfig.getBinanceCoin3());
        return "admin/zone/binanceCoin3/config";
    }

    /**
     * 바이낸스코인3 설정 변경
     */
    @RequestMapping(value = "zone/binanceCoin3/config", method = RequestMethod.POST)
    public String config(BinanceCoin3Config binanceCoin3Config, RedirectAttributes ra) {
        boolean success = binanceCoin3Service.updateConfig(binanceCoin3Config);
        if (success) {
            ra.addFlashAttribute("message", "비트코인3 게임설정을 변경하였습니다.");
        } else {
            ra.addFlashAttribute("message", "비트코인3 게임설정 변경에 실패하였습니다.");
        }
        return "redirect:" + Config.getPathAdmin() + "/zone/binanceCoin3/config";
    }

    /**
     * 바이낸스코인3 진행
     */
    @RequestMapping(value = "zone/binanceCoin3/complete", method = RequestMethod.GET)
    public String complete(ModelMap map) {
        map.addAttribute("list", binanceCoin3Service.getComplete());
        map.addAttribute("config", ZoneConfig.getBinanceCoin3());
        return "admin/zone/binanceCoin3/complete";
    }

    /**
     * 바이낸스코인3 완료
     */
    @RequestMapping(value = "zone/binanceCoin3/closing", method = RequestMethod.GET)
    public String closing(ModelMap map, @ModelAttribute("command") ZoneDto.Command command,
                          @PageableDefault(size = 20, direction = Sort.Direction.DESC, sort = "gameDate") Pageable pageable) {
        map.addAttribute("page", binanceCoin3Service.getClosing(command, pageable));
        map.addAttribute("config", ZoneConfig.getBinanceCoin3());
        return "admin/zone/binanceCoin3/closing";
    }

    /**
     * 바이낸스코인3 스코어 입력 폼
     */
    @RequestMapping(value = "zone/binanceCoin3/score", method = RequestMethod.GET)
    public String score(ModelMap map, Long id) {
        map.addAttribute("score", binanceCoin3Service.findScore(id));
        return "admin/zone/binanceCoin3/score";
    }

    /**
     * 바이낸스코인3 결과처리
     */
    @RequestMapping(value = "zone/binanceCoin3/score", method = RequestMethod.POST)
    public String score(BinanceCoin3Dto.Score score, RedirectAttributes ra) {
        boolean success = binanceCoin3Service.closingGame(score);

        if (success) {
            ra.addFlashAttribute("message", "비트코인3 결과처리를 완료 하였습니다.");
        } else {
            ra.addFlashAttribute("message", "비트코인3 결과처리에 실패 하였습니다.");
        }
        ra.addFlashAttribute("popup", "closing");

        return "redirect:" + Config.getPathAdmin() + "/zone/binanceCoin3/score?id=" + score.getId();
    }

    /**
     * 바이낸스코인3 베팅 없는경기 모두 결과처리
     */
    @ResponseBody
    @RequestMapping(value = "zone/binanceCoin3/closing", method = RequestMethod.POST)
    public AjaxResult closing() {
        return binanceCoin3Service.closingAllGame();
    }

}
