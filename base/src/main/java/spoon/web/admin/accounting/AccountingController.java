package spoon.web.admin.accounting;

import lombok.AllArgsConstructor;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import spoon.accounting.domain.AccountingDto;
import spoon.accounting.service.AccountingService;
import spoon.common.net.HttpParsing;
import spoon.common.utils.JsonUtils;
import spoon.common.utils.StringUtils;
import spoon.config.domain.Config;
import spoon.member.service.MemberService;

@AllArgsConstructor
@Controller("admin.accountingController")
@RequestMapping("#{config.pathAdmin}")
public class AccountingController {

    private AccountingService accountingService;

    private MemberService memberService;

    @RequestMapping(value = "accounting/daily", method = RequestMethod.GET)
    public String daily(ModelMap map, @ModelAttribute("command") AccountingDto.Command command) {

        if(command != null && StringUtils.notEmpty(command.getAgency())) {
            String[] agency = command.getAgency().split("##");
            if (agency != null && agency.length == 2) {
                command.setAgency(agency[0]);
            }
        }
        map.addAttribute("list", accountingService.daily(command));
        map.addAttribute("agencies", memberService.getAgencyList());
        return "admin/accounting/daily";
    }

    @RequestMapping(value = "accounting/detail", method = RequestMethod.GET)
    public String detail(ModelMap map, @ModelAttribute("command") AccountingDto.Command command) {
        if(command != null && StringUtils.notEmpty(command.getAgency())) {
            String[] agency = command.getAgency().split("##");
            if (agency != null && agency.length == 2) {
                command.setAgency(agency[0]);
            }
        }

        map.addAttribute("list", accountingService.gameAccount(command));
        map.addAttribute("accountCasino", accountingService.gameAccountCasino(command));
        map.addAttribute("amount", accountingService.amount());
        map.addAttribute("money", accountingService.money(command));
        map.addAttribute("point", accountingService.point(command));
        map.addAttribute("board", accountingService.board(command));
        map.addAttribute("comment", accountingService.comment(command));
        map.addAttribute("fees", accountingService.fees(command));
        map.addAttribute("agencies", memberService.getAgencyList());

        return "admin/accounting/detail";
    }

    @RequestMapping(value = "accounting/dailyPbLotto", method = RequestMethod.GET)
    public String dailyPbLotto(ModelMap map, @ModelAttribute("command") AccountingDto.Command command) {

        if(command != null && StringUtils.notEmpty(command.getAgency())) {
            String[] agency = command.getAgency().split("##");
            if (agency != null && agency.length == 2) {
                command.setAgency(agency[0]);
            }
        }

        long agencyPoint = 0;
        long usersMoney = 0;
        String param = "?code="+ Config.getSysConfig().getZone().getPowerLottoCode();
        String json = HttpParsing.getJson(Config.getSysConfig().getZone().getPowerLottoUrl()+"/ap/usersMoney" + param);
        JSONParser parser = new JSONParser();
        try {
            JSONObject jsonObj = (JSONObject) parser.parse(json);
            if("00".equals(jsonObj.get("resultCode").toString())){
                agencyPoint = (long)jsonObj.get("agencyPoint");
                usersMoney = (long)jsonObj.get("money");
            }
        } catch (ParseException e) {}


        map.addAttribute("agencyPoint", agencyPoint);
        map.addAttribute("usersMoney", usersMoney);
        map.addAttribute("list", accountingService.dailyPbLotto(command));
        map.addAttribute("agencies", memberService.getAgencyList());
        return "admin/accounting/dailyPbLotto";
    }
}
