package spoon.web.admin.accounting.casino;

import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import spoon.casino.domain.BetCasinoDto;
import spoon.casino.service.CasinoListService;

import java.util.List;

@AllArgsConstructor
@Controller("admin.CasinoController")
@RequestMapping("#{config.pathAdmin}")
public class CasinoController {

    private CasinoListService casinoListService;

    /**
     * 카지노 베팅 리스트
     */
    @RequestMapping(value = "/casino/list", method = RequestMethod.GET)
    public String list(ModelMap map, BetCasinoDto.Command command,
            @PageableDefault(size = 20) Pageable pageable) {

        command.setSize(pageable.getPageSize());

//        System.out.println("command="+command.toString());

        List<BetCasinoDto.BetList> list = casinoListService.casinoBetList(command);

//        System.out.println(list.toString());

        Page<BetCasinoDto.BetList> page = new PageImpl<>(list, pageable, casinoListService.casinoBetListTotal(command));

        map.addAttribute("page", page);
        map.addAttribute("command", command);

        return "admin/casino/list";
    }

}
