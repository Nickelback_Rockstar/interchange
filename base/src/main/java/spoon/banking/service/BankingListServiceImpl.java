package spoon.banking.service;

import com.querydsl.core.BooleanBuilder;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import spoon.banking.domain.BankingCode;
import spoon.banking.domain.BankingDto;
import spoon.banking.entity.Banking;
import spoon.banking.entity.QBanking;
import spoon.banking.repository.BankingRepository;
import spoon.common.utils.DateUtils;
import spoon.common.utils.StringUtils;
import spoon.member.entity.Member;
import spoon.member.service.MemberService;

@Slf4j
@AllArgsConstructor
@Service
public class BankingListServiceImpl implements BankingListService {

    private BankingRepository bankingRepository;

    private static QBanking q = QBanking.banking;

    private MemberService memberService;

    @Transactional(readOnly = true)
    @Override
    public Iterable<Banking> list(String userid, BankingCode bankingCode) {
        if (userid == null) return null;
        return bankingRepository.findAll(q.userid.eq(userid).and(q.bankingCode.eq(bankingCode)).and(q.hidden.isFalse()), new Sort(Sort.Direction.DESC, "regDate"));
    }

    @Transactional(readOnly = true)
    @Override
    public Page<Banking> page(BankingDto.Command command, Pageable pageable) {
        BooleanBuilder builder = new BooleanBuilder();

        //카지노 충환 포함 수정
        if(command.getBankingCode() == BankingCode.IN) {
            builder = new BooleanBuilder(q.bankingCode.eq(BankingCode.IN).or(q.bankingCode.eq(BankingCode.CASINO_IN)));
        }else if(command.getBankingCode() == BankingCode.OUT){
            builder = new BooleanBuilder(q.bankingCode.eq(BankingCode.OUT).or(q.bankingCode.eq(BankingCode.CASINO_OUT)));
        }

        if (command.isClosing()) {
            builder.and(q.closing.isTrue());
        } else {
            builder.and(q.closing.isFalse());
        }

        if (StringUtils.notEmpty(command.getDate())) {
            builder.and(q.regDate.goe(DateUtils.start(command.getDate())).and(q.regDate.lt(DateUtils.end(command.getDate()))));
        }

        if (StringUtils.notEmpty(command.getUsername())) {
            if (command.isMatch()) {
                builder.and(q.userid.eq(command.getUsername()).or(q.nickname.eq(command.getUsername())));
            } else {
                builder.and(q.userid.like("%" + command.getUsername() + "%").or(q.nickname.like("%" + command.getUsername() + "%")));
            }
        }

        if (StringUtils.notEmpty(command.getDepositor())) {
            builder.and(q.depositor.eq(command.getDepositor()));
        }



        return bankingRepository.findAll(builder, pageable);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<Banking> bankingPage(String userid, Pageable pageable) {
        return bankingRepository.findAll(q.userid.eq(userid), pageable);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<Banking> bankingPage(BankingDto.Seller command, Pageable pageable) {
        BooleanBuilder builder = new BooleanBuilder(q.closing.isTrue().and(q.cancel.isFalse()));

        if (StringUtils.notEmpty(command.getBankingCode())) {
            builder.and(q.bankingCode.eq(BankingCode.valueOf(command.getBankingCode())));
        }

        String agency = "";
        int agencyDepth = 0;
        if (StringUtils.notEmpty(command.getAgency())) {
            Member member = memberService.getMember(command.getAgency());
            if(member != null){
                agency = member.getUserid();
                agencyDepth = member.getAgencyDepth();
            }
        }

        if (StringUtils.notEmpty(agency) && agencyDepth > 0) {
            if(agencyDepth == 1) {
                builder.and(q.agency1.eq(agency));
            }else if(agencyDepth == 2) {
                builder.and(q.agency2.eq(agency));
            }else if(agencyDepth == 3) {
                builder.and(q.agency3.eq(agency));
            }else if(agencyDepth == 4) {
                builder.and(q.agency4.eq(agency));
            }else if(agencyDepth == 5) {
                builder.and(q.agency5.eq(agency));
            }else if(agencyDepth == 6) {
                builder.and(q.agency6.eq(agency));
            }else if(agencyDepth == 7) {
                builder.and(q.agency7.eq(agency));
            }
        }else {
            if (StringUtils.notEmpty(command.getAgency1())) {
                builder.and(q.agency1.eq(command.getAgency1()));
            }

            if (StringUtils.notEmpty(command.getAgency2())) {
                builder.and(q.agency2.eq(command.getAgency2()));
            }

            if (StringUtils.notEmpty(command.getAgency3())) {
                builder.and(q.agency3.eq(command.getAgency3()));
            }

            if (StringUtils.notEmpty(command.getAgency4())) {
                builder.and(q.agency4.eq(command.getAgency4()));
            }

            if (StringUtils.notEmpty(command.getAgency5())) {
                builder.and(q.agency5.eq(command.getAgency5()));
            }

            if (StringUtils.notEmpty(command.getAgency6())) {
                builder.and(q.agency6.eq(command.getAgency6()));
            }

            if (StringUtils.notEmpty(command.getAgency7())) {
                builder.and(q.agency7.eq(command.getAgency7()));
            }
        }

        if (StringUtils.notEmpty(command.getUsername())) {
            if (command.isMatch()) {
                builder.and(q.userid.eq(command.getUsername()).or(q.nickname.eq(command.getUsername())));
            } else {
                builder.and(q.userid.like("%" + command.getUsername() + "%").or(q.nickname.like("%" + command.getUsername() + "%")));
            }
        }

        return bankingRepository.findAll(builder, pageable);
    }
}
