package spoon.banking.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionInterceptor;
import spoon.banking.domain.BankingCode;
import spoon.banking.domain.DepositDto;
import spoon.banking.domain.Rolling;
import spoon.banking.domain.WithdrawDto;
import spoon.banking.entity.Banking;
import spoon.banking.entity.QBanking;
import spoon.banking.repository.BankingRepository;
import spoon.common.utils.*;
import spoon.config.domain.Config;
import spoon.event.service.DailyEventService;
import spoon.event.service.LottoService;
import spoon.member.entity.Member;
import spoon.member.service.MemberService;
import spoon.payment.domain.MoneyCode;
import spoon.payment.domain.PaymentDto;
import spoon.payment.domain.PointCode;
import spoon.payment.service.PaymentService;
import spoon.support.web.AjaxResult;

import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class BankingServiceImpl implements BankingService {

    private MemberService memberService;

    private PaymentService paymentService;

    private DailyEventService dailyEventService;

    private LottoService lottoService;

    private BankingRepository bankingRepository;

    private static QBanking q = QBanking.banking;

    @Transactional(readOnly = true)
    @Override
    public Banking getBanking(long id) {
        return bankingRepository.findOne(id);
    }

    @Transactional(readOnly = true)
    @Override
    public boolean existCasinoBanking(String userid) {
        return bankingRepository.count(q.userid.eq(userid).and(
                q.bankingCode.eq(BankingCode.CASINO_IN).or(q.bankingCode.eq(BankingCode.CASINO_OUT))
        ).and(q.cancel.isFalse()).and(q.closing.isFalse())) > 0;
    }

    @Transactional(readOnly = true)
    @Override
    public boolean existWorkBanking(String userid) {
        return bankingRepository.count(q.userid.eq(userid).and(
                q.bankingCode.eq(BankingCode.IN).or(q.bankingCode.eq(BankingCode.OUT))
        ).and(q.cancel.isFalse()).and(q.closing.isFalse())) > 0;
    }

    @Transactional
    @Override
    public boolean addDeposit(Banking banking) {
        try {
            bankingRepository.saveAndFlush(banking);
            return true;
        } catch (RuntimeException e) {
            log.error("충전신청을 저장하지 못하였습니다. - {}", e.getMessage());
            log.warn("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
    }

    @Transactional
    @Override
    public boolean cancelDeposit(long id) {
        try {
            Banking banking = getBanking(id);
            if (banking.isClosing() || banking.isCancel()) {
                throw new RuntimeException();
            }
            banking.setCancel(true);
            banking.setClosing(true);
            banking.setClosingDate(new Date());
            bankingRepository.saveAndFlush(banking);
            return true;
        } catch (RuntimeException e) {
            log.error("충전신청 취소를 하지 못하였습니다. - {}", e.getMessage());
            log.warn("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
    }

    @Transactional
    @Override
    public boolean submitDeposit(long id) {
        try {
            Banking banking = getBanking(id);

            if (banking.isCancel() || banking.isClosing() || banking.isReset()) {
                throw new BankingException("이미 취소 되었거나 완료처리가 되었습니다.");
            }

            if(banking.getBankingCode() == BankingCode.CASINO_IN){
                banking.setClosing(true);
                banking.setClosingDate(new Date());
                bankingRepository.saveAndFlush(banking);
                DepositDto.Add add = new DepositDto.Add();
                add.setAmount(banking.getAmount());
                add.setUserid(banking.getUserid());
                AjaxResult ajax = depositCasino(add);
                if(ajax.isSuccess()){
                    String url = CasinoUtils.API_URL + "?mode=addMemberPoint&amount="+banking.getAmount()+"&operatorID=" + CasinoUtils.API_ID + "&transactionID=" + ("AP" + new Date().getTime()) + "&userID=" + CasinoUtils.getConvertUserid(banking.getUserid()) + "&vendorID=0";
                    System.out.println("url = " + url);
                    JSONObject result = JsonUtils.getJson(url);
                    System.out.println("result = " + result.toString());
                    if(!"0".equals(result.get("returnCode").toString())) {
                        throw new BankingException("카지노 충전 처리가 되지 않았습니다.[1]");
                    }
                }else{
                    throw new BankingException("카지노 충전 처리가 되지 않았습니다.[2]");
                }
            }else {
                banking.setClosing(true);
                banking.setClosingDate(new Date());
                banking.setWorker(WebUtils.userid());
                bankingRepository.saveAndFlush(banking);

                paymentService.addMoney(MoneyCode.DEPOSIT, id, banking.getUserid(), banking.getAmount(), banking.getBonus());
                if (banking.getBonusPoint() > 0) {
                    paymentService.addPoint(PointCode.DEPOSIT, id, banking.getUserid(), banking.getBonusPoint(), banking.getBonus());
                }

                // 출석체크 이벤트
                if (Config.getSysConfig().getEvent().isDaily()) {
                    dailyEventService.checkEvent(banking.getUserid());
                }
            }

            return true;
        } catch (RuntimeException e) {
            log.error("충전신청 완료 처리를 하지 못하였습니다. - {}", e.getMessage());
            log.warn("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
    }

    @Transactional
    @Override
    public AjaxResult rollbackDeposit(long id) {
        try {
            Banking banking = getBanking(id);

            if (banking.isCancel()) {
                throw new BankingException("이미 취소된 충전신청 입니다.");
            }

            if (!banking.isClosing()) { // 클로징이 안 되었다는 것은 입금 처리가 되지 않았다는 것이다.
                throw new BankingException("입금 완료 처리가 되지 않았습니다.");
            }

            if (banking.isReset()) {
                throw new BankingException("이미 충전취소 - 포인트 복원된 충전요청 입니다.");
            }

            if(banking.getBankingCode() == BankingCode.CASINO_IN){
                String url1 = CasinoUtils.API_URL + "?mode=collectAccountGameBalanceAll&operatorID=" + CasinoUtils.API_ID + "&userID=" + CasinoUtils.getConvertUserid(banking.getUserid()) + "&vendorID=0";
                System.out.println("url1 = " + url1);
                JSONObject result1 = JsonUtils.getJson(url1);
                System.out.println(result1.toString());

                if("0".equals(result1.get("returnCode").toString()) || "1".equals(result1.get("returnCode").toString())){
                    //유저 -> 관리자에게 포인트 이동
                    String url2 = CasinoUtils.API_URL + "?mode=subtractMemberPoint&amount="+banking.getAmount()+"&operatorID=" + CasinoUtils.API_ID + "&transactionID=" + ("SP" + new Date().getTime()) + "&userID=" + CasinoUtils.getConvertUserid(banking.getUserid()) + "&vendorID=0";
                    System.out.println("url2 = " + url2);
                    JSONObject result2 = JsonUtils.getJson(url2);
                    System.out.println(result2.toString());

                    WithdrawDto.Add add = new WithdrawDto.Add();
                    add.setAmount(banking.getAmount());
                    add.setUserid(banking.getUserid());
                    add.setCode("CANCEL");
                    AjaxResult ajax = withdrawCasino(add); //돈넣어줌

                    if(!"0".equals(result2.get("returnCode").toString())){
                        TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
                        return new AjaxResult(false, "카지노 메세지2 : "+result2.get("description").toString());
                    }else{
                        banking.setCancel(true);
                        banking.setReset(true);
                        banking.setClosingDate(new Date());
                        bankingRepository.saveAndFlush(banking);
                    }
                }else{
                    TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
                    return new AjaxResult(false, "카지노 메세지1 : "+result1.get("description").toString());
                }
            }else {

                banking.setCancel(true);
                banking.setReset(true);
                banking.setClosingDate(new Date());
                bankingRepository.saveAndFlush(banking);

                paymentService.addMoney(MoneyCode.DEPOSIT_ROLLBACK, id, banking.getUserid(), -banking.getAmount(), banking.getBonus());
                if (banking.getBonusPoint() > 0) {
                    paymentService.addPoint(PointCode.DEPOSIT_ROLLBACK, id, banking.getUserid(), -banking.getBonusPoint(), banking.getBonus());
                }
            }
            return new AjaxResult(true, "충전취소 - 포인트 복원을 완료 하였습니다.");
        } catch (RuntimeException e) {
            log.error("충전취소를 롤백하지 못하였습니다. - {}", e.getMessage());
            log.warn("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return new AjaxResult(false, "현재 요청을 수행 할 수 없습니다.[0]");
        }
    }

    @Transactional
    @Override
    public boolean addWithdraw(Banking banking) {
        try {
            bankingRepository.saveAndFlush(banking);

            //은행 환전만 머니 빼줌
            if(banking.getBankingCode() != BankingCode.CASINO_OUT) {
                paymentService.addMoney(MoneyCode.WITHDRAW, banking.getId(), banking.getUserid(), -banking.getAmount(), getRolling(banking.getRolling()));
            }
            return true;
        } catch (RuntimeException e) {
            log.error("환전 신청을 하지 못하였습니다. - {}", e.getMessage());
            log.warn("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
    }

    @Transactional
    @Override
    public boolean addSellerWithdraw(Banking banking) {
        try {
            bankingRepository.saveAndFlush(banking);
            paymentService.addPoint(PointCode.WITHDRAW, banking.getId(), banking.getUserid(), -banking.getAmount(), banking.getRole().getName() + " 출금 신청");
            return true;
        } catch (RuntimeException e) {
            log.error("총판 출금 신청을 하지 못하였습니다. - {}", e.getMessage());
            log.warn("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
    }

    @Transactional
    @Override
    public AjaxResult submitWithdraw(Banking banking) {
        try {
            if (banking.isCancel() || banking.isClosing() || banking.isReset()) {
                throw new BankingException("이미 취소 되었거나 완료처리가 되었습니다.");
            }

            if(banking.getBankingCode() == BankingCode.CASINO_OUT){

                String url1 = CasinoUtils.API_URL + "?mode=collectAccountGameBalanceAll&operatorID=" + CasinoUtils.API_ID + "&userID=" + CasinoUtils.getConvertUserid(banking.getUserid()) + "&vendorID=0";
                System.out.println("url1 = " + url1);
                JSONObject result1 = JsonUtils.getJson(url1);
                System.out.println(result1.toString());

                if("0".equals(result1.get("returnCode").toString()) || "1".equals(result1.get("returnCode").toString())){
                    //유저 -> 관리자에게 포인트 이동
                    String url2 = CasinoUtils.API_URL + "?mode=subtractMemberPoint&amount="+banking.getAmount()+"&operatorID=" + CasinoUtils.API_ID + "&transactionID=" + ("SP" + new Date().getTime()) + "&userID=" + CasinoUtils.getConvertUserid(banking.getUserid()) + "&vendorID=0";
                    System.out.println("url2 = " + url2);
                    JSONObject result2 = JsonUtils.getJson(url2);
                    System.out.println(result2.toString());

                    WithdrawDto.Add add = new WithdrawDto.Add();
                    add.setAmount(banking.getAmount());
                    add.setUserid(banking.getUserid());
                    AjaxResult ajax = withdrawCasino(add);

                    if(!"0".equals(result2.get("returnCode").toString())){
                        TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
                        return new AjaxResult(false, "카지노 메세지2 : "+result2.get("description").toString());
                    }else{
                        banking.setWorker(WebUtils.userid());
                        banking.setClosing(true);
                        banking.setClosingDate(new Date());
                        bankingRepository.saveAndFlush(banking);
                    }
                }else{
                    TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
                    return new AjaxResult(false, "카지노 메세지1 : "+result1.get("description").toString());
                }
            }else {
                banking.setWorker(WebUtils.userid());
                banking.setClosing(true);
                banking.setClosingDate(new Date());
                bankingRepository.saveAndFlush(banking);
            }

            return new AjaxResult(true, "환전신청을 완료 하였습니다.");
        } catch (RuntimeException e) {
            log.error("환전신청 완료 처리를 하지 못하였습니다. - {}", e.getMessage());
            log.warn("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return new AjaxResult(false, "현재 요청을 수행 할 수 없습니다.");
        }
    }

    @Override
    public AjaxResult rollbackWithdraw(long id) {
        try {
            Banking banking = getBanking(id);

            if (banking.isCancel()) {
                throw new BankingException("이미 취소된 환전신청 입니다.");
            }

            if (banking.isReset()) {
                throw new BankingException("이미 취소된 환전신청 입니다.");
            }

            banking.setCancel(true);
            banking.setReset(true);
            banking.setClosing(true);
            banking.setClosingDate(new Date());
            bankingRepository.saveAndFlush(banking);

            if(banking.getBankingCode() == BankingCode.CASINO_OUT) {
                return new AjaxResult(true, "카지노 환전 취소가 완료되었습니다.");
            }else{
                paymentService.addMoney(MoneyCode.WITHDRAW_ROLLBACK, id, banking.getUserid(), banking.getAmount(), getRolling(banking.getRolling()));
                return new AjaxResult(true, "환전취소 - 포인트 복원을 완료 하였습니다.");
            }
        } catch (RuntimeException e) {
            log.error("환전신청 취소 (롤백) 처리를 하지 못하였습니다. - {}", e.getMessage());
            log.warn("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return new AjaxResult(false, "현재 요청을 수행 할 수 없습니다.");
        }
    }


    @Transactional
    @Override
    public boolean delete(long id) {
        try {
            Banking banking = getBanking(id);
            banking.setHidden(true);
            bankingRepository.saveAndFlush(banking);
            return true;
        } catch (RuntimeException e) {
            log.error("충환전 내역 삭제에 실패하였습니다. - {}", e.getMessage());
            log.warn("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
    }

    @Transactional
    @Override
    public boolean deleteAll() {
            try {
                QBanking q = QBanking.banking;
                Iterable<Banking> bankings = bankingRepository.findAll(q.closing.isTrue().and(q.hidden.isFalse()).and(q.bankingCode.eq(BankingCode.IN)));
                for (Banking banking : bankings) {
                    banking.setHidden(true);
                    bankingRepository.saveAndFlush(banking);
                }
                return true;
            } catch (RuntimeException e) {
                log.error("전체충전 내역삭제에 실패하였습니다. - {}", e.getMessage());
                log.warn("{}", ErrorUtils.trace(e.getStackTrace()));
                return false;
            }
    }

    @Transactional
    @Override
    public boolean deleteAll2() {
        try {
            QBanking q = QBanking.banking;
            Iterable<Banking> bankings = bankingRepository.findAll(q.closing.isTrue().and(q.hidden.isFalse()).and(q.bankingCode.eq(BankingCode.OUT)));
            for (Banking banking : bankings) {
                banking.setHidden(true);
                bankingRepository.saveAndFlush(banking);
            }
            return true;
        } catch (RuntimeException e) {
            log.error("전체환전 내역삭제에 실패하였습니다. - {}", e.getMessage());
            log.warn("{}", ErrorUtils.trace(e.getStackTrace()));
            return false;
        }
    }

    @Transactional
    @Override
    public void stop(long id) {
        try {
            Banking banking = getBanking(id);
            banking.setAlarm(false);
            bankingRepository.saveAndFlush(banking);
        } catch (RuntimeException e) {
            log.error("충환전 대기에 실패하였습니다. - {}", e.getMessage());
            log.warn("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
        }
    }

    @Transactional(readOnly = true)
    @Override
    public boolean isFirstDeposit(String userid) {
        String today = DateUtils.todayString();
        return bankingRepository.count(q.userid.eq(userid)
                .and(q.regDate.goe(DateUtils.start(today))).and(q.regDate.lt(DateUtils.end(today)))
                .and(q.bankingCode.eq(BankingCode.IN))
                .and(q.cancel.isFalse()).and(q.closing.isTrue())) == 0;
    }

    @Transactional(readOnly = true)
    @Override
    public boolean isEveryDeposit(String userid) {
        String today = DateUtils.todayString();
        return bankingRepository.count(q.userid.eq(userid)
                .and(q.regDate.goe(DateUtils.start(today))).and(q.regDate.lt(DateUtils.end(today)))
                .and(q.bankingCode.eq(BankingCode.OUT))
                .and(q.cancel.isFalse()).and(q.closing.isTrue())) == 0;
    }

    private String getRolling(Rolling rolling) {
        return "스포츠: " + rolling.getRollingSports() + "%, 게임존: " + rolling.getRollingZone() + "%";
    }

    @Override
    public AjaxResult depositCasino(DepositDto.Add add) {
        String userid = add.getUserid();
        if (userid == null) {
            return new AjaxResult(false, "현재 요청을 수행 할 수 없습니다.");
        }

        Member member = memberService.getMember(userid);
        if(member.getMoney() >= add.getAmount()){
            PaymentDto.Add pa = new PaymentDto.Add();
            pa.setUserid(member.getUserid());
            pa.setMemo("카지노 충전 - 금액 차감");
            pa.setPlus(false);
            pa.setAmount(add.getAmount());
            pa.setType("CASINO");
            AjaxResult ar = paymentService.addMoney(pa);
            return ar;
        }else{
            return new AjaxResult(false, "현재 요청을 수행 할 수 없습니다.");
        }
    }

    @Override
    public AjaxResult withdrawCasino(WithdrawDto.Add add) {
        String userid = add.getUserid();
        if (userid == null) {
            return new AjaxResult(false, "현재 요청을 수행 할 수 없습니다.");
        }

        Member member = memberService.getMember(userid);
        PaymentDto.Add pa = new PaymentDto.Add();
        pa.setUserid(member.getUserid());
        if("CANCEL".equals(add.getCode())){
            pa.setMemo("카지노 충전취소 - 금액 증액");
        }else {
            pa.setMemo("카지노 환전 - 금액 증액");
        }
        pa.setPlus(true);
        pa.setAmount(add.getAmount());
        pa.setType("CASINO");
        AjaxResult ar = paymentService.addMoney(pa);
        System.out.println(ar.toString());
        System.out.println("증액 완료");
        return ar;
    }


}
