package spoon.banking.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import spoon.banking.entity.Banking;

public interface BankingRepository extends JpaRepository<Banking, Long>, QueryDslPredicateExecutor<Banking> {

}
