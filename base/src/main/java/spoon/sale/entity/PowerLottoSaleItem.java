package spoon.sale.entity;

import lombok.Data;
import spoon.member.domain.Role;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "POWERLOTTOSALE_ITEM", indexes = {
        @Index(name = "IDX_userid", columnList = "userid")
})
public class PowerLottoSaleItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "saleId", insertable = false, updatable = false)
    private PowerLottoSale powerLottoSale;

    @Column(columnDefinition = "NVARCHAR(64)")
    private String userid;

    @Column(columnDefinition = "NVARCHAR(64)")
    private String nickname;

    @Column(name="agencyDepth")
    private int agencyDepth = 0;

    @Column(columnDefinition = "NVARCHAR(64)")
    private String agency1;

    @Column(columnDefinition = "NVARCHAR(64)")
    private String agency2;

    @Column(columnDefinition = "NVARCHAR(64)")
    private String agency3;

    @Column(columnDefinition = "NVARCHAR(64)")
    private String agency4;

    @Column(columnDefinition = "NVARCHAR(64)")
    private String agency5;

    @Column(columnDefinition = "NVARCHAR(64)")
    private String agency6;

    @Column(columnDefinition = "NVARCHAR(64)")
    private String agency7;

    @Enumerated(EnumType.STRING)
    @Column(length = 16, nullable = false)
    private Role role;

    private long inMoney;

    private long outMoney;

    private long betSports;

    private long betZone;

    @Column(name="betZone1")
    private long betZone1;

    @Column(name="betZone2")
    private long betZone2;

    @Column(name="betPowerLotto")
    private long betPowerLotto;

    private long users;

    @Column(length = 16)
    private String rateCode;

    private double rateShare;

    private double rateSports;

    private double rateZone;

    @Column(name="rateZone1")
    private double rateZone1;

    @Column(name="rateZone2")
    private double rateZone2;

    @Column(name="ratePowerLotto")
    private double ratePowerLotto;

    private long lastMoney;

    private long totalMoney;

    @Temporal(TemporalType.TIMESTAMP)
    private Date regDate;

    private boolean closing;

    @Column(name="tmpMoney1")
    private long tmpMoney1 = 0;

    @Column(name="tmpMoney2")
    private long tmpMoney2 = 0;

    @Column(name="tmpMoney3")
    private long tmpMoney3 = 0;

    @Column(name="tmpMoney4")
    private long tmpMoney4 = 0;

    @Column(name="tmpMoney5")
    private long tmpMoney5 = 0;

    @Column(name="tmpMoney6")
    private long tmpMoney6 = 0;

    @Column(name="tmpMoney7")
    private long tmpMoney7 = 0;

    public long getCalcMoney() {
        return (long) (this.betPowerLotto * this.ratePowerLotto / 100D);
    }
}

