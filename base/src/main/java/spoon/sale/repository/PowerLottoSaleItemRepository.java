package spoon.sale.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import spoon.sale.entity.PowerLottoSaleItem;

public interface PowerLottoSaleItemRepository extends JpaRepository<PowerLottoSaleItem, Long>, QueryDslPredicateExecutor<PowerLottoSaleItem> {

}
