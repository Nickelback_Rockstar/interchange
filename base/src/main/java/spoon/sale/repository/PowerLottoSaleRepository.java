package spoon.sale.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import spoon.sale.entity.PowerLottoSale;

public interface PowerLottoSaleRepository extends JpaRepository<PowerLottoSale, Long>, QueryDslPredicateExecutor<PowerLottoSale> {

}
