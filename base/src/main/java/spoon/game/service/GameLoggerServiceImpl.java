package spoon.game.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import spoon.game.entity.GameLogger;
import spoon.game.entity.QGameLogger;
import spoon.game.repository.GameLoggerRepository;

@AllArgsConstructor
@Service
public class GameLoggerServiceImpl implements GameLoggerService {

    private GameLoggerRepository gameLoggerRepository;

    private static QGameLogger q = QGameLogger.gameLogger;

    @Transactional
    @Override
    public void addGameLogger(GameLogger logger) {
        gameLoggerRepository.saveAndFlush(logger);
    }

    @Transactional
    @Override
    public void deleteGameLogger(long gameId) {
        gameLoggerRepository.deleteBeforeGameId(gameId);
    }

    @Transactional(readOnly = true)
    @Override
    public Iterable<GameLogger> getGameLogger(long gameId) {
        return gameLoggerRepository.findAll(q.gameId.eq(gameId));
    }
}
