package spoon.game.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import spoon.game.entity.InGame;
import spoon.game.entity.QInGame;
import spoon.game.repository.InGameRepository;

@Slf4j
@AllArgsConstructor
@Service
public class InGameServiceImpl implements InGameService {

    private InGameRepository inGameRepository;

    private static QInGame q = QInGame.inGame;

    @Transactional(readOnly = true)
    @Override
    public InGame getGame(String gameCode) {
        return inGameRepository.findOne(q.gameCode.eq(gameCode));
    }

    @Transactional
    @Override
    public void addGame(InGame inGame) {
        inGameRepository.saveAndFlush(inGame);
    }

    @Transactional(readOnly = true)
    @Override
    public boolean isExist(String gameCode) {
        return inGameRepository.count(q.gameCode.eq(gameCode)) > 0;
    }
}
