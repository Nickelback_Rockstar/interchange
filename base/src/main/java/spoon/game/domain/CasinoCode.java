package spoon.game.domain;

public enum CasinoCode {

    TPC1(1, "VIVO"),
    TPC4(4, "AllBet"),
    TPC5(5, "Oriental"),
    TPC10(10, "PragmaticPlay"),
    TPC11(11, "StarGame"),
    TPC15(15, "BBIN"),
    TPC16(16, "TAISHAN"),
    TPC19(19, "RTG"),
    TPC21(21, "MG_DASHUR_TC"),
    TPC22(22, "HoGaming"),
    TPC23(23, "CQ9"),
    TPC24(24, "HABANERO"),
    TPC28(28, "DREAMGAME"),
    TPC29(29, "PLAYSTAR"),
    TPC30(30, "GAMEART"),
    TPC32(32, "TTG"),
    TPC33(33, "GENESIS"),
    TPC34(34, "TPG"),
    TPC37(37, "BNG"),
    TPC39(39, "BETSOFT_NEW"),
    TPC40(40, "EVOPLAY"),
    TPC41(41, "DREAMTECH"),
    TPC44(44, "AG_NEW"),
    TPC45(45, "Evolution"),
    TPC99(99, "");

    private int value;

    private String name;

    CasinoCode(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public String getName() {
        return name;
    }

}
