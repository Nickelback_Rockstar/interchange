package spoon.game.domain;

import spoon.config.domain.Config;

import java.util.ArrayList;
import java.util.List;

public enum MenuCode {

    MATCH(100, "승무패"),
    HANDICAP(200, "핸디캡"),
    CROSS(300, "크로스"),
    SPECIAL(600, "스페셜"),
    LIVE(700, "라이브"),
    INGAME(800, "인게임"),
    SPORTS(999, "스포츠"),

    LADDER(10100, "사다리"),
    SNAIL(10200, "달팽이"),
    NEWSNAIL(10250, "NEW달팽이"),
    DARI(10300, "다리다리"),

    FXGAME1(10400, "FX게임 1분"),
    FXGAME2(10410, "FX게임 2분"),
    FXGAME3(10420, "FX게임 3분"),
    FXGAME4(10430, "FX게임 4분"),
    FXGAME5(10430, "FX게임 5분"),

    EOSPOWER1(11110, "EOS파워볼1"),
    EOSPOWER2(11111, "EOS파워볼2"),
    EOSPOWER3(11112, "EOS파워볼3"),
    EOSPOWER4(11113, "EOS파워볼4"),
    EOSPOWER5(11114, "EOS파워볼5"),

    POWER(11100, "파워볼"),
    POWERFREEKICK(11150, "파워프리킥"),
    POWER_LADDER(11200, "파워사다리"),
    POWER_LOTTO(11300, "파워볼로또"),
    CASINO(11400, "카지노"),

    ALADDIN(12100, "알라딘"),
    LOWHI(12200, "로하이"),

    SOCCER(20100, "가상축구"),
    DOG(20200, "개경주"),

    ODDEVEN(21100, "홀짝"),
    BACCARAT(21200, "바카라"),

    LODDEVEN(21300, "로투스홀짝"),
    LBACCARAT(21400, "로투스바카라"),
    LBACCARAT2(21500, "로투스바카라2"),

    LUCK(22100, "세븐럭"),

    KENOLADDER(32100, "키노사다리"),
    KENOSPEED(42100, "키노스피드"),

    SPEEDHOMERUN(52100, "스피드홈런"),

    BINANCECOIN1(53100, "비트코인1분"),
    BINANCECOIN3(54100, "비트코인3분"),

    BOGLELADDER(55100,"보글사다리"),
    BOGLEPOWER(56100,"보글파워볼"),

    TKHILO(61000, "하이로우10"),
    TKHILO5(61100, "하이로우5"),
    TKROULETTE(62000, "룰렛"),
    TKBACCARAT(63000, "바카라"),

    NONE(99999, "삭제경기");

    private int value;

    private String name;

    MenuCode(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public String getName() {
        return name;
    }

    public static boolean isSports(MenuCode menuCode) {
        return menuCode.getValue() <= MenuCode.SPORTS.getValue();
    }

    public static List<MenuCode> getZoneList() {
        List<MenuCode> list = new ArrayList<>();
        if (Config.getSysConfig().getZone().isLadder()) list.add(MenuCode.LADDER);
        if (Config.getSysConfig().getZone().isDari()) list.add(MenuCode.DARI);
        if (Config.getSysConfig().getZone().isSnail()) list.add(MenuCode.SNAIL);
        if (Config.getSysConfig().getZone().isNewSnail()) list.add(MenuCode.NEWSNAIL);
        if (Config.getSysConfig().getZone().isPower()) list.add(MenuCode.POWER);
        if (Config.getSysConfig().getZone().isPowerLadder()) list.add(MenuCode.POWER_LADDER);
        if (Config.getSysConfig().getZone().isPowerFreeKick()) list.add(MenuCode.POWERFREEKICK);
        if (Config.getSysConfig().getZone().isAladdin()) list.add(MenuCode.ALADDIN);
        if (Config.getSysConfig().getZone().isLowhi()) list.add(MenuCode.LOWHI);
        if (Config.getSysConfig().getZone().isOddeven()) list.add(MenuCode.ODDEVEN);
        if (Config.getSysConfig().getZone().isBaccarat()) list.add(MenuCode.BACCARAT);
        if (Config.getSysConfig().getZone().isLotusOddeven()) list.add(MenuCode.LODDEVEN);
        if (Config.getSysConfig().getZone().isLotusBaccarat()) list.add(MenuCode.LBACCARAT);
        if (Config.getSysConfig().getZone().isLotusBaccarat2()) list.add(MenuCode.LBACCARAT2);
        if (Config.getSysConfig().getZone().isSoccer()) list.add(MenuCode.SOCCER);
        if (Config.getSysConfig().getZone().isDog()) list.add(MenuCode.DOG);
        if (Config.getSysConfig().getZone().isLuck()) list.add(MenuCode.LUCK);
        if (Config.getSysConfig().getZone().isKenoLadder()) list.add(MenuCode.KENOLADDER);
        if (Config.getSysConfig().getZone().isKenoSpeed()) list.add(MenuCode.KENOSPEED);
        if (Config.getSysConfig().getZone().isSpeedHomeRun()) list.add(MenuCode.SPEEDHOMERUN);
        if (Config.getSysConfig().getZone().isBinanceCoin1()) list.add(MenuCode.BINANCECOIN1);
        if (Config.getSysConfig().getZone().isBinanceCoin3()) list.add(MenuCode.BINANCECOIN3);
        if (Config.getSysConfig().getZone().isFxGame1()) list.add(MenuCode.FXGAME1);
        if (Config.getSysConfig().getZone().isFxGame2()) list.add(MenuCode.FXGAME2);
        if (Config.getSysConfig().getZone().isFxGame3()) list.add(MenuCode.FXGAME3);
        if (Config.getSysConfig().getZone().isFxGame4()) list.add(MenuCode.FXGAME4);
        if (Config.getSysConfig().getZone().isFxGame5()) list.add(MenuCode.FXGAME5);
        if (Config.getSysConfig().getZone().isEosPower1()) list.add(MenuCode.EOSPOWER1);
        if (Config.getSysConfig().getZone().isEosPower2()) list.add(MenuCode.EOSPOWER2);
        if (Config.getSysConfig().getZone().isEosPower3()) list.add(MenuCode.EOSPOWER3);
        if (Config.getSysConfig().getZone().isEosPower4()) list.add(MenuCode.EOSPOWER4);
        if (Config.getSysConfig().getZone().isEosPower5()) list.add(MenuCode.EOSPOWER5);
        if (Config.getSysConfig().getZone().isBogleLadder()) list.add(MenuCode.BOGLELADDER);
        if (Config.getSysConfig().getZone().isBoglePower()) list.add(MenuCode.BOGLEPOWER);
        if (Config.getSysConfig().getZone().isTkHilo()) list.add(MenuCode.TKHILO);
        if (Config.getSysConfig().getZone().isTkRoulette()) list.add(MenuCode.TKROULETTE);
        if (Config.getSysConfig().getZone().isTkBaccarat()) list.add(MenuCode.TKBACCARAT);

        return list;
    }

}
