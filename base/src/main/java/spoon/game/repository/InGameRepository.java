package spoon.game.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import spoon.game.entity.InGame;

public interface InGameRepository extends JpaRepository<InGame, Long>, QueryDslPredicateExecutor<InGame> {
}
