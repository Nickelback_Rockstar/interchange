package spoon.game.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@NoArgsConstructor
@Data
@Entity
@Table(name = "INGAME_RESULT", indexes = {
        @Index(name = "gameCode", columnList = "gameCode", unique = true)
})
public class InGame {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 64, unique = true)
    private String gameCode;

    @Column(length = 64)
    private String code;

    @Column(length = 64)
    private String sports;

    @Column(length = 256)
    private String league;

    private boolean closing;

    @Column(length = 2)
    private String status;

    @Column(length = 256)
    private String teamName1;

    @Column(length = 256)
    private String temaName2;

    private int score1;
    private int score2;

    @Column(length = 256)
    private String closeGameInfo;

    @Temporal(value = TemporalType.TIMESTAMP)
    private Date regDate;

    @Column(columnDefinition = "NVARCHAR(MAX)")
    private String history;


}
