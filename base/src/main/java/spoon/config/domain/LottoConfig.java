package spoon.config.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class LottoConfig {

    private String option = "출석"; //출석, 충전

    private boolean ones = true;

    private long[] amount = new long[5];

    private int[] daily = new int[5];

    private int[] lotto = new int[5];

    private String[] memo = new String[5];

    private int[] lottoRate = new int[7];
    private int[] lottoWon = new int[7];

    private int toDayAmount = 0;
    private int toDayLotto = 0;

}
