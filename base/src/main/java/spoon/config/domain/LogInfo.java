package spoon.config.domain;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "logInfo")
public class LogInfo {

    @Getter
    @Setter
    private static String bet365ParsingDate;

    @Getter
    @Setter
    private static String betParsingDate;


}
