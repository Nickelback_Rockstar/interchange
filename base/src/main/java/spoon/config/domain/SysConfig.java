package spoon.config.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class SysConfig {

    private Sports sports = new Sports();

    private Zone zone = new Zone();

    private Event event = new Event();

    private Casino casino = new Casino();

    private String apiKey;

    @JsonIgnoreProperties(ignoreUnknown = true)
    @Data
    public class Sports {
        private String bet365Api = "http://api65.stelladb.com";

        // none, all, cross, special
        private String bet365 = "none";

        private String bestApi = "http://api43.stelladb.com";

        private String imageUrl = "";

        // none, all, cross, special
        private String best = "none";

        // 핸디캡, 오버언더 스프레드 최대 갯수
        private int spread = 1;

        // 라이브 메뉴 보여줄지 유무
        private boolean canLive;

        private String inGameApi = "http://api.meviusdata.com";
        private String inGame = "none";
        private int InGameApiTime = 100;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    @Data
    public class Zone {

        // 게임 및 게임존 파싱 여부 (데모페이지 만들때 필요)
        private boolean enabled = true;

        //파워로또 가입 등록코드
        private boolean powerLotto;
        private String powerLottoUrl = "";
        private String powerLottoCode = "";

        // 발란스 사용여부
        private boolean balance;
        private String balanceType = "";

        //발란스 게임 자동 잠김처리
        private boolean balanceGameAutoLock;

        // 발란스 유출픽 효율
        private double leakRate = 1.0;

        // 발란스다리 유출픽 효율
        private double leakDariRate = 1.0;
        // 발란스다리 역배당 효율
        private double reverseDariRate = 1.0;

        // 발란스사디리 유출픽 효율
        private double leakLadderRate = 1.0;
        // 발란스사디리 역배당 효율
        private double reverseLadderRate = 1.0;

        // 발란스파워볼 유출픽 효율
        private double leakPowerRate = 1.0;
        // 발란스파워볼 역배당 효율
        private double reversePowerRate = 1.0;

        // 폴리곤 발란스
        private String balanceUrl = "http://poly.upndown-88.com:8084";

        // 게이트 발란스
        private String balanceGateDariUrl = "HTTPS://API-GATE.COM/DO_BET_NSDD_V3.ASP";
        private String balanceGateLadderUrl = "HTTPS://API-GATE.COM/DO_BET_NSDR_V3.ASP";
        private String balanceGateSuttaUrl = "HTTPS://API-GATE.COM/DO_BET_NSGS_V3.ASP";
        private String balanceGatePowerUrl = "HTTPS://API-GATE.COM/DO_BET_NSPB_V3.ASP";
        private String balanceGateDariUrl2 = "HTTPS://API-GATE.COM/DO_BET_NSDD_V3.ASP";
        private String balanceGateLadderUrl2 = "HTTPS://API-GATE.COM/DO_BET_NSDR_V3.ASP";
        private String balanceGatePowerUrl2 = "HTTPS://API-GATE.COM/DO_BET_NSPB_V3.ASP";

        public boolean getCanBalanceAladdin() {
            return "polygon".equals(this.balanceType);
        }

        public boolean getCanBalanceLowhi() {
            return "polygon".equals(this.balanceType);
        }

        //그래프
        private boolean graph;
        private String graphUrl = "";

        // 사다리
        private boolean ladder;
        private String ladderUrl = "http://api22.stelladb.com/ladder";
        private String ladderPlayer = "http://ladder.named.com/main.php";

        private boolean dari;
        private String dariUrl = "http://api22.stelladb.com/dari";
        private String dariPlayer = "http://daridari.named.com/";

        private boolean snail;
        private String snailUrl = "http://api22.stelladb.com/snail";
        private String snailPlayer = "http://named.com/games/racing/view.php";

        private boolean newSnail;
        private String newSnailUrl = "";
        private String newSnailPlayer = "";

        private boolean binanceCoin1;
        private String binanceCoin1Url = "http://api2.meviusdata.com/binanceCoin";
        private String binanceCoin1Player = "http://bitlive.co.kr/index.php?m=game&m2=ib1m";

        private boolean binanceCoin3;
        private String binanceCoin3Url = "http://api2.meviusdata.com/binanceCoin3";
        private String binanceCoin3Player = "http://bitlive.co.kr/index.php?m=game&m2=ib3m";

        private boolean power;
        private String powerUrl = "http://api22.stelladb.com/power";
        private String powerPlayer = "http://ntry.com/scores/powerball/live.php";
        private String powerMPlayer = "http://ntry.com/scores/powerball/live.php";
        private long powerBase = 1483492380000L;
        private int powerRound = 643085;
        private int powerDay = 142;

        private boolean powerLadder;
        private String powerLadderUrl = "http://api22.stelladb.com/power_ladder";
        private String powerLadderPlayer = "http://ntry.com/scores/power_ladder/live.php";

        private boolean powerFreeKick;
        private String powerFreeKickUrl = "";
        private String powerFreeKickPlayer = "https://livescore.co.kr/sports/score_board/new_game/game.php?t=powerball_freekick&link";
        private String powerFreeKickMPlayer = "https://livescore.co.kr/sports/score_board/new_game/game_mobile.php?t=powerball_freekick&link";

        private boolean fxGame1;
        private String fxGame1Url = "http://api2.meviusdata.com/fxGame1";
        private String fxGame1Player = "https://fxgame.com/fxgame/1min";

        private boolean fxGame2;
        private String fxGame2Url = "http://api2.meviusdata.com/fxGame2";
        private String fxGame2Player = "https://fxgame.com/fxgame/2min";

        private boolean fxGame3;
        private String fxGame3Url = "http://api2.meviusdata.com/fxGame3";
        private String fxGame3Player = "https://fxgame.com/fxgame/3min";

        private boolean fxGame4;
        private String fxGame4Url = "http://api2.meviusdata.com/fxGame4";
        private String fxGame4Player = "https://fxgame.com/fxgame/4min";

        private boolean fxGame5;
        private String fxGame5Url = "http://api2.meviusdata.com/fxGame5";
        private String fxGame5Player = "https://fxgame.com/fxgame/5min";

        private boolean eosPower1;
        private String eosPower1Url = "http://125.129.171.54:8021/eosPower1";
        private String eosPower1Player = "http://ntry.com/scores/eos_powerball/1min/main.php";
        private String eosPower1MPlayer = "http://ntry.com/scores/eos_powerball/1min/main.php";

        private boolean eosPower2;
        private String eosPower2Url = "http://125.129.171.54:8021/eosPower2";
        private String eosPower2Player = "http://ntry.com/scores/eos_powerball/2min/main.php";
        private String eosPower2MPlayer = "http://ntry.com/scores/eos_powerball/2min/main.php";

        private boolean eosPower3;
        private String eosPower3Url = "http://125.129.171.54:8021/eosPower3";
        private String eosPower3Player = "http://ntry.com/scores/eos_powerball/3min/main.php";
        private String eosPower3MPlayer = "http://ntry.com/scores/eos_powerball/3min/main.php";

        private boolean eosPower4;
        private String eosPower4Url = "http://125.129.171.54:8021/eosPower4";
        private String eosPower4Player = "http://ntry.com/scores/eos_powerball/4min/main.php";
        private String eosPower4MPlayer = "http://ntry.com/scores/eos_powerball/4min/main.php";

        private boolean eosPower5;
        private String eosPower5Url = "http://125.129.171.54:8021/eosPower5";
        private String eosPower5Player = "http://ntry.com/scores/eos_powerball/5min/main.php";
        private String eosPower5MPlayer = "http://ntry.com/scores/eos_powerball/5min/main.php";

        private boolean aladdin;
        private String aladdinUrl = "http://api23.stelladb.com/aladdin";
        private String aladdinPlayer = "http://www.hafline.com/Theme/Ladder/Game.aspx";

        private boolean lowhi;
        private String lowhiUrl = "http://api23.stelladb.com/lowhi";
        private String lowhiPlayer = "http://www.hafline.com/Theme/LowHI/Game.aspx";

        private boolean oddeven;
        private boolean oddevenBasic = true;
        private String oddevenUrl = "http://api21.stelladb.com/oddeven";
        private String oddevenPlayer = "http://mgm.stelladb.com/live102.html";

        private boolean baccarat;
        private boolean baccaratBasic = true;
        private String baccaratUrl = "http://api21.stelladb.com/baccarat";
        private String baccaratPlayer = "http://mgm.stelladb.com/baccarat102.html";

        private boolean soccer;
        private String soccerUrl = "http://61.82.187.95:8021/soccer2";
        private String soccerPlayer1 = "https://mmootgga.com/bet365/s_p.html";
        private String soccerPlayer2 = "https://mmootgga.com/bet365/s_s.html";
        private String soccerPlayer3 = "https://mmootgga.com/bet365/s_w.html";
        private String soccerPlayer4 = "https://mmootgga.com/bet365/s_w2.html";

        private boolean dog;
        private String dogUrl = "http://api21.stelladb.com/dog";
        private String dogPlayer1 = "";
        private String dogPlayer2 = "";

        private boolean luck;
        private String luckUrl = "http://api23.stelladb.com/luck";
        private String luckPlayer = "http://7luck-card.com/macau/";

        private boolean lotusOddeven;
        private boolean lotusOddevenBasic = true;
        private String lotusOddevenUrl = "";
        private String lotusOddevenPlayer = "";

        private boolean lotusBaccarat;
        private boolean lotusBaccaratBasic = true;
        private String lotusBaccaratUrl = "";
        private String lotusBaccaratPlayer = "";

        private boolean lotusBaccarat2;
        private boolean lotusBaccarat2Basic = true;
        private String lotusBaccarat2Url = "";
        private String lotusBaccarat2Player = "";

        private boolean kenoLadder;
        private String kenoLadderUrl = "";
        private String kenoLadderPlayer = "";

        private boolean kenoSpeed;
        private String kenoSpeedUrl = "";
        private String kenoSpeedPlayer = "";

        private boolean speedHomeRun;
        private String speedHomeRunUrl = "";
        private String speedHomeRunPlayer = "";

        private boolean bogleLadder;
        private String bogleLadderUrl = "http://61.82.187.95:8021/bogleLadder";
        private String bogleLadderPlayer = "https://bepick.net/live/bubbleladder";

        private boolean boglePower;
        private String boglePowerUrl = "http://61.82.187.95:8021/boglePower";
        private String boglePowerPlayer = "https://bepick.net/live/bubblepower/scrap";

        private boolean motherPowerBet;
        private String motherPowerCp = "";
        private String motherPowerUrl = "";

        private boolean tkBaccarat;
        private boolean tkHilo;
        private boolean tkHilo5;
        private boolean tkRoulette;

        public String getGnb() {
            if (this.ladder) {
                return "ladder";
            } else if (this.dari) {
                return "dari";
            } else if (this.snail) {
                return "snail";
            } else if (this.newSnail) {
                return "newSnail";
            } else if (this.power) {
                return "power";
            } else if (this.powerFreeKick) {
                return "powerFreeKick";
            } else if (this.powerLadder) {
                return "power_ladder";
            } else if (this.aladdin) {
                return "aladdin";
            } else if (this.lowhi) {
                return "lowhi";
            } else if (this.oddeven) {
                return "oddeven";
            } else if (this.baccarat) {
                return "baccarat";
            } else if (this.soccer) {
                return "soccer";
            } else if (this.dog) {
                return "dog";
            } else if (this.luck) {
                return "luck";
            } else if (this.lotusOddeven) {
                return "lotusOddeven";
            } else if (this.lotusBaccarat) {
                return "lotusBaccarat";
            } else if (this.lotusBaccarat2) {
                return "lotusBaccarat2";
            } else if (this.kenoLadder) {
                return "keno_ladder";
            } else if (this.kenoSpeed) {
                return "keno_speed";
            } else if (this.speedHomeRun) {
                return "speed_homeRun";
            } else if (this.binanceCoin1) {
                return "binanceCoin1";
            } else if (this.binanceCoin3) {
                return "binanceCoin3";
            } else if (this.fxGame1) {
                return "fxgame1";
            } else if (this.fxGame2) {
                return "fxGame2";
            } else if (this.fxGame3) {
                return "fxGame3";
            } else if (this.fxGame4) {
                return "fxGame4";
            } else if (this.fxGame5) {
                return "fxGame5";
            } else if (this.eosPower1) {
                return "eosPower1";
            } else if (this.eosPower2) {
                return "eosPower2";
            } else if (this.eosPower3) {
                return "eosPower3";
            } else if (this.eosPower4) {
                return "eosPower4";
            } else if (this.eosPower5) {
                return "eosPower5";
            } else if (this.bogleLadder) {
                return "bogleLadder";
            } else if (this.boglePower) {
                return "boglePower";
            } else {
                return "";
            }
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    @Data
    public class Event {
        private boolean daily = true;
        private boolean lotto = true;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    @Data
    public class Casino {
        private boolean enabled = false;
    }

}
