package spoon.event.domain;

public enum EventCode {

    DAILY(1000, "출석"),
    DEPOSIT(2000, "충전"),
    TODAYDEPOSIT(2100, "일충전"),

    ADMIN(3000, "관리자"),

    UNKNOWN(9999, "알수없음");

    private int value;
    private String name;

    EventCode(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public String getName() {
        return name;
    }
}
