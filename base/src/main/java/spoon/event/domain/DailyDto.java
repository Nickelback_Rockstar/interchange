package spoon.event.domain;

import lombok.Data;
import spoon.common.utils.DateUtils;

public class DailyDto {

    @Data
    public static class Command {
        private String sdate;
        private String edate;
        private String userid;
        private String username;

        public Command() {
            this.sdate = DateUtils.todayString();
            this.edate = this.sdate;
        }

        public String getStart() {
            return this.sdate.replaceAll("\\.", "-");
        }

        public String getEnd() {
            return this.edate.replaceAll("\\.", "-");
        }
    }

    @Data
    public static class Lotto {
        private int eventDay;
        private int lotto;
    }

    @Data
    public static class Add {
        private String userid;
        private String memo;
        private long id;
    }

}
