package spoon.event.entity;

import lombok.Data;
import spoon.event.domain.EventCode;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "LOTTO")
public class Lotto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(columnDefinition = "NVARCHAR(64)")
    private String userid;

    private long point;

    @Column(columnDefinition = "NVARCHAR(255)")
    private String memo;

    private boolean startEnd;

    private boolean cancel;

    private boolean closing;

    @Enumerated(EnumType.STRING)
    @Column(length = 32)
    private EventCode eventCode;

    @Temporal(TemporalType.TIMESTAMP)
    private Date regDate;

    @Temporal(TemporalType.TIMESTAMP)
    private Date closingDate;

    @Column(columnDefinition = "nvarchar(64)")
    private String worker;

}
