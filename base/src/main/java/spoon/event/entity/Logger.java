package spoon.event.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "LOG")
public class Logger {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(columnDefinition = "NVARCHAR(20)")
    private String code;

    @Column(columnDefinition = "NVARCHAR(MAX)")
    private String data;

    @Column(columnDefinition = "NVARCHAR(256)")
    private String etc1;

    @Column(columnDefinition = "NVARCHAR(256)")
    private String etc2;

    @Temporal(value = TemporalType.TIMESTAMP)
    private Date regDate;

}
