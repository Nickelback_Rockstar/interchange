package spoon.event.service;

import spoon.event.entity.Logger;

public interface LoggerService {

    void addLog(Logger logger);

}
