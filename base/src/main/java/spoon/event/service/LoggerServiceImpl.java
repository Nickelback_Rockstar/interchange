package spoon.event.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import spoon.event.entity.Logger;
import spoon.event.repository.LoggerRepository;

@Slf4j
@AllArgsConstructor
@Service
public class LoggerServiceImpl implements LoggerService {

    private LoggerRepository loggerRepository;

    @Transactional
    @Override
    public void addLog(Logger logger) {
        loggerRepository.saveAndFlush(logger);
    }


}
