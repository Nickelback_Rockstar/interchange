package spoon.event.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import spoon.banking.entity.Banking;
import spoon.config.domain.LottoConfig;
import spoon.event.domain.DailyDto;
import spoon.event.entity.Lotto;
import spoon.support.web.AjaxResult;

public interface LottoService {

    boolean updateConfig(LottoConfig lottoConfig);

    void checkLotto(Banking banking);

    long getLottoCount(String userid);

    Page<Lotto> lottoPage(DailyDto.Command command, Pageable pageable);

    AjaxResult addLotto(DailyDto.Add add);

    AjaxResult changeCancel(DailyDto.Add add);

    Page<Lotto> lottoListPage(DailyDto.Command command, Pageable pageable);

    long lottoRolling(String userid);

    AjaxResult addTotalLotto(DailyDto.Add add);
}
