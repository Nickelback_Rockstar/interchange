package spoon.event.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import spoon.event.entity.Logger;

public interface LoggerRepository extends JpaRepository<Logger, Long>, QueryDslPredicateExecutor<Logger> {

}
