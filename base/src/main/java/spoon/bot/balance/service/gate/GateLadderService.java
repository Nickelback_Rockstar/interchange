package spoon.bot.balance.service.gate;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import spoon.bet.entity.BetItem;
import spoon.bet.service.BetGameService;
import spoon.bot.balance.entity.PolygonBalance;
import spoon.bot.balance.repository.PolygonBalanceRepository;
import spoon.bot.balance.repository.PolygonBalanceRepository1;
import spoon.bot.balance.repository.PolygonBalanceRepository2;
import spoon.bot.balance.service.BalanceService;
import spoon.bot.support.ZoneMaker;
import spoon.common.net.HttpParsing;
import spoon.common.utils.DateUtils;
import spoon.common.utils.StringUtils;
import spoon.config.domain.Config;
import spoon.game.domain.MenuCode;
import spoon.gameZone.ZoneConfig;
import spoon.member.domain.MemberDto;
import spoon.member.service.MemberService;

import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class GateLadderService {

    private BetGameService betGameService;

    private MemberService memberService;

    private PolygonBalanceRepository polygonBalanceRepository;
    private PolygonBalanceRepository1 polygonBalanceRepository1;
    private PolygonBalanceRepository2 polygonBalanceRepository2;

    private BalanceService balanceService;

    @Async
    public void balance() {

        double rate = Config.getGameConfig().getBalanceLadderRate();
        double leakRate = Config.getSysConfig().getZone().getLeakLadderRate();
        double reverseRate = Config.getSysConfig().getZone().getReverseLadderRate();

        if (rate == 0 || !Config.getGameConfig().isBalanceLadder()) return;

        ZoneMaker zoneMaker = ZoneConfig.getLadder().getZoneMaker();

        Date gameDate = zoneMaker.getGameDate();
        String sdate = DateUtils.format(gameDate, "yyyyMMddHHmm");

        long calc;
        long tmpCalc;
        long reverseCalc;
        long odd = 0, even = 0, left = 0, right = 0, line3 = 0, line4 = 0;
        long tmpOdd = 0, tmpEven = 0, tmpLeft = 0, tmpRight = 0, tmpLine3 = 0, tmpLine4 = 0;
        long reverseOdd = 0, reverseEven = 0, reverseLeft = 0, reverseRight = 0, reverseLine3 = 0, reverseLine4 = 0;
        long totalOddeven = 0, totalStart = 0, totalLine = 0;
        long totalTmpOddeven = 0, totalTmpStart = 0, totalTmpLine = 0;
        long totalReverseOddeven = 0, totalReverseStart = 0, totalReverseLine = 0;
        int posOddeven = 0, posStart = 0, posLine = 0;
        boolean canOddeven = false, canStart = false, canLine = false;
        int posReverseOddeven = 0, posReverseStart = 0, posReverseLine = 0;
        boolean canReverseOddeven = false, canReverseStart = false, canReverseLine = false;
        String round = String.format("%03d", zoneMaker.getRound());

        Iterable<BetItem> items = betGameService.getBalanceBet(MenuCode.LADDER, sdate);

        for (BetItem item : items) {
            MemberDto.Balance bal = memberService.getUserBalance(item.getUserid());
            boolean balanceLeak = bal.isBalanceLadderLeak();
            boolean balanceReverse = bal.isBalanceLadderReverse();
            switch (item.getSpecial()) {
                case "oddeven":
                    if (balanceReverse) {
                        if ("home".equals(item.getBetTeam())) {
                            reverseOdd += item.getBetMoney() * reverseRate;
                        } else if ("away".equals(item.getBetTeam())) {
                            reverseEven += item.getBetMoney() * reverseRate;
                        }
                    } else {
                        if ("home".equals(item.getBetTeam())) {
                            odd += (balanceLeak) ? item.getBetMoney() * leakRate : item.getBetMoney();
                            tmpOdd += item.getBetMoney();
                        } else if ("away".equals(item.getBetTeam())) {
                            even += (balanceLeak) ? item.getBetMoney() * leakRate : item.getBetMoney();
                            tmpEven += item.getBetMoney();
                        }
                    }
                    break;
                case "start":
                    if (balanceReverse) {
                        if ("home".equals(item.getBetTeam())) {
                            reverseLeft += item.getBetMoney() * reverseRate;
                        } else if ("away".equals(item.getBetTeam())) {
                            reverseRight += item.getBetMoney() * reverseRate;
                        }
                    } else {
                        if ("home".equals(item.getBetTeam())) {
                            left += (balanceLeak) ? item.getBetMoney() * leakRate : item.getBetMoney();
                            tmpLeft += item.getBetMoney();
                        } else if ("away".equals(item.getBetTeam())) {
                            right += (balanceLeak) ? item.getBetMoney() * leakRate : item.getBetMoney();
                            tmpRight += item.getBetMoney();
                        }
                    }
                    break;
                case "line":
                    if (balanceReverse) {
                        if ("home".equals(item.getBetTeam())) {
                            reverseLine3 += item.getBetMoney() * reverseRate;
                        } else if ("away".equals(item.getBetTeam())) {
                            reverseLine4 += item.getBetMoney() * reverseRate;
                        }
                    } else {
                        if ("home".equals(item.getBetTeam())) {
                            line3 += (balanceLeak) ? item.getBetMoney() * leakRate : item.getBetMoney();
                            tmpLine3 += item.getBetMoney();
                        } else if ("away".equals(item.getBetTeam())) {
                            line4 += (balanceLeak) ? item.getBetMoney() * leakRate : item.getBetMoney();
                            tmpLine4 += item.getBetMoney();
                        }
                    }
                    break;
            }
        }

        // 홀짝
        calc = (long) ((odd - even) * rate / 100);
        tmpCalc = (long) ((tmpOdd - tmpEven) * rate / 100);
        if (calc >= 5000) {
            totalOddeven = calc;
            totalTmpOddeven = tmpCalc;
            posOddeven = 1;
            canOddeven = true;
        }
        calc = (long) ((even - odd) * rate / 100);
        tmpCalc = (long) ((tmpEven - tmpOdd) * rate / 100);
        if (calc >= 5000) {
            totalOddeven = calc;
            totalTmpOddeven = tmpCalc;
            posOddeven = 2;
            canOddeven = true;
        }

        // 좌우
        calc = (long) ((left - right) * rate / 100);
        tmpCalc = (long) ((tmpLeft - tmpRight) * rate / 100);
        if (calc >= 5000) {
            totalStart = calc;
            totalTmpStart = tmpCalc;
            posStart = 1;
            canStart = true;
        }
        calc = (long) ((right - left) * rate / 100);
        tmpCalc = (long) ((tmpRight - tmpLeft) * rate / 100);
        if (calc >= 5000) {
            totalStart = calc;
            totalTmpStart = tmpCalc;
            posStart = 2;
            canStart = true;
        }

        // 3줄4줄
        calc = (long) ((line3 - line4) * rate / 100);
        tmpCalc = (long) ((tmpLine3 - tmpLine4) * rate / 100);
        if (calc >= 5000) {
            totalLine = calc;
            totalTmpLine = tmpCalc;
            posLine = 1;
            canLine = true;
        }
        calc = (long) ((line4 - line3) * rate / 100);
        tmpCalc = (long) ((tmpLine4 - tmpLine3) * rate / 100);
        if (calc >= 5000) {
            totalLine = calc;
            totalTmpLine = tmpCalc;
            posLine = 2;
            canLine = true;
        }

        if (canOddeven) {
            String url = Config.getSysConfig().getZone().getBalanceGateLadderUrl()
                    + "?UserId=" + Config.getGameConfig().getBalanceGateId()
                    + "&AuthCode=" + Config.getGameConfig().getBalanceGateKey()
                    + "&SiteName=STAR"
                    + "&GameType=A"
                    + "&BetMoney=" + totalOddeven
                    + "&PickNum=" + posOddeven
                    + "&GameTypeSub=A";
            sendQuery(url, sdate, round, "홀짝", posOddeven == 1 ? "홀" : "짝", totalOddeven, totalTmpOddeven);
        }

        if (canStart) {
            String url = Config.getSysConfig().getZone().getBalanceGateLadderUrl()
                    + "?UserId=" + Config.getGameConfig().getBalanceGateId()
                    + "&AuthCode=" + Config.getGameConfig().getBalanceGateKey()
                    + "&SiteName=STAR"
                    + "&GameType=A"
                    + "&BetMoney=" + totalStart
                    + "&PickNum=" + posStart
                    + "&GameTypeSub=B";
            sendQuery(url, sdate, round, "좌우", posStart == 1 ? "좌" : "우", totalStart, totalTmpStart);
        }

        if (canLine) {
            String url = Config.getSysConfig().getZone().getBalanceGateLadderUrl()
                    + "?UserId=" + Config.getGameConfig().getBalanceGateId()
                    + "&AuthCode=" + Config.getGameConfig().getBalanceGateKey()
                    + "&SiteName=STAR"
                    + "&GameType=A"
                    + "&BetMoney=" + totalLine
                    + "&PickNum=" + posLine
                    + "&GameTypeSub=C";
            sendQuery(url, sdate, round, "3줄4줄", posLine == 1 ? "3줄" : "4줄", totalLine, totalTmpLine);
        }

    }

    private void sendQuery(String param, String betDate, String round, String gameType, String betType, long price, long viewPrice) {

        System.out.println("param1=" + param);

        String json = HttpParsing.getJson(param);
        if (json == null) return;
        //GateResult result = JsonUtils.toModel(json, GateResult.class);
        //if (result == null) return;

        JSONParser parser = new JSONParser();
        JSONObject jsonObj = new JSONObject();
        if (!StringUtils.empty(json)) {
            try {
                jsonObj = (JSONObject) parser.parse(json);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        PolygonBalance b = new PolygonBalance();
        b.setGame("사다리");
        b.setGameDate(betDate);
        b.setRound(round);
        b.setGameType(gameType);
        b.setBetType(betType);
        if (viewPrice < 5000) {
            viewPrice = 0;
        }
        b.setPrice(viewPrice);
        b.setRealPrice(price);
        b.setRegDate(new Date());
        b.setMessage(jsonObj.get("resultcode").toString());
        polygonBalanceRepository.saveAndFlush(b);

    }

}
