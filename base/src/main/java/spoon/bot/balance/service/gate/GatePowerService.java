package spoon.bot.balance.service.gate;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import spoon.bet.entity.BetItem;
import spoon.bet.service.BetGameService;
import spoon.bot.balance.domain.GateResult;
import spoon.bot.balance.entity.PolygonBalance;
import spoon.bot.balance.entity.PolygonBalance2;
import spoon.bot.balance.repository.PolygonBalanceRepository;
import spoon.bot.balance.repository.PolygonBalanceRepository1;
import spoon.bot.balance.repository.PolygonBalanceRepository2;
import spoon.bot.balance.service.BalanceService;
import spoon.bot.support.PowerMaker;
import spoon.common.net.HttpParsing;
import spoon.common.utils.DateUtils;
import spoon.common.utils.JsonUtils;
import spoon.common.utils.StringUtils;
import spoon.config.domain.Config;
import spoon.event.entity.Logger;
import spoon.event.service.LoggerService;
import spoon.game.domain.MenuCode;
import spoon.gameZone.ZoneConfig;
import spoon.member.domain.MemberDto;
import spoon.member.service.MemberService;

import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class GatePowerService {

    private BetGameService betGameService;

    private MemberService memberService;

    private PolygonBalanceRepository polygonBalanceRepository;
    private PolygonBalanceRepository1 polygonBalanceRepository1;
    private PolygonBalanceRepository2 polygonBalanceRepository2;

    private BalanceService balanceService;

    private LoggerService loggerService;

    @Async
    public void balance() {

        double rate = Config.getGameConfig().getBalancePowerRate();
        double leakRate = Config.getSysConfig().getZone().getLeakPowerRate();
        double reverseRate = Config.getSysConfig().getZone().getReversePowerRate();

        if (rate == 0 || !Config.getGameConfig().isBalancePower()) return;


        PowerMaker powerMaker = ZoneConfig.getPower().getPowerMaker();
        long times = powerMaker.getTimes() + 1;

        Date gameDate = powerMaker.getGameDate(times);
        String sdate = DateUtils.format(gameDate, "yyyyMMddHHmm");

        long calc;
        long tmpCalc;
        long reverseCalc;
        long pOdd = 0, pEven = 0, odd = 0, even = 0, over = 0, under = 0, pOver = 0, pUnder = 0;
        long tmpPOdd = 0, tmpPEven = 0, tmpOdd = 0, tmpEven = 0, tmpOver = 0, tmpUnder = 0, tmpPOver = 0, tmpPUnder = 0;
        long reversePOdd = 0, reversePEven = 0, reverseOdd = 0, reverseEven = 0, reverseOver = 0, reverseUnder = 0;
        long totalPoe = 0, totalOe = 0, totalOu = 0, totalPOu = 0;
        long totalTmpPoe = 0, totalTmpOe = 0, totalTmpOu = 0, totalTmpPOu = 0;
        long totalReversePoe = 0, totalReverseOe = 0, totalReverseOu = 0;
        int posPoe = 0, posOe = 0, posOu = 0, posPOu = 0;
        boolean canPoe = false, canOe = false, canOu = false, canPOu = false;
        int posReversePoe = 0, posReverseOe = 0, posReverseOu = 0;
        boolean canReversePoe = false, canReverseOe = false, canReverseOu = false;
        String round = String.format("%03d", times);

        Iterable<BetItem> items = betGameService.getBalanceBet(MenuCode.POWER, sdate);

        for (BetItem item : items) {
            MemberDto.Balance bal = memberService.getUserBalance(item.getUserid());
            boolean balanceLeak = bal.isBalancePowerLeak();
            boolean balanceReverse = bal.isBalancePowerReverse();
            switch (item.getSpecial()) {
                case "pb_oddeven":
                    if ("home".equals(item.getBetTeam())) {
                        pOdd += (balanceLeak) ? item.getBetMoney() * leakRate : item.getBetMoney();
                        tmpPOdd += item.getBetMoney();
                    } else if ("away".equals(item.getBetTeam())) {
                        pEven += (balanceLeak) ? item.getBetMoney() * leakRate : item.getBetMoney();
                        tmpPEven += item.getBetMoney();
                    }
                    break;
                case "oddeven":
                    if ("home".equals(item.getBetTeam())) {
                        odd += (balanceLeak) ? item.getBetMoney() * leakRate : item.getBetMoney();
                        tmpOdd += item.getBetMoney();
                    } else if ("away".equals(item.getBetTeam())) {
                        even += (balanceLeak) ? item.getBetMoney() * leakRate : item.getBetMoney();
                        tmpEven += item.getBetMoney();
                    }
                    break;
                case "pb_overunder":
                    if ("home".equals(item.getBetTeam())) {
                        pOver += (balanceLeak) ? item.getBetMoney() * leakRate : item.getBetMoney();
                        tmpPOver += item.getBetMoney();
                    } else if ("away".equals(item.getBetTeam())) {
                        pUnder += (balanceLeak) ? item.getBetMoney() * leakRate : item.getBetMoney();
                        tmpPUnder += item.getBetMoney();
                    }
                    break;
                case "overunder":
                    if ("home".equals(item.getBetTeam())) {
                        over += (balanceLeak) ? item.getBetMoney() * leakRate : item.getBetMoney();
                        tmpOver += item.getBetMoney();
                    } else if ("away".equals(item.getBetTeam())) {
                        under += (balanceLeak) ? item.getBetMoney() * leakRate : item.getBetMoney();
                        tmpUnder += item.getBetMoney();
                    }
                    break;
            }
        }

        System.out.println("sdate=" + sdate);
        System.out.println("파워볼 pOdd=" + pOdd);
        System.out.println("파워볼 pEven=" + over);
        System.out.println("파워볼 pOver=" + pOver);
        System.out.println("파워볼 pUnder=" + pUnder);

        System.out.println("일반볼 odd=" + odd);
        System.out.println("일반볼 even=" + even);
        System.out.println("일반볼 over=" + over);
        System.out.println("일반볼 under=" + under);

        // 파워볼 홀짝
        calc = (long) ((pOdd - pEven) * rate / 100);
        tmpCalc = (long) ((tmpPOdd - tmpPEven) * rate / 100);
        if (calc >= 5000) {
            totalPoe = calc;
            totalTmpPoe = tmpCalc;
            posPoe = 1;
            canPoe = true;
        }
        calc = (long) ((pEven - pOdd) * rate / 100);
        tmpCalc = (long) ((tmpPEven - tmpPOdd) * rate / 100);
        if (calc >= 5000) {
            totalPoe = calc;
            totalTmpPoe = tmpCalc;
            posPoe = 2;
            canPoe = true;
        }

        // 일반볼 홀짝
        calc = (long) ((odd - even) * rate / 100);
        tmpCalc = (long) ((tmpOdd - tmpEven) * rate / 100);
        if (calc >= 5000) {
            totalOe = calc;
            totalTmpOe = tmpCalc;
            posOe = 1;
            canOe = true;
        }
        calc = (long) ((even - odd) * rate / 100);
        tmpCalc = (long) ((tmpEven - tmpOdd) * rate / 100);
        if (calc >= 5000) {
            totalOe = calc;
            totalTmpOe = tmpCalc;
            posOe = 2;
            canOe = true;
        }

        // 일반 오버언더

//        System.out.println("일반 오버="+over);
//        System.out.println("일반 언더="+under);
        calc = (long) ((over - under) * rate / 100);
        tmpCalc = (long) ((tmpOver - tmpUnder) * rate / 100);
        if (calc >= 5000) {
            totalOu = calc;
            totalTmpOu = tmpCalc;
            posOu = 1;
            canOu = true;
        }
        calc = (long) ((under - over) * rate / 100);
        tmpCalc = (long) ((tmpUnder - tmpOver) * rate / 100);
        if (calc >= 5000) {
            totalOu = calc;
            totalTmpOu = tmpCalc;
            posOu = 2;
            canOu = true;
        }

//        System.out.println("일반 totalOu="+totalOu);
//        System.out.println("일반 totalTmpOu="+totalTmpOu);

        // 파워볼 오버언더
        calc = (long) ((pOver - pUnder) * rate / 100);
        tmpCalc = (long) ((tmpPOver - tmpPUnder) * rate / 100);
        if (calc >= 5000) {
            totalPOu = calc;
            totalTmpPOu = tmpCalc;
            posPOu = 1;
            canPOu = true;
        }
        calc = (long) ((pUnder - pOver) * rate / 100);
        tmpCalc = (long) ((tmpPUnder - tmpPOver) * rate / 100);
        if (calc >= 5000) {
            totalPOu = calc;
            totalTmpPOu = tmpCalc;
            posPOu = 2;
            canPOu = true;
        }


        if (canPoe) {


            String url = Config.getSysConfig().getZone().getBalanceGatePowerUrl()
                    + "?UserId=" + Config.getGameConfig().getBalanceGateId()
                    + "&AuthCode=" + Config.getGameConfig().getBalanceGateKey()
                    + "&SiteName=STAR"
                    + "&GameType=C"
                    + "&BetMoney=" + totalPoe
                    + "&PickNum=" + posPoe
                    + "&GameTypeSub=A";
            sendQuery(url, sdate, round, "파워볼 홀짝", posPoe == 1 ? "홀" : "짝", totalPoe, totalTmpPoe);
        }

        if (canPOu) {
            String url = Config.getSysConfig().getZone().getBalanceGatePowerUrl()
                    + "?UserId=" + Config.getGameConfig().getBalanceGateId()
                    + "&AuthCode=" + Config.getGameConfig().getBalanceGateKey()
                    + "&SiteName=STAR"
                    + "&GameType=C"
                    + "&BetMoney=" + totalPOu
                    + "&PickNum=" + posPOu
                    + "&GameTypeSub=B";
            sendQuery(url, sdate, round, "파워볼 오버언더", posPOu == 1 ? "오버" : "언더", totalPOu, totalTmpPOu);
        }

        if (canOe) {
            String url = Config.getSysConfig().getZone().getBalanceGatePowerUrl()
                    + "?UserId=" + Config.getGameConfig().getBalanceGateId()
                    + "&AuthCode=" + Config.getGameConfig().getBalanceGateKey()
                    + "&SiteName=STAR"
                    + "&GameType=C"
                    + "&BetMoney=" + totalOe
                    + "&PickNum=" + posOe
                    + "&GameTypeSub=C";
            sendQuery(url, sdate, round, "일반볼 홀짝", posOe == 1 ? "홀" : "짝", totalOe, totalTmpOe);
        }

        if (canOu) {
            String url = Config.getSysConfig().getZone().getBalanceGatePowerUrl()
                    + "?UserId=" + Config.getGameConfig().getBalanceGateId()
                    + "&AuthCode=" + Config.getGameConfig().getBalanceGateKey()
                    + "&SiteName=STAR"
                    + "&GameType=C"
                    + "&BetMoney=" + totalOu
                    + "&PickNum=" + posOu
                    + "&GameTypeSub=D";
            sendQuery(url, sdate, round, "일반볼 오버언더", posOu == 1 ? "오버" : "언더", totalOu, totalTmpOu);
        }

    }

    private void sendQuery(String param, String betDate, String round, String gameType, String betType, long price, long viewPrice) {

        Logger logger = new Logger();
        logger.setCode("POWER_BALANCE");
        logger.setData(param);
        logger.setRegDate(new Date());
        loggerService.addLog(logger);

        //System.out.println("param1="+param);
        System.out.println("param1=" + param);

        String json = HttpParsing.getJson(param);
        if (json == null) return;
        //GateResult result = JsonUtils.toModel(json, GateResult.class);
        //if (result == null) return;

        JSONParser parser = new JSONParser();
        JSONObject jsonObj = new JSONObject();
        if (!StringUtils.empty(json)) {
            try {
                jsonObj = (JSONObject) parser.parse(json);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        System.out.println("jsonObj=" + jsonObj.toString());

        PolygonBalance b = new PolygonBalance();
        b.setGame("파워볼");
        b.setGameDate(betDate);
        b.setRound(round);
        b.setGameType(gameType);
        b.setBetType(betType);
        if (viewPrice < 5000) {
            viewPrice = 0;
        }
        b.setPrice(viewPrice);
        b.setRealPrice(price);
        b.setRegDate(new Date());
        b.setMessage(jsonObj.get("resultcode").toString());
        polygonBalanceRepository.saveAndFlush(b);

        /*if(Config.getSysConfig().getZone().isBalanceGameAutoLock()) {
            if (result.getFlag() == 1) {
            } else if (result.getFlag() == 101) {
                balanceService.lockDariBalance();
                balanceService.lockLadderBalance();
                balanceService.lockPowerBalance();
            } else {
                balanceService.lockPowerBalance();
            }
        }*/
    }

    private void sendQuery2(String param, String betDate, String round, String gameType, String betType, long price) {
        //System.out.println("param2="+param);
        String json = HttpParsing.getJson(param);
        if (json == null) return;
        GateResult result = JsonUtils.toModel(json, GateResult.class);
        if (result == null) return;
        PolygonBalance2 b = new PolygonBalance2();
        b.setGame("파워볼");
        b.setGameDate(betDate);
        b.setRound(round);
        b.setGameType(gameType);
        b.setBetType(betType);
        b.setPrice(price);
        b.setRealPrice(price);
        b.setRegDate(new Date());
        b.setMessage(result.getMessage());
        polygonBalanceRepository2.saveAndFlush(b);
    }

}
