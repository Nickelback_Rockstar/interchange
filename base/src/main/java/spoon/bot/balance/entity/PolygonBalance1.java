package spoon.bot.balance.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "POLYGON_BALANCE1")
public class PolygonBalance1 {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String game;

    private String gameDate;

    private String round;

    private String gameType;

    private String betType;

    private long price;

    @Column(columnDefinition = "bigint")
    private long realPrice;

    private String message;

    private Date regDate;

}
