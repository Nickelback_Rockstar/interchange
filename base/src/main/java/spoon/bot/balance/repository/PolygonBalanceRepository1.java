package spoon.bot.balance.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import spoon.bot.balance.entity.PolygonBalance1;
import spoon.bot.balance.entity.PolygonBalance2;

public interface PolygonBalanceRepository1 extends JpaRepository<PolygonBalance1, Long>, QueryDslPredicateExecutor<PolygonBalance1> {
    
}
