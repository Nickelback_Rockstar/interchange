package spoon.bot.balance.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import spoon.bot.balance.entity.PolygonBalance2;

public interface PolygonBalanceRepository2 extends JpaRepository<PolygonBalance2, Long>, QueryDslPredicateExecutor<PolygonBalance2> {
    
}
