package spoon.bot.sports.inGame.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class BotInGame {

    private String code;
    private String sports;
    private String gameCode;
    private String league;
    private String link;
    private String status;

    private String teamName1;
    private String teamName2;

    private String score1;
    private String score2;

    private String winLose1;
    private String winLose2;
    private String winLose3;

    private String asianHD1;
    private String asianHD2;

    private String asianOU1;
    private String asianOU2;

    private String bandicap1;
    private String bandicap2;

    private String overUnder1;
    private String overUnder2;

    private String close;

    private String history;

    public String getSiteCode() {
        return "InGame";
    }

    public String leagueKey() {
        return this.sports + "-" + this.league;
    }

    public String teamHomeKey() {
        return this.sports + "-" + this.league + "-" + this.teamName1;
    }

    public String teamAwayKey() {
        return this.sports + "-" + this.league + "-" + this.teamName2;
    }
}
