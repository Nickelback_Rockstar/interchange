package spoon.bot.sports.inGame;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import spoon.bot.sports.service.ParsingGame;
import spoon.bot.sports.service.ParsingResult;
import spoon.config.domain.Config;

@Slf4j
@AllArgsConstructor
@Component
public class InGameTask {

    private ParsingGame inGameParsingGame;

    private ParsingResult inGameParsingResult;

    /*@Scheduled(cron = "0 * * * * *")
    public void parsingGame() {
        if ("none".equals(Config.getSysConfig().getSports().getInGame())) return;
        inGameParsingGame.parsingGame();
    }*/

    @Scheduled(cron = "0/30 * * * * *")
    public void parsingResult() {
        if ("none".equals(Config.getSysConfig().getSports().getInGame())) return;
        inGameParsingResult.closingGame();
    }

}
