package spoon.bot.support;

import org.springframework.util.Assert;
import spoon.common.utils.StringUtils;
import spoon.game.domain.GameResult;
import spoon.gameZone.ZoneScore;

public class ZoneHelper {

    public static ZoneScore zoneResult(String result, boolean cancel) {
        Assert.notNull(result, "결과는 null 이 될 수 없습니다.");

        if (cancel) {
            return new ZoneScore(0, 0, GameResult.CANCEL);
        }

        switch (result) {
            case "ODD":
            case "odd":
            case "ODD_OVER":
            case "EVEN_OVER":
            case "PODD_POVER":
            case "PEVEN_POVER":
            case "O":
            case "LEFT":
            case "3":
            case "p1":
            case "OVER":
            case "over":
            case "대":
            case "홀대":
            case "짝대":
            case "LOW":
            case "1": // 7luck  1번선수
            case "레드":
            case "좌":
            case "Y":
            case "DRAGON":
            case "BUY":
                return new ZoneScore(1, 0, GameResult.HOME);
            case "p2":
            case "중":
            case "홀중":
            case "짝중":
            case "TIE":
//            case "2": // 7luck
                return new ZoneScore(1, 1, GameResult.DRAW);
            case "EVEN":
            case "even":
            case "E":
            case "RIGHT":
            case "2": // 2번선수
            case "4":
            case "p3":
            case "UNDER":
            case "under":
            case "ODD_UNDER":
            case "EVEN_UNDER":
            case "PODD_PUNDER":
            case "PEVEN_PUNDER":
            case "소":
            case "홀소":
            case "짝소":
            case "HI":
            case "HIGH":
//            case "0": // 7luck
            case "블랙":
            case "우":
            case "N":
            case "TIGER":
            case "SELL":
                return new ZoneScore(0, 1, GameResult.AWAY);
            default:
                return new ZoneScore(0, 0, GameResult.NONE);
        }
    }

    public static ZoneScore zoneDragonTigerOddEven(int sum, boolean cancel) {
        if (cancel) {
            return new ZoneScore(0, 0, GameResult.CANCEL);
        }
        if(sum % 2 == 1){
            return new ZoneScore(1, 0, GameResult.HOME);
        }else{
            return new ZoneScore(0, 1, GameResult.AWAY);
        }
    }

/*    public static ZoneScore zoneResult(String result, boolean cancel) {
        if (cancel) {
            return new ZoneScore(0, 0, GameResult.CANCEL);
        }
        return zoneResult(result);
    }*/

    public static ZoneScore line3StartResult(String line, String start , boolean cancel) {
        switch (zoneResult(line , cancel).getGameResult()) {
            case HOME:
                return zoneResult(start , cancel);
            case AWAY:
                return new ZoneScore(0, 0, GameResult.NONE);
            default:
                return new ZoneScore(0, 0, GameResult.CANCEL);
        }
    }

    public static ZoneScore line4StartResult(String line, String start , boolean cancel) {
        switch (zoneResult(line , cancel).getGameResult()) {
            case HOME:
                return new ZoneScore(0, 0, GameResult.NONE);
            case AWAY:
                return zoneResult(start , cancel);
            default:
                return new ZoneScore(0, 0, GameResult.CANCEL);
        }
    }

    public static ZoneScore lowOddevenResult(String lowhi, String oddeven, boolean cancel) {
        switch (zoneResult(lowhi , cancel).getGameResult()) {
            case HOME:
                return zoneResult(oddeven , cancel);
            case AWAY:
                return new ZoneScore(0, 0, GameResult.NONE);
            default:
                return new ZoneScore(0, 0, GameResult.CANCEL);
        }
    }

    public static ZoneScore hiOddevenResult(String lowhi, String oddeven, boolean cancel) {
        switch (zoneResult(lowhi , cancel).getGameResult()) {
            case HOME:
                return new ZoneScore(0, 0, GameResult.NONE);
            case AWAY:
                return zoneResult(oddeven , cancel);
            default:
                return new ZoneScore(0, 0, GameResult.CANCEL);
        }
    }

    public static ZoneScore pattern1Result(String pattern, boolean cancel) {
        if (cancel) {
            return new ZoneScore(0, 0, GameResult.CANCEL);
        }
        switch (pattern) {
            case "스페이드":
            case "S":
                return new ZoneScore(1, 0, GameResult.HOME);
            case "하트":
            case "H":
                return new ZoneScore(0, 1, GameResult.AWAY);
            default:
                return new ZoneScore(0, 0, GameResult.NONE);
        }
    }

    public static ZoneScore pattern2Result(String pattern, boolean cancel) {
        if (cancel) {
            return new ZoneScore(0, 0, GameResult.CANCEL);
        }
        switch (pattern) {
            case "크로바":
            case "C":
                return new ZoneScore(1, 0, GameResult.HOME);
            case "다이아":
            case "D":
                return new ZoneScore(0, 1, GameResult.AWAY);
            default:
                return new ZoneScore(0, 0, GameResult.NONE);
        }
    }

    public static ZoneScore colorResult(String result) {
        switch (result) {
            case "D":
            case "H":
                // Red
                return new ZoneScore(1, 0, GameResult.HOME);
            case "S":
            case "C":
                // Black
                return new ZoneScore(0, 1, GameResult.AWAY);
            default:
                return new ZoneScore(0, 0, GameResult.CANCEL);
        }
    }

    public static ZoneScore zoneResultDraw(String result) {
        switch (result) {
            case "P":
            case "1":
                return new ZoneScore(1, 0, GameResult.HOME);
            case "B":
            case "0":
                return new ZoneScore(0, 1, GameResult.AWAY);
            case "T":
            case "2":
                return new ZoneScore(1, 1, GameResult.DRAW_HIT);
            default:
                return new ZoneScore(0, 0, GameResult.CANCEL);
        }
    }

    public static ZoneScore zoneNewSnailRank(String type, String rank, boolean cancel) {
        if("snail1".equals(type) && type.equals(rank)){
            return new ZoneScore(1, 0, GameResult.HOME);
        }else if("snail2".equals(type) && type.equals(rank)){
            return new ZoneScore(1, 0, GameResult.HOME);
        }else if("snail3".equals(type) && type.equals(rank)){
            return new ZoneScore(1, 0, GameResult.HOME);
        }else if("snail4".equals(type) && type.equals(rank)) {
            return new ZoneScore(1, 0, GameResult.HOME);
        }else if (cancel) {
            return new ZoneScore(0, 0, GameResult.CANCEL);
        }else{
            return new ZoneScore(0, 1, GameResult.AWAY);
        }
    }

    public static ZoneScore zoneNewSnailQuinella(String type, String rank1, String rank2, boolean cancel) {
        if("quinella12".equals(type) && ( ("snail1".equals(rank1) && "snail2".equals(rank2)) || ("snail2".equals(rank1) && "snail1".equals(rank2)) ) ) {
            return new ZoneScore(1, 0, GameResult.HOME);
        }else if("quinella13".equals(type) && ( ("snail1".equals(rank1) && "snail3".equals(rank2)) || ("snail3".equals(rank1) && "snail1".equals(rank2)) ) ){
            return new ZoneScore(1, 0, GameResult.HOME);
        }else if("quinella14".equals(type) && ( ("snail1".equals(rank1) && "snail4".equals(rank2)) || ("snail4".equals(rank1) && "snail1".equals(rank2)) ) ){
            return new ZoneScore(1, 0, GameResult.HOME);
        }else if("quinella23".equals(type) && ( ("snail2".equals(rank1) && "snail3".equals(rank2)) || ("snail3".equals(rank1) && "snail2".equals(rank2)) ) ){
            return new ZoneScore(1, 0, GameResult.HOME);
        }else if("quinella24".equals(type) && ( ("snail2".equals(rank1) && "snail4".equals(rank2)) || ("snail4".equals(rank1) && "snail2".equals(rank2)) ) ){
            return new ZoneScore(1, 0, GameResult.HOME);
        }else if("quinella34".equals(type) && ( ("snail3".equals(rank1) && "snail4".equals(rank2)) || ("snail4".equals(rank1) && "snail3".equals(rank2)) ) ){
            return new ZoneScore(1, 0, GameResult.HOME);
        }else if (cancel) {
            return new ZoneScore(0, 0, GameResult.CANCEL);
        }else{
            return new ZoneScore(0, 1, GameResult.AWAY);
        }
    }

    public static ZoneScore zoneNewSnailExacta(String type, String rank1, String rank2, boolean cancel) {
        if("exacta12".equals(type) && "snail1".equals(rank1) && "snail2".equals(rank2)) {
            return new ZoneScore(1, 0, GameResult.HOME);
        }else if("exacta13".equals(type) && "snail1".equals(rank1) && "snail3".equals(rank2)) {
            return new ZoneScore(1, 0, GameResult.HOME);
        }else if("exacta14".equals(type) && "snail1".equals(rank1) && "snail4".equals(rank2)) {
            return new ZoneScore(1, 0, GameResult.HOME);
        }else if("exacta21".equals(type) && "snail2".equals(rank1) && "snail1".equals(rank2)) {
            return new ZoneScore(1, 0, GameResult.HOME);
        }else if("exacta23".equals(type) && "snail2".equals(rank1) && "snail3".equals(rank2)) {
            return new ZoneScore(1, 0, GameResult.HOME);
        }else if("exacta24".equals(type) && "snail2".equals(rank1) && "snail4".equals(rank2)) {
            return new ZoneScore(1, 0, GameResult.HOME);
        }else if("exacta31".equals(type) && "snail3".equals(rank1) && "snail1".equals(rank2)) {
            return new ZoneScore(1, 0, GameResult.HOME);
        }else if("exacta32".equals(type) && "snail3".equals(rank1) && "snail2".equals(rank2)) {
            return new ZoneScore(1, 0, GameResult.HOME);
        }else if("exacta34".equals(type) && "snail3".equals(rank1) && "snail4".equals(rank2)) {
            return new ZoneScore(1, 0, GameResult.HOME);
        }else if("exacta41".equals(type) && "snail4".equals(rank1) && "snail1".equals(rank2)) {
            return new ZoneScore(1, 0, GameResult.HOME);
        }else if("exacta42".equals(type) && "snail4".equals(rank1) && "snail2".equals(rank2)) {
            return new ZoneScore(1, 0, GameResult.HOME);
        }else if("exacta43".equals(type) && "snail4".equals(rank1) && "snail3".equals(rank2)) {
            return new ZoneScore(1, 0, GameResult.HOME);
        }else if (cancel) {
            return new ZoneScore(0, 0, GameResult.CANCEL);
        }else{
            return new ZoneScore(0, 1, GameResult.AWAY);
        }
    }

    public static ZoneScore zoneBaccaratPair(String pair, boolean result) {
        if(( "PP".equals(pair) || "BP".equals(pair)) && result) {
            return new ZoneScore(1, 0, GameResult.HOME);
        }else if(( "PP".equals(pair) || "BP".equals(pair)) && !result){
            return new ZoneScore(0, 1, GameResult.AWAY);
        }else{
            return new ZoneScore(0, 0, GameResult.CANCEL);
        }
    }

    public static ZoneScore maResult(int scoreHome, int scoreAway, boolean cancel) {
        if (cancel) {
            return new ZoneScore(0, 0, GameResult.CANCEL);
        } else if (scoreHome > scoreAway) {
            return new ZoneScore(scoreHome, scoreAway, GameResult.HOME);
        } else if (scoreHome < scoreAway) {
            return new ZoneScore(scoreHome, scoreAway, GameResult.AWAY);
        }
        return new ZoneScore(scoreHome, scoreAway, GameResult.DRAW);
    }

    public static ZoneScore ahResult(int scoreHome, int scoreAway, double ahDraw, boolean cancel) {
        double result = scoreHome + ahDraw - scoreAway;
        if (cancel) {
            return new ZoneScore(0, 0, GameResult.CANCEL);
        } else if (result > 0) {
            return new ZoneScore(scoreHome, scoreAway, GameResult.HOME);
        } else if (result < 0) {
            return new ZoneScore(scoreHome, scoreAway, GameResult.AWAY);
        }
        return new ZoneScore(scoreHome, scoreAway, GameResult.HIT);
    }

    public static ZoneScore ouResult(int scoreHome, int scoreAway, double ouDraw, boolean cancel) {
        double result = scoreHome + scoreAway - ouDraw;
        if (cancel) {
            return new ZoneScore(0, 0, GameResult.CANCEL);
        } else if (result > 0) {
            return new ZoneScore(scoreHome, scoreAway, GameResult.OVER);
        } else if (result < 0) {
            return new ZoneScore(scoreHome, scoreAway, GameResult.UNDER);
        }
        return new ZoneScore(scoreHome, scoreAway, GameResult.HIT);
    }

    public static ZoneScore zoneKSResult(String type, String num, boolean cancel) {

        if (cancel) {
            return new ZoneScore(0, 0, GameResult.CANCEL);
        }

        switch (type) {
            case "num0":
                if("0".equals(num)) return new ZoneScore(1, 0, GameResult.HOME);
                else return new ZoneScore(0, 1, GameResult.AWAY);
            case "num1":
                if("1".equals(num)) return new ZoneScore(1, 0, GameResult.HOME);
                else return new ZoneScore(0, 1, GameResult.AWAY);
            case "num2":
                if("2".equals(num)) return new ZoneScore(1, 0, GameResult.HOME);
                else return new ZoneScore(0, 1, GameResult.AWAY);
            case "num3":
                if("3".equals(num)) return new ZoneScore(1, 0, GameResult.HOME);
                else return new ZoneScore(0, 1, GameResult.AWAY);
            case "num4":
                if("4".equals(num)) return new ZoneScore(1, 0, GameResult.HOME);
                else return new ZoneScore(0, 1, GameResult.AWAY);
            case "num5":
                if("5".equals(num)) return new ZoneScore(1, 0, GameResult.HOME);
                else return new ZoneScore(0, 1, GameResult.AWAY);
            case "num6":
                if("6".equals(num)) return new ZoneScore(1, 0, GameResult.HOME);
                else return new ZoneScore(0, 1, GameResult.AWAY);
            case "num7":
                if("7".equals(num)) return new ZoneScore(1, 0, GameResult.HOME);
                else return new ZoneScore(0, 1, GameResult.AWAY);
            case "num8":
                if("8".equals(num)) return new ZoneScore(1, 0, GameResult.HOME);
                else return new ZoneScore(0, 1, GameResult.AWAY);
            case "num9":
                if("9".equals(num)) return new ZoneScore(1, 0, GameResult.HOME);
                else return new ZoneScore(0, 1, GameResult.AWAY);
            default:
                return new ZoneScore(0, 0, GameResult.CANCEL);
        }
    }

    public static ZoneScore zoneResultRoulette(String betNumber, String result) {
        if(StringUtils.empty(result)){
            return new ZoneScore(0, 0, GameResult.CANCEL);
        }
        String num[] = betNumber.split(",");
        if(num != null && num.length > 0){
            for(int i=0;i<num.length;i++){
                if(Integer.parseInt(num[i]) == Integer.parseInt(result)) {
                    return new ZoneScore(1, 0, GameResult.HOME);
                }
            }
        }
        return new ZoneScore(0, 1, GameResult.AWAY);
    }
}
