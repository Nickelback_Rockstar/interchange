package spoon.bot.zone.boglePower;

import lombok.AllArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@AllArgsConstructor
@Component
public class BoglePowerTask {

    private BoglePowerParsing boglePowerParsing;

    @Scheduled(cron = "0 0/2 * * * ?")
    public void boglePowerGameTask() {
        boglePowerParsing.parsingGame();
    }

    @Scheduled(cron = "5,8 * * * * ?")
    public void boglePowerClosingNtryTask() {
        boglePowerParsing.closingGame();
    }

}
