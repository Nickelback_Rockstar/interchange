package spoon.bot.zone.boglePower;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import spoon.bot.zone.service.GameBotParsing;
import spoon.common.net.HttpParsing;
import spoon.common.utils.JsonUtils;
import spoon.config.domain.Config;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.boglePower.BoglePower;
import spoon.gameZone.boglePower.service.BoglePowerBotService;

import java.util.Calendar;
import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class BoglePowerParsing implements GameBotParsing {

    private BoglePowerBotService boglePowerBotService;

    private static Date sdate = new Date();

    @Override
    public void parsingGame() {
        int count = 0;
        int round = ZoneConfig.getBoglePower().getZoneMaker().getRound();
        Calendar cal = Calendar.getInstance();
        cal.setTime(ZoneConfig.getBoglePower().getZoneMaker().getGameDate());

        for (int i = 0; i < 10; i++) {
            if (cal.getTime().before(sdate)) continue;
            if (boglePowerBotService.notExist(cal.getTime())) {
                BoglePower boglePower = new BoglePower(round > 720 ? round % 720 : round, cal.getTime());
                /*String hh = boglePower.getSdate().substring(8,10);//시간
                if(Integer.parseInt(hh) < 6){
                    //경기 생성중 6시 미만 경기는 생성 안한다.
                    continue;
                }*/
                boglePower.setOdds(ZoneConfig.getBoglePower().getOdds());
                boglePowerBotService.addGame(boglePower);
                count++;
            }
            round++;
            cal.add(Calendar.MINUTE, 2);
        }
        sdate = cal.getTime();
        log.debug("보글파워볼 경기등록 : {}건", count);
    }

    @Override
    public void closingGame() {
        Date gameDate = new Date(ZoneConfig.getBoglePower().getZoneMaker().getGameDate().getTime() - 2 * 60 * 1000);

        String json = HttpParsing.getJson(Config.getSysConfig().getZone().getBoglePowerUrl());
        if (json == null) {
            return;
        }

        BoglePower result = JsonUtils.toModel(json, BoglePower.class);
        if (result == null) {
            return;
        }

        if (!gameDate.equals(result.getGameDate())) {
            return;
        }

        boglePowerBotService.closingGame(result);

        log.debug("보글파워볼 경기 종료 : {}회차", result.getRound());
    }

    @Override
    public void checkResult() {
        boglePowerBotService.checkResult();
    }

    @Override
    public void deleteGame() {
        boglePowerBotService.deleteGame(2);
    }


}
