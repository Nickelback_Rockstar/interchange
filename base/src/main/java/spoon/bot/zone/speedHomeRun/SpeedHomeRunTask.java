package spoon.bot.zone.speedHomeRun;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import spoon.bot.zone.service.GameBotParsing;
import spoon.config.domain.Config;
import spoon.gameZone.ZoneConfig;

@Slf4j
@AllArgsConstructor
@Component
public class SpeedHomeRunTask {

    private GameBotParsing speedHomeRunParsing;

    @Scheduled(cron = "2 0/5 * * * *")
    public void parsingGame() {
        if (notParsing()) return;
        speedHomeRunParsing.parsingGame();
    }

    @Scheduled(cron = "47,52 * * * * *")
    public void parsingResult() {
        if (notParsing()) return;
        speedHomeRunParsing.closingGame();
    }

    @Scheduled(cron = "3/10 1/5 * * * *")
    public void checkResult() {
        if (notParsing()) return;
        speedHomeRunParsing.checkResult();
    }

    @Scheduled(cron = "2 1 4 * * * ")
    public void deleteGame() {
        speedHomeRunParsing.deleteGame();
    }

    private boolean notParsing() {
        return !Config.getSysConfig().getZone().isEnabled() || !Config.getSysConfig().getZone().isSpeedHomeRun() || !ZoneConfig.getSpeedHomeRun().isEnabled();
    }

}
