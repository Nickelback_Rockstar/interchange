package spoon.bot.zone.speedHomeRun;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import spoon.bot.zone.service.GameBotParsing;
import spoon.common.net.HttpParsing;
import spoon.common.utils.JsonUtils;
import spoon.config.domain.Config;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.speedHomeRun.SpeedHomeRun;
import spoon.gameZone.speedHomeRun.service.SpeedHomeRunBotService;

import java.util.Calendar;
import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class SpeedHomeRunParsing implements GameBotParsing {

    private SpeedHomeRunBotService speedHomeRunBotService;

    private static boolean isClosing = false;

    private static Date sdate = new Date();

    @Async
    @Override
    public void parsingGame() {
        isClosing = false;

        int count = 0;
        int times = ZoneConfig.getSpeedHomeRun().getPowerMaker().getTimes();
        Calendar cal = Calendar.getInstance();
        cal.setTime(ZoneConfig.getSpeedHomeRun().getPowerMaker().getGameDate(times));

        for (int i = 0; i < 6; i++) {
            times++;
            cal.add(Calendar.MINUTE, 5);

            if (cal.getTime().before(sdate)) continue;

            if (speedHomeRunBotService.notExist(cal.getTime())) {
                SpeedHomeRun speedHomeRun = new SpeedHomeRun(times, cal.getTime());
                String hh = speedHomeRun.getSdate().substring(8,10);//시간
                if(Integer.parseInt(hh) < 6){
                    //경기 생성중 6시 미만 경기는 생성 안한다.
                    continue;
                }
                speedHomeRun.setOdds(ZoneConfig.getSpeedHomeRun().getOdds());
                speedHomeRunBotService.addGame(speedHomeRun);
                count++;
            }
        }

        log.debug("스피드 홈런 경기등록 : {}건", count);
    }

    @Async
    @Override
    public void closingGame() {
        //if (isClosing) return;
        isClosing = true;

        int times = ZoneConfig.getSpeedHomeRun().getPowerMaker().getTimes();
        Date gameDate = ZoneConfig.getSpeedHomeRun().getPowerMaker().getGameDate(times);

        String json = HttpParsing.getJson(Config.getSysConfig().getZone().getSpeedHomeRunUrl());
        if (json == null) {
            isClosing = false;
            return;
        }

        SpeedHomeRun result = JsonUtils.toModel(json, SpeedHomeRun.class);
        if (result == null) {
            isClosing = false;
            return;
        }

        if (!gameDate.equals(result.getGameDate())) {
            isClosing = false;
            return;
        }

        isClosing = speedHomeRunBotService.closingGame(result);

        log.debug("스피드 홈런 경기 종료 : {}회차", result.getRound());
    }

    @Async
    @Override
    public void checkResult() {
        speedHomeRunBotService.checkResult();
    }

    @Async
    @Override
    public void deleteGame() {
        speedHomeRunBotService.deleteGame(3);
    }
}
