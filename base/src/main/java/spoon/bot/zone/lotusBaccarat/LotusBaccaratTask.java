package spoon.bot.zone.lotusBaccarat;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import spoon.bot.zone.service.GameBotParsing;
import spoon.config.domain.Config;
import spoon.gameZone.ZoneConfig;

@Slf4j
@AllArgsConstructor
@Component
public class LotusBaccaratTask {

    private GameBotParsing lotusBaccaratParsing;

    @Scheduled(cron = "0/32 * * * * *")
    public void parsingGame() {
        if (notParsing()) return;
        lotusBaccaratParsing.parsingGame();
    }

    @Scheduled(cron = "7/22 * * * * *")
    public void parsingResult() {
        if (notParsing()) return;
        lotusBaccaratParsing.closingGame();
    }

    @Scheduled(cron = "1 * * * * *")
    public void checkResult() {
        if (notParsing()) return;
        lotusBaccaratParsing.checkResult();
    }

    @Scheduled(cron = "15 1 4 * * * ")
    public void deleteGame() {
        lotusBaccaratParsing.deleteGame();
    }

    private boolean notParsing() {
        return !Config.getSysConfig().getZone().isEnabled() || !Config.getSysConfig().getZone().isLotusBaccarat() || !ZoneConfig.getLotusBaccarat().isEnabled();
    }
}
