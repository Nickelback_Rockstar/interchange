package spoon.bot.zone.fxGame1;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import spoon.bot.zone.service.GameBotParsing;
import spoon.common.net.HttpParsing;
import spoon.common.utils.GameUtils;
import spoon.common.utils.JsonUtils;
import spoon.config.domain.Config;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.fxGame1.FxGame1;
import spoon.gameZone.fxGame1.service.FxGame1BotService;

import java.util.Calendar;
import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class FxGame1Parsing implements GameBotParsing {

    private FxGame1BotService fxGame1BotService;

    private static Date sdate = new Date();

    @Override
    public void parsingGame() {
        Calendar c = Calendar.getInstance();
        if(GameUtils.notFxGame(c)) return;

        int count = 0;
        Calendar cal = Calendar.getInstance();
        cal.setTime(ZoneConfig.getFxGame1().getZoneMaker().getGameDate());

        for (int i = 0; i < 10; i++) {
            if (cal.getTime().before(sdate)) continue;
            if (fxGame1BotService.notExist(cal.getTime())) {
                if(GameUtils.notFxGame(cal)) continue;
                int round = Integer.parseInt(cal.get(Calendar.HOUR_OF_DAY) + GameUtils.preFixZero(cal.get(Calendar.MINUTE)));
                FxGame1 fxGame1 = new FxGame1(round, cal.getTime());
                fxGame1.setOdds(ZoneConfig.getFxGame1().getOdds());
                fxGame1BotService.addGame(fxGame1);
                count++;
            }
            cal.add(Calendar.MINUTE, 1);
        }
        sdate = cal.getTime();
        log.debug("fxGame1 경기등록 : {}건", count);
    }

    @Override
    public void closingGame() {
        Date gameDate = new Date(ZoneConfig.getFxGame1().getZoneMaker().getGameDate().getTime() - 1 * 60 * 1000);

        String json = HttpParsing.getJson(Config.getSysConfig().getZone().getFxGame1Url());
        if (json == null) {
            return;
        }

        FxGame1 result = JsonUtils.toModel(json, FxGame1.class);
        if (result == null) {
            return;
        }

        if (!gameDate.equals(result.getGameDate())) {
            return;
        }

        fxGame1BotService.closingGame(result);

        log.debug("fxGame1 경기 종료 : {}회차", result.getRound());
    }

    @Override
    public void checkResult() {
        fxGame1BotService.checkResult();
    }

    @Override
    public void deleteGame() {
        fxGame1BotService.deleteGame(3);
    }


}
