package spoon.bot.zone.fxGame1;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import spoon.bot.zone.service.GameBotParsing;
import spoon.config.domain.Config;
import spoon.gameZone.ZoneConfig;

@Slf4j
@AllArgsConstructor
@Component
public class FxGame1Task {

    private GameBotParsing fxGame1Parsing;

    @Scheduled(cron = "0 * * * * *")
    public void parsingGame() {
        if (notParsing()) return;
        fxGame1Parsing.parsingGame();
    }

    @Scheduled(cron = "8 * * * * *")
    public void parsingResult() {
        if (notParsing()) return;
        fxGame1Parsing.closingGame();
    }

    @Scheduled(cron = "10 * * * * *")
    public void checkResult() {
        if (notParsing()) return;
        fxGame1Parsing.checkResult();
    }

    @Scheduled(cron = "1 1 4 * * * ")
    public void deleteGame() {
        fxGame1Parsing.deleteGame();
    }

    private boolean notParsing() {
        return !Config.getSysConfig().getZone().isEnabled() || !Config.getSysConfig().getZone().isFxGame1() || !ZoneConfig.getFxGame1().isEnabled();
    }

}
