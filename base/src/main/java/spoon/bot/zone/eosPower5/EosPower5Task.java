package spoon.bot.zone.eosPower5;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import spoon.bot.zone.service.GameBotParsing;
import spoon.config.domain.Config;
import spoon.gameZone.ZoneConfig;

@Slf4j
@AllArgsConstructor
@Component
public class EosPower5Task {

    private GameBotParsing eosPower5Parsing;

    @Scheduled(cron = "0 0/5 * * * *")
    public void parsingGame() {
        if (notParsing()) return;
        eosPower5Parsing.parsingGame();
    }

    @Scheduled(cron = "10 * * * * *")
    public void parsingResult() {
        if (notParsing()) return;
        eosPower5Parsing.closingGame();
    }

    @Scheduled(cron = "5,8 5/5 * * * *")
    public void checkResult() {
        if (notParsing()) return;
        eosPower5Parsing.checkResult();
    }

    @Scheduled(cron = "7 1 4 * * * ")
    public void deleteGame() {
        eosPower5Parsing.deleteGame();
    }

    private boolean notParsing() {
        return !Config.getSysConfig().getZone().isEnabled() || !Config.getSysConfig().getZone().isEosPower5() || !ZoneConfig.getEosPower5().isEnabled();
    }

}
