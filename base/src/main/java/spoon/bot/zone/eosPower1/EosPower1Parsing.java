package spoon.bot.zone.eosPower1;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import spoon.bot.zone.service.GameBotParsing;
import spoon.common.net.HttpParsing;
import spoon.config.domain.Config;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.eosPower1.EosPower1;
import spoon.gameZone.eosPower1.EosPower1Dto;
import spoon.gameZone.eosPower1.service.EosPower1BotService;

import java.util.Calendar;
import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class EosPower1Parsing implements GameBotParsing {

    private EosPower1BotService eosPower1BotService;

    private static boolean isClosing = false;

    private static Date sdate = new Date();

    @Async
    @Override
    public void parsingGame() {
        isClosing = false;

        int count = 0;
        int round = ZoneConfig.getEosPower1().getZoneMaker().getRound();
        Calendar cal = Calendar.getInstance();
        cal.setTime(ZoneConfig.getEosPower1().getZoneMaker().getGameDate());

        for (int i = 0; i < 6; i++) {
            if (cal.getTime().before(sdate)) continue;

            if (eosPower1BotService.notExist(cal.getTime())) {
                EosPower1 eosPower1 = new EosPower1(round > 1440 ? round % 1440 : round, cal.getTime());
                eosPower1.setOdds(ZoneConfig.getEosPower1().getOdds());
                eosPower1BotService.addGame(eosPower1);
                count++;
            }
            round++;
            cal.add(Calendar.MINUTE, 1);
        }
        log.debug("EOS파워볼 1분 경기등록 : {}건", count);
    }

    @Async
    @Override
    public void closingGame() {
        if (isClosing) return;
        isClosing = true;

        Date gameDate = new Date(ZoneConfig.getEosPower1().getZoneMaker().getGameDate().getTime() - 1 * 60 * 1000);

        String json = HttpParsing.getJson(Config.getSysConfig().getZone().getEosPower1Url());
        if (json == null) {
            isClosing = false;
            return;
        }

        //={"gameDate":1541586180000,
        // "round":836176,
        // "times":233,
        // "ball":"27,13,17,22,11",
        // "pb":"06",
        // "oddeven":"EVEN",
        // "pb_oddeven":"EVEN",
        // "overunder":"OVER",
        // "pb_overunder":"OVER",
        // "size":"대",
        // "sum":90,
        // "sdate":"201811071923",
        // "closing":true}

        try {
            JSONParser parser = new JSONParser();
            JSONObject obj = (JSONObject) parser.parse(json);
            EosPower1 result = new EosPower1();
            long gDate = Long.parseLong(obj.get("gameDate").toString());
            result.setGameDate(new Date(gDate));
            result.setRound(Integer.parseInt(obj.get("round").toString()));
            result.setTimes(Integer.parseInt(obj.get("times").toString()));
            result.setBall(obj.get("ball").toString());
            result.setPb(obj.get("pb").toString());
            result.setSum(Integer.parseInt(obj.get("sum").toString()));
            result.setSdate(obj.get("sdate").toString());
            result.setClosing(Boolean.parseBoolean(obj.get("closing").toString()));

//            result.setOddeven(obj.get("oddeven").toString());
//            result.setPb_oddeven(obj.get("pb_oddeven").toString());
//            result.setOverunder(obj.get("overunder").toString());
//            result.setPb_overunder(obj.get("pb_overunder").toString());
//            result.setSize(obj.get("size").toString());

            //결과 처리 score
            EosPower1Dto.Score score = new EosPower1Dto.Score();
            score.setBall(result.getBall());
            score.setPb(result.getPb());
            result.updateScore(score);

            if (result == null) {
                isClosing = false;
                return;
            }

            if (!gameDate.equals(result.getGameDate())) {
                isClosing = false;
                return;
            }

            //System.out.println("EOS파워볼 클로징 게임 result =" + result);

            isClosing = eosPower1BotService.closingGame(result);

            log.debug("EOS파워볼 1분 경기 종료 : {}회차", result.getRound());

        } catch (ParseException e) {
            e.printStackTrace();
            isClosing = false;
            return;
        }

    }

    @Async
    @Override
    public void checkResult() {
        eosPower1BotService.checkResult();
    }

    @Async
    @Override
    public void deleteGame() {
        eosPower1BotService.deleteGame(3);
    }
}
