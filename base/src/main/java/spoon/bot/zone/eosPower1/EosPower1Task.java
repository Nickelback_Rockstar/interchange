package spoon.bot.zone.eosPower1;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import spoon.bot.zone.service.GameBotParsing;
import spoon.config.domain.Config;
import spoon.gameZone.ZoneConfig;

@Slf4j
@AllArgsConstructor
@Component
public class EosPower1Task {

    private GameBotParsing eosPower1Parsing;

    @Scheduled(cron = "0 0/1 * * * *")
    public void parsingGame() {
        if (notParsing()) return;
        eosPower1Parsing.parsingGame();
    }

    @Scheduled(cron = "10 * * * * *")
    public void parsingResult() {
        if (notParsing()) return;
        eosPower1Parsing.closingGame();
    }

    @Scheduled(cron = "5,8 1/1 * * * *")
    public void checkResult() {
        if (notParsing()) return;
        eosPower1Parsing.checkResult();
    }

    @Scheduled(cron = "7 1 4 * * * ")
    public void deleteGame() {
        eosPower1Parsing.deleteGame();
    }

    private boolean notParsing() {
        return !Config.getSysConfig().getZone().isEnabled() || !Config.getSysConfig().getZone().isEosPower1() || !ZoneConfig.getEosPower1().isEnabled();
    }

}
