package spoon.bot.zone.fxGame2;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import spoon.bot.zone.service.GameBotParsing;
import spoon.common.net.HttpParsing;
import spoon.common.utils.GameUtils;
import spoon.common.utils.JsonUtils;
import spoon.config.domain.Config;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.fxGame2.FxGame2;
import spoon.gameZone.fxGame2.service.FxGame2BotService;

import java.util.Calendar;
import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class FxGame2Parsing implements GameBotParsing {

    private FxGame2BotService fxGame2BotService;

    private static Date sdate = new Date();

    @Override
    public void parsingGame() {
        Calendar c = Calendar.getInstance();
        if(GameUtils.notFxGame(c)) return;

        int count = 0;
        Calendar cal = Calendar.getInstance();
        cal.setTime(ZoneConfig.getFxGame2().getZoneMaker().getGameDate());

        for (int i = 0; i < 10; i++) {
            if (cal.getTime().before(sdate)) continue;
            if (fxGame2BotService.notExist(cal.getTime())) {
                if(GameUtils.notFxGame(cal)) continue;
                int round = Integer.parseInt(cal.get(Calendar.HOUR_OF_DAY) + GameUtils.preFixZero(cal.get(Calendar.MINUTE)));
                FxGame2 fxGame2 = new FxGame2(round, cal.getTime());
                fxGame2.setOdds(ZoneConfig.getFxGame2().getOdds());
                fxGame2BotService.addGame(fxGame2);
                count++;
            }
            cal.add(Calendar.MINUTE, ZoneConfig.getFxGame2().getZoneMaker().getInterval());
        }
        sdate = cal.getTime();
        log.debug("fxGame2 경기등록 : {}건", count);
    }

    @Override
    public void closingGame() {
        Date gameDate = new Date(ZoneConfig.getFxGame2().getZoneMaker().getGameDate().getTime() - ZoneConfig.getFxGame2().getZoneMaker().getInterval() * 60 * 1000);

        String json = HttpParsing.getJson(Config.getSysConfig().getZone().getFxGame2Url());
        if (json == null) {
            return;
        }

        FxGame2 result = JsonUtils.toModel(json, FxGame2.class);
        if (result == null) {
            return;
        }

        if (!gameDate.equals(result.getGameDate())) {
            return;
        }

        fxGame2BotService.closingGame(result);

        log.debug("fxGame2 경기 종료 : {}회차", result.getRound());
    }

    @Override
    public void checkResult() {
        fxGame2BotService.checkResult();
    }

    @Override
    public void deleteGame() {
        fxGame2BotService.deleteGame(3);
    }


}
