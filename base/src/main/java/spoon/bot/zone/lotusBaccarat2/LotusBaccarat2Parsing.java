package spoon.bot.zone.lotusBaccarat2;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import spoon.bot.zone.service.GameBotParsing;
import spoon.common.net.HttpParsing;
import spoon.common.utils.DateUtils;
import spoon.common.utils.JsonUtils;
import spoon.common.utils.StringUtils;
import spoon.config.domain.Config;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.lotusBaccarat2.LotusBaccarat2;
import spoon.gameZone.lotusBaccarat2.service.LotusBaccarat2BotService;
import spoon.mapper.GameMapper;

import java.util.Calendar;
import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class LotusBaccarat2Parsing implements GameBotParsing {

    private LotusBaccarat2BotService lotusBaccarat2BotService;

    private static boolean isClosing = false;

    private GameMapper gameMapper;

    private static Date sdate = DateUtils.beforeMinutes(2);

    @Async
    @Override
    public void parsingGame() {
        isClosing = false;

        int count = 0;
        int round = ZoneConfig.getLotusBaccarat().getZoneMaker().getLotusBaccaratRound();
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date(ZoneConfig.getLotusBaccarat().getZoneMaker().getLotusBaccaratGameDate().getTime() - 50 * 1000));

        for (int i = 0; i < 30; i++) {
            if (cal.getTime().before(sdate)) continue;

            if (lotusBaccarat2BotService.notExist(cal.getTime())) {
                LotusBaccarat2 lotusBaccarat2 = new LotusBaccarat2(round > 1728 ? round % 1728 : round, cal.getTime());
                lotusBaccarat2.setOdds(ZoneConfig.getLotusBaccarat2().getOdds());
                lotusBaccarat2BotService.addGame(lotusBaccarat2);
                count++;
            }
            round++;
            cal.add(Calendar.SECOND, 50);
        }
        sdate = cal.getTime();
        log.debug("로투스 바카라2 경기등록 : {}건", count);
    }

    @Async
    @Override
    public void closingGame() {

        //if (isClosing) return;
        //isClosing = true;

        // 한회차 땡겨 줘야 한다.
        //Date gameDate1 = new Date(ZoneConfig.getLotusBaccarat().getZoneMaker().getLotusBaccaratGameDate().getTime() - 2 * 50 * 1000);
        //Date gameDate2 = new Date(ZoneConfig.getLotusBaccarat().getZoneMaker().getLotusBaccaratGameDate().getTime() - 3 * 50 * 1000);

        String sdate = gameMapper.closingLotusBaccarat2Sdate();

        String json = null;
        if(StringUtils.notEmpty(sdate)) {
            json = HttpParsing.getJson(Config.getSysConfig().getZone().getLotusBaccarat2Url() + "/result?sdate=" + sdate);
        }

        if (json == null) {
            isClosing = false;
            return;
        }else{
            try {
                JSONParser parser = new JSONParser();
                JSONArray jsonArr = (JSONArray) parser.parse(json);
                if(jsonArr != null && jsonArr.size() > 0){
                    JSONObject obj = (JSONObject) jsonArr.get(0);
                    if(obj.containsKey("result") && obj.get("result") != null && !"".equals(obj.get("result").toString())){
                        json = jsonArr.get(0).toString();
                    }else{
                        return;
                    }
                }
            } catch (ParseException e) {
                e.printStackTrace();
                return;
            }
        }

        LotusBaccarat2 result = JsonUtils.toModel(json, LotusBaccarat2.class);

        lotusBaccarat2BotService.closingGame(result);

        log.debug("로투스 바카라2 경기 종료 : {}회차", result.getRound());
    }

    @Async
    @Override
    public void checkResult() {
        lotusBaccarat2BotService.checkResult();
    }

    @Async
    @Override
    public void deleteGame() {
        lotusBaccarat2BotService.deleteGame();
    }
}
