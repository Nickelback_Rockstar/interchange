package spoon.bot.zone.powerFreeKick;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import spoon.bot.zone.service.GameBotParsing;
import spoon.config.domain.Config;
import spoon.gameZone.ZoneConfig;

@Slf4j
@AllArgsConstructor
@Component
public class PowerFreeKickTask {

    private GameBotParsing powerFreeKickParsing;

    @Scheduled(cron = "55 2/5 * * * *")
    public void parsingGame() {
        if (notParsing()) return;
        powerFreeKickParsing.parsingGame();
    }

    @Scheduled(cron = "42,47 * * * * *")
    public void parsingResult() {
        if (notParsing()) return;
        powerFreeKickParsing.closingGame();
    }

    @Scheduled(cron = "55 4/5 * * * *")
    public void checkResult() {
        if (notParsing()) return;
        powerFreeKickParsing.checkResult();
    }

    @Scheduled(cron = "7 1 4 * * * ")
    public void deleteGame() {
        powerFreeKickParsing.deleteGame();
    }

    private boolean notParsing() {
        //파워프리킥
        return !Config.getSysConfig().getZone().isEnabled() ||
                !Config.getSysConfig().getZone().isPowerFreeKick() ||
                !ZoneConfig.getPowerFreeKick().isEnabled();
    }

}
