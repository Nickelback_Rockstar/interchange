package spoon.bot.zone.fxGame4;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import spoon.bot.zone.service.GameBotParsing;
import spoon.common.net.HttpParsing;
import spoon.common.utils.GameUtils;
import spoon.common.utils.JsonUtils;
import spoon.config.domain.Config;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.fxGame4.FxGame4;
import spoon.gameZone.fxGame4.service.FxGame4BotService;

import java.util.Calendar;
import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class FxGame4Parsing implements GameBotParsing {

    private FxGame4BotService fxGame4BotService;

    private static Date sdate = new Date();

    @Override
    public void parsingGame() {
        Calendar c = Calendar.getInstance();
        if(GameUtils.notFxGame(c)) return;

        int count = 0;
        Calendar cal = Calendar.getInstance();
        cal.setTime(ZoneConfig.getFxGame4().getZoneMaker().getGameDate());

        for (int i = 0; i < 10; i++) {
            if (cal.getTime().before(sdate)) continue;
            if (fxGame4BotService.notExist(cal.getTime())) {
                if(GameUtils.notFxGame(cal)) continue;
                int round = Integer.parseInt(cal.get(Calendar.HOUR_OF_DAY) + GameUtils.preFixZero(cal.get(Calendar.MINUTE)));
                FxGame4 fxGame4 = new FxGame4(round, cal.getTime());
                fxGame4.setOdds(ZoneConfig.getFxGame4().getOdds());
                fxGame4BotService.addGame(fxGame4);
                count++;
            }
            cal.add(Calendar.MINUTE, ZoneConfig.getFxGame4().getZoneMaker().getInterval());
        }
        sdate = cal.getTime();
        log.debug("fxGame4 경기등록 : {}건", count);
    }

    @Override
    public void closingGame() {
        Date gameDate = new Date(ZoneConfig.getFxGame4().getZoneMaker().getGameDate().getTime() - ZoneConfig.getFxGame4().getZoneMaker().getInterval() * 60 * 1000);

        String json = HttpParsing.getJson(Config.getSysConfig().getZone().getFxGame4Url());
        if (json == null) {
            return;
        }

        FxGame4 result = JsonUtils.toModel(json, FxGame4.class);
        if (result == null) {
            return;
        }

        if (!gameDate.equals(result.getGameDate())) {
            return;
        }

        fxGame4BotService.closingGame(result);

        log.debug("fxGame4 경기 종료 : {}회차", result.getRound());
    }

    @Override
    public void checkResult() {
        fxGame4BotService.checkResult();
    }

    @Override
    public void deleteGame() {
        fxGame4BotService.deleteGame(3);
    }


}
