package spoon.bot.zone.fxGame5;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import spoon.bot.zone.service.GameBotParsing;
import spoon.common.net.HttpParsing;
import spoon.common.utils.GameUtils;
import spoon.common.utils.JsonUtils;
import spoon.config.domain.Config;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.fxGame5.FxGame5;
import spoon.gameZone.fxGame5.service.FxGame5BotService;

import java.util.Calendar;
import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class FxGame5Parsing implements GameBotParsing {

    private FxGame5BotService fxGame5BotService;

    private static Date sdate = new Date();

    @Override
    public void parsingGame() {
        Calendar c = Calendar.getInstance();
        if(GameUtils.notFxGame(c)) return;

        int count = 0;
        Calendar cal = Calendar.getInstance();
        cal.setTime(ZoneConfig.getFxGame5().getZoneMaker().getGameDate());

        for (int i = 0; i < 10; i++) {
            if (cal.getTime().before(sdate)) continue;
            if (fxGame5BotService.notExist(cal.getTime())) {
                if(GameUtils.notFxGame(cal)) continue;
                int round = Integer.parseInt(cal.get(Calendar.HOUR_OF_DAY) + GameUtils.preFixZero(cal.get(Calendar.MINUTE)));
                FxGame5 fxGame5 = new FxGame5(round, cal.getTime());
                fxGame5.setOdds(ZoneConfig.getFxGame5().getOdds());
                fxGame5BotService.addGame(fxGame5);
                count++;
            }
            cal.add(Calendar.MINUTE, ZoneConfig.getFxGame5().getZoneMaker().getInterval());
        }
        sdate = cal.getTime();
        log.debug("fxGame5 경기등록 : {}건", count);
    }

    @Override
    public void closingGame() {
        Date gameDate = new Date(ZoneConfig.getFxGame5().getZoneMaker().getGameDate().getTime() - ZoneConfig.getFxGame5().getZoneMaker().getInterval() * 60 * 1000);

        String json = HttpParsing.getJson(Config.getSysConfig().getZone().getFxGame5Url());
        if (json == null) {
            return;
        }

        FxGame5 result = JsonUtils.toModel(json, FxGame5.class);
        if (result == null) {
            return;
        }

        if (!gameDate.equals(result.getGameDate())) {
            return;
        }

        fxGame5BotService.closingGame(result);

        log.debug("fxGame5 경기 종료 : {}회차", result.getRound());
    }

    @Override
    public void checkResult() {
        fxGame5BotService.checkResult();
    }

    @Override
    public void deleteGame() {
        fxGame5BotService.deleteGame(3);
    }


}
