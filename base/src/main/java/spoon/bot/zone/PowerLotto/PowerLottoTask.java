package spoon.bot.zone.PowerLotto;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import spoon.bot.zone.service.GameBotParsing;
import spoon.config.domain.Config;

@Slf4j
@AllArgsConstructor
@Component
public class PowerLottoTask {

    private GameBotParsing powerLottoParsing;

    @Scheduled(cron = "20 3/5 * * * *")
    public void parsingGame() {
        if (!Config.getSysConfig().getZone().isPowerLotto()) return;
        powerLottoParsing.parsingGame();
    }

}
