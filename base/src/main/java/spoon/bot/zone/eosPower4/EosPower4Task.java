package spoon.bot.zone.eosPower4;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import spoon.bot.zone.service.GameBotParsing;
import spoon.config.domain.Config;
import spoon.gameZone.ZoneConfig;

@Slf4j
@AllArgsConstructor
@Component
public class EosPower4Task {

    private GameBotParsing eosPower4Parsing;

    @Scheduled(cron = "0 0/4 * * * *")
    public void parsingGame() {
        if (notParsing()) return;
        eosPower4Parsing.parsingGame();
    }

    @Scheduled(cron = "10 * * * * *")
    public void parsingResult() {
        if (notParsing()) return;
        eosPower4Parsing.closingGame();
    }

    @Scheduled(cron = "5,8 4/4 * * * *")
    public void checkResult() {
        if (notParsing()) return;
        eosPower4Parsing.checkResult();
    }

    @Scheduled(cron = "7 1 4 * * * ")
    public void deleteGame() {
        eosPower4Parsing.deleteGame();
    }

    private boolean notParsing() {
        return !Config.getSysConfig().getZone().isEnabled() || !Config.getSysConfig().getZone().isEosPower4() || !ZoneConfig.getEosPower4().isEnabled();
    }

}
