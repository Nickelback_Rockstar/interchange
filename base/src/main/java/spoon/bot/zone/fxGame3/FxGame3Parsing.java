package spoon.bot.zone.fxGame3;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import spoon.bot.zone.service.GameBotParsing;
import spoon.common.net.HttpParsing;
import spoon.common.utils.GameUtils;
import spoon.common.utils.JsonUtils;
import spoon.config.domain.Config;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.fxGame3.FxGame3;
import spoon.gameZone.fxGame3.service.FxGame3BotService;

import java.util.Calendar;
import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class FxGame3Parsing implements GameBotParsing {

    private FxGame3BotService fxGame3BotService;

    private static Date sdate = new Date();

    @Override
    public void parsingGame() {
        Calendar c = Calendar.getInstance();
        if(GameUtils.notFxGame(c)) return;

        int count = 0;
        Calendar cal = Calendar.getInstance();
        cal.setTime(ZoneConfig.getFxGame3().getZoneMaker().getGameDate());

        for (int i = 0; i < 10; i++) {
            if (cal.getTime().before(sdate)) continue;
            if (fxGame3BotService.notExist(cal.getTime())) {
                if(GameUtils.notFxGame(cal)) continue;
                int round = Integer.parseInt(cal.get(Calendar.HOUR_OF_DAY) + GameUtils.preFixZero(cal.get(Calendar.MINUTE)));
                FxGame3 fxGame3 = new FxGame3(round, cal.getTime());
                fxGame3.setOdds(ZoneConfig.getFxGame3().getOdds());
                fxGame3BotService.addGame(fxGame3);
                count++;
            }
            cal.add(Calendar.MINUTE, ZoneConfig.getFxGame3().getZoneMaker().getInterval());
        }
        sdate = cal.getTime();
        log.debug("fxGame3 경기등록 : {}건", count);
    }

    @Override
    public void closingGame() {
        Date gameDate = new Date(ZoneConfig.getFxGame3().getZoneMaker().getGameDate().getTime() - ZoneConfig.getFxGame3().getZoneMaker().getInterval() * 60 * 1000);

        String json = HttpParsing.getJson(Config.getSysConfig().getZone().getFxGame3Url());
        if (json == null) {
            return;
        }

        FxGame3 result = JsonUtils.toModel(json, FxGame3.class);
        if (result == null) {
            return;
        }

        if (!gameDate.equals(result.getGameDate())) {
            return;
        }

        fxGame3BotService.closingGame(result);

        log.debug("fxGame3 경기 종료 : {}회차", result.getRound());
    }

    @Override
    public void checkResult() {
        fxGame3BotService.checkResult();
    }

    @Override
    public void deleteGame() {
        fxGame3BotService.deleteGame(3);
    }


}
