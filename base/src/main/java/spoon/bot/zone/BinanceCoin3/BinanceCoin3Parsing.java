package spoon.bot.zone.BinanceCoin3;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import spoon.bot.zone.service.GameBotParsing;
import spoon.common.net.HttpParsing;
import spoon.common.utils.JsonUtils;
import spoon.config.domain.Config;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.binanceCoin3.BinanceCoin3;
import spoon.gameZone.binanceCoin3.service.BinanceCoin3BotService;

import java.util.Calendar;
import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class BinanceCoin3Parsing implements GameBotParsing {

    private BinanceCoin3BotService binanceCoin3BotService;

    private static boolean isClosing = false;

    private static Date sdate = new Date();

    @Async
    @Override
    public void parsingGame() {
//        System.out.println("바이낸스코인3 파싱");
        isClosing = false;

        int count = 0;
        int round = ZoneConfig.getBinanceCoin3().getZoneMaker().getRound();
        Calendar cal = Calendar.getInstance();
        cal.setTime(ZoneConfig.getBinanceCoin3().getZoneMaker().getGameDate());

        for (int i = 0; i < 6; i++) {
            if (cal.getTime().before(sdate)) continue;

            if (binanceCoin3BotService.notExist(cal.getTime())) {
                BinanceCoin3 binanceCoin3 = new BinanceCoin3(round > 480 ? round % 480 : round, cal.getTime());
                binanceCoin3.setOdds(ZoneConfig.getBinanceCoin3().getOdds());
                binanceCoin3BotService.addGame(binanceCoin3);
                count++;
            }
            round++;
            cal.add(Calendar.MINUTE, 3);
        }
        sdate = cal.getTime();
        log.debug("비트코인3 경기등록 : {}건", count);
    }

    @Async
    @Override
    public void closingGame() {
        if (isClosing) return;
        isClosing = true;

        Date gameDate = new Date(ZoneConfig.getBinanceCoin3().getZoneMaker().getGameDate().getTime() - 3 * 60 * 1000);

        String json = HttpParsing.getJson(Config.getSysConfig().getZone().getBinanceCoin3Url());
        if (json == null) {
            isClosing = false;
            return;
        }

        BinanceCoin3 result = JsonUtils.toModel(json, BinanceCoin3.class);
        if (result == null) {
            isClosing = false;
            return;
        }

        if (!gameDate.equals(result.getGameDate())) {
            isClosing = false;
            return;
        }

        isClosing = binanceCoin3BotService.closingGame(result);

        log.debug("비트코인3 경기 종료 : {}회차", result.getRound());
    }

    @Async
    @Override
    public void checkResult() {
        binanceCoin3BotService.checkResult();
    }

    @Async
    @Override
    public void deleteGame() {
        binanceCoin3BotService.deleteGame(3);
    }
}
