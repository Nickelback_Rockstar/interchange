package spoon.bot.zone.BinanceCoin3;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import spoon.bot.zone.service.GameBotParsing;
import spoon.config.domain.Config;
import spoon.gameZone.ZoneConfig;

@Slf4j
@AllArgsConstructor
@Component
public class BinanceCoin3Task {

    private GameBotParsing binanceCoin3Parsing;

    @Scheduled(cron = "1 * * * * *")
    public void parsingGame() {
        if (notParsing()) return;
        binanceCoin3Parsing.parsingGame();
    }

    @Scheduled(cron = "8,11,15 * * * * *")
    public void parsingResult() {
        if (notParsing()) return;
        binanceCoin3Parsing.closingGame();
    }

    @Scheduled(cron = "2 * * * * *")
    public void checkResult() {
        if (notParsing()) return;
        binanceCoin3Parsing.checkResult();
    }

    @Scheduled(cron = "5 1 4 * * * ")
    public void deleteGame() {
        binanceCoin3Parsing.deleteGame();
    }

    private boolean notParsing() {
        return !Config.getSysConfig().getZone().isEnabled() || !Config.getSysConfig().getZone().isBinanceCoin3() || !ZoneConfig.getBinanceCoin3().isEnabled();
    }
}
