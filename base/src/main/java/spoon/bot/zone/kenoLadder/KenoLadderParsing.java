package spoon.bot.zone.kenoLadder;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import spoon.bot.zone.service.GameBotParsing;
import spoon.common.net.HttpParsing;
import spoon.common.utils.JsonUtils;
import spoon.config.domain.Config;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.kenoLadder.KenoLadder;
import spoon.gameZone.kenoLadder.service.KenoLadderBotService;

import java.util.Calendar;
import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class KenoLadderParsing implements GameBotParsing {

    private KenoLadderBotService kenoLadderBotService;

    private static boolean isClosing = false;

    private static Date sdate = new Date();

    @Async
    @Override
    public void parsingGame() {
        isClosing = false;

        int count = 0;
        int times = ZoneConfig.getKenoLadder().getPowerMaker().getTimes();
        Calendar cal = Calendar.getInstance();
        cal.setTime(ZoneConfig.getKenoLadder().getPowerMaker().getGameDate(times));

        for (int i = 0; i < 6; i++) {
            times++;
            cal.add(Calendar.MINUTE, 5);

            if (cal.getTime().before(sdate)) continue;

            if (kenoLadderBotService.notExist(cal.getTime())) {
                KenoLadder kenoLadder = new KenoLadder(times, cal.getTime());
                String hh = kenoLadder.getSdate().substring(8, 10);//시간
                if (Integer.parseInt(hh) < 6) {
                    //경기 생성중 6시 미만 경기는 생성 안한다.
                    continue;
                }
                kenoLadder.setOdds(ZoneConfig.getKenoLadder().getOdds());
                kenoLadderBotService.addGame(kenoLadder);
                count++;
            }
        }

        log.debug("키노 사다리 경기등록 : {}건", count);
    }

    @Async
    @Override
    public void closingGame() {
        //if (isClosing) return;
        isClosing = true;

        int times = ZoneConfig.getKenoLadder().getPowerMaker().getTimes();
        Date gameDate = ZoneConfig.getKenoLadder().getPowerMaker().getGameDate(times);

        String json = HttpParsing.getJson(Config.getSysConfig().getZone().getKenoLadderUrl());
        if (json == null) {
            isClosing = false;
            return;
        }

        KenoLadder result = JsonUtils.toModel(json, KenoLadder.class);
        if (result == null) {
            isClosing = false;
            return;
        }

        if (!gameDate.equals(result.getGameDate())) {
            isClosing = false;
            return;
        }

        isClosing = kenoLadderBotService.closingGame(result);

        log.debug("키노 사다리 경기 종료 : {}회차", result.getRound());
    }

    @Async
    @Override
    public void checkResult() {
        kenoLadderBotService.checkResult();
    }

    @Async
    @Override
    public void deleteGame() {
        kenoLadderBotService.deleteGame(3);
    }
}
