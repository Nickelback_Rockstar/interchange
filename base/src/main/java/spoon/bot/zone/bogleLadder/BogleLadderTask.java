package spoon.bot.zone.bogleLadder;

import lombok.AllArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@AllArgsConstructor
@Component
public class BogleLadderTask {

    private BogleLadderParsing bogleLadderParsing;

    @Scheduled(cron = "0 0/3 * * * ?")
    public void bogleLadderGameTask() {
        bogleLadderParsing.parsingGame();
    }

    @Scheduled(cron = "5,8 * * * * ?")
    public void bogleLadderClosingNtryTask() {
        bogleLadderParsing.closingGame();
    }

}
