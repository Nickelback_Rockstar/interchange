package spoon.bot.zone.bogleLadder;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import spoon.bot.zone.service.GameBotParsing;
import spoon.common.net.HttpParsing;
import spoon.common.utils.JsonUtils;
import spoon.config.domain.Config;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.bogleLadder.BogleLadder;
import spoon.gameZone.bogleLadder.service.BogleLadderBotService;

import java.util.Calendar;
import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class BogleLadderParsing implements GameBotParsing {

    private BogleLadderBotService bogleLadderBotService;

    private static boolean isClosing = false;

    private static Date sdate = new Date();

    @Override
    public void parsingGame() {
        isClosing = false;

        int count = 0;
        int round = ZoneConfig.getBogleLadder().getZoneMaker().getRound();
        Calendar cal = Calendar.getInstance();
        cal.setTime(ZoneConfig.getBogleLadder().getZoneMaker().getGameDate());

        for (int i = 0; i < 10; i++) {
            if (cal.getTime().before(sdate)) continue;
            if (bogleLadderBotService.notExist(cal.getTime())) {
                BogleLadder bogleLadder = new BogleLadder(round > 480 ? round % 480 : round, cal.getTime());
                bogleLadder.setOdds(ZoneConfig.getBogleLadder().getOdds());
                bogleLadderBotService.addGame(bogleLadder);
                count++;
            }
            round++;
            cal.add(Calendar.MINUTE, 3);
        }
        sdate = cal.getTime();
        log.debug("보글사다리 경기등록 : {}건", count);
    }

    @Override
    public void closingGame() {
        if (isClosing) return;
        isClosing = true;

        Date gameDate = new Date(ZoneConfig.getBogleLadder().getZoneMaker().getGameDate().getTime() - 3 * 60 * 1000);

        String json = HttpParsing.getJson(Config.getSysConfig().getZone().getBogleLadderUrl());
        if (json == null) {
            isClosing = false;
            return;
        }

        BogleLadder result = JsonUtils.toModel(json, BogleLadder.class);
        if (result == null) {
            isClosing = false;
            return;
        }

        if (!gameDate.equals(result.getGameDate())) {
            isClosing = false;
            return;
        }

        bogleLadderBotService.closingGame(result);

        log.debug("보글사다리 경기 종료 : {}회차", result.getRound());
    }

    @Override
    public void checkResult() {
        bogleLadderBotService.checkResult();
    }

    @Override
    public void deleteGame() {
        bogleLadderBotService.deleteGame(3);
    }


}
