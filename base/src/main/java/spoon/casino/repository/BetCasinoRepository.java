package spoon.casino.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import spoon.casino.entity.BetCasino;

public interface BetCasinoRepository extends JpaRepository<BetCasino, Long>, QueryDslPredicateExecutor<BetCasino> {
    
}
