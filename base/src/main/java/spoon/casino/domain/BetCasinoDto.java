package spoon.casino.domain;

import lombok.Data;
import spoon.common.utils.DateUtils;

import java.util.Date;

public class BetCasinoDto {

    @Data
    public static class Command {
        private String sdate = DateUtils.format(new Date());
        private String edate = DateUtils.format(new Date());
        private String orderBy = "";
        private String userid;
        private boolean match;
        private int page = 1;
        private int size = 20;

        public String getStart() {
            return this.sdate.replaceAll("\\.", "-");
        }
        public String getEnd() {
            return this.edate.replaceAll("\\.", "-");
        }
    }

    @Data
    public static class BetList {
        private String transId;
        private String transId2;
        private long amount;
        private long betAmount;
        private long winAmount;
        private String category;
        private String gameId;
        private String history;
        private Date regDate;
        private String roundId;
        private int thirdParty;
        private Date transTime;
        private String transType;

        private String casinoId;
        private String casinoNick;
        private String userid;
        private String nickname;
    }

}
