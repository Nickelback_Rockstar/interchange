package spoon.casino.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import spoon.casino.entity.BetCasino;
import spoon.casino.repository.BetCasinoRepository;
import spoon.common.net.HttpParsing;
import spoon.common.utils.DateUtils;
import spoon.common.utils.StringUtils;
import spoon.mapper.BetMapper;

import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class CasinoParsing {

    private BetCasinoRepository betCasinoRepository;
    private BetMapper betMapper;

    @Async
    public void parsing() {
        System.out.println("카지노 베팅내역 배치");

        String maxDate = betMapper.casinoMaxDate(); //마지막 시간에서 10분전

        String url = "http://fafagogo.com/block/casinoApi?mode=getBetWinHistoryAll" +
                "&endDate=" + DateUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss") +
                "&operatorID=Interchange" +
                "&pageSize=10000" +
                "&pageStart=1" +
                "&startDate=" + maxDate +
                "&vendorID=0";



        JSONObject jsonObj;
        String json = HttpParsing.getJson2(url);

        System.out.println("========================================================");
        System.out.println(url);
        System.out.println(json);
        System.out.println("========================================================");

        JSONParser parser = new JSONParser();
        if (!StringUtils.empty(json)) {
            try {
                jsonObj = (JSONObject) parser.parse(json);
                /*
                    {
                      "returnCode": 0,
                      "description": "Success",
                      "totalDataSize": 8,
                      "totalPageSize": 1,
                      "history": [
                            {
                              "gameID": "roulette",
                              "thirdParty": 45,
                              "amount": 0.0,
                              "transTime": "2019-06-27 21:43:49",
                              "transType": "WIN",
                              "transID": "560841020462533890_WIN_0",

                              "history": "{\"outcomes\":[{\"number\":\"1\",\"type\":\"Odd\",\"color\":\"Red\"}],\"luckyNumbers\":{\"15\":499,\"23\":99,\"12\":99}}",
                              "category": null,
                              "userID": "testcc",
                              "roundID": "15ac0ec95168fc75485a1fa4"
                            }
                       ]
                     }
                */

                if("0".equals(jsonObj.get("returnCode").toString())){
                    JSONArray arr = (JSONArray) jsonObj.get("history");
                    for(int i=0;i<arr.size();i++){
                        JSONObject j = (JSONObject) arr.get(i);
                        BetCasino b = new BetCasino();
                        b.setGameID(StringUtils.nvl(j.get("transID")));
                        b.setThirdParty(Integer.parseInt(j.get("thirdParty").toString()));
                        b.setAmount((long) Double.parseDouble((j.get("amount").toString())));
                        b.setTransTime(DateUtils.parse(StringUtils.nvl(j.get("transTime")), "yyyy-MM-dd HH:mm:ss"));
                        b.setTransType(StringUtils.nvl(j.get("transType")));
                        b.setTransID(StringUtils.nvl(j.get("transID")));
                        b.setHistory(StringUtils.nvl(j.get("history")));
                        b.setCategory(StringUtils.nvl(j.get("category")));
                        b.setUserID(StringUtils.nvl(j.get("userID")));
                        b.setRoundID(StringUtils.nvl(j.get("roundID")));
                        b.setRegDate(new Date());
                        betCasinoRepository.saveAndFlush(b);
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }


}
