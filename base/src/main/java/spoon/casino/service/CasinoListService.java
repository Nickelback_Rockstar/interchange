package spoon.casino.service;

import spoon.casino.domain.BetCasinoDto;

import java.util.List;

public interface CasinoListService {

    List<BetCasinoDto.BetList> casinoBetList(BetCasinoDto.Command command);

    long casinoBetListTotal(BetCasinoDto.Command command);
}
