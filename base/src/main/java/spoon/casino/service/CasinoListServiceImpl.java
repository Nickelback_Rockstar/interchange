package spoon.casino.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import spoon.casino.domain.BetCasinoDto;
import spoon.mapper.BetMapper;

import java.util.List;

@Slf4j
@AllArgsConstructor
@Service
public class CasinoListServiceImpl implements CasinoListService {

    private BetMapper betMapper;

    @Transactional(readOnly = true)
    @Override
    public List<BetCasinoDto.BetList> casinoBetList(BetCasinoDto.Command command) {
        return betMapper.casinoBetList(command);
    }


    @Transactional(readOnly = true)
    @Override
    public long casinoBetListTotal(BetCasinoDto.Command command) {
        return betMapper.casinoBetListTotal(command);
    }

}
