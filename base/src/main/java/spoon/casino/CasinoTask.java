package spoon.casino;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import spoon.casino.service.CasinoParsing;
import spoon.config.domain.Config;

@Slf4j
@AllArgsConstructor
@Component
public class CasinoTask {

    private CasinoParsing casinoParsing;

    @Scheduled(cron = "0 0/5 * * * ?")
    public void powerBalance() {
        if(Config.getSysConfig().getCasino().isEnabled()) {
            casinoParsing.parsing();
        }
    }


}
