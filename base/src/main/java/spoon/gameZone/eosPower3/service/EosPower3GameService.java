package spoon.gameZone.eosPower3.service;

import spoon.gameZone.ZoneDto;
import spoon.gameZone.eosPower3.EosPower3;
import spoon.support.web.AjaxResult;

public interface EosPower3GameService {

    /**
     * 파워볼 게임을 베팅한다.
     */
    AjaxResult betting(ZoneDto.Bet bet);

    /**
     * 파워볼 게임 베팅을 클로징 한다.
     */
    void closingBetting(EosPower3 power);

    /**
     * 파워볼 게임을 롤백 한다.
     */
    void rollbackPayment(EosPower3 power);
}
