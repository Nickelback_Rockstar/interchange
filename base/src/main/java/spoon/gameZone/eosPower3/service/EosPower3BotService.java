package spoon.gameZone.eosPower3.service;

import spoon.gameZone.eosPower3.EosPower3;

import java.util.Date;

public interface EosPower3BotService {

    boolean notExist(Date gameDate);

    void addGame(EosPower3 power);

    boolean closingGame(EosPower3 result);

    void checkResult();

    void deleteGame(int days);

}
