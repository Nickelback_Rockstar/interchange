package spoon.gameZone.fxGame5.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionInterceptor;
import spoon.common.utils.DateUtils;
import spoon.common.utils.ErrorUtils;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.fxGame5.FxGame5;
import spoon.gameZone.fxGame5.FxGame5Repository;
import spoon.gameZone.fxGame5.QFxGame5;

import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class FxGame5BotServiceImpl implements FxGame5BotService {

    private FxGame5GameService fxGame5GameService;

    private FxGame5Repository fxGame5Repository;

    private static QFxGame5 q = QFxGame5.fxGame5;

    @Transactional(readOnly = true)
    @Override
    public boolean notExist(Date gameDate) {
        return fxGame5Repository.count(q.sdate.eq(DateUtils.format(gameDate, "yyyyMMddHHmm"))) == 0;
    }

    @Transactional
    @Override
    public void addGame(FxGame5 fxGame5) {
        fxGame5Repository.saveAndFlush(fxGame5);
    }

    @Transactional
    @Override
    public boolean closingGame(FxGame5 result) {
        FxGame5 fxGame5 = fxGame5Repository.findOne(q.sdate.eq(result.getSdate()));
        if (fxGame5 == null) {
            return true;
        }

        try {
            fxGame5.setFxResult(result.getFxResult());
            fxGame5.setOddeven(result.getOddeven());
            fxGame5.setOverunder(result.getOverunder());
            fxGame5.setClosing(true);

            fxGame5Repository.saveAndFlush(fxGame5);
            fxGame5GameService.closingBetting(fxGame5);
        } catch (RuntimeException e) {
            log.error("fxGame5 {}회차 결과 업데이트에 실패하였습니다. - {}", fxGame5.getRound(), e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional(readOnly = true)
    @Override
    public void checkResult() {
        long cnt = fxGame5Repository.count(q.gameDate.before(new Date()).and(q.closing.isFalse()));
        ZoneConfig.getFxGame5().setResult(cnt);
    }

    @Transactional
    @Override
    public void deleteGame(int days) {
        fxGame5Repository.deleteGame(DateUtils.beforeDays(days));
    }
}
