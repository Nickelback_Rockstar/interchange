package spoon.gameZone.fxGame5.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.fxGame5.FxGame5;
import spoon.gameZone.fxGame5.FxGame5Config;
import spoon.gameZone.fxGame5.FxGame5Dto;
import spoon.support.web.AjaxResult;

public interface FxGame5Service {

    /**
     * FxGame5 설정을 변경한다.
     */
    boolean updateConfig(FxGame5Config fxGame5Config);

    /**
     * FxGame5 등록된 게임을 가져온다.
     */
    Iterable<FxGame5> getComplete();

    /**
     * FxGame5 종료된 게임을 가져온다.
     */
    Page<FxGame5> getClosing(ZoneDto.Command command, Pageable pageable);

    /**
     * FxGame5 봇에 접속하여 기존 결과가 있는지 확인 한다.
     */
    FxGame5Dto.Score findScore(Long id);

    /**
     * FxGame5 스코어를 가지고 결과처리를 한다.
     */
    boolean closingGame(FxGame5Dto.Score score);

    /**
     * 결과처리가 되지 않고 베팅이 없는 모든 경기를 종료처리 한다.
     */
    AjaxResult closingAllGame();

    /**
     * 현재 진행중인 경기의 설정을 가져온다.
     */
    FxGame5Dto.Config gameConfig(int r);

}
