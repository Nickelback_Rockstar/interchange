package spoon.gameZone.fxGame5.service;

import com.querydsl.core.BooleanBuilder;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionInterceptor;
import spoon.bet.entity.QBetItem;
import spoon.bet.repository.BetItemRepository;
import spoon.common.net.HttpParsing;
import spoon.common.utils.*;
import spoon.config.domain.Config;
import spoon.config.service.ConfigService;
import spoon.game.domain.MenuCode;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.fxGame5.*;
import spoon.member.domain.Role;
import spoon.member.entity.Member;
import spoon.member.service.MemberService;
import spoon.support.web.AjaxResult;

import java.util.Calendar;
import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class FxGame5ServiceImpl implements FxGame5Service {

    private ConfigService configService;

    private MemberService memberService;

    private FxGame5GameService fxGame5GameService;

    private FxGame5BotService fxGame5BotService;

    private FxGame5Repository fxGame5Repository;

    private BetItemRepository betItemRepository;

    private static QFxGame5 q = QFxGame5.fxGame5;

    @Transactional
    @Override
    public boolean updateConfig(FxGame5Config fxGame5Config) {
        try {
            configService.updateZoneConfig("fxGame5", JsonUtils.toString(fxGame5Config));
            ZoneConfig.setFxGame5(fxGame5Config);
            // 이미 등록된 게임의 배당을 변경한다.
            fxGame5Repository.updateOdds(ZoneConfig.getFxGame5().getOdds());
        } catch (RuntimeException e) {
            log.error("fxGame5 설정 변경에 실패하였습니다. - {}", e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional(readOnly = true)
    @Override
    public Iterable<FxGame5> getComplete() {
        return fxGame5Repository.findAll(q.closing.isFalse(), new Sort(Sort.Direction.ASC, "sdate"));
    }

    @Transactional(readOnly = true)
    @Override
    public Page<FxGame5> getClosing(ZoneDto.Command command, Pageable pageable) {
        BooleanBuilder builder = new BooleanBuilder(q.closing.isTrue());

        // 날짜별 검색
        if (StringUtils.notEmpty(command.getGameDate())) {
            builder.and(q.sdate.like(DateUtils.sdate(command.getGameDate())));
        }
        // 회차별 검색
        if (command.getRound() != null) {
            builder.and(q.round.eq(command.getRound()));
        }

        return fxGame5Repository.findAll(builder, pageable);
    }

    @Transactional(readOnly = true)
    @Override
    public FxGame5Dto.Score findScore(Long id) {
        FxGame5 fxGame5 = fxGame5Repository.findOne(id);

        FxGame5Dto.Score score = new FxGame5Dto.Score();
        score.setId(fxGame5.getId());
        score.setRound(fxGame5.getRound());
        score.setGameDate(fxGame5.getGameDate());
        score.setFxResult(fxGame5.getFxResult());
        score.setOddeven(fxGame5.getOddeven());
        score.setOverunder(fxGame5.getOverunder());
        score.setCancel(fxGame5.isCancel());

        // 봇 연결
        String json = HttpParsing.getJson(Config.getSysConfig().getZone().getFxGame5Url() + "?sdate=" + fxGame5.getSdate());
        if (json == null) return score;

        fxGame5 = JsonUtils.toModel(json, FxGame5.class);
        if (fxGame5 == null) return score;

        // 봇에 결과가 있다면
        if (fxGame5.isClosing()) {
            score.setFxResult(fxGame5.getFxResult());
            score.setOddeven(fxGame5.getOddeven());
            score.setOverunder(fxGame5.getOverunder());

            if (!"BUY".equals(score.getFxResult()) && !"SELL".equals(score.getFxResult())
                    && !"ODD".equals(score.getOddeven()) && !"EVEN".equals(score.getOddeven())
                    && !"OVER".equals(score.getOverunder()) && !"UNDER".equals(score.getOverunder())
            ) {
                score.setCancel(true);
            }
        }

        return score;
    }

    @Transactional
    @Override
    public boolean closingGame(FxGame5Dto.Score score) {
        FxGame5 fxGame5 = fxGame5Repository.findOne(score.getId());

        try {
            if (fxGame5.isChangeResult(score)) {
                // 현재 지급된 머니 포인트를 되돌린다.
                fxGame5GameService.rollbackPayment(fxGame5);
            }

            // 스코어 입력
            fxGame5.updateScore(score);
            fxGame5Repository.saveAndFlush(fxGame5);
            fxGame5GameService.closingBetting(fxGame5);
            fxGame5BotService.checkResult();
        } catch (RuntimeException e) {
            log.error("fxGame5 {} - {}회차 수동처리를 하지 못하였습니다. - {}", fxGame5.getGameDate(), fxGame5.getRound(), e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional
    @Override
    public AjaxResult closingAllGame() {
        QBetItem qi = QBetItem.betItem;
        int total = 0;
        int closing = 0;

        Iterable<FxGame5> iterable = fxGame5Repository.findAll(q.closing.isFalse().and(q.gameDate.before(DateUtils.beforeMinutes(5))));
        for (FxGame5 fxGame5 : iterable) {
            total++;
            long count = betItemRepository.count(qi.menuCode.eq(MenuCode.FXGAME5).and(qi.groupId.eq(fxGame5.getSdate())).and(qi.cancel.isFalse()));
            if (count > 0) continue;

            String json = HttpParsing.getJson(Config.getSysConfig().getZone().getFxGame5Url() + "?sdate=" + fxGame5.getSdate());
            if (json == null) continue;

            FxGame5 result = JsonUtils.toModel(json, FxGame5.class);
            if (result == null) continue;

            if (result.isClosing()) {
                fxGame5.setFxResult(result.getFxResult());
                fxGame5.setClosing(true);
                fxGame5Repository.saveAndFlush(fxGame5);
                closing++;
            }
        }
        fxGame5BotService.checkResult();
        return new AjaxResult(true, "전체 " + total + "경기중 " + closing + "경기를 종료처리 했습니다.");
    }

    @Override
    public FxGame5Dto.Config gameConfig(int r) {
        FxGame5Dto.Config gameConfig = new FxGame5Dto.Config();
        FxGame5Config config = ZoneConfig.getFxGame5();

        Date oriGameDate = config.getZoneMaker().getGameDate();
        Date gameDate = config.getZoneMaker().getGameDate();
        Calendar cal = Calendar.getInstance();
        cal.setTime(gameDate);
        cal.add(Calendar.MINUTE, (config.getZoneMaker().getInterval() * r));

        FxGame5 fxGame5 = fxGame5Repository.findOne(q.gameDate.eq(cal.getTime()));

        if (fxGame5 == null) {
            gameConfig.setGameDate2(oriGameDate);
            gameConfig.setGameDate(gameDate);
            gameConfig.setRound(0);
            return gameConfig;
        }

        String userid = WebUtils.userid();
        if (userid == null) return gameConfig;

        Member member = memberService.getMember(userid);
        int level = member.getLevel();
        gameConfig.setEnabled(config.isEnabled() && Config.getSysConfig().getZone().isFxGame5());
        if (member.getRole() == Role.DUMMY) {
            gameConfig.setMoney(10000000);
        } else {
            gameConfig.setMoney(member.getMoney());
        }
        gameConfig.setNextRound(r);
        gameConfig.setGameDate2(oriGameDate);
        gameConfig.setGameDate(fxGame5.getGameDate());
        gameConfig.setSdate(fxGame5.getSdate());
        gameConfig.setRound(fxGame5.getRound());
        gameConfig.setWin(config.getWin()[level]);
        gameConfig.setMax(config.getMax()[level]);
        gameConfig.setMin(config.getMin()[level]);
        gameConfig.setOdds(fxGame5.getOdds());

        int betTime = (int) (oriGameDate.getTime() - new Date().getTime() - config.getBetTime() * 1000) / 1000;
        if (betTime < 0) betTime = 0;
        gameConfig.setBetTime(betTime);

        gameConfig.setFxResult(config.isFxResult());
        gameConfig.setOddeven(config.isOddeven());
        gameConfig.setOverunder(config.isOverunder());

        return gameConfig;
    }
}
