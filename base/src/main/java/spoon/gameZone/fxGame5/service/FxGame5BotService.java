package spoon.gameZone.fxGame5.service;

import spoon.gameZone.fxGame5.FxGame5;

import java.util.Date;

public interface FxGame5BotService {

    boolean notExist(Date gameDate);

    void addGame(FxGame5 fxGame5);

    boolean closingGame(FxGame5 result);

    void checkResult();

    void deleteGame(int days);

}
