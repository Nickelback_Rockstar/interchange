package spoon.gameZone.lotusBaccarat;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import spoon.bot.support.ZoneMaker;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class LotusBaccaratConfig {

    @JsonIgnore
    private ZoneMaker zoneMaker;

    private boolean enabled;

    // 결과처리가 잘 되었는지 체크 (관리자에서 사용)
    private long result;

    private int betTime;

    // 회차별 베팅 숫자
    private int betMax;

    /**
     * 0:PLAYER(baccarat), 1:TIE(baccarat), 2:BANKER(baccarat)
     * 3:PLAYER PAIR(baccarat), 4:PLAYER PAIR LOSE(baccarat)
     * 5:BANKER PAIR(baccarat), 6:BANKER PAIR LOSE(baccarat)
     */
    private double[] odds = new double[7];

    private int[] win = {3000000, 3000000, 3000000, 3000000, 3000000, 3000000, 3000000, 3000000, 3000000, 3000000, 3000000};

    private int[] max = {1000000, 1000000, 1000000, 1000000, 1000000, 1000000, 1000000, 1000000, 1000000, 1000000, 1000000};

    private int[] min = {5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000};

    public LotusBaccaratConfig() {
        this.zoneMaker = new ZoneMaker(1);
    }

}
