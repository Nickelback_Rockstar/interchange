package spoon.gameZone.lotusBaccarat.service;

import com.querydsl.core.BooleanBuilder;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionInterceptor;
import spoon.bet.entity.QBetItem;
import spoon.bet.repository.BetItemRepository;
import spoon.common.net.HttpParsing;
import spoon.common.utils.*;
import spoon.config.domain.Config;
import spoon.config.service.ConfigService;
import spoon.game.domain.MenuCode;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.lotusBaccarat.*;
import spoon.mapper.GameMapper;
import spoon.member.domain.Role;
import spoon.member.entity.Member;
import spoon.member.service.MemberService;
import spoon.support.web.AjaxResult;

import java.time.zone.ZoneRulesException;
import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class LotusBaccaratServiceImpl implements LotusBaccaratService {

    private ConfigService configService;

    private MemberService memberService;

    private LotusBaccaratBotService lotusBaccaratBotService;

    private LotusBaccaratGameService lotusBaccaratGameService;

    private LotusBaccaratRepository lotusBaccaratRepository;

    private BetItemRepository betItemRepository;

    private static QLotusBaccarat q = QLotusBaccarat.lotusBaccarat;

    private GameMapper gameMapper;

    @Transactional
    @Override
    public boolean updateConfig(LotusBaccaratConfig lotusBaccaratConfig) {
        try {
            configService.updateZoneConfig("lotusBaccarat", JsonUtils.toString(lotusBaccaratConfig));
            ZoneConfig.setLotusBaccarat(lotusBaccaratConfig);

            // 이미 등록된 게임의 배당을 변경한다.
            lotusBaccaratRepository.updateOdds(ZoneConfig.getLotusBaccarat().getOdds());
        } catch (RuntimeException e) {
            log.error("바카라 설정 변경에 실패하였습니다. - {}", e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional(readOnly = true)
    @Override
    public Iterable<LotusBaccarat> getComplete() {
        return lotusBaccaratRepository.findAll(q.closing.isFalse(), new Sort(Sort.Direction.ASC, "sdate"));
    }

    @Transactional(readOnly = true)
    @Override
    public Page<LotusBaccarat> getClosing(ZoneDto.Command command, Pageable pageable) {
        BooleanBuilder builder = new BooleanBuilder(q.closing.isTrue());

        // 날짜별 검색
        if (StringUtils.notEmpty(command.getGameDate())) {
            builder.and(q.sdate.like(DateUtils.sdate(command.getGameDate())));
        }
        // 회차별 검색
        if (command.getRound() != null) {
            builder.and(q.round.eq(command.getRound()));
        }

        return lotusBaccaratRepository.findAll(builder, pageable);
    }

    @Transactional(readOnly = true)
    @Override
    public LotusBaccaratDto.Score findScore(Long id) {
        LotusBaccarat lotusBaccarat = lotusBaccaratRepository.findOne(id);

        LotusBaccaratDto.Score score = new LotusBaccaratDto.Score();
        score.setId(lotusBaccarat.getId());
        score.setRound(lotusBaccarat.getRound());
        score.setGameDate(lotusBaccarat.getGameDate());
        score.convertCard(lotusBaccarat.getP1(), lotusBaccarat.getP2(), lotusBaccarat.getP3(), lotusBaccarat.getB1(), lotusBaccarat.getB2(), lotusBaccarat.getB3());
        score.setCancel(lotusBaccarat.isCancel());

        // 봇 연결
        String json = HttpParsing.getJson(Config.getSysConfig().getZone().getLotusBaccaratUrl() + "?sdate=" + lotusBaccarat.getSdate());
        if (json == null) return score;

        lotusBaccarat = JsonUtils.toModel(json, LotusBaccarat.class);
        if (lotusBaccarat == null) return score;

        // 봇에 결과가 있다면
        if (lotusBaccarat.isClosing()) {
            //score.convertCard(lotusBaccarat.getP1(), lotusBaccarat.getP2(), lotusBaccarat.getP3(), lotusBaccarat.getB1(), lotusBaccarat.getB2(), lotusBaccarat.getB3());
            score.setP1(lotusBaccarat.getP1());
            score.setP2(lotusBaccarat.getP2());
            score.setP3(lotusBaccarat.getP3());
            score.setB1(lotusBaccarat.getB1());
            score.setB2(lotusBaccarat.getB2());
            score.setB3(lotusBaccarat.getB3());

            if (StringUtils.empty(lotusBaccarat.getP1())) {
                score.setCancel(true);
            }
        }
        return score;
    }

    @Transactional
    @Override
    public boolean closingGame(LotusBaccaratDto.Score score) {
        LotusBaccarat lotusBaccarat = lotusBaccaratRepository.findOne(score.getId());

        try {
            // 스코어 입력
            boolean success = score.convertCardNew();
            if (!success) {
                throw new ZoneRulesException("결과를 변환하지 못하였습니다.");
            }

            if (lotusBaccarat.isChangeResult(score)) {
                // 현재 지급된 머니 포인트를 되돌린다.
                lotusBaccaratGameService.rollbackPayment(lotusBaccarat);
            }

            lotusBaccarat.updateScore(score);

            lotusBaccaratRepository.saveAndFlush(lotusBaccarat);
            lotusBaccaratGameService.closingBetting(lotusBaccarat);
            lotusBaccaratBotService.checkResult();

        } catch (RuntimeException e) {
            log.error("바카라 {} - {}회차 수동처리를 하지 못하였습니다. - {}", lotusBaccarat.getGameDate(), lotusBaccarat.getRound(), e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional
    @Override
    public AjaxResult closingAllGame() {
        QBetItem qi = QBetItem.betItem;
        int total = 0;
        int closing = 0;

        Iterable<LotusBaccarat> iterable = lotusBaccaratRepository.findAll(q.closing.isFalse().and(q.gameDate.before(DateUtils.beforeMinutes(1))));
        for (LotusBaccarat lotusBaccarat : iterable) {
            total++;
            long count = betItemRepository.count(qi.menuCode.eq(MenuCode.LBACCARAT).and(qi.groupId.eq(lotusBaccarat.getSdate())).and(qi.cancel.isFalse()));
            if (count > 0) continue;

            String json = HttpParsing.getJson(Config.getSysConfig().getZone().getLotusBaccaratUrl() + "?sdate=" + lotusBaccarat.getSdate());
            if (json == null) continue;

            LotusBaccarat result = JsonUtils.toModel(json, LotusBaccarat.class);
            if (result == null) continue;

            if (result.isClosing()) {
                lotusBaccarat.setP1(result.getP1());
                lotusBaccarat.setP2(result.getP2());
                lotusBaccarat.setP3(result.getP3());
                lotusBaccarat.setB1(result.getB1());
                lotusBaccarat.setB2(result.getB2());
                lotusBaccarat.setB3(result.getB3());
                lotusBaccarat.setResult(result.getResult());

                lotusBaccarat.setClosing(true);
                lotusBaccaratRepository.saveAndFlush(lotusBaccarat);
                closing++;
            }
        }

        lotusBaccaratBotService.checkResult();
        return new AjaxResult(true, "전체 " + total + "경기중 " + closing + "경기를 종료처리 했습니다.");
    }

    @Override
    public LotusBaccaratDto.Config gameConfig() {
        LotusBaccaratDto.Config gameConfig = new LotusBaccaratDto.Config();
        LotusBaccaratConfig config = ZoneConfig.getLotusBaccarat();

        // 홀짝 바카라는 1분씩 댕겨 줘야 한다.
        Date gameDate = new Date(config.getZoneMaker().getLotusBaccaratGameDate().getTime() - 1000 * 50);
//        System.out.println("gameDate="+gameDate);
        LotusBaccarat lotusBaccarat = lotusBaccaratRepository.findOne(q.gameDate.eq(gameDate));

        if (lotusBaccarat == null) {
//            System.out.println("gameDate="+gameDate);
            gameConfig.setGameDate(gameDate);
            gameConfig.setRound(config.getZoneMaker().getLotusBaccaratRound());
            return gameConfig;
        }

        String userid = WebUtils.userid();
        if (userid == null) return gameConfig;

        Member member = memberService.getMember(userid);
        int level = member.getLevel();
        gameConfig.setEnabled(config.isEnabled() && Config.getSysConfig().getZone().isLotusBaccarat());
        if (member.getRole() == Role.DUMMY) {
            gameConfig.setMoney(10000000);
        } else {
            gameConfig.setMoney(member.getMoney());
        }
        gameConfig.setGameDate(lotusBaccarat.getGameDate());
        gameConfig.setSdate(lotusBaccarat.getSdate());
        gameConfig.setRound(lotusBaccarat.getRound());
        gameConfig.setWin(config.getWin()[level]);
        gameConfig.setMax(config.getMax()[level]);
        gameConfig.setMin(config.getMin()[level]);
        gameConfig.setOdds(lotusBaccarat.getOdds());

        // 60초 보정
        int betTime = (int) (lotusBaccarat.getGameDate().getTime() - new Date().getTime() - config.getBetTime() * 1000 + 50000) / 1000;
        if (betTime < 0) betTime = 0;
        gameConfig.setBetTime(betTime);

        return gameConfig;
    }

    @Transactional
    @Override
    public AjaxResult deleteAllGame() {
        gameMapper.deleteLotusBaccarat();
        return new AjaxResult(true, "베팅, 결과없는 경기 삭제처리 했습니다.");
    }
}
