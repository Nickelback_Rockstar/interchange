package spoon.gameZone.lotusBaccarat.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionInterceptor;
import spoon.common.utils.DateUtils;
import spoon.common.utils.ErrorUtils;
import spoon.common.utils.StringUtils;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.lotusBaccarat.LotusBaccarat;
import spoon.gameZone.lotusBaccarat.LotusBaccaratRepository;
import spoon.gameZone.lotusBaccarat.QLotusBaccarat;
import spoon.mapper.GameMapper;

import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class LotusBaccaratBotServiceImpl implements LotusBaccaratBotService {

    private LotusBaccaratGameService lotusBaccaratGameService;

    private LotusBaccaratRepository lotusBaccaratRepository;

    private GameMapper gameMapper;

    private static QLotusBaccarat q = QLotusBaccarat.lotusBaccarat;

    @Transactional(readOnly = true)
    @Override
    public boolean notExist(Date gameDate) {
        return lotusBaccaratRepository.count(q.sdate.eq(DateUtils.format(gameDate, "yyyyMMddHHmmss"))) == 0;
    }

    @Transactional
    @Override
    public void addGame(LotusBaccarat lotusBaccarat) {
        lotusBaccaratRepository.saveAndFlush(lotusBaccarat);
    }

    @Transactional
    @Override
    public boolean closingGame(LotusBaccarat result) {
        LotusBaccarat lotusBaccarat = lotusBaccaratRepository.findOne(q.sdate.eq(result.getSdate()));



        if (lotusBaccarat == null) {
            return true;
        }

        try {
            lotusBaccarat.setP1(result.getP1());
            lotusBaccarat.setP2(result.getP2());
            lotusBaccarat.setP3(result.getP3());
            lotusBaccarat.setB1(result.getB1());
            lotusBaccarat.setB2(result.getB2());
            lotusBaccarat.setB3(result.getB3());
            lotusBaccarat.setPp(result.isPp());
            lotusBaccarat.setBp(result.isBp());
            lotusBaccarat.setResult(result.getResult());

            if (StringUtils.empty(result.getP1())) {
                lotusBaccarat.setCancel(true);
            } else {
                lotusBaccarat.setCancel(false);
            }
            lotusBaccarat.setClosing(true);


            lotusBaccaratRepository.saveAndFlush(lotusBaccarat);
            lotusBaccaratGameService.closingBetting(lotusBaccarat);

        } catch (RuntimeException e) {
            log.error("바카라 {}회차 결과 업데이트에 실패하였습니다. - {}", lotusBaccarat.getRound(), e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional(readOnly = true)
    @Override
    public void checkResult() {
        long cnt = lotusBaccaratRepository.count(q.gameDate.before(DateUtils.beforeSeconds(100)).and(q.closing.isFalse()));
        ZoneConfig.getLotusBaccarat().setResult(cnt);
    }

    @Transactional
    @Override
    public void deleteGame() {
        gameMapper.deleteOldLotusBaccarat();
    }

}
