package spoon.gameZone.bogleLadder.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionInterceptor;
import spoon.common.utils.DateUtils;
import spoon.common.utils.ErrorUtils;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.bogleLadder.BogleLadder;
import spoon.gameZone.bogleLadder.BogleLadderRepository;
import spoon.gameZone.bogleLadder.QBogleLadder;

import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class BogleLadderBotServiceImpl implements BogleLadderBotService {

    private BogleLadderGameService bogleLadderGameService;

    private BogleLadderRepository bogleLadderRepository;

    private static QBogleLadder q = QBogleLadder.bogleLadder;

    @Transactional(readOnly = true)
    @Override
    public boolean notExist(Date gameDate) {
        return bogleLadderRepository.count(q.sdate.eq(DateUtils.format(gameDate, "yyyyMMddHHmm"))) == 0;
    }

    @Transactional
    @Override
    public void addGame(BogleLadder bogleLadder) {
        bogleLadderRepository.saveAndFlush(bogleLadder);
    }

    @Transactional
    @Override
    public boolean closingGame(BogleLadder result) {
        BogleLadder bogleLadder = bogleLadderRepository.findOne(q.sdate.eq(result.getSdate()));
        if (bogleLadder == null) {
            return true;
        }

        try {
            bogleLadder.setStart(result.getStart());
            bogleLadder.setLine(result.getLine());
            bogleLadder.setOddeven(result.getOddeven());
            bogleLadder.setClosing(true);

            bogleLadderRepository.saveAndFlush(bogleLadder);
            bogleLadderGameService.closingBetting(bogleLadder);
        } catch (RuntimeException e) {
            log.error("보글사다리 {}회차 결과 업데이트에 실패하였습니다. - {}", bogleLadder.getRound(), e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional(readOnly = true)
    @Override
    public void checkResult() {
        long cnt = bogleLadderRepository.count(q.gameDate.before(new Date()).and(q.closing.isFalse()));
        ZoneConfig.getBogleLadder().setResult(cnt);
    }

    @Transactional
    @Override
    public void deleteGame(int days) {
        bogleLadderRepository.deleteGame(DateUtils.beforeDays(days));
    }
}
