package spoon.gameZone.bogleLadder.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.bogleLadder.BogleLadder;
import spoon.gameZone.bogleLadder.BogleLadderConfig;
import spoon.gameZone.bogleLadder.BogleLadderDto;
import spoon.support.web.AjaxResult;

public interface BogleLadderService {

    /**
     * BogleLadder 설정을 변경한다.
     */
    boolean updateConfig(BogleLadderConfig BogleLadderConfig);

    /**
     * BogleLadder 등록된 게임을 가져온다.
     */
    Iterable<BogleLadder> getComplete();

    /**
     * BogleLadder 종료된 게임을 가져온다.
     */
    Page<BogleLadder> getClosing(ZoneDto.Command command, Pageable pageable);

    /**
     * BogleLadder 봇에 접속하여 기존 결과가 있는지 확인 한다.
     */
    BogleLadderDto.Score findScore(Long id);

    /**
     * BogleLadder 스코어를 가지고 결과처리를 한다.
     */
    boolean closingGame(BogleLadderDto.Score score);

    /**
     * 결과처리가 되지 않고 베팅이 없는 모든 경기를 종료처리 한다.
     */
    AjaxResult closingAllGame();

    /**
     * 현재 진행중인 경기의 설정을 가져온다.
     */
    BogleLadderDto.Config gameConfig();

}
