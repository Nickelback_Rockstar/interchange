package spoon.gameZone.bogleLadder.service;

import spoon.gameZone.ZoneDto;
import spoon.gameZone.bogleLadder.BogleLadder;
import spoon.support.web.AjaxResult;

public interface BogleLadderGameService {

    /**
     * bogleLadder 게임을 베팅한다.
     */
    AjaxResult betting(ZoneDto.Bet bet);

    /**
     * bogleLadder 게임 베팅을 클로징 한다.
     */
    void closingBetting(BogleLadder ladder);

    /**
     * bogleLadder 게임을 롤백 한다.
     */
    void rollbackPayment(BogleLadder ladder);
}
