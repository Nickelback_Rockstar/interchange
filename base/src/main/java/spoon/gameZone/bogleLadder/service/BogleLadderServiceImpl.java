package spoon.gameZone.bogleLadder.service;

import com.querydsl.core.BooleanBuilder;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionInterceptor;
import spoon.bet.entity.QBetItem;
import spoon.bet.repository.BetItemRepository;
import spoon.common.net.HttpParsing;
import spoon.common.utils.*;
import spoon.config.domain.Config;
import spoon.config.service.ConfigService;
import spoon.game.domain.MenuCode;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.bogleLadder.*;
import spoon.member.domain.Role;
import spoon.member.entity.Member;
import spoon.member.service.MemberService;
import spoon.support.web.AjaxResult;

import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class BogleLadderServiceImpl implements BogleLadderService {

    private ConfigService configService;

    private MemberService memberService;

    private BogleLadderGameService bogleLadderGameService;

    private BogleLadderBotService bogleLadderBotService;

    private BogleLadderRepository bogleLadderRepository;

    private BetItemRepository betItemRepository;

    private static QBogleLadder q = QBogleLadder.bogleLadder;

    @Transactional
    @Override
    public boolean updateConfig(BogleLadderConfig bogleLadderConfig) {
        try {
            configService.updateZoneConfig("bogleLadder", JsonUtils.toString(bogleLadderConfig));
            ZoneConfig.setBogleLadder(bogleLadderConfig);
            // 이미 등록된 게임의 배당을 변경한다.
            bogleLadderRepository.updateOdds(ZoneConfig.getBogleLadder().getOdds());
        } catch (RuntimeException e) {
            log.error("bogleLadder 설정 변경에 실패하였습니다. - {}", e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional(readOnly = true)
    @Override
    public Iterable<BogleLadder> getComplete() {
        return bogleLadderRepository.findAll(q.closing.isFalse(), new Sort(Sort.Direction.ASC, "sdate"));
    }

    @Transactional(readOnly = true)
    @Override
    public Page<BogleLadder> getClosing(ZoneDto.Command command, Pageable pageable) {
        BooleanBuilder builder = new BooleanBuilder(q.closing.isTrue());

        // 날짜별 검색
        if (StringUtils.notEmpty(command.getGameDate())) {
            builder.and(q.sdate.like(DateUtils.sdate(command.getGameDate())));
        }
        // 회차별 검색
        if (command.getRound() != null) {
            builder.and(q.round.eq(command.getRound()));
        }

        return bogleLadderRepository.findAll(builder, pageable);
    }

    @Transactional(readOnly = true)
    @Override
    public BogleLadderDto.Score findScore(Long id) {
        BogleLadder bogleLadder = bogleLadderRepository.findOne(id);

        BogleLadderDto.Score score = new BogleLadderDto.Score();
        score.setId(bogleLadder.getId());
        score.setRound(bogleLadder.getRound());
        score.setGameDate(bogleLadder.getGameDate());
        score.setStart(bogleLadder.getStart());
        score.setLine(bogleLadder.getLine());
        score.setOddeven(bogleLadder.getOddeven());
        score.setCancel(bogleLadder.isCancel());

        // 봇 연결
        String json = HttpParsing.getJson(Config.getSysConfig().getZone().getBogleLadderUrl() + "?sdate=" + bogleLadder.getSdate());
        if (json == null) return score;

        bogleLadder = JsonUtils.toModel(json, BogleLadder.class);
        if (bogleLadder == null) return score;

        // 봇에 결과가 있다면
        if (bogleLadder.isClosing()) {
            score.setStart(bogleLadder.getStart());
            score.setLine(bogleLadder.getLine());
            score.setOddeven(bogleLadder.getOddeven());

            if (!"ODD".equals(score.getOddeven()) && !"EVEN".equals(score.getOddeven())) {
                score.setCancel(true);
            }
        }

        return score;
    }

    @Transactional
    @Override
    public boolean closingGame(BogleLadderDto.Score score) {
        BogleLadder bogleLadder = bogleLadderRepository.findOne(score.getId());

        try {
            if (bogleLadder.isChangeResult(score)) {
                // 현재 지급된 머니 포인트를 되돌린다.
                bogleLadderGameService.rollbackPayment(bogleLadder);
            }

            // 스코어 입력
            bogleLadder.updateScore(score);
            bogleLadderRepository.saveAndFlush(bogleLadder);
            bogleLadderGameService.closingBetting(bogleLadder);
            bogleLadderBotService.checkResult();
        } catch (RuntimeException e) {
            log.error("bogleLadder {} - {}회차 수동처리를 하지 못하였습니다. - {}", bogleLadder.getGameDate(), bogleLadder.getRound(), e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional
    @Override
    public AjaxResult closingAllGame() {
        QBetItem qi = QBetItem.betItem;
        int total = 0;
        int closing = 0;

        Iterable<BogleLadder> iterable = bogleLadderRepository.findAll(q.closing.isFalse().and(q.gameDate.before(DateUtils.beforeMinutes(3))));
        for (BogleLadder bogleLadder : iterable) {
            total++;
            long count = betItemRepository.count(qi.menuCode.eq(MenuCode.BOGLELADDER).and(qi.groupId.eq(bogleLadder.getSdate())).and(qi.cancel.isFalse()));
            if (count > 0) continue;

            String json = HttpParsing.getJson(Config.getSysConfig().getZone().getBogleLadderUrl() + "?sdate=" + bogleLadder.getSdate());
            if (json == null) continue;

            BogleLadder result = JsonUtils.toModel(json, BogleLadder.class);
            if (result == null) continue;

            if (result.isClosing()) {
                bogleLadder.setStart(result.getStart());
                bogleLadder.setLine(result.getLine());
                bogleLadder.setOddeven(result.getOddeven());
                bogleLadder.setClosing(true);
                bogleLadderRepository.saveAndFlush(bogleLadder);
                closing++;
            }
        }
        bogleLadderBotService.checkResult();
        return new AjaxResult(true, "전체 " + total + "경기중 " + closing + "경기를 종료처리 했습니다.");
    }

    @Override
    public BogleLadderDto.Config gameConfig() {
        BogleLadderDto.Config gameConfig = new BogleLadderDto.Config();
        BogleLadderConfig config = ZoneConfig.getBogleLadder();

        Date gameDate = config.getZoneMaker().getGameDate();
        BogleLadder bogleLadder = bogleLadderRepository.findOne(q.gameDate.eq(gameDate));

        if (bogleLadder == null) {
            gameConfig.setGameDate(gameDate);
            gameConfig.setRound(config.getZoneMaker().getRound());
            return gameConfig;
        }

        String userid = WebUtils.userid();
        if (userid == null) return gameConfig;

        Member member = memberService.getMember(userid);
        int level = member.getLevel();
        gameConfig.setEnabled(config.isEnabled() && Config.getSysConfig().getZone().isBogleLadder());
        if (member.getRole() == Role.DUMMY) {
            gameConfig.setMoney(10000000);
        } else {
            gameConfig.setMoney(member.getMoney());
        }
        gameConfig.setGameDate(bogleLadder.getGameDate());
        gameConfig.setSdate(bogleLadder.getSdate());
        gameConfig.setRound(bogleLadder.getRound());
        gameConfig.setWin(config.getWin()[level]);
        gameConfig.setMax(config.getMax()[level]);
        gameConfig.setMin(config.getMin()[level]);
        gameConfig.setOdds(bogleLadder.getOdds());

        int betTime = (int) (bogleLadder.getGameDate().getTime() - new Date().getTime() - config.getBetTime() * 1000) / 1000;
        if (betTime < 0) betTime = 0;
        gameConfig.setBetTime(betTime);

        gameConfig.setOddeven(config.isOddeven());
        gameConfig.setStart(config.isStart());
        gameConfig.setLine(config.isLine());
        gameConfig.setLineStart(config.isLineStart());

        return gameConfig;
    }
}
