package spoon.gameZone.bogleLadder.service;

import spoon.gameZone.bogleLadder.BogleLadder;

import java.util.Date;

public interface BogleLadderBotService {

    boolean notExist(Date gameDate);

    void addGame(BogleLadder bogleLadder);

    boolean closingGame(BogleLadder result);

    void checkResult();

    void deleteGame(int days);

}
