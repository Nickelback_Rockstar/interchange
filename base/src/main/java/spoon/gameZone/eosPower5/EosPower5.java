package spoon.gameZone.eosPower5;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;
import spoon.bot.support.ZoneHelper;
import spoon.common.utils.DateUtils;
import spoon.game.domain.MenuCode;
import spoon.gameZone.GameZoneException;
import spoon.gameZone.Zone;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.ZoneScore;
import spoon.support.convert.DoubleArrayConvert;
import spoon.support.convert.LongArrayConvert;

import javax.persistence.*;
import java.util.Arrays;
import java.util.Date;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Data
@Entity
@Table(name = "ZONE_EOSPOWER5", indexes = {
        @Index(name = "IDX_sdate", columnList = "sdate")
})
public class EosPower5 {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Long id;

    @Temporal(value = TemporalType.TIMESTAMP)
    private Date gameDate;

    private int round;

    private int times;

    // 일반볼 콤마로 연결
    @Column(length = 64)
    private String ball;

    // 파워볼
    @Column(length = 16)
    private String pb;

    @Column(length = 16)
    private String oddeven;

    @Column(length = 16)
    private String odd_overunder;

    @Column(length = 16)
    private String even_overunder;

    @Column(length = 16)
    private String pb_oddeven;

    @Column(length = 16)
    private String pb_odd_overunder;

    @Column(length = 16)
    private String pb_even_overunder;

    @Column(length = 16)
    private String overunder;

    @Column(length = 16)
    private String pb_overunder;

    @Column(length = 16)
    private String pb_odd_pb_overunder;

    @Column(length = 16)
    private String pb_even_pb_overunder;

    @Column(length = 16)
    private String size;

    @Column(length = 16)
    private String odd_size;

    @Column(length = 16)
    private String even_size;

    private int sum;

    @Column(length = 16)
    private String sdate;

    @Convert(converter = LongArrayConvert.class)
    private long[] amount = new long[29];

    @Convert(converter = DoubleArrayConvert.class)
    private double[] odds = new double[29];

    private boolean closing;

    private boolean cancel;

    //----------------------------------------------------------------

    public EosPower5(int round, Date gameDate) {
        this.round = round;
        this.gameDate = gameDate;
        this.sdate = DateUtils.format(gameDate, "yyyyMMddHHmm");
    }

    public boolean isBeforeGameDate() {
        return this.gameDate.getTime() - System.currentTimeMillis() > 0;
    }

    // 스코어 입력
    public void updateScore(EosPower5Dto.Score score) {
        if (score.isCancel()) {
            this.pb = "";
            this.ball = "";
            this.oddeven = "";
            this.odd_overunder = "";
            this.even_overunder = "";
            this.pb_oddeven = "";
            this.pb_odd_overunder = "";
            this.pb_even_overunder = "";
            this.overunder = "";
            this.pb_overunder = "";
            this.pb_odd_pb_overunder = "";
            this.pb_even_pb_overunder = "";
            this.size = "";
            this.odd_size = "";
            this.even_size = "";
            this.cancel = true;
        } else {
            int[] balls = Arrays.stream(score.getBall().trim().split(",")).mapToInt(x -> Integer.parseInt(x, 10)).toArray();
            int pball = Integer.parseInt(score.getPb(), 10);
            int sum = IntStream.of(balls).sum();

            //일반홀짝
            this.oddeven = sum % 2 == 1 ? "ODD" : "EVEN";

            //일반홀 일반오버언더
            if(sum % 2 == 1 && sum < 73){
                this.odd_overunder = "ODD_UNDER";
            }else if(sum % 2 == 1 && sum >= 73){
                this.odd_overunder = "ODD_OVER";
            }else{
                this.odd_overunder = "";
            }

            //일반짝 일반오버언더
            if(sum % 2 == 0 && sum < 73){
                this.even_overunder = "EVEN_UNDER";
            }else if(sum % 2 == 0 && sum >= 73){
                this.even_overunder = "EVEN_OVER";
            }else{
                this.even_overunder = "";
            }

            //파워홀짝
            this.pb_oddeven = pball % 2 == 1 ? "ODD" : "EVEN";

            //일반홀 파워오버언더
            if(sum % 2 == 1 && pball < 5){
                this.pb_odd_overunder = "ODD_UNDER";
            }else if(sum % 2 == 1 && pball >= 5){
                this.pb_odd_overunder = "ODD_OVER";
            }else{
                this.pb_odd_overunder = "";
            }

            //일반짝 파워오버언더
            if(sum % 2 == 0 && pball < 5){
                this.pb_even_overunder = "EVEN_UNDER";
            }else if(sum % 2 == 0 && pball >= 5){
                this.pb_even_overunder = "EVEN_OVER";
            }else{
                this.pb_even_overunder = "";
            }

            this.overunder = sum < 73 ? "UNDER" : "OVER";
            this.pb_overunder = pball < 5 ? "UNDER" : "OVER";
            this.size = sum > 80 ? "대" : (sum < 65 ? "소" : "중");

            //일반홀 파워오버언더
            if(sum % 2 == 1 && sum > 80){
                this.odd_size = "홀대";
            }else if(sum % 2 == 1 && sum < 65){
                this.odd_size = "홀소";
            }else if(sum % 2 == 1){
                this.odd_size = "홀중";
            }else{
                this.odd_size = "";
            }

            //일반짝 파워오버언더
            if(sum % 2 == 0 && sum > 80){
                this.even_size = "짝대";
            }else if(sum % 2 == 0 && sum < 65){
                this.even_size = "짝소";
            }else if(sum % 2 == 0){
                this.even_size = "짝중";
            }else{
                this.even_size = "";
            }

            //파워홀 파워오버언더
            if(pball % 2 == 1 && pball < 5){
                this.pb_odd_pb_overunder = "PODD_PUNDER";
            }else if(pball % 2 == 1 && pball >= 5){
                this.pb_odd_pb_overunder = "PODD_POVER";
            }else{
                this.pb_odd_pb_overunder = "";
            }

            //파워짝 파워오버언더
            if(pball % 2 == 0 && pball < 5){
                this.pb_even_pb_overunder = "PEVEN_PUNDER";
            }else if(pball % 2 == 0 && pball >= 5){
                this.pb_even_pb_overunder = "PEVEN_POVER";
            }else{
                this.pb_even_pb_overunder = "";
            }

            this.pb = score.getPb();
            this.ball = Arrays.stream(balls).mapToObj(String::valueOf).collect(Collectors.joining(","));
            this.sum = sum;
            this.cancel = false;

        }
        this.closing = true;
    }

    // 경기 결과
    public ZoneScore getGameResult(String gameCode) {
        switch (gameCode) {
            case "oddeven":
                return ZoneHelper.zoneResult(oddeven, cancel);
            case "odd_overunder":
                return ZoneHelper.zoneResult(odd_overunder, cancel);
            case "even_overunder":
                return ZoneHelper.zoneResult(even_overunder, cancel);
            case "pb_oddeven":
                return ZoneHelper.zoneResult(pb_oddeven, cancel);
            case "pb_odd_overunder":
                return ZoneHelper.zoneResult(pb_odd_overunder, cancel);
            case "pb_even_overunder":
                return ZoneHelper.zoneResult(pb_even_overunder, cancel);
            case "overunder":
                return ZoneHelper.zoneResult(overunder, cancel);
            case "pb_overunder":
                return ZoneHelper.zoneResult(pb_overunder, cancel);
            case "pb_odd_pb_overunder":
                return ZoneHelper.zoneResult(pb_odd_pb_overunder , cancel);
            case "pb_even_pb_overunder":
                return ZoneHelper.zoneResult(pb_even_pb_overunder , cancel);
            case "size":
                return ZoneHelper.zoneResult(size, cancel);
            case "odd_size":
                return ZoneHelper.zoneResult(odd_size, cancel);
            case "even_size":
                return ZoneHelper.zoneResult(even_size, cancel);
            default:
                throw new GameZoneException("EOS파워볼 5분 코드 " + gameCode + " 를 확인 할 수 없습니다.");
        }
    }

    public Zone getZone(String gameCode) {
        switch (gameCode) {
            case "oddeven":
                return getPowerZone("oddeven", "일반볼", "홀", "짝", 0, 1);
            case "pb_oddeven":
                return getPowerZone("pb_oddeven", "파워볼", "홀", "짝", 2, 3);
            case "overunder":
                return getPowerZone("overunder", "일반볼", "오버", "언더", 4, 5);
            case "pb_overunder":
                return getPowerZone("pb_overunder", "파워볼", "오버", "언더", 6, 7);
            case "size":
                Zone zone = getPowerZone("size", "일반볼", "대", "소", 8, 10);
                zone.setOddsDraw(ZoneConfig.getEosPower5().getOdds()[9]);
                return zone;
            case "odd_overunder":
                return getPowerZone("odd_overunder", "일반볼", "홀 + 오버", "홀 + 언더", 11, 12);
            case "even_overunder":
                return getPowerZone("even_overunder", "일반볼", "짝 + 오버", "짝 + 언더", 13, 14);
            case "pb_odd_overunder":
                return getPowerZone("pb_odd_overunder", "일반볼 + 파워볼", "홀 + 오버", "홀 + 언더", 15, 16);
            case "pb_even_overunder":
                return getPowerZone("pb_even_overunder", "일반볼 + 파워볼", "짝 + 오버", "짝 + 언더", 17, 18);
            case "pb_odd_pb_overunder":
                return getPowerZone("pb_odd_pb_overunder", "파워볼", "홀 + 오버", "홀 + 언더", 25, 26);
            case "pb_even_pb_overunder":
                return getPowerZone("pb_even_pb_overunder", "파워볼", "짝 + 오버", "짝 + 언더", 27, 28);
            case "odd_size":
                Zone zone1 = getPowerZone("odd_size", "일반볼", "홀 + 대", "홀 + 소", 19, 21);
                zone1.setOddsDraw(ZoneConfig.getEosPower5().getOdds()[20]);
                return zone1;
            case "even_size":
                Zone zone2 = getPowerZone("even_size", "일반볼", "짝 + 대", "짝 + 소", 22, 24);
                zone2.setOddsDraw(ZoneConfig.getEosPower5().getOdds()[23]);
                return zone2;
            default:
                throw new GameZoneException("EOS파워볼 5분 코드 " + gameCode + " 를 확인 할 수 없습니다.");
        }
    }

    private Zone getPowerZone(String gameCode, String type, String teamHome, String teamAway, int idxHome, int idxAway) {
        Zone zone = new Zone();
        zone.setId(this.getId());
        zone.setSdate(this.getSdate());
        zone.setMenuCode(MenuCode.EOSPOWER5);
        zone.setGameCode(gameCode);
        zone.setLeague(String.format("%03d회차 EOS파워볼 5분", this.round));
        zone.setTeamHome(String.format("%03d회차 %s [%s]", this.round, type, teamHome));
        zone.setTeamAway(String.format("%03d회차 %s [%s]", this.round, type, teamAway));
        zone.setHandicap(0);
        zone.setOddsHome(ZoneConfig.getEosPower5().getOdds()[idxHome]);
        zone.setOddsDraw(0);
        zone.setOddsAway(ZoneConfig.getEosPower5().getOdds()[idxAway]);
        zone.setGameDate(this.gameDate);
        return zone;
    }

    public boolean isChangeResult(EosPower5Dto.Score score) {
        return !this.closing || this.cancel != score.isCancel()
                || !(score.getPb().equals(this.pb) && score.getBall().equals(this.ball));

    }
}
