package spoon.gameZone.eosPower5.service;

import spoon.gameZone.eosPower5.EosPower5;

import java.util.Date;

public interface EosPower5BotService {

    boolean notExist(Date gameDate);

    void addGame(EosPower5 power);

    boolean closingGame(EosPower5 result);

    void checkResult();

    void deleteGame(int days);

}
