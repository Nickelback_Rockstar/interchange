package spoon.gameZone.tokenRoulette.service;

import spoon.gameZone.tokenRoulette.TkRouletteBetDto;
import spoon.gameZone.tokenRoulette.TkRouletteBetInfoDto;
import spoon.gameZone.tokenRoulette.TkRouletteResultDto;
import spoon.support.web.AjaxResult;

public interface TkRouletteGameService {

    AjaxResult betting(TkRouletteBetInfoDto betInfoDto, TkRouletteBetDto betDto);

    void closingBetting(TkRouletteBetInfoDto betInfoDto, TkRouletteBetDto betDto, TkRouletteResultDto resultDto);

}
