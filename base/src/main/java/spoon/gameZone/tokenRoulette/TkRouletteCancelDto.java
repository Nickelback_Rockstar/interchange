package spoon.gameZone.tokenRoulette;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class TkRouletteCancelDto {

        @JsonProperty("money")
        private String money;

        @JsonProperty("date")
        private String date;

        @JsonProperty("time")
        private String time;

}
