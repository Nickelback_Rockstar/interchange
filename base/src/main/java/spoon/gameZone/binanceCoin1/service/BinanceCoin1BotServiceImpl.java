package spoon.gameZone.binanceCoin1.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionInterceptor;
import spoon.common.utils.DateUtils;
import spoon.common.utils.ErrorUtils;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.binanceCoin1.BinanceCoin1;
import spoon.gameZone.binanceCoin1.BinanceCoin1Repository;
import spoon.gameZone.binanceCoin1.QBinanceCoin1;

import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class BinanceCoin1BotServiceImpl implements BinanceCoin1BotService {

    private BinanceCoin1GameService binanceCoin1GameService;

    private BinanceCoin1Repository binanceCoin1Repository;

    private static QBinanceCoin1 q = QBinanceCoin1.binanceCoin1;

    @Transactional(readOnly = true)
    @Override
    public boolean notExist(Date gameDate) {
        return binanceCoin1Repository.count(q.sdate.eq(DateUtils.format(gameDate, "yyyyMMddHHmm"))) == 0;
    }

    @Transactional
    @Override
    public void addGame(BinanceCoin1 binanceCoin1) {
        binanceCoin1Repository.saveAndFlush(binanceCoin1);
    }

    @Transactional
    @Override
    public boolean closingGame(BinanceCoin1 result) {
        BinanceCoin1 binanceCoin1 = binanceCoin1Repository.findOne(q.sdate.eq(result.getSdate()));
        if (binanceCoin1 == null) {
            return true;
        }

        if (binanceCoin1.isClosing()) {
            return true;
        }

        try {
            binanceCoin1.setLowHigh(result.getLowHigh());
            binanceCoin1.setOverUnder(result.getOverUnder());
            binanceCoin1.setOddEven(result.getOddEven());
            binanceCoin1.setClosing(true);

            binanceCoin1Repository.saveAndFlush(binanceCoin1);
            binanceCoin1GameService.closingBetting(binanceCoin1);
        } catch (RuntimeException e) {
            log.error("NEW달팽이 {}회차 결과 업데이트에 실패하였습니다. - {}", binanceCoin1.getRound(), e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional(readOnly = true)
    @Override
    public void checkResult() {
        long cnt = binanceCoin1Repository.count(q.gameDate.before(new Date()).and(q.closing.isFalse()));
        ZoneConfig.getBinanceCoin1().setResult(cnt);
    }

    @Transactional
    @Override
    public void deleteGame(int days) {
        binanceCoin1Repository.deleteGame(DateUtils.beforeDays(days));
    }
}
