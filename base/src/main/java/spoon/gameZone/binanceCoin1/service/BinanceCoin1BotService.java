package spoon.gameZone.binanceCoin1.service;

import spoon.gameZone.binanceCoin1.BinanceCoin1;

import java.util.Date;

public interface BinanceCoin1BotService {

    boolean notExist(Date gameDate);

    void addGame(BinanceCoin1 binanceCoin1);

    boolean closingGame(BinanceCoin1 binanceCoin1);

    void checkResult();

    void deleteGame(int days);

}
