package spoon.gameZone.binanceCoin1.service;

import spoon.gameZone.ZoneDto;
import spoon.gameZone.binanceCoin1.BinanceCoin1;
import spoon.support.web.AjaxResult;

public interface BinanceCoin1GameService {

    /**
     * 달팽이 게임을 베팅한다.
     */
    AjaxResult betting(ZoneDto.Bet bet);

    /**
     * 달팽이 게임 베팅을 클로징 한다.
     */
    void closingBetting(BinanceCoin1 binanceCoin1);

    /**
     * 달팽이 게임을 롤백 한다.
     */
    void rollbackPayment(BinanceCoin1 binanceCoin1);
}
