package spoon.gameZone.lotusBaccarat2.service;

import com.querydsl.core.BooleanBuilder;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionInterceptor;
import spoon.bet.entity.QBetItem;
import spoon.bet.repository.BetItemRepository;
import spoon.common.net.HttpParsing;
import spoon.common.utils.*;
import spoon.config.domain.Config;
import spoon.config.service.ConfigService;
import spoon.game.domain.MenuCode;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.lotusBaccarat2.*;
import spoon.mapper.GameMapper;
import spoon.member.domain.Role;
import spoon.member.entity.Member;
import spoon.member.service.MemberService;
import spoon.support.web.AjaxResult;

import java.time.zone.ZoneRulesException;
import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class LotusBaccarat2ServiceImpl implements LotusBaccarat2Service {

    private ConfigService configService;

    private MemberService memberService;

    private LotusBaccarat2BotService lotusBaccarat2BotService;

    private LotusBaccarat2GameService lotusBaccarat2GameService;

    private LotusBaccarat2Repository lotusBaccarat2Repository;

    private BetItemRepository betItemRepository;

    private GameMapper gameMapper;

    private static QLotusBaccarat2 q = QLotusBaccarat2.lotusBaccarat2;

    @Transactional
    @Override
    public boolean updateConfig(LotusBaccarat2Config lotusBaccarat2Config) {
        try {
            configService.updateZoneConfig("lotusBaccarat2", JsonUtils.toString(lotusBaccarat2Config));
            ZoneConfig.setLotusBaccarat2(lotusBaccarat2Config);

            // 이미 등록된 게임의 배당을 변경한다.
            lotusBaccarat2Repository.updateOdds(ZoneConfig.getLotusBaccarat2().getOdds());
        } catch (RuntimeException e) {
            log.error("바카라2 설정 변경에 실패하였습니다. - {}", e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional(readOnly = true)
    @Override
    public Iterable<LotusBaccarat2> getComplete() {
        return lotusBaccarat2Repository.findAll(q.closing.isFalse(), new Sort(Sort.Direction.ASC, "sdate"));
    }

    @Transactional(readOnly = true)
    @Override
    public Page<LotusBaccarat2> getClosing(ZoneDto.Command command, Pageable pageable) {
        BooleanBuilder builder = new BooleanBuilder(q.closing.isTrue());

        // 날짜별 검색
        if (StringUtils.notEmpty(command.getGameDate())) {
            builder.and(q.sdate.like(DateUtils.sdate(command.getGameDate())));
        }
        // 회차별 검색
        if (command.getRound() != null) {
            builder.and(q.round.eq(command.getRound()));
        }

        return lotusBaccarat2Repository.findAll(builder, pageable);
    }

    @Transactional(readOnly = true)
    @Override
    public LotusBaccarat2Dto.Score findScore(Long id) {
        LotusBaccarat2 lotusBaccarat2 = lotusBaccarat2Repository.findOne(id);

        LotusBaccarat2Dto.Score score = new LotusBaccarat2Dto.Score();
        score.setId(lotusBaccarat2.getId());
        score.setRound(lotusBaccarat2.getRound());
        score.setGameDate(lotusBaccarat2.getGameDate());
        score.convertCard(lotusBaccarat2.getP1(), lotusBaccarat2.getP2(), lotusBaccarat2.getP3(), lotusBaccarat2.getB1(), lotusBaccarat2.getB2(), lotusBaccarat2.getB3());
        score.setCancel(lotusBaccarat2.isCancel());

        // 봇 연결
        String json = HttpParsing.getJson(Config.getSysConfig().getZone().getLotusBaccarat2Url() + "?sdate=" + lotusBaccarat2.getSdate());
        if (json == null) return score;

        lotusBaccarat2 = JsonUtils.toModel(json, LotusBaccarat2.class);
        if (lotusBaccarat2 == null) return score;

        // 봇에 결과가 있다면
        if (lotusBaccarat2.isClosing()) {
            //score.convertCard(lotusBaccarat2.getP1(), lotusBaccarat2.getP2(), lotusBaccarat2.getP3(), lotusBaccarat2.getB1(), lotusBaccarat2.getB2(), lotusBaccarat2.getB3());
            score.setP1(lotusBaccarat2.getP1());
            score.setP2(lotusBaccarat2.getP2());
            score.setP3(lotusBaccarat2.getP3());
            score.setB1(lotusBaccarat2.getB1());
            score.setB2(lotusBaccarat2.getB2());
            score.setB3(lotusBaccarat2.getB3());
            if (StringUtils.empty(lotusBaccarat2.getP1())) {
                score.setCancel(true);
            }
        }
        return score;
    }

    @Transactional
    @Override
    public boolean closingGame(LotusBaccarat2Dto.Score score) {
        LotusBaccarat2 lotusBaccarat2 = lotusBaccarat2Repository.findOne(score.getId());

        try {
            // 스코어 입력
            boolean success = score.convertCardNew();
            if (!success) {
                throw new ZoneRulesException("결과를 변환하지 못하였습니다.");
            }

            if (lotusBaccarat2.isChangeResult(score)) {
                // 현재 지급된 머니 포인트를 되돌린다.
                lotusBaccarat2GameService.rollbackPayment(lotusBaccarat2);
            }

            lotusBaccarat2.updateScore(score);

            lotusBaccarat2Repository.saveAndFlush(lotusBaccarat2);
            lotusBaccarat2GameService.closingBetting(lotusBaccarat2);
            lotusBaccarat2BotService.checkResult();

        } catch (RuntimeException e) {
            log.error("바카라 {} - {}회차 수동처리를 하지 못하였습니다. - {}", lotusBaccarat2.getGameDate(), lotusBaccarat2.getRound(), e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional
    @Override
    public AjaxResult closingAllGame() {
        QBetItem qi = QBetItem.betItem;
        int total = 0;
        int closing = 0;

        Iterable<LotusBaccarat2> iterable = lotusBaccarat2Repository.findAll(q.closing.isFalse().and(q.gameDate.before(DateUtils.beforeMinutes(1))));
        for (LotusBaccarat2 lotusBaccarat2 : iterable) {
            total++;
            long count = betItemRepository.count(qi.menuCode.eq(MenuCode.LBACCARAT2).and(qi.groupId.eq(lotusBaccarat2.getSdate())).and(qi.cancel.isFalse()));
            if (count > 0) continue;

            String json = HttpParsing.getJson(Config.getSysConfig().getZone().getLotusBaccarat2Url() + "?sdate=" + lotusBaccarat2.getSdate());
            if (json == null) continue;

            LotusBaccarat2 result = JsonUtils.toModel(json, LotusBaccarat2.class);
            if (result == null) continue;

            if (result.isClosing()) {
                lotusBaccarat2.setP1(result.getP1());
                lotusBaccarat2.setP2(result.getP2());
                lotusBaccarat2.setP3(result.getP3());
                lotusBaccarat2.setB1(result.getB1());
                lotusBaccarat2.setB2(result.getB2());
                lotusBaccarat2.setB3(result.getB3());
                lotusBaccarat2.setResult(result.getResult());

                lotusBaccarat2.setClosing(true);
                lotusBaccarat2Repository.saveAndFlush(lotusBaccarat2);
                closing++;
            }
        }

        lotusBaccarat2BotService.checkResult();
        return new AjaxResult(true, "전체 " + total + "경기중 " + closing + "경기를 종료처리 했습니다.");
    }

    @Override
    public LotusBaccarat2Dto.Config gameConfig() {
        LotusBaccarat2Dto.Config gameConfig = new LotusBaccarat2Dto.Config();
        LotusBaccarat2Config config = ZoneConfig.getLotusBaccarat2();

        // 홀짝 바카라는 1분씩 댕겨 줘야 한다.
        Date gameDate = new Date(config.getZoneMaker().getLotusBaccaratGameDate().getTime() - 1000 * 50);
//        System.out.println("gameDate="+gameDate);
        LotusBaccarat2 lotusBaccarat2 = lotusBaccarat2Repository.findOne(q.gameDate.eq(gameDate));

        if (lotusBaccarat2 == null) {
//            System.out.println("gameDate="+gameDate);
            gameConfig.setGameDate(gameDate);
            gameConfig.setRound(config.getZoneMaker().getLotusBaccaratRound());
            return gameConfig;
        }

        String userid = WebUtils.userid();
        if (userid == null) return gameConfig;

        Member member = memberService.getMember(userid);
        int level = member.getLevel();
        gameConfig.setEnabled(config.isEnabled() && Config.getSysConfig().getZone().isLotusBaccarat2());
        if (member.getRole() == Role.DUMMY) {
            gameConfig.setMoney(10000000);
        } else {
            gameConfig.setMoney(member.getMoney());
        }
        gameConfig.setGameDate(lotusBaccarat2.getGameDate());
        gameConfig.setSdate(lotusBaccarat2.getSdate());
        gameConfig.setRound(lotusBaccarat2.getRound());
        gameConfig.setWin(config.getWin()[level]);
        gameConfig.setMax(config.getMax()[level]);
        gameConfig.setMin(config.getMin()[level]);
        gameConfig.setOdds(lotusBaccarat2.getOdds());

        // 60초 보정
        int betTime = (int) (lotusBaccarat2.getGameDate().getTime() - new Date().getTime() - config.getBetTime() * 1000 + 50000) / 1000;
        if (betTime < 0) betTime = 0;
        gameConfig.setBetTime(betTime);

        return gameConfig;
    }

    @Transactional
    @Override
    public AjaxResult deleteAllGame() {
        gameMapper.deleteLotusBaccarat2();
        return new AjaxResult(true, "베팅, 결과없는 경기 삭제처리 했습니다.");
    }
}
