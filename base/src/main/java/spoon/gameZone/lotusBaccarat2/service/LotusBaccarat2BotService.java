package spoon.gameZone.lotusBaccarat2.service;

import spoon.gameZone.lotusBaccarat2.LotusBaccarat2;

import java.util.Date;

public interface LotusBaccarat2BotService {

    boolean notExist(Date gameDate);

    void addGame(LotusBaccarat2 baccarat2);

    boolean closingGame(LotusBaccarat2 result);

    void checkResult();

    void deleteGame();

}
