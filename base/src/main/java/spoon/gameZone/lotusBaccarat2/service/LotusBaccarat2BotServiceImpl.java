package spoon.gameZone.lotusBaccarat2.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionInterceptor;
import spoon.common.utils.DateUtils;
import spoon.common.utils.ErrorUtils;
import spoon.common.utils.StringUtils;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.lotusBaccarat2.LotusBaccarat2;
import spoon.gameZone.lotusBaccarat2.LotusBaccarat2Repository;
import spoon.gameZone.lotusBaccarat2.QLotusBaccarat2;
import spoon.mapper.GameMapper;

import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class LotusBaccarat2BotServiceImpl implements LotusBaccarat2BotService {

    private LotusBaccarat2GameService lotusBaccarat2GameService;

    private LotusBaccarat2Repository lotusBaccarat2Repository;

    private static QLotusBaccarat2 q = QLotusBaccarat2.lotusBaccarat2;

    private GameMapper gameMapper;

    @Transactional(readOnly = true)
    @Override
    public boolean notExist(Date gameDate) {
        return lotusBaccarat2Repository.count(q.sdate.eq(DateUtils.format(gameDate, "yyyyMMddHHmmss"))) == 0;
    }

    @Transactional
    @Override
    public void addGame(LotusBaccarat2 lotusBaccarat2) {
        lotusBaccarat2Repository.saveAndFlush(lotusBaccarat2);
    }

    @Transactional
    @Override
    public boolean closingGame(LotusBaccarat2 result) {
        LotusBaccarat2 lotusBaccarat2 = lotusBaccarat2Repository.findOne(q.sdate.eq(result.getSdate()));
        if (lotusBaccarat2 == null) {
            return true;
        }

        try {
            lotusBaccarat2.setP1(result.getP1());
            lotusBaccarat2.setP2(result.getP2());
            lotusBaccarat2.setP3(result.getP3());
            lotusBaccarat2.setB1(result.getB1());
            lotusBaccarat2.setB2(result.getB2());
            lotusBaccarat2.setB3(result.getB3());
            lotusBaccarat2.setPp(result.isPp());
            lotusBaccarat2.setBp(result.isBp());
            lotusBaccarat2.setResult(result.getResult());

            if (StringUtils.empty(result.getP1()) || "C".equals(result.getResult())) {
                lotusBaccarat2.setCancel(true);
            } else {
                lotusBaccarat2.setCancel(false);
            }
            lotusBaccarat2.setClosing(true);

            lotusBaccarat2Repository.saveAndFlush(lotusBaccarat2);
            lotusBaccarat2GameService.closingBetting(lotusBaccarat2);

        } catch (RuntimeException e) {
            log.error("바카라 {}회차 결과 업데이트에 실패하였습니다. - {}", lotusBaccarat2.getRound(), e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional(readOnly = true)
    @Override
    public void checkResult() {
        long cnt = lotusBaccarat2Repository.count(q.gameDate.before(DateUtils.beforeSeconds(100)).and(q.closing.isFalse()));
        ZoneConfig.getLotusBaccarat2().setResult(cnt);
    }

    @Transactional
    @Override
    public void deleteGame() {
        gameMapper.deleteOldLotusBaccarat2();
    }

}
