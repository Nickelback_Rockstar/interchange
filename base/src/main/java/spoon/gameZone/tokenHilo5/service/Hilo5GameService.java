package spoon.gameZone.tokenHilo5.service;

import spoon.gameZone.tokenHilo5.Hilo5BetDto;
import spoon.gameZone.tokenHilo5.Hilo5BetInfoDto;
import spoon.gameZone.tokenHilo5.Hilo5ResultDto;
import spoon.support.web.AjaxResult;

public interface Hilo5GameService {

    /**
     * hilo5초 게임을 베팅한다.
     */
    AjaxResult betting(Hilo5BetInfoDto betInfoDto, Hilo5BetDto betDto);

    void closingBetting(Hilo5BetInfoDto betInfoDto, Hilo5BetDto betDto, Hilo5ResultDto resultDto);

}
