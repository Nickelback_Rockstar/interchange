package spoon.gameZone.tokenHilo5;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class Hilo5ResultDto {

        @JsonProperty("result")
        private String result;

        @JsonProperty("rate")
        private String rate;

        @JsonProperty("money")
        private String money;

        @JsonProperty("date")
        private String date;

        @JsonProperty("time")
        private String time;

}
