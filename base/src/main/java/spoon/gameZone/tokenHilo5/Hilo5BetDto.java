package spoon.gameZone.tokenHilo5;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import spoon.common.utils.StringUtils;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class Hilo5BetDto {


        @JsonProperty("transactionid ")
        private String transactionid1;

        @JsonProperty("transactionid")
        private String transactionid2;

        @JsonProperty("type")
        private String type;

        @JsonProperty("rate")
        private String rate;

        @JsonProperty("money")
        private long money;

        @JsonProperty("date")
        private String date;

        @JsonProperty("time")
        private String time;

        @JsonProperty("bet_ip")
        private String bet_ip;

        public String getTransactionid(){
                return StringUtils.notEmpty(this.transactionid1) ? this.transactionid1 : this.transactionid2;

        }
}
