package spoon.gameZone.fxGame2;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import spoon.bot.support.ZoneHelper;
import spoon.common.utils.DateUtils;
import spoon.game.domain.MenuCode;
import spoon.gameZone.GameZoneException;
import spoon.gameZone.Zone;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.ZoneScore;
import spoon.support.convert.DoubleArrayConvert;
import spoon.support.convert.LongArrayConvert;

import javax.persistence.*;
import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Data
@Entity
@Table(name = "ZONE_FXGAME2", indexes = {
        @Index(name = "IDX_sdate", columnList = "sdate")
})
public class FxGame2 {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Long id;

    @Temporal(value = TemporalType.TIMESTAMP)
    private Date gameDate;

    private int round;

    @JsonProperty("finishResult")
    @Column(length = 16)
    private String fxResult;

    @JsonProperty("oddEven")
    @Column(length = 16)
    private String oddeven;

    @JsonProperty("underOver")
    @Column(length = 16)
    private String overunder;

    @Column(length = 16)
    private String sdate;

    @Convert(converter = LongArrayConvert.class)
    private long[] amount = new long[6];

    @Convert(converter = DoubleArrayConvert.class)
    private double[] odds = new double[6];

    private boolean closing;

    private boolean cancel;

    //--------------------------------------------------------

    public FxGame2(int round, Date gameDate) {
        this.round = round;
        this.gameDate = gameDate;
        this.sdate = DateUtils.format(gameDate, "yyyyMMddHHmm");
    }

    public boolean isBeforeGameDate() {
        return this.gameDate.getTime() - System.currentTimeMillis() > 0;
    }

    // 스코어 입력
    public void updateScore(FxGame2Dto.Score score) {
        if (score.isCancel()) {
            this.fxResult = "";
            this.oddeven = "";
            this.overunder = "";
            this.cancel = true;
        } else {
            this.fxResult = score.getFxResult();
            this.oddeven = score.getOddeven();
            this.overunder = score.getOverunder();
            this.cancel = false;
        }
        this.closing = true;
    }

    // 경기 결과
    public ZoneScore getGameResult(String gameCode) {
        switch (gameCode) {
            case "fxResult":
                return ZoneHelper.zoneResult(fxResult, cancel);
            case "oddeven":
                return ZoneHelper.zoneResult(oddeven, cancel);
            case "overunder":
                return ZoneHelper.zoneResult(overunder, cancel);
            default:
                throw new GameZoneException("FxGame2 코드 " + gameCode + " 를 확인 할 수 없습니다.");
        }
    }

    public Zone getZone(String gameCode) {
        switch (gameCode) {
            case "fxResult":
                return getFxGame2Zone("fxResult", "매수", "매도", 0, 1);
            case "oddeven":
                return getFxGame2Zone("oddeven", "홀", "짝", 2, 3);
            case "overunder":
                return getFxGame2Zone("overunder", "오버", "언더", 4, 5);
            default:
                throw new GameZoneException("FxGame2 코드 " + gameCode + " 를 확인 할 수 없습니다.");
        }
    }

    private Zone getFxGame2Zone(String gameCode, String teamHome, String teamAway, int idxHome, int idxAway) {
        Zone zone = new Zone();
        zone.setId(this.getId());
        zone.setSdate(this.getSdate());
        zone.setMenuCode(MenuCode.FXGAME2);
        zone.setGameCode(gameCode);
        zone.setLeague(String.format("%03d회차 FX게임 2분", this.round));
        zone.setTeamHome(String.format("%03d회차 FX게임 2분 [%s]", this.round, teamHome));
        zone.setTeamAway(String.format("%03d회차 FX게임 2분 [%s]", this.round, teamAway));
        zone.setHandicap(0);
        zone.setOddsHome(ZoneConfig.getFxGame2().getOdds()[idxHome]);
        zone.setOddsDraw(0);
        zone.setOddsAway(ZoneConfig.getFxGame2().getOdds()[idxAway]);
        zone.setGameDate(this.gameDate);
        return zone;
    }

    public boolean isChangeResult(FxGame2Dto.Score score) {
        return !this.closing || this.cancel != score.isCancel()
                || !score.getFxResult().equals(this.fxResult)
                || !score.getOddeven().equals(this.oddeven)
                || !score.getOverunder().equals(this.overunder);

    }
}
