package spoon.gameZone.fxGame2.service;

import spoon.gameZone.ZoneDto;
import spoon.gameZone.fxGame2.FxGame2;
import spoon.support.web.AjaxResult;

public interface FxGame2GameService {

    /**
     * fxGame1 게임을 베팅한다.
     */
    AjaxResult betting(ZoneDto.Bet bet);

    /**
     * fxGame1 게임 베팅을 클로징 한다.
     */
    void closingBetting(FxGame2 fxGame2);

    /**
     * fxGame1 게임을 롤백 한다.
     */
    void rollbackPayment(FxGame2 fxGame2);
}
