package spoon.gameZone.fxGame2.service;

import com.querydsl.core.BooleanBuilder;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionInterceptor;
import spoon.bet.entity.QBetItem;
import spoon.bet.repository.BetItemRepository;
import spoon.common.net.HttpParsing;
import spoon.common.utils.*;
import spoon.config.domain.Config;
import spoon.config.service.ConfigService;
import spoon.game.domain.MenuCode;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.fxGame2.*;
import spoon.member.domain.Role;
import spoon.member.entity.Member;
import spoon.member.service.MemberService;
import spoon.support.web.AjaxResult;

import java.util.Calendar;
import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class FxGame2ServiceImpl implements FxGame2Service {

    private ConfigService configService;

    private MemberService memberService;

    private FxGame2GameService fxGame2GameService;

    private FxGame2BotService fxGame2BotService;

    private FxGame2Repository fxGame2Repository;

    private BetItemRepository betItemRepository;

    private static QFxGame2 q = QFxGame2.fxGame2;

    @Transactional
    @Override
    public boolean updateConfig(FxGame2Config fxGame2Config) {
        try {
            configService.updateZoneConfig("fxGame2", JsonUtils.toString(fxGame2Config));
            ZoneConfig.setFxGame2(fxGame2Config);
            // 이미 등록된 게임의 배당을 변경한다.
            fxGame2Repository.updateOdds(ZoneConfig.getFxGame2().getOdds());
        } catch (RuntimeException e) {
            log.error("fxGame2 설정 변경에 실패하였습니다. - {}", e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional(readOnly = true)
    @Override
    public Iterable<FxGame2> getComplete() {
        return fxGame2Repository.findAll(q.closing.isFalse(), new Sort(Sort.Direction.ASC, "sdate"));
    }

    @Transactional(readOnly = true)
    @Override
    public Page<FxGame2> getClosing(ZoneDto.Command command, Pageable pageable) {
        BooleanBuilder builder = new BooleanBuilder(q.closing.isTrue());

        // 날짜별 검색
        if (StringUtils.notEmpty(command.getGameDate())) {
            builder.and(q.sdate.like(DateUtils.sdate(command.getGameDate())));
        }
        // 회차별 검색
        if (command.getRound() != null) {
            builder.and(q.round.eq(command.getRound()));
        }

        return fxGame2Repository.findAll(builder, pageable);
    }

    @Transactional(readOnly = true)
    @Override
    public FxGame2Dto.Score findScore(Long id) {
        FxGame2 fxGame2 = fxGame2Repository.findOne(id);

        FxGame2Dto.Score score = new FxGame2Dto.Score();
        score.setId(fxGame2.getId());
        score.setRound(fxGame2.getRound());
        score.setGameDate(fxGame2.getGameDate());
        score.setFxResult(fxGame2.getFxResult());
        score.setOddeven(fxGame2.getOddeven());
        score.setOverunder(fxGame2.getOverunder());
        score.setCancel(fxGame2.isCancel());

        // 봇 연결
        String json = HttpParsing.getJson(Config.getSysConfig().getZone().getFxGame2Url() + "?sdate=" + fxGame2.getSdate());
        if (json == null) return score;

        fxGame2 = JsonUtils.toModel(json, FxGame2.class);
        if (fxGame2 == null) return score;

        // 봇에 결과가 있다면
        if (fxGame2.isClosing()) {
            score.setFxResult(fxGame2.getFxResult());
            score.setOddeven(fxGame2.getOddeven());
            score.setOverunder(fxGame2.getOverunder());

            if (!"BUY".equals(score.getFxResult()) && !"SELL".equals(score.getFxResult())
                    && !"ODD".equals(score.getOddeven()) && !"EVEN".equals(score.getOddeven())
                    && !"OVER".equals(score.getOverunder()) && !"UNDER".equals(score.getOverunder())
            ) {
                score.setCancel(true);
            }
        }

        return score;
    }

    @Transactional
    @Override
    public boolean closingGame(FxGame2Dto.Score score) {
        FxGame2 fxGame2 = fxGame2Repository.findOne(score.getId());

        try {
            if (fxGame2.isChangeResult(score)) {
                // 현재 지급된 머니 포인트를 되돌린다.
                fxGame2GameService.rollbackPayment(fxGame2);
            }

            // 스코어 입력
            fxGame2.updateScore(score);
            fxGame2Repository.saveAndFlush(fxGame2);
            fxGame2GameService.closingBetting(fxGame2);
            fxGame2BotService.checkResult();
        } catch (RuntimeException e) {
            log.error("fxGame2 {} - {}회차 수동처리를 하지 못하였습니다. - {}", fxGame2.getGameDate(), fxGame2.getRound(), e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional
    @Override
    public AjaxResult closingAllGame() {
        QBetItem qi = QBetItem.betItem;
        int total = 0;
        int closing = 0;

        Iterable<FxGame2> iterable = fxGame2Repository.findAll(q.closing.isFalse().and(q.gameDate.before(DateUtils.beforeMinutes(5))));
        for (FxGame2 fxGame2 : iterable) {
            total++;
            long count = betItemRepository.count(qi.menuCode.eq(MenuCode.FXGAME2).and(qi.groupId.eq(fxGame2.getSdate())).and(qi.cancel.isFalse()));
            if (count > 0) continue;

            String json = HttpParsing.getJson(Config.getSysConfig().getZone().getFxGame2Url() + "?sdate=" + fxGame2.getSdate());
            if (json == null) continue;

            FxGame2 result = JsonUtils.toModel(json, FxGame2.class);
            if (result == null) continue;

            if (result.isClosing()) {
                fxGame2.setFxResult(result.getFxResult());
                fxGame2.setClosing(true);
                fxGame2Repository.saveAndFlush(fxGame2);
                closing++;
            }
        }
        fxGame2BotService.checkResult();
        return new AjaxResult(true, "전체 " + total + "경기중 " + closing + "경기를 종료처리 했습니다.");
    }

    @Override
    public FxGame2Dto.Config gameConfig(int r) {
        FxGame2Dto.Config gameConfig = new FxGame2Dto.Config();
        FxGame2Config config = ZoneConfig.getFxGame2();

        Date oriGameDate = config.getZoneMaker().getGameDate();
        Date gameDate = config.getZoneMaker().getGameDate();
        Calendar cal = Calendar.getInstance();
        cal.setTime(gameDate);
        cal.add(Calendar.MINUTE, (config.getZoneMaker().getInterval() * r));

        FxGame2 fxGame2 = fxGame2Repository.findOne(q.gameDate.eq(cal.getTime()));

        if (fxGame2 == null) {
            gameConfig.setGameDate2(oriGameDate);
            gameConfig.setGameDate(gameDate);
            gameConfig.setRound(0);
            return gameConfig;
        }

        String userid = WebUtils.userid();
        if (userid == null) return gameConfig;

        Member member = memberService.getMember(userid);
        int level = member.getLevel();
        gameConfig.setEnabled(config.isEnabled() && Config.getSysConfig().getZone().isFxGame2());
        if (member.getRole() == Role.DUMMY) {
            gameConfig.setMoney(10000000);
        } else {
            gameConfig.setMoney(member.getMoney());
        }
        gameConfig.setNextRound(r);
        gameConfig.setGameDate2(oriGameDate);
        gameConfig.setGameDate(fxGame2.getGameDate());
        gameConfig.setSdate(fxGame2.getSdate());
        gameConfig.setRound(fxGame2.getRound());
        gameConfig.setWin(config.getWin()[level]);
        gameConfig.setMax(config.getMax()[level]);
        gameConfig.setMin(config.getMin()[level]);
        gameConfig.setOdds(fxGame2.getOdds());

        int betTime = (int) (oriGameDate.getTime() - new Date().getTime() - config.getBetTime() * 1000) / 1000;
        if (betTime < 0) betTime = 0;
        gameConfig.setBetTime(betTime);

        gameConfig.setFxResult(config.isFxResult());
        gameConfig.setOddeven(config.isOddeven());
        gameConfig.setOverunder(config.isOverunder());

        return gameConfig;
    }
}
