package spoon.gameZone.fxGame2.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.fxGame2.FxGame2;
import spoon.gameZone.fxGame2.FxGame2Config;
import spoon.gameZone.fxGame2.FxGame2Dto;
import spoon.support.web.AjaxResult;

public interface FxGame2Service {

    /**
     * FxGame2 설정을 변경한다.
     */
    boolean updateConfig(FxGame2Config fxGame2Config);

    /**
     * FxGame2 등록된 게임을 가져온다.
     */
    Iterable<FxGame2> getComplete();

    /**
     * FxGame2 종료된 게임을 가져온다.
     */
    Page<FxGame2> getClosing(ZoneDto.Command command, Pageable pageable);

    /**
     * FxGame2 봇에 접속하여 기존 결과가 있는지 확인 한다.
     */
    FxGame2Dto.Score findScore(Long id);

    /**
     * FxGame2 스코어를 가지고 결과처리를 한다.
     */
    boolean closingGame(FxGame2Dto.Score score);

    /**
     * 결과처리가 되지 않고 베팅이 없는 모든 경기를 종료처리 한다.
     */
    AjaxResult closingAllGame();

    /**
     * 현재 진행중인 경기의 설정을 가져온다.
     */
    FxGame2Dto.Config gameConfig(int r);

}
