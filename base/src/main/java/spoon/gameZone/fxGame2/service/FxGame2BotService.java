package spoon.gameZone.fxGame2.service;

import spoon.gameZone.fxGame2.FxGame2;

import java.util.Date;

public interface FxGame2BotService {

    boolean notExist(Date gameDate);

    void addGame(FxGame2 fxGame2);

    boolean closingGame(FxGame2 result);

    void checkResult();

    void deleteGame(int days);

}
