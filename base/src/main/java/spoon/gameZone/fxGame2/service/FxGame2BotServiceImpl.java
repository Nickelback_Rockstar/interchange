package spoon.gameZone.fxGame2.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionInterceptor;
import spoon.common.utils.DateUtils;
import spoon.common.utils.ErrorUtils;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.fxGame2.FxGame2;
import spoon.gameZone.fxGame2.FxGame2Repository;
import spoon.gameZone.fxGame2.QFxGame2;

import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class FxGame2BotServiceImpl implements FxGame2BotService {

    private FxGame2GameService fxGame2GameService;

    private FxGame2Repository fxGame2Repository;

    private static QFxGame2 q = QFxGame2.fxGame2;

    @Transactional(readOnly = true)
    @Override
    public boolean notExist(Date gameDate) {
        return fxGame2Repository.count(q.sdate.eq(DateUtils.format(gameDate, "yyyyMMddHHmm"))) == 0;
    }

    @Transactional
    @Override
    public void addGame(FxGame2 fxGame2) {
        fxGame2Repository.saveAndFlush(fxGame2);
    }

    @Transactional
    @Override
    public boolean closingGame(FxGame2 result) {
        FxGame2 fxGame2 = fxGame2Repository.findOne(q.sdate.eq(result.getSdate()));
        if (fxGame2 == null) {
            return true;
        }

        try {
            fxGame2.setFxResult(result.getFxResult());
            fxGame2.setOddeven(result.getOddeven());
            fxGame2.setOverunder(result.getOverunder());
            fxGame2.setClosing(true);

            fxGame2Repository.saveAndFlush(fxGame2);
            fxGame2GameService.closingBetting(fxGame2);
        } catch (RuntimeException e) {
            log.error("fxGame2 {}회차 결과 업데이트에 실패하였습니다. - {}", fxGame2.getRound(), e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional(readOnly = true)
    @Override
    public void checkResult() {
        long cnt = fxGame2Repository.count(q.gameDate.before(new Date()).and(q.closing.isFalse()));
        ZoneConfig.getFxGame2().setResult(cnt);
    }

    @Transactional
    @Override
    public void deleteGame(int days) {
        fxGame2Repository.deleteGame(DateUtils.beforeDays(days));
    }
}
