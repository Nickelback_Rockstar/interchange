package spoon.gameZone.fxGame2;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.query.Param;

import java.util.Date;

public interface FxGame2Repository extends JpaRepository<FxGame2, Long>, QueryDslPredicateExecutor<FxGame2> {

    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE FxGame2 o SET o.odds = :odds WHERE o.gameDate > CURRENT_TIMESTAMP")
    void updateOdds(@Param("odds") double[] odds);

    @Modifying(clearAutomatically = true)
    @Query(value = "DELETE FROM FxGame2 o WHERE o.gameDate < :gameDate")
    void deleteGame(@Param("gameDate") Date gameDate);

}
