package spoon.gameZone.fxGame4.service;

import spoon.gameZone.fxGame4.FxGame4;

import java.util.Date;

public interface FxGame4BotService {

    boolean notExist(Date gameDate);

    void addGame(FxGame4 fxGame4);

    boolean closingGame(FxGame4 result);

    void checkResult();

    void deleteGame(int days);

}
