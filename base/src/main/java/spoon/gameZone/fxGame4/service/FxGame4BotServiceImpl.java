package spoon.gameZone.fxGame4.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionInterceptor;
import spoon.common.utils.DateUtils;
import spoon.common.utils.ErrorUtils;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.fxGame4.FxGame4;
import spoon.gameZone.fxGame4.FxGame4Repository;
import spoon.gameZone.fxGame4.QFxGame4;

import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class FxGame4BotServiceImpl implements FxGame4BotService {

    private FxGame4GameService fxGame4GameService;

    private FxGame4Repository fxGame4Repository;

    private static QFxGame4 q = QFxGame4.fxGame4;

    @Transactional(readOnly = true)
    @Override
    public boolean notExist(Date gameDate) {
        return fxGame4Repository.count(q.sdate.eq(DateUtils.format(gameDate, "yyyyMMddHHmm"))) == 0;
    }

    @Transactional
    @Override
    public void addGame(FxGame4 fxGame4) {
        fxGame4Repository.saveAndFlush(fxGame4);
    }

    @Transactional
    @Override
    public boolean closingGame(FxGame4 result) {
        FxGame4 fxGame4 = fxGame4Repository.findOne(q.sdate.eq(result.getSdate()));
        if (fxGame4 == null) {
            return true;
        }

        try {
            fxGame4.setFxResult(result.getFxResult());
            fxGame4.setOddeven(result.getOddeven());
            fxGame4.setOverunder(result.getOverunder());
            fxGame4.setClosing(true);

            fxGame4Repository.saveAndFlush(fxGame4);
            fxGame4GameService.closingBetting(fxGame4);
        } catch (RuntimeException e) {
            log.error("fxGame4 {}회차 결과 업데이트에 실패하였습니다. - {}", fxGame4.getRound(), e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional(readOnly = true)
    @Override
    public void checkResult() {
        long cnt = fxGame4Repository.count(q.gameDate.before(new Date()).and(q.closing.isFalse()));
        ZoneConfig.getFxGame4().setResult(cnt);
    }

    @Transactional
    @Override
    public void deleteGame(int days) {
        fxGame4Repository.deleteGame(DateUtils.beforeDays(days));
    }
}
