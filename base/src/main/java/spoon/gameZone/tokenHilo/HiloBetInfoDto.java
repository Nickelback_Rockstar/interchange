package spoon.gameZone.tokenHilo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class HiloBetInfoDto {

        @JsonProperty("div")
        private String div;

        @JsonProperty("agent_id")
        private String agent_id;

        @JsonProperty("user_id")
        private String user_id;

        @JsonProperty("uid")
        private String uid;

        @JsonProperty("gtype")
        private String gtype;

        @JsonProperty("gRound")
        private String gRound;

        @JsonProperty("start_money")
        private String start_money;

        @JsonProperty("end_money")
        private String end_money;

}
