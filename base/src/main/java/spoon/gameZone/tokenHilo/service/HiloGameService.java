package spoon.gameZone.tokenHilo.service;

import spoon.gameZone.tokenHilo.HiloBetDto;
import spoon.gameZone.tokenHilo.HiloBetInfoDto;
import spoon.gameZone.tokenHilo.HiloResultDto;
import spoon.support.web.AjaxResult;

public interface HiloGameService {

    /**
     * hilo 게임을 베팅한다.
     */
    AjaxResult betting(HiloBetInfoDto betInfoDto, HiloBetDto betDto);

    void closingBetting(HiloBetInfoDto betInfoDto, HiloBetDto betDto, HiloResultDto resultDto);

}
