package spoon.gameZone.tokenHilo.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import spoon.bet.entity.Bet;
import spoon.bet.entity.BetItem;
import spoon.bet.entity.QBetItem;
import spoon.bet.repository.BetItemRepository;
import spoon.bet.service.BetClosingService;
import spoon.bet.service.BetService;
import spoon.common.utils.DateUtils;
import spoon.common.utils.StringUtils;
import spoon.common.utils.WebUtils;
import spoon.game.domain.GameCode;
import spoon.game.domain.GameResult;
import spoon.game.domain.MenuCode;
import spoon.gameZone.ZoneScore;
import spoon.gameZone.power.PowerRepository;
import spoon.gameZone.tokenHilo.HiloBetDto;
import spoon.gameZone.tokenHilo.HiloBetInfoDto;
import spoon.gameZone.tokenHilo.HiloResultDto;
import spoon.mapper.BetMapper;
import spoon.member.entity.Member;
import spoon.member.service.MemberService;
import spoon.support.web.AjaxResult;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Slf4j
@AllArgsConstructor
@Service
public class HiloGameServiceImpl implements HiloGameService {

    private MemberService memberService;

    private BetService betService;

    private PowerRepository powerRepository;

    private BetClosingService betClosingService;

    private BetItemRepository betItemRepository;

    private BetMapper betMapper;

    @Override
    public AjaxResult betting(HiloBetInfoDto betInfoDto, HiloBetDto betDto) {
        AjaxResult result = new AjaxResult();
        String userid = betInfoDto.getUser_id();

        if (userid == null) {
            result.setMessage("베팅에 실패하였습니다.");
            return result;
        }

        Member member = memberService.getMember(userid);
        if (member.isSecession() || !member.isEnabled() || !member.getTokenid().equals(betInfoDto.getUid()) || member.getMoney() != Long.parseLong(betInfoDto.getStart_money())) {
            result.setMessage("베팅에 실패하였습니다.");
            return result;
        }

        if (!member.getTokenid().equals(betInfoDto.getUid())) {
            result.setMessage("uid 를 다시 확인해주세요.");
            return result;
        }

        if (member.getMoney() != Long.parseLong(betInfoDto.getStart_money())) {
            result.setMessage("보유금을 다시 확인해주세요.");
            return result;
        }

        if(StringUtils.empty(betDto.getRate()) && ( "hi".equals(betDto.getType()) || "lo".equals(betDto.getType()) ) ){
            //하이로우 배당만 못받으니 1로 세팅해줌.
            betDto.setRate("1");
        }

        BetItem betItem = new BetItem();
        betItem.setBetMoney(betDto.getMoney());
        betItem.setGameId(Long.parseLong(betInfoDto.getGRound()));
        betItem.setGroupId(betDto.getTransactionid());
        betItem.setSports(MenuCode.TKHILO.getName());
        betItem.setLeague(MenuCode.TKHILO.getName());
        betItem.setSpecial(betDto.getType());
        betItem.setMenuCode(MenuCode.TKHILO);
        betItem.setGameCode(GameCode.ZONE);
        betItem.setTeamHome(betDto.getType());
        betItem.setTeamAway("");
        betItem.setHandicap(0);
        betItem.setOdds(Double.parseDouble(betDto.getRate()));
        betItem.setOddsHome(Double.parseDouble(betDto.getRate()));
        betItem.setOddsAway(0);
        betItem.setOddsDraw(0);
        String sDate = betDto.getDate()+ betDto.getTime();
        sDate = sDate.replaceAll("-","").replaceAll(":","").replaceAll(" ","");
        Date bDate = DateUtils.parse(sDate,"yyyyMMddHHmmss");
        betItem.setGameDate(bDate);
        betItem.setBetTeam("home");
        betItem.setBetZone(0);
        betItem.setUserid(userid);
        betItem.setRole(member.getRole());
        List<BetItem> betItems = new ArrayList<>();
        betItems.add(betItem);
        ///////////////////////////////////////////////////////////////////

        Bet bet = new Bet(member.getUser());
        bet.setBetMoney(betDto.getMoney());
        bet.setBetItems(betItems);
        bet.setMenuCode(MenuCode.TKHILO);
        bet.setBetCount(1);
        bet.setBetDate(new Date());
        bet.setBetOdds(Double.parseDouble(betDto.getRate()));
        bet.setStartDate(bDate);
        bet.setEndDate(bDate);
        bet.setExpMoney((long) (bet.getBetOdds() * bet.getBetMoney()));
        bet.setIp(WebUtils.ip());
        bet.setBalance(member.isBalancePower());

        boolean success = betService.addZoneBetting(bet);

        if (success) {
            result.setSuccess(true);
        } else {
            result.setMessage("베팅에 실패하였습니다. 잠시후 다시 시도하세요.");
        }

        return result;
    }

    @Transactional
    @Override
    public void closingBetting(HiloBetInfoDto betInfoDto, HiloBetDto betDto, HiloResultDto resultDto) {
        QBetItem qi = QBetItem.betItem;
        Iterable<BetItem> betItems = betItemRepository.findAll(qi.menuCode.eq(MenuCode.TKHILO).and(qi.groupId.eq(betDto.getTransactionid())).and(qi.cancel.isFalse()));
        for (BetItem item : betItems) {
            Bet bet = item.getBet();
            if (bet.isPayment()) continue;

            //하이 로우만 배당 변경해줌.
            if( "hi".equals(item.getSpecial()) || "lo".equals(item.getSpecial()) ){
                item.setOdds(StringUtils.empty(resultDto.getRate())? 1 : Double.parseDouble(resultDto.getRate()));
                item.setOddsHome(item.getOdds());
                bet.setBetOdds(item.getOdds());
                bet.setExpMoney((long) (bet.getBetOdds() * bet.getBetMoney()));
            }

            ZoneScore zoneScore;
            if(resultDto != null && "result".equals(betInfoDto.getDiv()) && Long.parseLong(resultDto.getMoney()) > 0) {
                zoneScore = new ZoneScore(1, 0, GameResult.HOME);
            }else if("cancel".equals(betInfoDto.getDiv())) {
                zoneScore = new ZoneScore(0, 0, GameResult.CANCEL);//취소
            }else{
                zoneScore = new ZoneScore(0, 1, GameResult.AWAY);//미적중
            }

            betClosingService.closingZoneBetting(bet, zoneScore);
        }
    }

}
