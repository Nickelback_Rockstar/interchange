package spoon.gameZone.fxGame1.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionInterceptor;
import spoon.common.utils.DateUtils;
import spoon.common.utils.ErrorUtils;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.fxGame1.FxGame1;
import spoon.gameZone.fxGame1.FxGame1Repository;
import spoon.gameZone.fxGame1.QFxGame1;

import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class FxGame1BotServiceImpl implements FxGame1BotService {

    private FxGame1GameService fxGame1GameService;

    private FxGame1Repository fxGame1Repository;

    private static QFxGame1 q = QFxGame1.fxGame1;

    @Transactional(readOnly = true)
    @Override
    public boolean notExist(Date gameDate) {
        return fxGame1Repository.count(q.sdate.eq(DateUtils.format(gameDate, "yyyyMMddHHmm"))) == 0;
    }

    @Transactional
    @Override
    public void addGame(FxGame1 fxGame1) {
        fxGame1Repository.saveAndFlush(fxGame1);
    }

    @Transactional
    @Override
    public boolean closingGame(FxGame1 result) {
        FxGame1 fxGame1 = fxGame1Repository.findOne(q.sdate.eq(result.getSdate()));
        if (fxGame1 == null) {
            return true;
        }

        try {
            fxGame1.setFxResult(result.getFxResult());
            fxGame1.setOddeven(result.getOddeven());
            fxGame1.setOverunder(result.getOverunder());
            fxGame1.setClosing(true);

            fxGame1Repository.saveAndFlush(fxGame1);
            fxGame1GameService.closingBetting(fxGame1);
        } catch (RuntimeException e) {
            log.error("FxGame1 {}회차 결과 업데이트에 실패하였습니다. - {}", fxGame1.getRound(), e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional(readOnly = true)
    @Override
    public void checkResult() {
        long cnt = fxGame1Repository.count(q.gameDate.before(new Date()).and(q.closing.isFalse()));
        ZoneConfig.getFxGame1().setResult(cnt);
    }

    @Transactional
    @Override
    public void deleteGame(int days) {
        fxGame1Repository.deleteGame(DateUtils.beforeDays(days));
    }
}
