package spoon.gameZone.fxGame1.service;

import spoon.gameZone.fxGame1.FxGame1;

import java.util.Date;

public interface FxGame1BotService {

    boolean notExist(Date gameDate);

    void addGame(FxGame1 ladder);

    boolean closingGame(FxGame1 result);

    void checkResult();

    void deleteGame(int days);

}
