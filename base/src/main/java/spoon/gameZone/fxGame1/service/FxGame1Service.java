package spoon.gameZone.fxGame1.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.fxGame1.FxGame1;
import spoon.gameZone.fxGame1.FxGame1Config;
import spoon.gameZone.fxGame1.FxGame1Dto;
import spoon.support.web.AjaxResult;

public interface FxGame1Service {

    /**
     * FxGame1 설정을 변경한다.
     */
    boolean updateConfig(FxGame1Config fxGame1Config);

    /**
     * FxGame1 등록된 게임을 가져온다.
     */
    Iterable<FxGame1> getComplete();

    /**
     * FxGame1 종료된 게임을 가져온다.
     */
    Page<FxGame1> getClosing(ZoneDto.Command command, Pageable pageable);

    /**
     * FxGame1 봇에 접속하여 기존 결과가 있는지 확인 한다.
     */
    FxGame1Dto.Score findScore(Long id);

    /**
     * FxGame1 스코어를 가지고 결과처리를 한다.
     */
    boolean closingGame(FxGame1Dto.Score score);

    /**
     * 결과처리가 되지 않고 베팅이 없는 모든 경기를 종료처리 한다.
     */
    AjaxResult closingAllGame();

    /**
     * 현재 진행중인 경기의 설정을 가져온다.
     */
    FxGame1Dto.Config gameConfig(int r);

}
