package spoon.gameZone.fxGame1.service;

import spoon.gameZone.ZoneDto;
import spoon.gameZone.fxGame1.FxGame1;
import spoon.support.web.AjaxResult;

public interface FxGame1GameService {

    /**
     * fxGame1 게임을 베팅한다.
     */
    AjaxResult betting(ZoneDto.Bet bet);

    /**
     * fxGame1 게임 베팅을 클로징 한다.
     */
    void closingBetting(FxGame1 ladder);

    /**
     * fxGame1 게임을 롤백 한다.
     */
    void rollbackPayment(FxGame1 ladder);
}
