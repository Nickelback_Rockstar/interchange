package spoon.gameZone.fxGame1.service;

import com.querydsl.core.BooleanBuilder;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionInterceptor;
import spoon.bet.entity.QBetItem;
import spoon.bet.repository.BetItemRepository;
import spoon.common.net.HttpParsing;
import spoon.common.utils.*;
import spoon.config.domain.Config;
import spoon.config.service.ConfigService;
import spoon.game.domain.MenuCode;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.fxGame1.*;
import spoon.member.domain.Role;
import spoon.member.entity.Member;
import spoon.member.service.MemberService;
import spoon.support.web.AjaxResult;

import java.util.Calendar;
import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class FxGame1ServiceImpl implements FxGame1Service {

    private ConfigService configService;

    private MemberService memberService;

    private FxGame1GameService fxGame1GameService;

    private FxGame1BotService fxGame1BotService;

    private FxGame1Repository fxGame1Repository;

    private BetItemRepository betItemRepository;

    private static QFxGame1 q = QFxGame1.fxGame1;

    @Transactional
    @Override
    public boolean updateConfig(FxGame1Config fxGame1Config) {
        try {
            configService.updateZoneConfig("fxGame1", JsonUtils.toString(fxGame1Config));
            ZoneConfig.setFxGame1(fxGame1Config);
            // 이미 등록된 게임의 배당을 변경한다.
            fxGame1Repository.updateOdds(ZoneConfig.getFxGame1().getOdds());
        } catch (RuntimeException e) {
            log.error("fxGame1 설정 변경에 실패하였습니다. - {}", e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional(readOnly = true)
    @Override
    public Iterable<FxGame1> getComplete() {
        return fxGame1Repository.findAll(q.closing.isFalse(), new Sort(Sort.Direction.ASC, "sdate"));
    }

    @Transactional(readOnly = true)
    @Override
    public Page<FxGame1> getClosing(ZoneDto.Command command, Pageable pageable) {
        BooleanBuilder builder = new BooleanBuilder(q.closing.isTrue());

        // 날짜별 검색
        if (StringUtils.notEmpty(command.getGameDate())) {
            builder.and(q.sdate.like(DateUtils.sdate(command.getGameDate())));
        }
        // 회차별 검색
        if (command.getRound() != null) {
            builder.and(q.round.eq(command.getRound()));
        }

        return fxGame1Repository.findAll(builder, pageable);
    }

    @Transactional(readOnly = true)
    @Override
    public FxGame1Dto.Score findScore(Long id) {
        FxGame1 fxGame1 = fxGame1Repository.findOne(id);

        FxGame1Dto.Score score = new FxGame1Dto.Score();
        score.setId(fxGame1.getId());
        score.setRound(fxGame1.getRound());
        score.setGameDate(fxGame1.getGameDate());
        score.setFxResult(fxGame1.getFxResult());
        score.setOddeven(fxGame1.getOddeven());
        score.setOverunder(fxGame1.getOverunder());
        score.setCancel(fxGame1.isCancel());

        // 봇 연결
        String json = HttpParsing.getJson(Config.getSysConfig().getZone().getFxGame1Url() + "?sdate=" + fxGame1.getSdate());
        if (json == null) return score;

        fxGame1 = JsonUtils.toModel(json, FxGame1.class);
        if (fxGame1 == null) return score;

        // 봇에 결과가 있다면
        if (fxGame1.isClosing()) {
            score.setFxResult(fxGame1.getFxResult());
            score.setOddeven(fxGame1.getOddeven());
            score.setOverunder(fxGame1.getOverunder());

            if (!"BUY".equals(score.getFxResult()) && !"SELL".equals(score.getFxResult())
                    && !"ODD".equals(score.getOddeven()) && !"EVEN".equals(score.getOddeven())
                    && !"OVER".equals(score.getOverunder()) && !"UNDER".equals(score.getOverunder())
            ) {
                score.setCancel(true);
            }
        }

        return score;
    }

    @Transactional
    @Override
    public boolean closingGame(FxGame1Dto.Score score) {
        FxGame1 fxGame1 = fxGame1Repository.findOne(score.getId());

        try {
            if (fxGame1.isChangeResult(score)) {
                // 현재 지급된 머니 포인트를 되돌린다.
                fxGame1GameService.rollbackPayment(fxGame1);
            }

            // 스코어 입력
            fxGame1.updateScore(score);
            fxGame1Repository.saveAndFlush(fxGame1);
            fxGame1GameService.closingBetting(fxGame1);
            fxGame1BotService.checkResult();
        } catch (RuntimeException e) {
            log.error("fxGame1 {} - {}회차 수동처리를 하지 못하였습니다. - {}", fxGame1.getGameDate(), fxGame1.getRound(), e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional
    @Override
    public AjaxResult closingAllGame() {
        QBetItem qi = QBetItem.betItem;
        int total = 0;
        int closing = 0;

        Iterable<FxGame1> iterable = fxGame1Repository.findAll(q.closing.isFalse().and(q.gameDate.before(DateUtils.beforeMinutes(5))));
        for (FxGame1 fxGame1 : iterable) {
            total++;
            long count = betItemRepository.count(qi.menuCode.eq(MenuCode.FXGAME1).and(qi.groupId.eq(fxGame1.getSdate())).and(qi.cancel.isFalse()));
            if (count > 0) continue;

            String json = HttpParsing.getJson(Config.getSysConfig().getZone().getFxGame1Url() + "?sdate=" + fxGame1.getSdate());
            if (json == null) continue;

            FxGame1 result = JsonUtils.toModel(json, FxGame1.class);
            if (result == null) continue;

            if (result.isClosing()) {
                fxGame1.setFxResult(result.getFxResult());
                fxGame1.setClosing(true);
                fxGame1Repository.saveAndFlush(fxGame1);
                closing++;
            }
        }
        fxGame1BotService.checkResult();
        return new AjaxResult(true, "전체 " + total + "경기중 " + closing + "경기를 종료처리 했습니다.");
    }

    @Override
    public FxGame1Dto.Config gameConfig(int r) {
        FxGame1Dto.Config gameConfig = new FxGame1Dto.Config();
        FxGame1Config config = ZoneConfig.getFxGame1();

        Date oriGameDate = config.getZoneMaker().getGameDate();
        Date gameDate = config.getZoneMaker().getGameDate();
        Calendar cal = Calendar.getInstance();
        cal.setTime(gameDate);
        cal.add(Calendar.MINUTE, (config.getZoneMaker().getInterval() * r));

        FxGame1 fxGame1 = fxGame1Repository.findOne(q.gameDate.eq(cal.getTime()));

        if (fxGame1 == null) {
            gameConfig.setGameDate2(oriGameDate);
            gameConfig.setGameDate(gameDate);
            gameConfig.setRound(0);
            return gameConfig;
        }

        String userid = WebUtils.userid();
        if (userid == null) return gameConfig;

        Member member = memberService.getMember(userid);
        int level = member.getLevel();
        gameConfig.setEnabled(config.isEnabled() && Config.getSysConfig().getZone().isFxGame1());
        if (member.getRole() == Role.DUMMY) {
            gameConfig.setMoney(10000000);
        } else {
            gameConfig.setMoney(member.getMoney());
        }
        gameConfig.setNextRound(r);
        gameConfig.setGameDate2(oriGameDate);
        gameConfig.setGameDate(fxGame1.getGameDate());
        gameConfig.setSdate(fxGame1.getSdate());
        gameConfig.setRound(fxGame1.getRound());
        gameConfig.setWin(config.getWin()[level]);
        gameConfig.setMax(config.getMax()[level]);
        gameConfig.setMin(config.getMin()[level]);
        gameConfig.setOdds(fxGame1.getOdds());

        int betTime = (int) (oriGameDate.getTime() - new Date().getTime() - config.getBetTime() * 1000) / 1000;
        if (betTime < 0) betTime = 0;
        gameConfig.setBetTime(betTime);

        gameConfig.setFxResult(config.isFxResult());
        gameConfig.setOddeven(config.isOddeven());
        gameConfig.setOverunder(config.isOverunder());


        return gameConfig;
    }
}
