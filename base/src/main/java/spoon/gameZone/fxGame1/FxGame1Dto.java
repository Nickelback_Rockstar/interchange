package spoon.gameZone.fxGame1;

import lombok.Data;
import spoon.common.utils.DateUtils;

import java.util.Date;

public class FxGame1Dto {

    @Data
    public static class Config {
        private long money;
        private boolean enabled;
        private int round;
        private Date gameDate;
        private Date gameDate2;
        private int betTime;
        private String sdate;
        private double[] odds;
        private int max;
        private int win;
        private int min;
        private int nextRound;

        private boolean fxResult;
        private boolean oddeven;
        private boolean overunder;

        public String getGameDateName() {
            return DateUtils.format(gameDate, "MM/dd(E)");
        }

        public String getGameTimeName() {
            return DateUtils.format(gameDate, "HH:mm");
        }
    }

    @Data
    public static class Score {
        private long id;
        private int round;
        private Date gameDate;
        private boolean cancel;

        private String fxResult;
        private String oddeven;
        private String overunder;

    }
}
