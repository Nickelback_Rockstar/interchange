package spoon.gameZone.eosPower4;

import lombok.Data;
import spoon.common.utils.DateUtils;

import java.util.Date;

public class EosPower4Dto {

    @Data
    public static class Config {
        private long money;
        private boolean enabled;
        private int round;
        private int times;
        private Date gameDate;
        private int betTime;
        private String sdate;
        private double[] odds;
        private int max;
        private int win;
        private int min;

        private boolean oddeven;
        private boolean odd_overunder;
        private boolean even_overunder;
        private boolean pb_oddeven;
        private boolean pb_odd_overunder;
        private boolean pb_even_overunder;
        private boolean pb_odd_pb_overunder;
        private boolean pb_even_pb_overunder;
        private boolean overunder;
        private boolean pb_overunder;
        private boolean size;
        private boolean odd_size;
        private boolean even_size;

        public String getGameDateName() {
            return DateUtils.format(gameDate, "MM/dd(E)");
        }

        public String getGameTimeName() {
            return DateUtils.format(gameDate, "HH:mm");
        }
    }

    @Data
    public static class Score {
        private long id;
        private int round;
        private int times;
        private Date gameDate;
        private boolean cancel;

        private String pb;
        private String ball;
    }


}
