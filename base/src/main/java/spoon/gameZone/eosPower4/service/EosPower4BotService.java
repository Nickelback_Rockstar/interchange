package spoon.gameZone.eosPower4.service;

import spoon.gameZone.eosPower4.EosPower4;

import java.util.Date;

public interface EosPower4BotService {

    boolean notExist(Date gameDate);

    void addGame(EosPower4 power);

    boolean closingGame(EosPower4 result);

    void checkResult();

    void deleteGame(int days);

}
