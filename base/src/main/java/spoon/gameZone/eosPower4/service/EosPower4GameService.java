package spoon.gameZone.eosPower4.service;

import spoon.gameZone.ZoneDto;
import spoon.gameZone.eosPower4.EosPower4;
import spoon.support.web.AjaxResult;

public interface EosPower4GameService {

    /**
     * 파워볼 게임을 베팅한다.
     */
    AjaxResult betting(ZoneDto.Bet bet);

    /**
     * 파워볼 게임 베팅을 클로징 한다.
     */
    void closingBetting(EosPower4 power);

    /**
     * 파워볼 게임을 롤백 한다.
     */
    void rollbackPayment(EosPower4 power);
}
