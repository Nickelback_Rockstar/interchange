package spoon.gameZone;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;
import spoon.bot.zone.service.GameBotParsing;
import spoon.common.utils.JsonUtils;
import spoon.config.domain.Config;
import spoon.config.entity.JsonConfig;
import spoon.config.repository.JsonRepository;
import spoon.gameZone.aladdin.AladdinConfig;
import spoon.gameZone.baccarat.BaccaratConfig;
import spoon.gameZone.binanceCoin1.BinanceCoin1Config;
import spoon.gameZone.binanceCoin3.BinanceCoin3Config;
import spoon.gameZone.bogleLadder.BogleLadderConfig;
import spoon.gameZone.boglePower.BoglePowerConfig;
import spoon.gameZone.dari.DariConfig;
import spoon.gameZone.dog.DogConfig;
import spoon.gameZone.eosPower1.EosPower1Config;
import spoon.gameZone.eosPower2.EosPower2Config;
import spoon.gameZone.eosPower3.EosPower3Config;
import spoon.gameZone.eosPower4.EosPower4Config;
import spoon.gameZone.eosPower5.EosPower5Config;
import spoon.gameZone.fxGame1.FxGame1Config;
import spoon.gameZone.fxGame2.FxGame2Config;
import spoon.gameZone.fxGame3.FxGame3Config;
import spoon.gameZone.fxGame4.FxGame4Config;
import spoon.gameZone.fxGame5.FxGame5Config;
import spoon.gameZone.kenoLadder.KenoLadderConfig;
import spoon.gameZone.kenoSpeed.KenoSpeedConfig;
import spoon.gameZone.ladder.LadderConfig;
import spoon.gameZone.lotusBaccarat.LotusBaccaratConfig;
import spoon.gameZone.lotusBaccarat2.LotusBaccarat2Config;
import spoon.gameZone.lotusOddeven.LotusOddevenConfig;
import spoon.gameZone.lowhi.LowhiConfig;
import spoon.gameZone.luck.LuckConfig;
import spoon.gameZone.newSnail.NewSnailConfig;
import spoon.gameZone.oddeven.OddevenConfig;
import spoon.gameZone.power.PowerConfig;
import spoon.gameZone.powerFreeKick.PowerFreeKickConfig;
import spoon.gameZone.powerLadder.PowerLadderConfig;
import spoon.gameZone.snail.SnailConfig;
import spoon.gameZone.soccer.SoccerConfig;
import spoon.gameZone.speedHomeRun.SpeedHomeRunConfig;

@AllArgsConstructor
@Component
public class ZoneConfig {

    private JsonRepository jsonRepository;

    private GameBotParsing ladderParsing;

    private GameBotParsing snailParsing;

    private GameBotParsing newSnailParsing;

    private GameBotParsing dariParsing;

    private GameBotParsing powerParsing;

    private GameBotParsing eosPower1Parsing;
    private GameBotParsing eosPower2Parsing;
    private GameBotParsing eosPower3Parsing;
    private GameBotParsing eosPower4Parsing;
    private GameBotParsing eosPower5Parsing;

    private GameBotParsing powerFreeKickParsing;

    private GameBotParsing powerLadderParsing;

    private GameBotParsing fxGame1Parsing;
    private GameBotParsing fxGame2Parsing;
    private GameBotParsing fxGame3Parsing;
    private GameBotParsing fxGame4Parsing;
    private GameBotParsing fxGame5Parsing;

    private GameBotParsing aladdinParsing;

    private GameBotParsing lowhiParsing;

    private GameBotParsing oddevenParsing;

    private GameBotParsing baccaratParsing;

    private GameBotParsing lotusOddevenParsing;

    private GameBotParsing lotusBaccaratParsing;

    private GameBotParsing lotusBaccarat2Parsing;

    private GameBotParsing soccerParsing;

    private GameBotParsing dogParsing;

    private GameBotParsing luckParsing;

    private GameBotParsing kenoLadderParsing;

    private GameBotParsing kenoSpeedParsing;

    private GameBotParsing speedHomeRunParsing;

    private GameBotParsing binanceCoin1Parsing;

    private GameBotParsing binanceCoin3Parsing;

    private GameBotParsing bogleLadderParsing;
    private GameBotParsing boglePowerParsing;

    public void loadZoneConfig() {
        initLadderConfig();
        initSnailConfig();
        initNewSnailConfig();
        initDariConfig();
        initPowerConfig();
        initEosPower1Config();
        initEosPower2Config();
        initEosPower3Config();
        initEosPower4Config();
        initEosPower5Config();
        initPowerFreeKickConfig();
        initPowerLadderConfig();
        initAladdinConfig();
        initLowhiConfig();
        initOddevenConfig();
        initBaccaratConfig();
        initLotusOddevenConfig();
        initLotusBaccaratConfig();
        initLotusBaccarat2Config();
        initSoccerConfig();
        initDogConfig();
        initLuckConfig();
        initKenoLadderConfig();
        initKenoSpeedConfig();
        initSpeedHomeRunConfig();
        initBinanceCoin1Config();
        initBinanceCoin3Config();
        initFxGame1Config();
        initFxGame2Config();
        initFxGame3Config();
        initFxGame4Config();
        initFxGame5Config();
        initBogleLadderConfig();
        initBoglePowerConfig();
    }

    private void initBogleLadderConfig() {
        // bogleLadder
        JsonConfig bogleLadderConfig = jsonRepository.findOne("bogleLadder");
        if (bogleLadderConfig == null) {
            ZoneConfig.setBogleLadder(new BogleLadderConfig());
        } else {
            ZoneConfig.setBogleLadder(JsonUtils.toModel(bogleLadderConfig.getJson(), BogleLadderConfig.class));
        }

        if (ZoneConfig.getBogleLadder().isEnabled() && Config.getSysConfig().getZone().isBogleLadder()) {
            bogleLadderParsing.parsingGame();
            bogleLadderParsing.checkResult();
        }
    }

    private void initBoglePowerConfig() {
        // boglePower
        JsonConfig boglePowerConfig = jsonRepository.findOne("boglePower");
        if (boglePowerConfig == null) {
            ZoneConfig.setBoglePower(new BoglePowerConfig());
        } else {
            ZoneConfig.setBoglePower(JsonUtils.toModel(boglePowerConfig.getJson(), BoglePowerConfig.class));
        }

        if (ZoneConfig.getBoglePower().isEnabled() && Config.getSysConfig().getZone().isBoglePower()) {
            boglePowerParsing.parsingGame();
            boglePowerParsing.checkResult();
        }
    }

    private void initFxGame1Config() {
        // fx1
        JsonConfig fxGame1Config = jsonRepository.findOne("fxGame1");
        if (fxGame1Config == null) {
            ZoneConfig.setFxGame1(new FxGame1Config());
        } else {
            ZoneConfig.setFxGame1(JsonUtils.toModel(fxGame1Config.getJson(), FxGame1Config.class));
        }

        if (ZoneConfig.getFxGame1().isEnabled() && Config.getSysConfig().getZone().isFxGame1()) {
            fxGame1Parsing.parsingGame();
            fxGame1Parsing.checkResult();
        }
    }

    private void initFxGame2Config() {
        // fx2
        JsonConfig fxGame2Config = jsonRepository.findOne("fxGame2");
        if (fxGame2Config == null) {
            ZoneConfig.setFxGame2(new FxGame2Config());
        } else {
            ZoneConfig.setFxGame2(JsonUtils.toModel(fxGame2Config.getJson(), FxGame2Config.class));
        }

        if (ZoneConfig.getFxGame2().isEnabled() && Config.getSysConfig().getZone().isFxGame2()) {
            fxGame2Parsing.parsingGame();
            fxGame2Parsing.checkResult();
        }
    }

    private void initFxGame3Config() {
        // fx3
        JsonConfig fxGame3Config = jsonRepository.findOne("fxGame3");
        if (fxGame3Config == null) {
            ZoneConfig.setFxGame3(new FxGame3Config());
        } else {
            ZoneConfig.setFxGame3(JsonUtils.toModel(fxGame3Config.getJson(), FxGame3Config.class));
        }

        if (ZoneConfig.getFxGame3().isEnabled() && Config.getSysConfig().getZone().isFxGame3()) {
            fxGame3Parsing.parsingGame();
            fxGame3Parsing.checkResult();
        }
    }

    private void initFxGame4Config() {
        // fx4
        JsonConfig fxGame4Config = jsonRepository.findOne("fxGame4");
        if (fxGame4Config == null) {
            ZoneConfig.setFxGame4(new FxGame4Config());
        } else {
            ZoneConfig.setFxGame4(JsonUtils.toModel(fxGame4Config.getJson(), FxGame4Config.class));
        }

        if (ZoneConfig.getFxGame4().isEnabled() && Config.getSysConfig().getZone().isFxGame4()) {
            fxGame4Parsing.parsingGame();
            fxGame4Parsing.checkResult();
        }
    }

    private void initFxGame5Config() {
        // fx5
        JsonConfig fxGame5Config = jsonRepository.findOne("fxGame5");
        if (fxGame5Config == null) {
            ZoneConfig.setFxGame5(new FxGame5Config());
        } else {
            ZoneConfig.setFxGame5(JsonUtils.toModel(fxGame5Config.getJson(), FxGame5Config.class));
        }

        if (ZoneConfig.getFxGame5().isEnabled() && Config.getSysConfig().getZone().isFxGame5()) {
            fxGame5Parsing.parsingGame();
            fxGame5Parsing.checkResult();
        }
    }

    private void initEosPower1Config() {
        // 파워볼
        JsonConfig eosPower1Config = jsonRepository.findOne("eosPower1");
        if (eosPower1Config == null) {
            ZoneConfig.setEosPower1(new EosPower1Config());
        } else {
            ZoneConfig.setEosPower1(JsonUtils.toModel(eosPower1Config.getJson(), EosPower1Config.class));
        }

        if (ZoneConfig.getEosPower1().isEnabled() && Config.getSysConfig().getZone().isPower()) {
            eosPower1Parsing.parsingGame();
            eosPower1Parsing.checkResult();
        }
    }

    private void initEosPower2Config() {
        // 파워볼
        JsonConfig eosPower2Config = jsonRepository.findOne("eosPower2");
        if (eosPower2Config == null) {
            ZoneConfig.setEosPower2(new EosPower2Config());
        } else {
            ZoneConfig.setEosPower2(JsonUtils.toModel(eosPower2Config.getJson(), EosPower2Config.class));
        }

        if (ZoneConfig.getEosPower2().isEnabled() && Config.getSysConfig().getZone().isPower()) {
            eosPower2Parsing.parsingGame();
            eosPower2Parsing.checkResult();
        }
    }

    private void initEosPower3Config() {
        // 파워볼
        JsonConfig eosPower3Config = jsonRepository.findOne("eosPower3");
        if (eosPower3Config == null) {
            ZoneConfig.setEosPower3(new EosPower3Config());
        } else {
            ZoneConfig.setEosPower3(JsonUtils.toModel(eosPower3Config.getJson(), EosPower3Config.class));
        }

        if (ZoneConfig.getEosPower3().isEnabled() && Config.getSysConfig().getZone().isPower()) {
            eosPower3Parsing.parsingGame();
            eosPower3Parsing.checkResult();
        }
    }

    private void initEosPower4Config() {
        // 파워볼
        JsonConfig eosPower4Config = jsonRepository.findOne("eosPower4");
        if (eosPower4Config == null) {
            ZoneConfig.setEosPower4(new EosPower4Config());
        } else {
            ZoneConfig.setEosPower4(JsonUtils.toModel(eosPower4Config.getJson(), EosPower4Config.class));
        }

        if (ZoneConfig.getEosPower4().isEnabled() && Config.getSysConfig().getZone().isPower()) {
            eosPower4Parsing.parsingGame();
            eosPower4Parsing.checkResult();
        }
    }

    private void initEosPower5Config() {
        // 파워볼
        JsonConfig eosPower5Config = jsonRepository.findOne("eosPower5");
        if (eosPower5Config == null) {
            ZoneConfig.setEosPower5(new EosPower5Config());
        } else {
            ZoneConfig.setEosPower5(JsonUtils.toModel(eosPower5Config.getJson(), EosPower5Config.class));
        }

        if (ZoneConfig.getEosPower5().isEnabled() && Config.getSysConfig().getZone().isPower()) {
            eosPower5Parsing.parsingGame();
            eosPower5Parsing.checkResult();
        }
    }

    private void initBinanceCoin1Config() {
        // 바이낸스코인 1분
        JsonConfig binanceCoin1Config = jsonRepository.findOne("binanceCoin1");
        if (binanceCoin1Config == null) {
            ZoneConfig.setBinanceCoin1(new BinanceCoin1Config());
        } else {
            ZoneConfig.setBinanceCoin1(JsonUtils.toModel(binanceCoin1Config.getJson(), BinanceCoin1Config.class));
        }

        if (ZoneConfig.getBinanceCoin1().isEnabled() && Config.getSysConfig().getZone().isBinanceCoin1()) {
            binanceCoin1Parsing.parsingGame();
            binanceCoin1Parsing.checkResult();
        }
    }

    private void initBinanceCoin3Config() {
        // 비트코인 3분
        JsonConfig binanceCoin3Config = jsonRepository.findOne("binanceCoin3");
        if (binanceCoin3Config == null) {
            ZoneConfig.setBinanceCoin3(new BinanceCoin3Config());
        } else {
            ZoneConfig.setBinanceCoin3(JsonUtils.toModel(binanceCoin3Config.getJson(), BinanceCoin3Config.class));
        }

        if (ZoneConfig.getBinanceCoin3().isEnabled() && Config.getSysConfig().getZone().isBinanceCoin3()) {
            binanceCoin3Parsing.parsingGame();
            binanceCoin3Parsing.checkResult();
        }
    }

    private void initPowerFreeKickConfig() {
        // 파워프리킥
        JsonConfig powerFreeKickConfig = jsonRepository.findOne("powerFreeKick");
        if (powerFreeKickConfig == null) {
            ZoneConfig.setPowerFreeKick(new PowerFreeKickConfig());
        } else {
            ZoneConfig.setPowerFreeKick(JsonUtils.toModel(powerFreeKickConfig.getJson(), PowerFreeKickConfig.class));
        }

        if (ZoneConfig.getPowerFreeKick().isEnabled() && Config.getSysConfig().getZone().isPowerFreeKick()) {
            powerFreeKickParsing.parsingGame();
            powerFreeKickParsing.checkResult();
        }
    }

    private void initLuckConfig() {
        // 세븐럭
        JsonConfig luckConfig = jsonRepository.findOne("luck");
        if (luckConfig == null) {
            ZoneConfig.setLuck(new LuckConfig());
        } else {
            ZoneConfig.setLuck(JsonUtils.toModel(luckConfig.getJson(), LuckConfig.class));
        }

        if (ZoneConfig.getLuck().isEnabled() && Config.getSysConfig().getZone().isLuck()) {
            luckParsing.parsingGame();
            luckParsing.checkResult();
        }
    }

    private void initDogConfig() {
        // 개경주
        JsonConfig dogConfig = jsonRepository.findOne("dog");
        if (dogConfig == null) {
            ZoneConfig.setDog(new DogConfig());
        } else {
            ZoneConfig.setDog(JsonUtils.toModel(dogConfig.getJson(), DogConfig.class));
        }

        if (ZoneConfig.getDog().isEnabled() && Config.getSysConfig().getZone().isDog()) {
            dogParsing.parsingGame();
            dogParsing.checkResult();
        }
    }

    private void initSoccerConfig() {
        // 가상축구
        JsonConfig soccerConfig = jsonRepository.findOne("soccer");
        if (soccerConfig == null) {
            ZoneConfig.setSoccer(new SoccerConfig());
        } else {
            ZoneConfig.setSoccer(JsonUtils.toModel(soccerConfig.getJson(), SoccerConfig.class));
        }

        if (ZoneConfig.getSoccer().isEnabled() && Config.getSysConfig().getZone().isSoccer()) {
            soccerParsing.parsingGame();
            soccerParsing.checkResult();
        }
    }

    private void initBaccaratConfig() {
        // 바카라
        JsonConfig baccaratConfig = jsonRepository.findOne("baccarat");
        if (baccaratConfig == null) {
            ZoneConfig.setBaccarat(new BaccaratConfig());
        } else {
            ZoneConfig.setBaccarat(JsonUtils.toModel(baccaratConfig.getJson(), BaccaratConfig.class));
        }

        if (ZoneConfig.getBaccarat().isEnabled() && Config.getSysConfig().getZone().isBaccarat()) {
            baccaratParsing.parsingGame();
            baccaratParsing.checkResult();
        }
    }

    private void initLotusBaccaratConfig() {
        // 바카라
        JsonConfig lotusBaccaratConfig = jsonRepository.findOne("lotusBaccarat");
        if (lotusBaccaratConfig == null) {
            ZoneConfig.setLotusBaccarat(new LotusBaccaratConfig());
        } else {
            ZoneConfig.setLotusBaccarat(JsonUtils.toModel(lotusBaccaratConfig.getJson(), LotusBaccaratConfig.class));
        }

        if (ZoneConfig.getLotusBaccarat().isEnabled() && Config.getSysConfig().getZone().isLotusBaccarat()) {
            lotusBaccaratParsing.parsingGame();
            lotusBaccaratParsing.checkResult();
        }
    }

    private void initLotusBaccarat2Config() {
        // 바카라
        JsonConfig lotusBaccarat2Config = jsonRepository.findOne("lotusBaccarat2");
        if (lotusBaccarat2Config == null) {
            ZoneConfig.setLotusBaccarat2(new LotusBaccarat2Config());
        } else {
            ZoneConfig.setLotusBaccarat2(JsonUtils.toModel(lotusBaccarat2Config.getJson(), LotusBaccarat2Config.class));
        }

        if (ZoneConfig.getLotusBaccarat2().isEnabled() && Config.getSysConfig().getZone().isLotusBaccarat2()) {
            lotusBaccarat2Parsing.parsingGame();
            lotusBaccarat2Parsing.checkResult();
        }
    }

    private void initOddevenConfig() {
        // 홀짝
        JsonConfig oddevenConfig = jsonRepository.findOne("oddeven");
        if (oddevenConfig == null) {
            ZoneConfig.setOddeven(new OddevenConfig());
        } else {
            ZoneConfig.setOddeven(JsonUtils.toModel(oddevenConfig.getJson(), OddevenConfig.class));
        }

        if (ZoneConfig.getOddeven().isEnabled() && Config.getSysConfig().getZone().isOddeven()) {
            oddevenParsing.parsingGame();
            oddevenParsing.checkResult();
        }
    }

    private void initLotusOddevenConfig() {
        // 홀짝
        JsonConfig lotusOddevenConfig = jsonRepository.findOne("lotusOddeven");
        if (lotusOddevenConfig == null) {
            ZoneConfig.setLotusOddeven(new LotusOddevenConfig());
        } else {
            ZoneConfig.setLotusOddeven(JsonUtils.toModel(lotusOddevenConfig.getJson(), LotusOddevenConfig.class));
        }

        if (ZoneConfig.getLotusOddeven().isEnabled() && Config.getSysConfig().getZone().isLotusOddeven()) {
            lotusOddevenParsing.parsingGame();
            lotusOddevenParsing.checkResult();
        }
    }

    private void initLowhiConfig() {
        // 로하이
        JsonConfig lowhiConfig = jsonRepository.findOne("lowhi");
        if (lowhiConfig == null) {
            ZoneConfig.setLowhi(new LowhiConfig());
        } else {
            ZoneConfig.setLowhi(JsonUtils.toModel(lowhiConfig.getJson(), LowhiConfig.class));
        }

        if (ZoneConfig.getLowhi().isEnabled() && Config.getSysConfig().getZone().isLowhi()) {
            lowhiParsing.parsingGame();
            lowhiParsing.checkResult();
        }
    }

    private void initAladdinConfig() {
        // 알라딘
        JsonConfig aladdinConfig = jsonRepository.findOne("aladdin");
        if (aladdinConfig == null) {
            ZoneConfig.setAladdin(new AladdinConfig());
        } else {
            ZoneConfig.setAladdin(JsonUtils.toModel(aladdinConfig.getJson(), AladdinConfig.class));
        }

        if (ZoneConfig.getAladdin().isEnabled() && Config.getSysConfig().getZone().isAladdin()) {
            aladdinParsing.parsingGame();
            aladdinParsing.checkResult();
        }
    }

    private void initPowerLadderConfig() {
        // 파워볼
        JsonConfig powerLadderConfig = jsonRepository.findOne("power_ladder");
        if (powerLadderConfig == null) {
            ZoneConfig.setPowerLadder(new PowerLadderConfig());
        } else {
            ZoneConfig.setPowerLadder(JsonUtils.toModel(powerLadderConfig.getJson(), PowerLadderConfig.class));
        }

        if (ZoneConfig.getPowerLadder().isEnabled() && Config.getSysConfig().getZone().isPowerLadder()) {
            powerLadderParsing.parsingGame();
            powerLadderParsing.checkResult();
        }
    }

    private void initPowerConfig() {
        // 파워볼
        JsonConfig powerConfig = jsonRepository.findOne("power");
        if (powerConfig == null) {
            ZoneConfig.setPower(new PowerConfig());
        } else {
            ZoneConfig.setPower(JsonUtils.toModel(powerConfig.getJson(), PowerConfig.class));
        }

        if (ZoneConfig.getPower().isEnabled() && Config.getSysConfig().getZone().isPower()) {
            powerParsing.parsingGame();
            powerParsing.checkResult();
        }
    }

    private void initDariConfig() {
        // 다리다리
        JsonConfig dariConfig = jsonRepository.findOne("dari");
        if (dariConfig == null) {
            ZoneConfig.setDari(new DariConfig());
        } else {
            ZoneConfig.setDari(JsonUtils.toModel(dariConfig.getJson(), DariConfig.class));
        }

        if (ZoneConfig.getDari().isEnabled() && Config.getSysConfig().getZone().isDari()) {
            dariParsing.parsingGame();
            dariParsing.checkResult();
        }
    }

    private void initSnailConfig() {
        // 달팽이
        JsonConfig snailConfig = jsonRepository.findOne("snail");
        if (snailConfig == null) {
            ZoneConfig.setSnail(new SnailConfig());
        } else {
            ZoneConfig.setSnail(JsonUtils.toModel(snailConfig.getJson(), SnailConfig.class));
        }

        if (ZoneConfig.getSnail().isEnabled() && Config.getSysConfig().getZone().isSnail()) {
            snailParsing.parsingGame();
            snailParsing.checkResult();
        }
    }

    private void initNewSnailConfig() {
        // 달팽이
        JsonConfig newSnailConfig = jsonRepository.findOne("newSnail");
        if (newSnailConfig == null) {
            ZoneConfig.setNewSnail(new NewSnailConfig());
        } else {
            ZoneConfig.setNewSnail(JsonUtils.toModel(newSnailConfig.getJson(), NewSnailConfig.class));
        }

        if (ZoneConfig.getNewSnail().isEnabled() && Config.getSysConfig().getZone().isNewSnail()) {
            newSnailParsing.parsingGame();
            newSnailParsing.checkResult();
        }
    }

    private void initLadderConfig() {
        // 사다리
        JsonConfig ladderConfig = jsonRepository.findOne("ladder");
        if (ladderConfig == null) {
            ZoneConfig.setLadder(new LadderConfig());
        } else {
            ZoneConfig.setLadder(JsonUtils.toModel(ladderConfig.getJson(), LadderConfig.class));
        }

        if (ZoneConfig.getLadder().isEnabled() && Config.getSysConfig().getZone().isLadder()) {
            ladderParsing.parsingGame();
            ladderParsing.checkResult();
        }
    }

    private void initKenoLadderConfig() {
        // 키노사다리
        JsonConfig KenoLadderConfig = jsonRepository.findOne("keno_ladder");

        System.out.println("KenoLadderConfig="+KenoLadderConfig);

        if (KenoLadderConfig == null) {
            ZoneConfig.setKenoLadder(new KenoLadderConfig());
        } else {
            ZoneConfig.setKenoLadder(JsonUtils.toModel(KenoLadderConfig.getJson(), KenoLadderConfig.class));
        }

        if (ZoneConfig.getKenoLadder().isEnabled() && Config.getSysConfig().getZone().isKenoLadder()) {
            kenoLadderParsing.parsingGame();
            kenoLadderParsing.checkResult();
        }
    }

    private void initKenoSpeedConfig() {
        // 키노스피드
        JsonConfig KenoSpeedConfig = jsonRepository.findOne("keno_speed");

        //System.out.println("KenoSpeedConfig="+KenoSpeedConfig);

        if (KenoSpeedConfig == null) {
            ZoneConfig.setKenoSpeed(new KenoSpeedConfig());
        } else {
            ZoneConfig.setKenoSpeed(JsonUtils.toModel(KenoSpeedConfig.getJson(), KenoSpeedConfig.class));
        }

        if (ZoneConfig.getKenoSpeed().isEnabled() && Config.getSysConfig().getZone().isKenoSpeed()) {
            kenoSpeedParsing.parsingGame();
            kenoSpeedParsing.checkResult();
        }
    }

    private void initSpeedHomeRunConfig() {
        // 스피드 홈런
        JsonConfig SpeedHomeRunConfig = jsonRepository.findOne("speed_homeRun");

        //System.out.println("SpeedHomeRunConfig=" + SpeedHomeRunConfig);

        if (SpeedHomeRunConfig == null) {
            ZoneConfig.setSpeedHomeRun(new SpeedHomeRunConfig());
        } else {
            ZoneConfig.setSpeedHomeRun(JsonUtils.toModel(SpeedHomeRunConfig.getJson(), SpeedHomeRunConfig.class));
        }

        if (ZoneConfig.getSpeedHomeRun().isEnabled() && Config.getSysConfig().getZone().isSpeedHomeRun()) {
            speedHomeRunParsing.parsingGame();
            speedHomeRunParsing.checkResult();
        }
    }

    @Getter
    @Setter
    private static LadderConfig ladder;

    @Getter
    @Setter
    private static SnailConfig snail;

    @Getter
    @Setter
    private static NewSnailConfig newSnail;

    @Getter
    @Setter
    private static BinanceCoin1Config binanceCoin1;

    @Getter
    @Setter
    private static BinanceCoin3Config binanceCoin3;

    @Getter
    @Setter
    private static DariConfig dari;

    @Getter
    @Setter
    private static PowerConfig power;

    @Getter
    @Setter
    private static EosPower1Config EosPower1;

    @Getter
    @Setter
    private static EosPower2Config EosPower2;

    @Getter
    @Setter
    private static EosPower3Config EosPower3;

    @Getter
    @Setter
    private static EosPower4Config EosPower4;

    @Getter
    @Setter
    private static EosPower5Config EosPower5;

    @Getter
    @Setter
    private static PowerFreeKickConfig powerFreeKick;

    @Getter
    @Setter
    private static PowerLadderConfig powerLadder;

    @Getter
    @Setter
    private static AladdinConfig aladdin;

    @Getter
    @Setter
    private static LowhiConfig lowhi;

    @Getter
    @Setter
    private static OddevenConfig oddeven;

    @Getter
    @Setter
    private static BaccaratConfig baccarat;

    @Getter
    @Setter
    private static LotusOddevenConfig lotusOddeven;

    @Getter
    @Setter
    private static LotusBaccaratConfig lotusBaccarat;

    @Getter
    @Setter
    private static LotusBaccarat2Config lotusBaccarat2;

    @Getter
    @Setter
    private static SoccerConfig soccer;

    @Getter
    @Setter
    private static DogConfig dog;

    @Getter
    @Setter
    private static LuckConfig luck;

    @Getter
    @Setter
    private static KenoLadderConfig kenoLadder;

    @Getter
    @Setter
    private static KenoSpeedConfig kenoSpeed;

    @Getter
    @Setter
    private static SpeedHomeRunConfig speedHomeRun;

    @Getter
    @Setter
    private static FxGame1Config fxGame1;

    @Getter
    @Setter
    private static FxGame2Config fxGame2;

    @Getter
    @Setter
    private static FxGame3Config fxGame3;

    @Getter
    @Setter
    private static FxGame4Config fxGame4;

    @Getter
    @Setter
    private static FxGame5Config fxGame5;

    @Getter
    @Setter
    private static BogleLadderConfig bogleLadder;

    @Getter
    @Setter
    private static BoglePowerConfig boglePower;
}
