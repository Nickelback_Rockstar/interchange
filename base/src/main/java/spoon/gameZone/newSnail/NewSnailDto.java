package spoon.gameZone.newSnail;

import lombok.Data;
import spoon.common.utils.DateUtils;

import java.util.Date;

public class NewSnailDto {

    @Data
    public static class Config {
        private long money;
        private boolean enabled;
        private int round;
        private Date gameDate;
        private int betTime;
        private String sdate;
        private double[] odds;
        private int max;
        private int win;
        private int min;

        public String getGameDateName() {
            return DateUtils.format(gameDate, "MM/dd(E)");
        }

        public String getGameTimeName() {
            return DateUtils.format(gameDate, "HH:mm");
        }
    }

    @Data
    public static class Score {
        private long id;
        private int round;
        private Date gameDate;
        private boolean cancel;

        private String rank1;
        private String rank2;
        private String rank3;
        private String rank4;
        private String hurdles1;
        private String hurdles2;
        private String hurdles3;
        private String hurdles4;
        private String oddeven;
        private String overunder;

    }


}
