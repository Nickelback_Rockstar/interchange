package spoon.gameZone.lotusOddeven;

import lombok.Data;
import spoon.common.utils.DateUtils;
import spoon.common.utils.StringUtils;

import java.util.Date;

public class LotusOddevenDto {

    @Data
    public static class Config {
        private long money;
        private boolean enabled;
        private int round;
        private Date gameDate;
        private int betTime;
        private String sdate;
        private double[] odds;
        private int max;
        private int win;
        private int min;

        private boolean oddeven;
        private boolean overunder;
        private boolean pattern;
        private boolean redblack;

        public String getGameDateName() {
            return DateUtils.format(gameDate, "MM/dd(E)");
        }

        public String getGameTimeName() {
            return DateUtils.format(gameDate, "HH:mm");
        }
    }

    @Data
    public static class Score {
        private long id;
        private int round;
        private Date gameDate;
        private boolean cancel;

        private String card1;
        private String card2;
        private String oddeven;
        private String overunder;
        private String pattern;
        private String redblack;

        private String card;

        /**
         * 봇 결과 두카드를 받아서 결과처리의 card형태로 반환한다.
         */
        public void convertCard(String card1, String card2) {
            this.card1 = card1;
            this.card2 = card2;
            this.card = bot2score(card1) + "+" + bot2score(card2);
            if ("+".equals(this.card)) {
                this.card = "";
            }
        }

        /**
         * 결과처리 형태를 봇 형태로 전환한다.
         */
        public boolean convertCard() {
            try {
                String card1 = this.card1;//this.card.split("\\+")[0].trim();
                String card2 = this.card2;//this.card.split("\\+")[1].trim(); // 히든카드

                if(!this.cancel && ( "000".equals(card1) || "000".equals(card2) )){
                    return false;
                }

                String p1 = card1.substring(0, 1);
                String p2 = card2.substring(0, 1);

                int c1 = Integer.parseInt(card1.substring(1,3));
                int c2 = Integer.parseInt(card2.substring(1,3));

                this.overunder = c2 > 5 ? "OVER" : "UNDER";
                this.oddeven = (c1 + c2) % 2 == 0 ? "E" : "O";
                this.pattern = convertPatternResult(p2);
                this.redblack = convertRedblackResult(p2);
                this.card1 = card1;
                this.card2 = card2;

                return true;
            } catch (Exception e) {
                return false;
            }
        }

        private String score2bot(String p, int c) {
            //if (c == 1) c = 14;
            return String.format("%s%02d", p, c);
        }

        private String bot2score(String card) {
            if (StringUtils.empty(card)) return "";
            if (card.length() != 3) return "";
            String card1 = card.substring(0, 1);
            String card2 = String.valueOf(Integer.parseInt(card.substring(1), 10));
            //if ("14".equals(card2)) card2 = "A";
            /*switch (card1) {
                case "1":
                    return "♠" + card2;
                case "2":
                    return "♥" + card2;
                case "3":
                    return "♣" + card2;
                case "4":
                    return "◆" + card2;
                default:
                    return "";
            }*/
            return card;
        }

        private String convertPattern(String p) {
            /*switch (p) {
                case "♠":
                    return "1";
                case "♥":
                    return "2";
                case "♣":
                    return "3";
                case "◆":
                    return "4";
                default:
                    return "";
            }*/
            switch (p) {
                case "♠":
                    return "0";
                case "♥":
                    return "2";
                case "♣":
                    return "3";
                case "◆":
                    return "1";
                default:
                    return "";
            }
        }

        private String convertPatternResult(String p) {
            switch (p) {
                case "0":
                    return "스페이드";
                case "2":
                    return "하트";
                case "3":
                    return "크로바";
                case "1":
                    return "다이아";
                default:
                    return "";
            }
        }

        private String convertRedblackResult(String p) {
            switch (p) {
                case "0":
                    return "블랙";
                case "2":
                    return "레드";
                case "3":
                    return "블랙";
                case "1":
                    return "레드";
                default:
                    return "";
            }
        }

        private int convertNumber(String p) {
            if ("A".equals(p)) p = "1";
            return Integer.parseInt(p, 10);
        }
    }
}
