package spoon.gameZone.lotusOddeven.service;

import spoon.gameZone.lotusOddeven.LotusOddeven;

import java.util.Date;

public interface LotusOddevenBotService {

    boolean notExist(Date gameDate);

    void addGame(LotusOddeven oddeven);

    boolean closingGame(LotusOddeven result);

    void checkResult();

    void deleteGame();

}
