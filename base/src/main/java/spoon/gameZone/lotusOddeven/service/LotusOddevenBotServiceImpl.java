package spoon.gameZone.lotusOddeven.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionInterceptor;
import spoon.common.utils.DateUtils;
import spoon.common.utils.ErrorUtils;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.lotusOddeven.LotusOddeven;
import spoon.gameZone.lotusOddeven.LotusOddevenRepository;
import spoon.gameZone.lotusOddeven.QLotusOddeven;
import spoon.mapper.GameMapper;

import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class LotusOddevenBotServiceImpl implements LotusOddevenBotService {

    private LotusOddevenGameService lotusOddevenGameService;

    private LotusOddevenRepository lotusOddevenRepository;

    private static QLotusOddeven q = QLotusOddeven.lotusOddeven;

    private GameMapper gameMapper;

    @Transactional(readOnly = true)
    @Override
    public boolean notExist(Date gameDate) {
        return lotusOddevenRepository.count(q.sdate.eq(DateUtils.format(gameDate, "yyyyMMddHHmm"))) == 0;
    }

    @Transactional
    @Override
    public void addGame(LotusOddeven lotusOddeven) {
        lotusOddevenRepository.saveAndFlush(lotusOddeven);
    }

    @Transactional
    @Override
    public boolean closingGame(LotusOddeven result) {
        LotusOddeven lotusOddeven = lotusOddevenRepository.findOne(q.sdate.eq(result.getSdate()).and(q.closing.isFalse()));
        if (lotusOddeven == null) {
            return true;
        }

        try {
            lotusOddeven.setCard1(result.getCard1());
            lotusOddeven.setCard2(result.getCard2());
            lotusOddeven.setOddeven(result.getOddeven());

            if ("O".equals(result.getOddeven()) || "E".equals(result.getOddeven())) {
                String pattern = result.getCard2().substring(0, 1);
                int hidden = Integer.parseInt(result.getCard2().substring(1), 10);
                if (hidden == 14) hidden = 1;
                lotusOddeven.setOverunder(hidden > 5 ? "OVER" : "UNDER");
                lotusOddeven.setPattern(getPattern(pattern));
                lotusOddeven.setRedblack(getRedblack(pattern));
                lotusOddeven.setCancel(false);
            } else {
                lotusOddeven.setOverunder("");
                lotusOddeven.setPattern("");
                lotusOddeven.setRedblack("");
                lotusOddeven.setCancel(true);
            }
            lotusOddeven.setClosing(true);

            lotusOddevenRepository.saveAndFlush(lotusOddeven);
            lotusOddevenGameService.closingBetting(lotusOddeven);
        } catch (RuntimeException e) {
            log.error("홀짝 {}회차 결과 업데이트에 실패하였습니다. - {}", lotusOddeven.getRound(), e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional(readOnly = true)
    @Override
    public void checkResult() {
        long cnt = lotusOddevenRepository.count(q.gameDate.before(DateUtils.beforeSeconds(120)).and(q.closing.isFalse()));
        ZoneConfig.getLotusOddeven().setResult(cnt);
    }

    @Transactional
    @Override
    public void deleteGame() {
        gameMapper.deleteOldLotusOddeven();
    }

    private String getPattern(String p) {
        switch (p) {
            case "0":
                return "스페이드";
            case "2":
                return "하트";
            case "3":
                return "크로바";
            case "1":
                return "다이아";
            default:
                return "";
        }
    }

    private String getRedblack(String p) {
        switch (p) {
            case "0":
                return "블랙";
            case "2":
                return "레드";
            case "3":
                return "블랙";
            case "1":
                return "레드";
            default:
                return "";
        }
    }
}
