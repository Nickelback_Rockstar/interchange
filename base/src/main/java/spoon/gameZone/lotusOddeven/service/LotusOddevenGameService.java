package spoon.gameZone.lotusOddeven.service;

import spoon.gameZone.ZoneDto;
import spoon.gameZone.lotusOddeven.LotusOddeven;
import spoon.support.web.AjaxResult;

public interface LotusOddevenGameService {

    /**
     * 홀짝 게임을 베팅한다.
     */
    AjaxResult betting(ZoneDto.Bet bet);

    /**
     * 홀짝 게임 베팅을 클로징 한다.
     */
    void closingBetting(LotusOddeven lotusOddeven);

    /**
     * 홀짝 게임을 롤백 한다.
     */
    void rollbackPayment(LotusOddeven lotusOddeven);
}
