package spoon.gameZone.powerFreeKick;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import spoon.bot.support.PowerMaker;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class PowerFreeKickConfig {

    @JsonIgnore
    private PowerMaker powerFreeKickMaker;

    private boolean enabled;

    // 결과처리가 잘 되었는지 체크 (관리자에서 사용)
    private long result;

    private int betTime;

    // 회차별 베팅 숫자
    private int betMax = 1;

    private boolean player;
    private boolean lastball;
    private boolean goal;
    private boolean direction;

    /**
     * 1번선수 0, 2번선수 1
     * 좌측 2, 우측 3
     * 골 4, 노골 5
     */
    private double[] odds = new double[6];

    private int[] win = {3000000, 3000000, 3000000, 3000000, 3000000, 3000000, 3000000, 3000000, 3000000, 3000000, 3000000};

    private int[] max = {1000000, 1000000, 1000000, 1000000, 1000000, 1000000, 1000000, 1000000, 1000000, 1000000, 1000000};

    private int[] min = {5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000};

    public PowerFreeKickConfig() {
        this.powerFreeKickMaker = new PowerMaker();
    }

}
