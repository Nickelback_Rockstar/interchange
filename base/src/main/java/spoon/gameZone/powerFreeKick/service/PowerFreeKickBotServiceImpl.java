package spoon.gameZone.powerFreeKick.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionInterceptor;
import spoon.common.utils.DateUtils;
import spoon.common.utils.ErrorUtils;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.powerFreeKick.PowerFreeKick;
import spoon.gameZone.powerFreeKick.PowerFreeKickRepository;
import spoon.gameZone.powerFreeKick.QPowerFreeKick;

import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class PowerFreeKickBotServiceImpl implements PowerFreeKickBotService {

    private PowerFreeKickGameService powerFreeKickGameService;

    private PowerFreeKickRepository powerFreeKickRepository;

    private static QPowerFreeKick q = QPowerFreeKick.powerFreeKick;

    @Transactional(readOnly = true)
    @Override
    public boolean notExist(Date gameDate) {
        return powerFreeKickRepository.count(q.sdate.eq(DateUtils.format(gameDate, "yyyyMMddHHmmss"))) == 0;
    }

    @Transactional
    @Override
    public void addGame(PowerFreeKick power) {
        powerFreeKickRepository.saveAndFlush(power);
    }

    @Transactional
    @Override
    public boolean closingGame(PowerFreeKick result) {
        PowerFreeKick powerFreeKick = powerFreeKickRepository.findOne(q.sdate.eq(result.getSdate()).and(q.closing.isFalse()));
        if (powerFreeKick == null) {
            return true;
        }

        try {
            powerFreeKick.setLastBall(result.getLastBall());
            powerFreeKick.setPlayer(result.getPlayer());
            powerFreeKick.setDirection(result.getDirection());
            powerFreeKick.setGoal(result.getGoal());
            powerFreeKick.setTimes(result.getRound());
            powerFreeKick.setClosing(true);

            powerFreeKickRepository.saveAndFlush(powerFreeKick);
            powerFreeKickGameService.closingBetting(powerFreeKick);
        } catch (RuntimeException e) {
            log.error("파워프리킥 {}회차 결과 업데이트에 실패하였습니다. - {}", powerFreeKick.getTimes(), e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional(readOnly = true)
    @Override
    public void checkResult() {
        long cnt = powerFreeKickRepository.count(q.gameDate.before(new Date()).and(q.closing.isFalse()));
        ZoneConfig.getPowerFreeKick().setResult(cnt);
    }

    @Transactional
    @Override
    public void deleteGame(int days) {
        powerFreeKickRepository.deleteGame(DateUtils.beforeDays(days));
    }
}
