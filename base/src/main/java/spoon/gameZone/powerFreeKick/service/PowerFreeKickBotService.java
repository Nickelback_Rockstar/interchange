package spoon.gameZone.powerFreeKick.service;

import spoon.gameZone.powerFreeKick.PowerFreeKick;

import java.util.Date;

public interface PowerFreeKickBotService {

    boolean notExist(Date gameDate);

    void addGame(PowerFreeKick powerFreeKick);

    boolean closingGame(PowerFreeKick result);

    void checkResult();

    void deleteGame(int days);

}
