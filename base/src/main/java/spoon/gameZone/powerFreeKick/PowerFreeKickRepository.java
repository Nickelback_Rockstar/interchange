package spoon.gameZone.powerFreeKick;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.query.Param;

import java.util.Date;

public interface PowerFreeKickRepository extends JpaRepository<PowerFreeKick, Long>, QueryDslPredicateExecutor<PowerFreeKick> {

    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE PowerFreeKick o SET o.odds = :odds WHERE o.gameDate > CURRENT_TIMESTAMP")
    void updateOdds(@Param("odds") double[] odds);

    @Modifying(clearAutomatically = true)
    @Query(value = "DELETE FROM PowerFreeKick o WHERE o.gameDate < :gameDate")
    void deleteGame(@Param("gameDate") Date gameDate);
}
