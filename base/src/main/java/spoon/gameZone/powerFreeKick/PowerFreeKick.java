package spoon.gameZone.powerFreeKick;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;
import spoon.bot.support.ZoneHelper;
import spoon.common.utils.DateUtils;
import spoon.config.domain.Config;
import spoon.game.domain.MenuCode;
import spoon.gameZone.GameZoneException;
import spoon.gameZone.Zone;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.ZoneScore;
import spoon.support.convert.DoubleArrayConvert;
import spoon.support.convert.LongArrayConvert;

import javax.persistence.*;
import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Data
@Entity
@Table(name = "ZONE_POWERFREEKICK", indexes = {
        @Index(name = "IDX_sdate", columnList = "sdate")
})
public class PowerFreeKick {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Long id;

    @Temporal(value = TemporalType.TIMESTAMP)
    private Date gameDate;

    private int round;

    private int times;

    @Column(length = 16)
    private String sdate;

    @Convert(converter = LongArrayConvert.class)
    private long[] amount = new long[6];

    @Convert(converter = DoubleArrayConvert.class)
    private double[] odds = new double[6];

    private boolean closing;

    private boolean cancel;

    //파워프리킥 s
    @Column(length = 10)
    private String goal; //골,노골

    @Column(length = 2)
    private String player; //1번선수, 2번선수

    private String lastBall; //일반볼 마지막

    @Column(length = 10)
    private String direction; //일반볼 마지막 방향
    //파워프리킥 e

    //----------------------------------------------------------------

    public PowerFreeKick(int times, Date gameDate) {
        this.times = 0;
        this.round = (Config.getSysConfig().getZone().getPowerDay() + times) % 288;
        if (this.round == 0) this.round = 288;
        this.gameDate = gameDate;
        this.sdate = DateUtils.format(gameDate, "yyyyMMddHHmmss");
    }

    public boolean isBeforeGameDate() {
        return this.gameDate.getTime() - System.currentTimeMillis() > 0;
    }

    // 스코어 입력
    public void updateScore(PowerFreeKickDto.Score score) {
        if (score.isCancel()) {
            this.player = "";
            this.direction = "";
            this.goal = "";
            this.cancel = true;
        } else {

            int lastBall = Integer.parseInt(score.getLastBall());
            this.lastBall = String.valueOf(lastBall);
            if(1 <= lastBall && lastBall <= 14){ //1~14 1번선수
                this.player = "1";
            }else if(15 <= lastBall && lastBall <= 28){ //15~28 2번선수
                this.player = "2";
            }

            this.direction = lastBall % 2 == 1 ? "LEFT" : "RIGHT";

            if("1".equals(this.player) && "LEFT".equals(this.direction)) {//1번선수가 좌측 이면 골
                this.goal = "Y";
            }else if("2".equals(this.player) && "RIGHT".equals(this.direction)){//2번선수가 우측 이면 골
                this.goal = "Y";
            }else{
                this.goal = "N";
            }

            this.cancel = false;

        }
        this.closing = true;
    }

    // 경기 결과
    public ZoneScore getGameResult(String gameCode) {
        switch (gameCode) {
            case "player":
                return ZoneHelper.zoneResult(player , cancel);
            case "direction":
                return ZoneHelper.zoneResult(direction , cancel);
            case "goal":
                return ZoneHelper.zoneResult(goal , cancel);
            default:
                throw new GameZoneException("파워프리킥 코드 " + gameCode + " 를 확인 할 수 없습니다.");
        }
    }

    public Zone getZone(String gameCode) {
        switch (gameCode) {
            case "player":
                return getPowerFreeKickZone("player", "선수", "1번", "2번", 0, 1);
            case "direction":
                return getPowerFreeKickZone("direction", "방향", "좌", "우", 2, 3);
            case "goal":
                return getPowerFreeKickZone("goal", "골", "골", "노골", 4, 5);
            default:
                throw new GameZoneException("파워볼 코드 " + gameCode + " 를 확인 할 수 없습니다.");
        }
    }
    private Zone getPowerFreeKickZone(String gameCode, String type, String teamHome, String teamAway, int idxHome, int idxAway) {
        Zone zone = new Zone();
        zone.setId(this.getId());
        zone.setSdate(this.getSdate());
        zone.setMenuCode(MenuCode.POWERFREEKICK);
        zone.setGameCode(gameCode);
        zone.setLeague(String.format("%03d회차 파워프리킥", this.round));
        zone.setTeamHome(String.format("%03d회차 %s [%s]", this.round, type, teamHome));
        zone.setTeamAway(String.format("%03d회차 %s [%s]", this.round, type, teamAway));
        zone.setHandicap(0);
        zone.setOddsHome(ZoneConfig.getPowerFreeKick().getOdds()[idxHome]);
        zone.setOddsDraw(0);
        zone.setOddsAway(ZoneConfig.getPowerFreeKick().getOdds()[idxAway]);
        zone.setGameDate(this.gameDate);
        return zone;
    }

    public boolean isChangeResult(PowerFreeKickDto.Score score) {
        return !this.closing || this.cancel != score.isCancel() || !(score.getLastBall().equals(this.lastBall) );

    }
}
