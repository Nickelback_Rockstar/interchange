package spoon.gameZone.powerFreeKick;

import lombok.Data;
import spoon.common.utils.DateUtils;

import java.util.Date;

public class PowerFreeKickDto {

    @Data
    public static class Config {
        private long money;
        private boolean enabled;
        private int round;
        private int times;
        private Date gameDate;
        private int betTime;
        private String sdate;
        private double[] odds;
        private int max;
        private int win;
        private int min;

        private boolean player;
        private boolean goal;
        private boolean direction;

        public String getGameDateName() {
            return DateUtils.format(gameDate, "MM/dd(E)");
        }

        public String getGameTimeName() {
            return DateUtils.format(gameDate, "HH:mm");
        }
    }

    @Data
    public static class Score {
        private long id;
        private int round;
        private int times;
        private Date gameDate;
        private boolean cancel;

        private String pb;
        private String ball;
        private String lastBall;
    }


}
