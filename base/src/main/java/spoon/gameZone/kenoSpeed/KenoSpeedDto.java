package spoon.gameZone.kenoSpeed;

import lombok.Data;
import spoon.common.utils.DateUtils;

import java.util.Date;

public class KenoSpeedDto {

    @Data
    public static class Config {
        private long money;
        private boolean enabled;
        private int round;
        private Date gameDate;
        private int betTime;
        private String sdate;
        private double[] odds;
        private int max;
        private int win;
        private int min;

        private boolean oddeven;
        private boolean overUnder;
        private boolean num0;
        private boolean num1;
        private boolean num2;
        private boolean num3;
        private boolean num4;
        private boolean num5;
        private boolean num6;
        private boolean num7;
        private boolean num8;
        private boolean num9;

        public String getGameDateName() {
            return DateUtils.format(gameDate, "MM/dd(E)");
        }

        public String getGameTimeName() {
            return DateUtils.format(gameDate, "HH:mm");
        }
    }

    @Data
    public static class Score {
        private long id;
        private int round;
        private Date gameDate;
        private boolean cancel;

        private String overUnder;
        private String oddeven;
        private String num;
        private String num0;
        private String num1;
        private String num2;
        private String num3;
        private String num4;
        private String num5;
        private String num6;
        private String num7;
        private String num8;
        private String num9;


    }
}
