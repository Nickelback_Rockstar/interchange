package spoon.gameZone.kenoSpeed.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.kenoSpeed.KenoSpeed;
import spoon.gameZone.kenoSpeed.KenoSpeedConfig;
import spoon.gameZone.kenoSpeed.KenoSpeedDto;
import spoon.support.web.AjaxResult;

public interface KenoSpeedService {

    /**
     * 스피드키노 설정을 변경한다.
     */
    boolean updateConfig(KenoSpeedConfig kenoLadderConfig);

    /**
     * 스피드키노 등록된 게임을 가져온다.
     */
    Iterable<KenoSpeed> getComplete();

    /**
     * 스피드키노 종료된 게임을 가져온다.
     */
    Page<KenoSpeed> getClosing(ZoneDto.Command command, Pageable pageable);

    /**
     * 스피드키노 봇에 접속하여 기존 결과가 있는지 확인 한다.
     */
    KenoSpeedDto.Score findScore(Long id);

    /**
     * 스피드키노 스코어를 가지고 결과처리를 한다.
     */
    boolean closingGame(KenoSpeedDto.Score score);

    /**
     * 결과처리가 되지 않고 베팅이 없는 모든 경기를 종료처리 한다.
     */
    AjaxResult closingAllGame();

    /**
     * 현재 진행중인 경기의 설정을 가져온다.
     */
    KenoSpeedDto.Config gameConfig();

}
