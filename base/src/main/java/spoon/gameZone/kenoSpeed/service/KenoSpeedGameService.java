package spoon.gameZone.kenoSpeed.service;

import spoon.gameZone.ZoneDto;
import spoon.gameZone.kenoSpeed.KenoSpeed;
import spoon.support.web.AjaxResult;

public interface KenoSpeedGameService {

    /**
     * 스피드키노 게임을 베팅한다.
     */
    AjaxResult betting(ZoneDto.Bet bet);

    /**
     * 스피드키노 게임 베팅을 클로징 한다.
     */
    void closingBetting(KenoSpeed ladder);

    /**
     * 스피드키노 게임을 롤백 한다.
     */
    void rollbackPayment(KenoSpeed ladder);
}
