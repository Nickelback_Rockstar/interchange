package spoon.gameZone.kenoSpeed.service;

import spoon.gameZone.kenoSpeed.KenoSpeed;

import java.util.Date;

public interface KenoSpeedBotService {

    boolean notExist(Date gameDate);

    void addGame(KenoSpeed kenoSpeed);

    boolean closingGame(KenoSpeed result);

    void checkResult();

    void deleteGame(int days);

}
