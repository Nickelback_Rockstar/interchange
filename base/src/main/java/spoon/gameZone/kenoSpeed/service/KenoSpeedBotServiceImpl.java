package spoon.gameZone.kenoSpeed.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionInterceptor;
import spoon.common.utils.DateUtils;
import spoon.common.utils.ErrorUtils;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.kenoSpeed.KenoSpeed;
import spoon.gameZone.kenoSpeed.KenoSpeedRepository;
import spoon.gameZone.kenoSpeed.QKenoSpeed;

import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class KenoSpeedBotServiceImpl implements KenoSpeedBotService {

    private KenoSpeedGameService kenoSpeedGameService;

    private KenoSpeedRepository kenoSpeedRepository;

    private static QKenoSpeed q = QKenoSpeed.kenoSpeed;

    @Transactional(readOnly = true)
    @Override
    public boolean notExist(Date gameDate) {
        return kenoSpeedRepository.count(q.sdate.eq(DateUtils.format(gameDate, "yyyyMMddHHmmss"))) == 0;
    }

    @Transactional
    @Override
    public void addGame(KenoSpeed kenoSpeed) {
        kenoSpeedRepository.saveAndFlush(kenoSpeed);
    }

    @Transactional
    @Override
    public boolean closingGame(KenoSpeed result) {
        KenoSpeed kenoSpeed = kenoSpeedRepository.findOne(q.sdate.eq(result.getSdate()).and(q.closing.isFalse()));
        if (kenoSpeed == null) {
            return true;
        }

        try {
            kenoSpeed.setOddeven(result.getOddeven());
            kenoSpeed.setOverUnder(result.getOverUnder());
            kenoSpeed.setNum0(result.getNum0());
            kenoSpeed.setNum1(result.getNum1());
            kenoSpeed.setNum2(result.getNum2());
            kenoSpeed.setNum3(result.getNum3());
            kenoSpeed.setNum4(result.getNum4());
            kenoSpeed.setNum5(result.getNum5());
            kenoSpeed.setNum6(result.getNum6());
            kenoSpeed.setNum7(result.getNum7());
            kenoSpeed.setNum8(result.getNum8());
            kenoSpeed.setNum9(result.getNum9());
            kenoSpeed.setClosing(true);

            kenoSpeedRepository.saveAndFlush(kenoSpeed);
            kenoSpeedGameService.closingBetting(kenoSpeed);
        } catch (RuntimeException e) {
            log.error("스피드키노 {}회차 결과 업데이트에 실패하였습니다. - {}", kenoSpeed.getRound(), e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional(readOnly = true)
    @Override
    public void checkResult() {
        long cnt = kenoSpeedRepository.count(q.gameDate.before(new Date()).and(q.closing.isFalse()));
        ZoneConfig.getKenoSpeed().setResult(cnt);
    }

    @Transactional
    @Override
    public void deleteGame(int days) {
        kenoSpeedRepository.deleteGame(DateUtils.beforeDays(days));
    }
}
