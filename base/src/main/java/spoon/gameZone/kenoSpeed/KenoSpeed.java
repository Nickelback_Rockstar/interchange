package spoon.gameZone.kenoSpeed;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;
import spoon.bot.support.ZoneHelper;
import spoon.common.utils.DateUtils;
import spoon.config.domain.Config;
import spoon.game.domain.MenuCode;
import spoon.gameZone.GameZoneException;
import spoon.gameZone.Zone;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.ZoneScore;
import spoon.support.convert.DoubleArrayConvert;
import spoon.support.convert.LongArrayConvert;

import javax.persistence.*;
import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Data
@Entity
@Table(name = "ZONE_KENOSPEED", indexes = {
        @Index(name = "IDX_sdate", columnList = "sdate")
})
public class KenoSpeed {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Long id;

    @Temporal(value = TemporalType.TIMESTAMP)
    private Date gameDate;

    private int round;

    // 홀짝
    @Column(length = 16)
    private String oddeven;

    // 오버언더
    @Column(length = 16)
    private String overUnder;

    //숫자
    @Column(length = 16)
    private String num0;
    @Column(length = 16)
    private String num1;
    @Column(length = 16)
    private String num2;
    @Column(length = 16)
    private String num3;
    @Column(length = 16)
    private String num4;
    @Column(length = 16)
    private String num5;
    @Column(length = 16)
    private String num6;
    @Column(length = 16)
    private String num7;
    @Column(length = 16)
    private String num8;
    @Column(length = 16)
    private String num9;

    @Column(length = 16)
    private String sdate;

    @Convert(converter = LongArrayConvert.class)
    private long[] amount = new long[15];

    @Convert(converter = DoubleArrayConvert.class)
    private double[] odds = new double[15];

    private boolean closing;

    private boolean cancel;

    //--------------------------------------------------------

    public KenoSpeed(int round, Date gameDate) {
        this.round = (Config.getSysConfig().getZone().getPowerDay() + round) % 288;
        if (this.round == 0) this.round = 288;
        this.gameDate = gameDate;
        this.sdate = DateUtils.format(gameDate, "yyyyMMddHHmmss");
    }

    public boolean isBeforeGameDate() {
        return this.gameDate.getTime() - System.currentTimeMillis() > 0;
    }

    // 스코어 입력
    public void updateScore(KenoSpeedDto.Score score) {
        if (score.isCancel()) {
            this.oddeven = "";
            this.overUnder = "";
            this.num0 = "";
            this.num1 = "";
            this.num2 = "";
            this.num3 = "";
            this.num4 = "";
            this.num5 = "";
            this.num6 = "";
            this.num7 = "";
            this.num8 = "";
            this.num9 = "";
            this.cancel = true;
        } else {
            this.oddeven = score.getOddeven();
            this.overUnder = score.getOverUnder();
            this.num0 = score.getNum0();
            this.num1 = score.getNum1();
            this.num2 = score.getNum2();
            this.num3 = score.getNum3();
            this.num4 = score.getNum4();
            this.num5 = score.getNum5();
            this.num6 = score.getNum6();
            this.num7 = score.getNum7();
            this.num8 = score.getNum8();
            this.num9 = score.getNum9();

            this.cancel = false;
        }
        this.closing = true;
    }

    // 경기 결과
    public ZoneScore getGameResult(String gameCode) {
        switch (gameCode) {
            case "oddeven":
                return ZoneHelper.zoneResult(oddeven, cancel);
            case "overUnder":
                return ZoneHelper.zoneResult(overUnder, cancel);
            case "num0":
                return ZoneHelper.zoneKSResult("num0", num0, cancel);
            case "num1":
                return ZoneHelper.zoneKSResult("num1", num1, cancel);
            case "num2":
                return ZoneHelper.zoneKSResult("num2", num2, cancel);
            case "num3":
                return ZoneHelper.zoneKSResult("num3", num3, cancel);
            case "num4":
                return ZoneHelper.zoneKSResult("num4", num4, cancel);
            case "num5":
                return ZoneHelper.zoneKSResult("num5", num5, cancel);
            case "num6":
                return ZoneHelper.zoneKSResult("num6", num6, cancel);
            case "num7":
                return ZoneHelper.zoneKSResult("num7", num7, cancel);
            case "num8":
                return ZoneHelper.zoneKSResult("num8", num8, cancel);
            case "num9":
                return ZoneHelper.zoneKSResult("num9", num9, cancel);
            default:
                throw new GameZoneException("스피드키노 코드 " + gameCode + " 를 확인 할 수 없습니다.");
        }
    }

    public Zone getZone(String gameCode) {
        switch (gameCode) {
            case "oddeven":
                return getKenoSpeedZone("oddeven", "홀", "짝", 0, 1);
            case "overUnder":
                return getKenoSpeedZone("overUnder", "오버", "언더", 2, 3);
            case "num0":
                return getKenoSpeedZone("num0", "숫자 0", "X", 4, 0);
            case "num1":
                return getKenoSpeedZone("num1", "숫자 1", "X", 4, 0);
            case "num2":
                return getKenoSpeedZone("num2", "숫자 2", "X", 4, 0);
            case "num3":
                return getKenoSpeedZone("num3", "숫자 3", "X", 4, 0);
            case "num4":
                return getKenoSpeedZone("num4", "숫자 4", "X", 4, 0);
            case "num5":
                return getKenoSpeedZone("num5", "숫자 5", "X", 4, 0);
            case "num6":
                return getKenoSpeedZone("num6", "숫자 6", "X", 4, 0);
            case "num7":
                return getKenoSpeedZone("num7", "숫자 7", "X", 4, 0);
            case "num8":
                return getKenoSpeedZone("num8", "숫자 8", "X", 4, 0);
            case "num9":
                return getKenoSpeedZone("num9", "숫자 9", "X", 4, 0);
            default:
                throw new GameZoneException("스피드키노 코드 " + gameCode + " 를 확인 할 수 없습니다.");
        }
    }

    private Zone getKenoSpeedZone(String gameCode, String teamHome, String teamAway, int idxHome, int idxAway) {
        Zone zone = new Zone();
        zone.setId(this.getId());
        zone.setSdate(this.getSdate());
        zone.setMenuCode(MenuCode.KENOSPEED);
        zone.setGameCode(gameCode);
        zone.setLeague(String.format("%03d회차 스피드키노", this.round));
        zone.setTeamHome(String.format("%03d회차 스피드키노 [%s]", this.round, teamHome));
        zone.setTeamAway(String.format("%03d회차 스피드키노 [%s]", this.round, teamAway));
        zone.setHandicap(0);
        zone.setOddsHome(ZoneConfig.getKenoSpeed().getOdds()[idxHome]);
        zone.setOddsDraw(0);
        if(gameCode.startsWith("num")){
            zone.setOddsAway(0);
        }else {
            zone.setOddsAway(ZoneConfig.getKenoSpeed().getOdds()[idxAway]);
        }
        zone.setGameDate(this.gameDate);
        return zone;
    }

    public boolean isChangeResult(KenoSpeedDto.Score score) {
        return !this.closing || this.cancel != score.isCancel()
                ||
                !(score.getOddeven().equals(this.oddeven)
                        && score.getOverUnder().equals(this.overUnder)
                        && score.getNum0().equals(this.num0)
                        && score.getNum1().equals(this.num1)
                        && score.getNum2().equals(this.num2)
                        && score.getNum3().equals(this.num3)
                        && score.getNum4().equals(this.num4)
                        && score.getNum5().equals(this.num5)
                        && score.getNum6().equals(this.num6)
                        && score.getNum7().equals(this.num7)
                        && score.getNum8().equals(this.num8)
                        && score.getNum9().equals(this.num9)
                );

    }
}
