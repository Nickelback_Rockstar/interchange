package spoon.gameZone.tokenBaccarat;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class TkBaccaratCancelDto {

        @JsonProperty("money")
        private String money;

        @JsonProperty("date")
        private String date;

        @JsonProperty("time")
        private String time;

}
