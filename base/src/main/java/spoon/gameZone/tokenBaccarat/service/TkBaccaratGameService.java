package spoon.gameZone.tokenBaccarat.service;

import spoon.gameZone.tokenBaccarat.TkBaccaratBetDto;
import spoon.gameZone.tokenBaccarat.TkBaccaratBetInfoDto;
import spoon.gameZone.tokenBaccarat.TkBaccaratResultDto;
import spoon.support.web.AjaxResult;

public interface TkBaccaratGameService {

    AjaxResult betting(TkBaccaratBetInfoDto betInfoDto, TkBaccaratBetDto betDto);

    void closingBetting(TkBaccaratBetInfoDto betInfoDto, TkBaccaratBetDto betDto, TkBaccaratResultDto resultDto);

}
