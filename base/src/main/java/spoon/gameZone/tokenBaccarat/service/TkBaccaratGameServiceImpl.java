package spoon.gameZone.tokenBaccarat.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import spoon.bet.entity.Bet;
import spoon.bet.entity.BetItem;
import spoon.bet.entity.QBetItem;
import spoon.bet.repository.BetItemRepository;
import spoon.bet.service.BetClosingService;
import spoon.bet.service.BetService;
import spoon.common.utils.DateUtils;
import spoon.common.utils.WebUtils;
import spoon.game.domain.GameCode;
import spoon.game.domain.GameResult;
import spoon.game.domain.MenuCode;
import spoon.gameZone.ZoneScore;
import spoon.gameZone.tokenBaccarat.TkBaccaratBetDto;
import spoon.gameZone.tokenBaccarat.TkBaccaratBetInfoDto;
import spoon.gameZone.tokenBaccarat.TkBaccaratResultDto;
import spoon.member.entity.Member;
import spoon.member.service.MemberService;
import spoon.support.web.AjaxResult;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Slf4j
@AllArgsConstructor
@Service
public class TkBaccaratGameServiceImpl implements TkBaccaratGameService {

    private MemberService memberService;

    private BetService betService;

    private BetClosingService betClosingService;

    private BetItemRepository betItemRepository;

    @Override
    public AjaxResult betting(TkBaccaratBetInfoDto betInfoDto, TkBaccaratBetDto betDto) {
        AjaxResult result = new AjaxResult();
        String userid = betInfoDto.getUser_id();

        if (userid == null) {
            result.setMessage("베팅에 실패하였습니다.");
            return result;
        }

        Member member = memberService.getMember(userid);
        if (member.isSecession() || !member.isEnabled() || !member.getTokenid().equals(betInfoDto.getUid()) || member.getMoney() != Long.parseLong(betInfoDto.getStart_money())) {
            result.setMessage("베팅에 실패하였습니다.");
            return result;
        }

        if (!member.getTokenid().equals(betInfoDto.getUid())) {
            result.setMessage("uid 를 다시 확인해주세요.");
            return result;
        }

        if (member.getMoney() != Long.parseLong(betInfoDto.getStart_money())) {
            result.setMessage("보유금을 다시 확인해주세요.");
            return result;
        }

        BetItem betItem = new BetItem();
        betItem.setBetMoney(betDto.getMoney());
        betItem.setGameId(Long.parseLong(betInfoDto.getGRound()));
        betItem.setGroupId(betDto.getTransactionid());
        betItem.setSports(MenuCode.TKBACCARAT.getName());
        betItem.setLeague(MenuCode.TKBACCARAT.getName());
        betItem.setSpecial(betDto.getType());
        betItem.setMenuCode(MenuCode.TKBACCARAT);
        betItem.setGameCode(GameCode.ZONE);
        betItem.setTeamHome(betDto.getType());
        betItem.setTeamAway("");
        betItem.setHandicap(0);
        betItem.setOdds(Double.parseDouble(betDto.getRate()));
        betItem.setOddsHome(Double.parseDouble(betDto.getRate()));
        betItem.setOddsAway(0);
        betItem.setOddsDraw(0);
        String sDate = betDto.getDate()+ betDto.getTime();
        sDate = sDate.replaceAll("-","").replaceAll(":","").replaceAll(" ","");
        Date bDate = DateUtils.parse(sDate,"yyyyMMddHHmmss");
        betItem.setGameDate(bDate);
        betItem.setBetTeam("home");
        betItem.setBetZone(0);
        betItem.setUserid(userid);
        betItem.setRole(member.getRole());
        List<BetItem> betItems = new ArrayList<>();
        betItems.add(betItem);
        ///////////////////////////////////////////////////////////////////

        Bet bet = new Bet(member.getUser());
        bet.setBetMoney(betDto.getMoney());
        bet.setBetItems(betItems);
        bet.setMenuCode(MenuCode.TKBACCARAT);
        bet.setBetCount(1);
        bet.setBetDate(new Date());
        bet.setBetOdds(Double.parseDouble(betDto.getRate()));
        bet.setStartDate(bDate);
        bet.setEndDate(bDate);
        bet.setExpMoney((long) (bet.getBetOdds() * bet.getBetMoney()));
        bet.setIp(WebUtils.ip());
        bet.setBalance(member.isBalancePower());

        boolean success = betService.addZoneBetting(bet);

        if (success) {
            result.setSuccess(true);
        } else {
            result.setMessage("베팅에 실패하였습니다. 잠시후 다시 시도하세요.");
        }

        return result;
    }

    @Transactional
    @Override
    public void closingBetting(TkBaccaratBetInfoDto betInfoDto, TkBaccaratBetDto betDto, TkBaccaratResultDto resultDto) {
        QBetItem qi = QBetItem.betItem;
        Iterable<BetItem> betItems = betItemRepository.findAll(qi.menuCode.eq(MenuCode.TKBACCARAT).and(qi.groupId.eq(betDto.getTransactionid())).and(qi.cancel.isFalse()));
        for (BetItem item : betItems) {
            Bet bet = item.getBet();
            if (bet.isPayment()) continue;

            ZoneScore zoneScore;
            if(resultDto != null && "result".equals(betInfoDto.getDiv()) && Long.parseLong(resultDto.getMoney()) > 0) {
                if(item.getBetMoney() >= Long.parseLong(resultDto.getMoney())){
                    zoneScore = new ZoneScore(0, 0, GameResult.HIT);
                }else {
                    zoneScore = new ZoneScore(1, 0, GameResult.HOME);
                }
            }else if("cancel".equals(betInfoDto.getDiv())) {
                zoneScore = new ZoneScore(0, 0, GameResult.CANCEL);//취소
            }else{
                zoneScore = new ZoneScore(0, 1, GameResult.AWAY);//미적중
            }

            betClosingService.closingZoneBetting(bet, zoneScore);
        }
    }

}
