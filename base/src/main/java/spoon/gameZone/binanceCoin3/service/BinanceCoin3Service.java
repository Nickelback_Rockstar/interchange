package spoon.gameZone.binanceCoin3.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.binanceCoin3.BinanceCoin3;
import spoon.gameZone.binanceCoin3.BinanceCoin3Config;
import spoon.gameZone.binanceCoin3.BinanceCoin3Dto;
import spoon.support.web.AjaxResult;

public interface BinanceCoin3Service {

    /**
     *바이낸스코인1 설정을 변경한다.
     */
    boolean updateConfig(BinanceCoin3Config binanceCoin3Config);

    /**
     *바이낸스코인1 등록된 게임을 가져온다.
     */
    Iterable<BinanceCoin3> getComplete();

    /**
     *바이낸스코인1 종료된 게임을 가져온다.
     */
    Page<BinanceCoin3> getClosing(Pageable pageable);
    Page<BinanceCoin3> getClosing(ZoneDto.Command command, Pageable pageable);

    /**
     *바이낸스코인1 봇에 접속하여 기존 결과가 있는지 확인 한다.
     */
    BinanceCoin3Dto.Score findScore(Long id);

    /**
     *바이낸스코인1 스코어를 가지고 결과처리를 한다.
     */
    boolean closingGame(BinanceCoin3Dto.Score score);

    /**
     * 결과처리가 되지 않고 베팅이 없는 모든 경기를 종료처리 한다.
     */
    AjaxResult closingAllGame();

    /**
     * 현재 진행중인 경기의 설정을 가져온다.
     */
    BinanceCoin3Dto.Config gameConfig();

}

