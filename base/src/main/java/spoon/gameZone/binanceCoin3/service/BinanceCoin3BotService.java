package spoon.gameZone.binanceCoin3.service;

import spoon.gameZone.binanceCoin3.BinanceCoin3;

import java.util.Date;

public interface BinanceCoin3BotService {

    boolean notExist(Date gameDate);

    void addGame(BinanceCoin3 binanceCoin1);

    boolean closingGame(BinanceCoin3 binanceCoin1);

    void checkResult();

    void deleteGame(int days);

}
