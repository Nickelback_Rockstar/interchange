package spoon.gameZone.binanceCoin3.service;

import com.querydsl.core.BooleanBuilder;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionInterceptor;
import spoon.bet.entity.QBetItem;
import spoon.bet.repository.BetItemRepository;
import spoon.common.net.HttpParsing;
import spoon.common.utils.*;
import spoon.config.domain.Config;
import spoon.config.service.ConfigService;
import spoon.game.domain.MenuCode;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.binanceCoin3.*;
import spoon.member.domain.Role;
import spoon.member.entity.Member;
import spoon.member.service.MemberService;
import spoon.support.web.AjaxResult;

import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class BinanceCoin3ServiceImpl implements BinanceCoin3Service {

    private ConfigService configService;

    private MemberService memberService;

    private BinanceCoin3GameService binanceCoin3GameService;

    private BinanceCoin3BotService binanceCoin3BotService;

    private BinanceCoin3Repository binanceCoin3Repository;

    private BetItemRepository betItemRepository;

    private static QBinanceCoin3 q = QBinanceCoin3.binanceCoin3;

    @Transactional
    @Override
    public boolean updateConfig(BinanceCoin3Config binanceCoin3Config) {
        try {
            configService.updateZoneConfig("binanceCoin3", JsonUtils.toString(binanceCoin3Config));
            ZoneConfig.setBinanceCoin3(binanceCoin3Config);
            // 이미 등록된 게임의 배당을 변경한다.
            binanceCoin3Repository.updateOdds(ZoneConfig.getBinanceCoin3().getOdds());
        } catch (RuntimeException e) {
            log.error("비트코인1 설정 변경에 실패하였습니다. - {}", e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional(readOnly = true)
    @Override
    public Iterable<BinanceCoin3> getComplete() {
        return binanceCoin3Repository.findAll(q.closing.isFalse(), new Sort(Sort.Direction.ASC, "sdate"));
    }

    @Override
    public Page<BinanceCoin3> getClosing(Pageable pageable) {
        BooleanBuilder builder = new BooleanBuilder(q.closing.isTrue());
        return binanceCoin3Repository.findAll(builder, pageable);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<BinanceCoin3> getClosing(ZoneDto.Command command, Pageable pageable) {
        BooleanBuilder builder = new BooleanBuilder(q.closing.isTrue());

        // 날짜별 검색
        if (StringUtils.notEmpty(command.getGameDate())) {
            builder.and(q.sdate.like(DateUtils.sdate(command.getGameDate())));
        }
        // 회차별 검색
        if (command.getRound() != null) {
            builder.and(q.round.eq(command.getRound()));
        }

        return binanceCoin3Repository.findAll(builder, pageable);
    }

    @Transactional(readOnly = true)
    @Override
    public BinanceCoin3Dto.Score findScore(Long id) {
        BinanceCoin3 binanceCoin3 = binanceCoin3Repository.findOne(id);

        BinanceCoin3Dto.Score score = new BinanceCoin3Dto.Score();
        score.setId(binanceCoin3.getId());
        score.setRound(binanceCoin3.getRound());
        score.setGameDate(binanceCoin3.getGameDate());
        score.setLowHigh(binanceCoin3.getLowHigh());
        score.setOverUnder(binanceCoin3.getOverUnder());
        score.setOddEven(binanceCoin3.getOddEven());
        score.setCancel(binanceCoin3.isCancel());

        // 봇 연결
        String json = HttpParsing.getJson(Config.getSysConfig().getZone().getBinanceCoin3Url() + "?sdate=" + binanceCoin3.getSdate());
        if (json == null) return score;

        binanceCoin3 = JsonUtils.toModel(json, BinanceCoin3.class);
        if (binanceCoin3 == null) return score;

        // 봇에 결과가 있다면
        if (binanceCoin3.isClosing()) {
            if (StringUtils.empty(score.getLowHigh())) {
                score.setLowHigh(binanceCoin3.getLowHigh());
                score.setOverUnder(binanceCoin3.getOverUnder());
                score.setOddEven(binanceCoin3.getOddEven());
            }
        }

        return score;
    }

    @Transactional
    @Override
    public boolean closingGame(BinanceCoin3Dto.Score score) {
        BinanceCoin3 binanceCoin3 = binanceCoin3Repository.findOne(score.getId());

        try {
            if (binanceCoin3.isChangeResult(score)) {
                // 현재 지급된 머니 포인트를 되돌린다.
                binanceCoin3GameService.rollbackPayment(binanceCoin3);
            }

            // 스코어 입력
            binanceCoin3.updateScore(score);
            binanceCoin3Repository.saveAndFlush(binanceCoin3);
            binanceCoin3GameService.closingBetting(binanceCoin3);
            binanceCoin3BotService.checkResult();
        } catch (RuntimeException e) {
            log.error("비트코인1 {} - {}회차 수동처리를 하지 못하였습니다. - {}", binanceCoin3.getGameDate(), binanceCoin3.getRound(), e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Override
    public AjaxResult closingAllGame() {
        QBetItem qi = QBetItem.betItem;
        int total = 0;
        int closing = 0;

        Iterable<BinanceCoin3> iterable = binanceCoin3Repository.findAll(q.closing.isFalse().and(q.gameDate.before(DateUtils.beforeMinutes(3))));
        for (BinanceCoin3 binanceCoin3 : iterable) {
            total++;
            long count = betItemRepository.count(qi.menuCode.eq(MenuCode.BINANCECOIN3).and(qi.groupId.eq(binanceCoin3.getSdate())).and(qi.cancel.isFalse()));
            if (count > 0) continue;

            String json = HttpParsing.getJson(Config.getSysConfig().getZone().getBinanceCoin3Url() + "?sdate=" + binanceCoin3.getSdate());
            if (json == null) continue;

            BinanceCoin3 result = JsonUtils.toModel(json, BinanceCoin3.class);
            if (result == null) continue;

            if (result.isClosing()) {
                binanceCoin3.setLowHigh(result.getLowHigh());
                binanceCoin3.setOverUnder(result.getOverUnder());
                binanceCoin3.setOddEven(result.getOddEven());
                binanceCoin3.setClosing(true);
                binanceCoin3Repository.saveAndFlush(binanceCoin3);
                closing++;
            }
        }
        binanceCoin3BotService.checkResult();
        return new AjaxResult(true, "전체 " + total + "경기중 " + closing + "경기를 종료처리 했습니다.");
    }

    @Override
    public BinanceCoin3Dto.Config gameConfig() {
        BinanceCoin3Dto.Config gameConfig = new BinanceCoin3Dto.Config();
        BinanceCoin3Config config = ZoneConfig.getBinanceCoin3();

        Date gameDate = config.getZoneMaker().getGameDate();
        BinanceCoin3 binanceCoin3 = binanceCoin3Repository.findOne(q.gameDate.eq(gameDate));

        if (binanceCoin3 == null) {
            gameConfig.setGameDate(gameDate);
            gameConfig.setRound(config.getZoneMaker().getRound());
            return gameConfig;
        }

        String userid = WebUtils.userid();
        if (userid == null) return gameConfig;

        Member member = memberService.getMember(userid);
        int level = member.getLevel();
        gameConfig.setEnabled(config.isEnabled() && Config.getSysConfig().getZone().isBinanceCoin3());
        if (member.getRole() == Role.DUMMY) {
            gameConfig.setMoney(10000000);
        } else {
            gameConfig.setMoney(member.getMoney());
        }
        gameConfig.setGameDate(binanceCoin3.getGameDate());
        gameConfig.setSdate(binanceCoin3.getSdate());
        gameConfig.setRound(binanceCoin3.getRound());
        gameConfig.setWin(config.getWin()[level]);
        gameConfig.setMax(config.getMax()[level]);
        gameConfig.setMin(config.getMin()[level]);
        gameConfig.setOdds(binanceCoin3.getOdds());

        int betTime = (int) (binanceCoin3.getGameDate().getTime() - new Date().getTime() - config.getBetTime() * 1000) / 1000;
        if (betTime < 0) betTime = 0;

        gameConfig.setBetTime(betTime);

        return gameConfig;
    }
}
