package spoon.gameZone.binanceCoin3.service;

import spoon.gameZone.ZoneDto;
import spoon.gameZone.binanceCoin3.BinanceCoin3;
import spoon.support.web.AjaxResult;

public interface BinanceCoin3GameService {

    /**
     * 달팽이 게임을 베팅한다.
     */
    AjaxResult betting(ZoneDto.Bet bet);

    /**
     * 달팽이 게임 베팅을 클로징 한다.
     */
    void closingBetting(BinanceCoin3 binanceCoin3);

    /**
     * 달팽이 게임을 롤백 한다.
     */
    void rollbackPayment(BinanceCoin3 binanceCoin3);
}
