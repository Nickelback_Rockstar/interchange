package spoon.gameZone.kenoLadder.service;

import spoon.gameZone.ZoneDto;
import spoon.gameZone.kenoLadder.KenoLadder;
import spoon.support.web.AjaxResult;

public interface KenoLadderGameService {

    /**
     * 키노사다리 게임을 베팅한다.
     */
    AjaxResult betting(ZoneDto.Bet bet);

    /**
     * 키노사다리 게임 베팅을 클로징 한다.
     */
    void closingBetting(KenoLadder ladder);

    /**
     * 키노사다리 게임을 롤백 한다.
     */
    void rollbackPayment(KenoLadder ladder);
}
