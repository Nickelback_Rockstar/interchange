package spoon.gameZone.fxGame3.service;

import com.querydsl.core.BooleanBuilder;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionInterceptor;
import spoon.bet.entity.QBetItem;
import spoon.bet.repository.BetItemRepository;
import spoon.common.net.HttpParsing;
import spoon.common.utils.*;
import spoon.config.domain.Config;
import spoon.config.service.ConfigService;
import spoon.game.domain.MenuCode;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.fxGame3.*;
import spoon.member.domain.Role;
import spoon.member.entity.Member;
import spoon.member.service.MemberService;
import spoon.support.web.AjaxResult;

import java.util.Calendar;
import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class FxGame3ServiceImpl implements FxGame3Service {

    private ConfigService configService;

    private MemberService memberService;

    private FxGame3GameService fxGame3GameService;

    private FxGame3BotService fxGame3BotService;

    private FxGame3Repository fxGame3Repository;

    private BetItemRepository betItemRepository;

    private static QFxGame3 q = QFxGame3.fxGame3;

    @Transactional
    @Override
    public boolean updateConfig(FxGame3Config fxGame3Config) {
        try {
            configService.updateZoneConfig("fxGame3", JsonUtils.toString(fxGame3Config));
            ZoneConfig.setFxGame3(fxGame3Config);
            // 이미 등록된 게임의 배당을 변경한다.
            fxGame3Repository.updateOdds(ZoneConfig.getFxGame3().getOdds());
        } catch (RuntimeException e) {
            log.error("fxGame3 설정 변경에 실패하였습니다. - {}", e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional(readOnly = true)
    @Override
    public Iterable<FxGame3> getComplete() {
        return fxGame3Repository.findAll(q.closing.isFalse(), new Sort(Sort.Direction.ASC, "sdate"));
    }

    @Transactional(readOnly = true)
    @Override
    public Page<FxGame3> getClosing(ZoneDto.Command command, Pageable pageable) {
        BooleanBuilder builder = new BooleanBuilder(q.closing.isTrue());

        // 날짜별 검색
        if (StringUtils.notEmpty(command.getGameDate())) {
            builder.and(q.sdate.like(DateUtils.sdate(command.getGameDate())));
        }
        // 회차별 검색
        if (command.getRound() != null) {
            builder.and(q.round.eq(command.getRound()));
        }

        return fxGame3Repository.findAll(builder, pageable);
    }

    @Transactional(readOnly = true)
    @Override
    public FxGame3Dto.Score findScore(Long id) {
        FxGame3 fxGame3 = fxGame3Repository.findOne(id);

        FxGame3Dto.Score score = new FxGame3Dto.Score();
        score.setId(fxGame3.getId());
        score.setRound(fxGame3.getRound());
        score.setGameDate(fxGame3.getGameDate());
        score.setFxResult(fxGame3.getFxResult());
        score.setOddeven(fxGame3.getOddeven());
        score.setOverunder(fxGame3.getOverunder());
        score.setCancel(fxGame3.isCancel());

        // 봇 연결
        String json = HttpParsing.getJson(Config.getSysConfig().getZone().getFxGame3Url() + "?sdate=" + fxGame3.getSdate());
        if (json == null) return score;

        fxGame3 = JsonUtils.toModel(json, FxGame3.class);
        if (fxGame3 == null) return score;

        // 봇에 결과가 있다면
        if (fxGame3.isClosing()) {
            score.setFxResult(fxGame3.getFxResult());
            score.setOddeven(fxGame3.getOddeven());
            score.setOverunder(fxGame3.getOverunder());

            if (!"BUY".equals(score.getFxResult()) && !"SELL".equals(score.getFxResult())
                    && !"ODD".equals(score.getOddeven()) && !"EVEN".equals(score.getOddeven())
                    && !"OVER".equals(score.getOverunder()) && !"UNDER".equals(score.getOverunder())
            ) {
                score.setCancel(true);
            }
        }

        return score;
    }

    @Transactional
    @Override
    public boolean closingGame(FxGame3Dto.Score score) {
        FxGame3 fxGame3 = fxGame3Repository.findOne(score.getId());

        try {
            if (fxGame3.isChangeResult(score)) {
                // 현재 지급된 머니 포인트를 되돌린다.
                fxGame3GameService.rollbackPayment(fxGame3);
            }

            // 스코어 입력
            fxGame3.updateScore(score);
            fxGame3Repository.saveAndFlush(fxGame3);
            fxGame3GameService.closingBetting(fxGame3);
            fxGame3BotService.checkResult();
        } catch (RuntimeException e) {
            log.error("fxGame3 {} - {}회차 수동처리를 하지 못하였습니다. - {}", fxGame3.getGameDate(), fxGame3.getRound(), e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional
    @Override
    public AjaxResult closingAllGame() {
        QBetItem qi = QBetItem.betItem;
        int total = 0;
        int closing = 0;

        Iterable<FxGame3> iterable = fxGame3Repository.findAll(q.closing.isFalse().and(q.gameDate.before(DateUtils.beforeMinutes(5))));
        for (FxGame3 fxGame3 : iterable) {
            total++;
            long count = betItemRepository.count(qi.menuCode.eq(MenuCode.FXGAME3).and(qi.groupId.eq(fxGame3.getSdate())).and(qi.cancel.isFalse()));
            if (count > 0) continue;

            String json = HttpParsing.getJson(Config.getSysConfig().getZone().getFxGame3Url() + "?sdate=" + fxGame3.getSdate());
            if (json == null) continue;

            FxGame3 result = JsonUtils.toModel(json, FxGame3.class);
            if (result == null) continue;

            if (result.isClosing()) {
                fxGame3.setFxResult(result.getFxResult());
                fxGame3.setClosing(true);
                fxGame3Repository.saveAndFlush(fxGame3);
                closing++;
            }
        }
        fxGame3BotService.checkResult();
        return new AjaxResult(true, "전체 " + total + "경기중 " + closing + "경기를 종료처리 했습니다.");
    }

    @Override
    public FxGame3Dto.Config gameConfig(int r) {
        FxGame3Dto.Config gameConfig = new FxGame3Dto.Config();
        FxGame3Config config = ZoneConfig.getFxGame3();

        Date oriGameDate = config.getZoneMaker().getGameDate();
        Date gameDate = config.getZoneMaker().getGameDate();
        Calendar cal = Calendar.getInstance();
        cal.setTime(gameDate);
        cal.add(Calendar.MINUTE, (config.getZoneMaker().getInterval() * r));

        FxGame3 fxGame3 = fxGame3Repository.findOne(q.gameDate.eq(cal.getTime()));

        if (fxGame3 == null) {
            gameConfig.setGameDate2(oriGameDate);
            gameConfig.setGameDate(gameDate);
            gameConfig.setRound(0);
            return gameConfig;
        }

        String userid = WebUtils.userid();
        if (userid == null) return gameConfig;

        Member member = memberService.getMember(userid);
        int level = member.getLevel();
        gameConfig.setEnabled(config.isEnabled() && Config.getSysConfig().getZone().isFxGame3());
        if (member.getRole() == Role.DUMMY) {
            gameConfig.setMoney(10000000);
        } else {
            gameConfig.setMoney(member.getMoney());
        }
        gameConfig.setNextRound(r);
        gameConfig.setGameDate2(oriGameDate);
        gameConfig.setGameDate(fxGame3.getGameDate());
        gameConfig.setSdate(fxGame3.getSdate());
        gameConfig.setRound(fxGame3.getRound());
        gameConfig.setWin(config.getWin()[level]);
        gameConfig.setMax(config.getMax()[level]);
        gameConfig.setMin(config.getMin()[level]);
        gameConfig.setOdds(fxGame3.getOdds());

        int betTime = (int) (oriGameDate.getTime() - new Date().getTime() - config.getBetTime() * 1000) / 1000;
        if (betTime < 0) betTime = 0;
        gameConfig.setBetTime(betTime);

        gameConfig.setFxResult(config.isFxResult());
        gameConfig.setOddeven(config.isOddeven());
        gameConfig.setOverunder(config.isOverunder());

        return gameConfig;
    }
}
