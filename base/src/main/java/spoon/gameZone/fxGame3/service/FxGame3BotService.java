package spoon.gameZone.fxGame3.service;

import spoon.gameZone.fxGame3.FxGame3;

import java.util.Date;

public interface FxGame3BotService {

    boolean notExist(Date gameDate);

    void addGame(FxGame3 fxGame3);

    boolean closingGame(FxGame3 result);

    void checkResult();

    void deleteGame(int days);

}
