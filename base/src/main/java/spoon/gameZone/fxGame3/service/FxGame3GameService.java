package spoon.gameZone.fxGame3.service;

import spoon.gameZone.ZoneDto;
import spoon.gameZone.fxGame3.FxGame3;
import spoon.support.web.AjaxResult;

public interface FxGame3GameService {

    /**
     * fxGame3 게임을 베팅한다.
     */
    AjaxResult betting(ZoneDto.Bet bet);

    /**
     * fxGame3 게임 베팅을 클로징 한다.
     */
    void closingBetting(FxGame3 ladder);

    /**
     * fxGame3 게임을 롤백 한다.
     */
    void rollbackPayment(FxGame3 ladder);
}
