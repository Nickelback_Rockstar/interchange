package spoon.gameZone.fxGame3.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionInterceptor;
import spoon.common.utils.DateUtils;
import spoon.common.utils.ErrorUtils;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.fxGame3.FxGame3;
import spoon.gameZone.fxGame3.FxGame3Repository;
import spoon.gameZone.fxGame3.QFxGame3;

import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class FxGame3BotServiceImpl implements FxGame3BotService {

    private FxGame3GameService fxGame3GameService;

    private FxGame3Repository fxGame3Repository;

    private static QFxGame3 q = QFxGame3.fxGame3;

    @Transactional(readOnly = true)
    @Override
    public boolean notExist(Date gameDate) {
        return fxGame3Repository.count(q.sdate.eq(DateUtils.format(gameDate, "yyyyMMddHHmm"))) == 0;
    }

    @Transactional
    @Override
    public void addGame(FxGame3 fxGame3) {
        fxGame3Repository.saveAndFlush(fxGame3);
    }

    @Transactional
    @Override
    public boolean closingGame(FxGame3 result) {
        FxGame3 fxGame3 = fxGame3Repository.findOne(q.sdate.eq(result.getSdate()));
        if (fxGame3 == null) {
            return true;
        }

        try {
            fxGame3.setFxResult(result.getFxResult());
            fxGame3.setOddeven(result.getOddeven());
            fxGame3.setOverunder(result.getOverunder());
            fxGame3.setClosing(true);

            fxGame3Repository.saveAndFlush(fxGame3);
            fxGame3GameService.closingBetting(fxGame3);
        } catch (RuntimeException e) {
            log.error("fxGame3 {}회차 결과 업데이트에 실패하였습니다. - {}", fxGame3.getRound(), e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional(readOnly = true)
    @Override
    public void checkResult() {
        long cnt = fxGame3Repository.count(q.gameDate.before(new Date()).and(q.closing.isFalse()));
        ZoneConfig.getFxGame3().setResult(cnt);
    }

    @Transactional
    @Override
    public void deleteGame(int days) {
        fxGame3Repository.deleteGame(DateUtils.beforeDays(days));
    }
}
