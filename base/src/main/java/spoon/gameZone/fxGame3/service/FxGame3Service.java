package spoon.gameZone.fxGame3.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.fxGame3.FxGame3;
import spoon.gameZone.fxGame3.FxGame3Config;
import spoon.gameZone.fxGame3.FxGame3Dto;
import spoon.support.web.AjaxResult;

public interface FxGame3Service {

    /**
     * FxGame3 설정을 변경한다.
     */
    boolean updateConfig(FxGame3Config fxGame3Config);

    /**
     * FxGame3 등록된 게임을 가져온다.
     */
    Iterable<FxGame3> getComplete();

    /**
     * FxGame3 종료된 게임을 가져온다.
     */
    Page<FxGame3> getClosing(ZoneDto.Command command, Pageable pageable);

    /**
     * FxGame3 봇에 접속하여 기존 결과가 있는지 확인 한다.
     */
    FxGame3Dto.Score findScore(Long id);

    /**
     * FxGame3 스코어를 가지고 결과처리를 한다.
     */
    boolean closingGame(FxGame3Dto.Score score);

    /**
     * 결과처리가 되지 않고 베팅이 없는 모든 경기를 종료처리 한다.
     */
    AjaxResult closingAllGame();

    /**
     * 현재 진행중인 경기의 설정을 가져온다.
     */
    FxGame3Dto.Config gameConfig(int r);

}
