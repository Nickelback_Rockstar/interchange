package spoon.gameZone.eosPower2.service;

import spoon.gameZone.eosPower2.EosPower2;

import java.util.Date;

public interface EosPower2BotService {

    boolean notExist(Date gameDate);

    void addGame(EosPower2 power);

    boolean closingGame(EosPower2 result);

    void checkResult();

    void deleteGame(int days);

}
