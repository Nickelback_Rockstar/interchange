package spoon.gameZone.eosPower2.service;

import spoon.gameZone.ZoneDto;
import spoon.gameZone.eosPower2.EosPower2;
import spoon.support.web.AjaxResult;

public interface EosPower2GameService {

    /**
     * 파워볼 게임을 베팅한다.
     */
    AjaxResult betting(ZoneDto.Bet bet);

    /**
     * 파워볼 게임 베팅을 클로징 한다.
     */
    void closingBetting(EosPower2 power);

    /**
     * 파워볼 게임을 롤백 한다.
     */
    void rollbackPayment(EosPower2 power);
}
