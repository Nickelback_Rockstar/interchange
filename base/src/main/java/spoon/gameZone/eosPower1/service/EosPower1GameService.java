package spoon.gameZone.eosPower1.service;

import spoon.gameZone.ZoneDto;
import spoon.gameZone.eosPower1.EosPower1;
import spoon.support.web.AjaxResult;

public interface EosPower1GameService {

    /**
     * 파워볼 게임을 베팅한다.
     */
    AjaxResult betting(ZoneDto.Bet bet);

    /**
     * 파워볼 게임 베팅을 클로징 한다.
     */
    void closingBetting(EosPower1 power);

    /**
     * 파워볼 게임을 롤백 한다.
     */
    void rollbackPayment(EosPower1 power);
}
