package spoon.gameZone.eosPower1.service;

import spoon.gameZone.eosPower1.EosPower1;

import java.util.Date;

public interface EosPower1BotService {

    boolean notExist(Date gameDate);

    void addGame(EosPower1 power);

    boolean closingGame(EosPower1 result);

    void checkResult();

    void deleteGame(int days);

}
