package spoon.gameZone.speedHomeRun;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import spoon.bot.support.PowerMaker;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class SpeedHomeRunConfig {

    @JsonIgnore
    private PowerMaker powerMaker;

    private boolean enabled;

    // 결과처리가 잘 되었는지 체크 (관리자에서 사용)
    private long result;

    private int betTime;

    // 회차별 베팅 숫자
    private int betMax = 1;

    private boolean player;

    private boolean lastBall;

    private boolean homeRun;

    private boolean direction;

    /**
     * gameCode - 베팅시 betItem special 에 들어갈 코드
     * 0:선수1(1), 1:선수2(2), 2:마지막 일반볼홀(ODD), 3:마지막 일반볼짝(EVEN), 4:(line), 5:4줄(line)
     * 6:3줄/좌(line3Start), 7:3줄/우(line3Start), 8:4줄/좌(line4Start), 9:4줄/우(line4Start)
     */
    private double[] odds = new double[8];

    private int[] win = {0, 3000000, 3000000, 3000000, 3000000, 3000000, 3000000, 3000000, 3000000, 3000000, 3000000};

    private int[] max = {0, 1000000, 1000000, 1000000, 1000000, 1000000, 1000000, 1000000, 1000000, 1000000, 1000000};

    private int[] min = {0, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000};

    public SpeedHomeRunConfig() {
        this.powerMaker = new PowerMaker();
    }
}
