package spoon.gameZone.speedHomeRun.service;

import spoon.gameZone.speedHomeRun.SpeedHomeRun;

import java.util.Date;

public interface SpeedHomeRunBotService {

    boolean notExist(Date gameDate);

    void addGame(SpeedHomeRun speedHomeRun);

    boolean closingGame(SpeedHomeRun result);

    void checkResult();

    void deleteGame(int days);
}
