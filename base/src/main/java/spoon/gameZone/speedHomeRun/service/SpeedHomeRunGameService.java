package spoon.gameZone.speedHomeRun.service;

import spoon.gameZone.ZoneDto;
import spoon.gameZone.speedHomeRun.SpeedHomeRun;
import spoon.support.web.AjaxResult;

public interface SpeedHomeRunGameService {

    /**
     * 스피드홈런 게임을 베팅한다.
     */
    AjaxResult betting(ZoneDto.Bet bet);

    /**
     * 스피드홈런 게임 베팅을 클로징 한다.
     */
    void closingBetting(SpeedHomeRun speedHomeRun);

    /**
     * 스피드홈런 게임을 롤백 한다.
     */
    void rollbackPayment(SpeedHomeRun speedHomeRun);
}
