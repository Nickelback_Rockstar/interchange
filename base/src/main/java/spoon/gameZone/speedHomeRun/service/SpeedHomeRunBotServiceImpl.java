package spoon.gameZone.speedHomeRun.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionInterceptor;
import spoon.common.utils.DateUtils;
import spoon.common.utils.ErrorUtils;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.speedHomeRun.QSpeedHomeRun;
import spoon.gameZone.speedHomeRun.SpeedHomeRun;
import spoon.gameZone.speedHomeRun.SpeedHomeRunRepository;

import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class SpeedHomeRunBotServiceImpl implements SpeedHomeRunBotService {

    private SpeedHomeRunGameService speedHomeRunGameService;

    private SpeedHomeRunRepository speedHomeRunRepository;

    private static QSpeedHomeRun q = QSpeedHomeRun.speedHomeRun;

    @Transactional(readOnly = true)
    @Override
    public boolean notExist(Date gameDate) {
        return speedHomeRunRepository.count(q.sdate.eq(DateUtils.format(gameDate, "yyyyMMddHHmmss"))) == 0;
    }

    @Transactional
    @Override
    public void addGame(SpeedHomeRun speedHomeRun) {
        speedHomeRunRepository.saveAndFlush(speedHomeRun);
    }

    @Transactional
    @Override
    public boolean closingGame(SpeedHomeRun result) {
        SpeedHomeRun speedHomeRun = speedHomeRunRepository.findOne(q.sdate.eq(result.getSdate()).and(q.closing.isFalse()));
        if (speedHomeRun == null) {
            return true;
        }

        try {
            speedHomeRun.setPlayer(result.getPlayer());
            speedHomeRun.setLastBall(result.getLastBall());
            speedHomeRun.setHomeRun(result.getHomeRun());
            speedHomeRun.setDirection(result.getDirection());
            speedHomeRun.setClosing(true);

            speedHomeRunRepository.saveAndFlush(speedHomeRun);
            speedHomeRunGameService.closingBetting(speedHomeRun);
        } catch (RuntimeException e) {
            log.error("스피드홈런 {}회차 결과 업데이트에 실패하였습니다. - {}", speedHomeRun.getRound(), e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional(readOnly = true)
    @Override
    public void checkResult() {
        long cnt = speedHomeRunRepository.count(q.gameDate.before(new Date()).and(q.closing.isFalse()));
        ZoneConfig.getSpeedHomeRun().setResult(cnt);
    }

    @Transactional
    @Override
    public void deleteGame(int days) {
        speedHomeRunRepository.deleteGame(DateUtils.beforeDays(days));
    }
}
