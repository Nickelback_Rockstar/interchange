package spoon.gameZone.speedHomeRun.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.speedHomeRun.SpeedHomeRun;
import spoon.gameZone.speedHomeRun.SpeedHomeRunConfig;
import spoon.gameZone.speedHomeRun.SpeedHomeRunDto;
import spoon.support.web.AjaxResult;

public interface SpeedHomeRunService {

    /**
     * 스피드홈런 설정을 변경한다.
     */
    boolean updateConfig(SpeedHomeRunConfig speedHomeRunConfig);

    /**
     * 스피드홈런 등록된 게임을 가져온다.
     */
    Iterable<SpeedHomeRun> getComplete();

    /**
     * 스피드홈런 종료된 게임을 가져온다.
     */
    Page<SpeedHomeRun> getClosing(ZoneDto.Command command, Pageable pageable);

    /**
     * 스피드홈런 봇에 접속하여 기존 결과가 있는지 확인 한다.
     */
    SpeedHomeRunDto.Score findScore(Long id);

    /**
     * 스피드홈런 스코어를 가지고 결과처리를 한다.
     */
    boolean closingGame(SpeedHomeRunDto.Score score);

    /**
     * 결과처리가 되지 않고 베팅이 없는 모든 경기를 종료처리 한다.
     */
    AjaxResult closingAllGame();

    /**
     * 현재 진행중인 경기의 설정을 가져온다.
     */
    SpeedHomeRunDto.Config gameConfig();
}
