package spoon.gameZone.speedHomeRun.service;

import com.querydsl.core.BooleanBuilder;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionInterceptor;
import spoon.bet.entity.QBetItem;
import spoon.bet.repository.BetItemRepository;
import spoon.common.net.HttpParsing;
import spoon.common.utils.*;
import spoon.config.domain.Config;
import spoon.config.service.ConfigService;
import spoon.game.domain.MenuCode;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.speedHomeRun.*;
import spoon.member.domain.Role;
import spoon.member.entity.Member;
import spoon.member.service.MemberService;
import spoon.support.web.AjaxResult;

import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class SpeedHomeRunServiceImpl implements SpeedHomeRunService {

    private ConfigService configService;

    private MemberService memberService;

    private SpeedHomeRunGameService speedHomeRunGameService;

    private SpeedHomeRunBotService speedHomeRunBotService;

    private SpeedHomeRunRepository speedHomeRunRepository;

    private BetItemRepository betItemRepository;

    private static QSpeedHomeRun q = QSpeedHomeRun.speedHomeRun;

    @Transactional
    @Override
    public boolean updateConfig(SpeedHomeRunConfig speedHomeRunConfig) {
        try {
            configService.updateZoneConfig("speed_homeRun", JsonUtils.toString(speedHomeRunConfig));
            ZoneConfig.setSpeedHomeRun(speedHomeRunConfig);
            // 이미 등록된 게임의 배당을 변경한다.
            speedHomeRunRepository.updateOdds(ZoneConfig.getSpeedHomeRun().getOdds());
        } catch (RuntimeException e) {
            log.error("스피드홈런 설정 변경에 실패하였습니다. - {}", e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional(readOnly = true)
    @Override
    public Iterable<SpeedHomeRun> getComplete() {
        return speedHomeRunRepository.findAll(q.closing.isFalse(), new Sort(Sort.Direction.ASC, "sdate"));
    }

    @Transactional(readOnly = true)
    @Override
    public Page<SpeedHomeRun> getClosing(ZoneDto.Command command, Pageable pageable) {
        BooleanBuilder builder = new BooleanBuilder(q.closing.isTrue());

        // 날짜별 검색
        if (StringUtils.notEmpty(command.getGameDate())) {
            builder.and(q.sdate.like(DateUtils.sdate(command.getGameDate())));
        }
        // 회차별 검색
        if (command.getRound() != null) {
            builder.and(q.round.eq(command.getRound()));
        }

        return speedHomeRunRepository.findAll(builder, pageable);
    }

    @Transactional(readOnly = true)
    @Override
    public SpeedHomeRunDto.Score findScore(Long id) {
        SpeedHomeRun speedHomeRun = speedHomeRunRepository.findOne(id);

        SpeedHomeRunDto.Score score = new SpeedHomeRunDto.Score();
        score.setId(speedHomeRun.getId());
        score.setRound(speedHomeRun.getRound());
        score.setGameDate(speedHomeRun.getGameDate());
        score.setPlayer(speedHomeRun.getPlayer());
        score.setLastBall(speedHomeRun.getLastBall());
        score.setHomeRun(speedHomeRun.getHomeRun());
        score.setDirection(speedHomeRun.getDirection());
        score.setCancel(speedHomeRun.isCancel());

        // 봇 연결
        String json = HttpParsing.getJson(Config.getSysConfig().getZone().getSpeedHomeRunUrl() + "?sdate=" + speedHomeRun.getSdate());
        if (json == null) return score;

        speedHomeRun = JsonUtils.toModel(json, SpeedHomeRun.class);
        if (speedHomeRun == null) return score;

        // 봇에 결과가 있다면
        if (speedHomeRun.isClosing()) {
            score.setPlayer(speedHomeRun.getPlayer());
            score.setLastBall(speedHomeRun.getLastBall());
            score.setHomeRun(speedHomeRun.getHomeRun());
            score.setDirection(speedHomeRun.getDirection());

/*            if (!"ODD".equals(score.getOddeven()) && !"EVEN".equals(score.getOddeven())) {
                score.setCancel(true);
            }*/
        }

        return score;
    }

    @Transactional
    @Override
    public boolean closingGame(SpeedHomeRunDto.Score score) {
        SpeedHomeRun speedHomeRun = speedHomeRunRepository.findOne(score.getId());

        try {
            if (speedHomeRun.isChangeResult(score)) {
                // 현재 지급된 머니 포인트를 되돌린다.
                speedHomeRunGameService.rollbackPayment(speedHomeRun);
            }

            // 스코어 입력
            speedHomeRun.updateScore(score);
            speedHomeRunRepository.saveAndFlush(speedHomeRun);
            speedHomeRunGameService.closingBetting(speedHomeRun);
            speedHomeRunBotService.checkResult();
        } catch (RuntimeException e) {
            log.error("스피드홈런 {} - {}회차 수동처리를 하지 못하였습니다. - {}", speedHomeRun.getGameDate(), speedHomeRun.getRound(), e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional
    @Override
    public AjaxResult closingAllGame() {
        QBetItem qi = QBetItem.betItem;
        int total = 0;
        int closing = 0;

        Iterable<SpeedHomeRun> iterable = speedHomeRunRepository.findAll(q.closing.isFalse().and(q.gameDate.before(DateUtils.beforeMinutes(5))));
        for (SpeedHomeRun speedHomeRun : iterable) {
            total++;
            long count = betItemRepository.count(qi.menuCode.eq(MenuCode.SPEEDHOMERUN).and(qi.groupId.eq(speedHomeRun.getSdate())).and(qi.cancel.isFalse()));
            if (count > 0) continue;

            String json = HttpParsing.getJson(Config.getSysConfig().getZone().getSpeedHomeRunUrl() + "?sdate=" + speedHomeRun.getSdate());
            if (json == null) continue;

            SpeedHomeRun result = JsonUtils.toModel(json, SpeedHomeRun.class);
            if (result == null) continue;

            if (result.isClosing()) {
                speedHomeRun.setPlayer(result.getPlayer());
                speedHomeRun.setLastBall(result.getLastBall());
                speedHomeRun.setHomeRun(result.getHomeRun());
                speedHomeRun.setDirection(result.getDirection());
                speedHomeRun.setClosing(true);
                speedHomeRunRepository.saveAndFlush(speedHomeRun);
                closing++;
            }
        }
        speedHomeRunBotService.checkResult();
        return new AjaxResult(true, "전체 " + total + "경기중 " + closing + "경기를 종료처리 했습니다.");
    }

    @Override
    public SpeedHomeRunDto.Config gameConfig() {
        SpeedHomeRunDto.Config gameConfig = new SpeedHomeRunDto.Config();
        SpeedHomeRunConfig config = ZoneConfig.getSpeedHomeRun();

        // 동행 현재 회차에서 1을 더해준다.
        int times = ZoneConfig.getSpeedHomeRun().getPowerMaker().getTimes() + 1;
        Date gameDate = ZoneConfig.getSpeedHomeRun().getPowerMaker().getGameDate(times);
        SpeedHomeRun speedHomeRun = speedHomeRunRepository.findOne(q.gameDate.eq(gameDate));

        if (speedHomeRun == null) {
            gameConfig.setGameDate(gameDate);
            gameConfig.setRound(config.getPowerMaker().getRound(times));
            return gameConfig;
        }

        String userid = WebUtils.userid();
        if (userid == null) return gameConfig;

        Member member = memberService.getMember(userid);
        int level = member.getLevel();
        gameConfig.setEnabled(config.isEnabled() && Config.getSysConfig().getZone().isSpeedHomeRun());
        if (member.getRole() == Role.DUMMY) {
            gameConfig.setMoney(10000000);
        } else {
            gameConfig.setMoney(member.getMoney());
        }
        gameConfig.setGameDate(speedHomeRun.getGameDate());
        gameConfig.setSdate(speedHomeRun.getSdate());
        gameConfig.setRound(speedHomeRun.getRound());
        gameConfig.setWin(config.getWin()[level]);
        gameConfig.setMax(config.getMax()[level]);
        gameConfig.setMin(config.getMin()[level]);
        gameConfig.setOdds(speedHomeRun.getOdds());

        int betTime = (int) (speedHomeRun.getGameDate().getTime() - new Date().getTime() - config.getBetTime() * 1000) / 1000;
        if (betTime < 0) betTime = 0;
        gameConfig.setBetTime(betTime);

        gameConfig.setPlayer(config.isPlayer());
        gameConfig.setLastBall(config.isLastBall());
        gameConfig.setHomeRun(config.isHomeRun());
        gameConfig.setDirection(config.isDirection());

        return gameConfig;
    }
}
