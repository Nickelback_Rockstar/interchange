package spoon.gameZone.boglePower.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import spoon.gameZone.ZoneDto;
import spoon.gameZone.boglePower.BoglePower;
import spoon.gameZone.boglePower.BoglePowerConfig;
import spoon.gameZone.boglePower.BoglePowerDto;
import spoon.support.web.AjaxResult;

public interface BoglePowerService {

    /**
     * BoglePower 설정을 변경한다.
     */
    boolean updateConfig(BoglePowerConfig BoglePowerConfig);

    /**
     * BoglePower 등록된 게임을 가져온다.
     */
    Iterable<BoglePower> getComplete();

    /**
     * BoglePower 종료된 게임을 가져온다.
     */
    Page<BoglePower> getClosing(ZoneDto.Command command, Pageable pageable);

    /**
     * BoglePower 봇에 접속하여 기존 결과가 있는지 확인 한다.
     */
    BoglePowerDto.Score findScore(Long id);

    /**
     * BoglePower 스코어를 가지고 결과처리를 한다.
     */
    boolean closingGame(BoglePowerDto.Score score);

    /**
     * 결과처리가 되지 않고 베팅이 없는 모든 경기를 종료처리 한다.
     */
    AjaxResult closingAllGame();

    /**
     * 현재 진행중인 경기의 설정을 가져온다.
     */
    BoglePowerDto.Config gameConfig();

}
