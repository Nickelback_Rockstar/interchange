package spoon.gameZone.boglePower.service;

import spoon.gameZone.ZoneDto;
import spoon.gameZone.boglePower.BoglePower;
import spoon.support.web.AjaxResult;

public interface BoglePowerGameService {

    /**
     * boglePower 게임을 베팅한다.
     */
    AjaxResult betting(ZoneDto.Bet bet);

    /**
     * boglePower 게임 베팅을 클로징 한다.
     */
    void closingBetting(BoglePower Power);

    /**
     * boglePower 게임을 롤백 한다.
     */
    void rollbackPayment(BoglePower Power);
}
