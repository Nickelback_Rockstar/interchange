package spoon.gameZone.boglePower.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionInterceptor;
import spoon.common.utils.DateUtils;
import spoon.common.utils.ErrorUtils;
import spoon.gameZone.ZoneConfig;
import spoon.gameZone.boglePower.BoglePower;
import spoon.gameZone.boglePower.BoglePowerRepository;
import spoon.gameZone.boglePower.QBoglePower;

import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class BoglePowerBotServiceImpl implements BoglePowerBotService {

    private BoglePowerGameService boglePowerGameService;

    private BoglePowerRepository boglePowerRepository;

    private static QBoglePower q = QBoglePower.boglePower;

    @Transactional(readOnly = true)
    @Override
    public boolean notExist(Date gameDate) {
        return boglePowerRepository.count(q.sdate.eq(DateUtils.format(gameDate, "yyyyMMddHHmm"))) == 0;
    }

    @Transactional
    @Override
    public void addGame(BoglePower boglePower) {
        boglePowerRepository.saveAndFlush(boglePower);
    }

    @Transactional
    @Override
    public boolean closingGame(BoglePower result) {
        BoglePower power = boglePowerRepository.findOne(q.sdate.eq(result.getSdate()));
        if (power == null) {
            return true;
        }

        try {
            power.setOddeven(result.getOddeven());
            power.setOdd_overunder(result.getOdd_overunder());
            power.setEven_overunder(result.getEven_overunder());

            power.setPb_oddeven(result.getPb_oddeven());
            power.setPb_odd_overunder(result.getPb_odd_overunder());
            power.setPb_even_overunder(result.getPb_even_overunder());

            power.setOverunder(result.getOverunder());
            power.setPb_overunder(result.getPb_overunder());

            power.setPb_odd_pb_overunder(result.getPb_odd_pb_overunder());
            power.setPb_even_pb_overunder(result.getPb_even_pb_overunder());

            power.setSize(result.getSize());
            power.setOdd_size(result.getOdd_size());
            power.setEven_size(result.getEven_size());

            power.setPb(result.getPb());
            power.setBall(result.getBall());
            power.setSize(result.getSize());
            power.setSum(result.getSum());

            power.setClosing(true);

            boglePowerRepository.saveAndFlush(power);
            boglePowerGameService.closingBetting(power);
        } catch (RuntimeException e) {
            log.error("보글파워볼 {}회차 결과 업데이트에 실패하였습니다. - {}", power.getRound(), e.getMessage());
            log.info("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
        return true;
    }

    @Transactional(readOnly = true)
    @Override
    public void checkResult() {
        long cnt = boglePowerRepository.count(q.gameDate.before(new Date()).and(q.closing.isFalse()));
        ZoneConfig.getBoglePower().setResult(cnt);
    }

    @Transactional
    @Override
    public void deleteGame(int days) {
        boglePowerRepository.deleteGame(DateUtils.beforeDays(days));
    }
}
