package spoon.gameZone.PowerLotto.service;

import spoon.gameZone.power.PowerLotto;

import java.util.List;

public interface PowerLottoBotService {

    void savePowerLotto(List<PowerLotto> list);
}
