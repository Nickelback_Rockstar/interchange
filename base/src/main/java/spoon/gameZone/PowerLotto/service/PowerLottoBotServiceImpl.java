package spoon.gameZone.PowerLotto.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import spoon.gameZone.PowerLotto.PowerLottoRepository;
import spoon.gameZone.power.PowerLotto;

import java.util.List;

@Slf4j
@AllArgsConstructor
@Service
public class PowerLottoBotServiceImpl implements PowerLottoBotService {

    private PowerLottoRepository powerLottoRepository;

    @Transactional
    @Override
    public void savePowerLotto(List<PowerLotto> list) {
        for(PowerLotto pl : list){
            powerLottoRepository.saveAndFlush(pl);
        }
    }
}
