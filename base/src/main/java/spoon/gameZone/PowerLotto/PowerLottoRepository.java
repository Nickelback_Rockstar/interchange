package spoon.gameZone.PowerLotto;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import spoon.gameZone.power.PowerLotto;

public interface PowerLottoRepository extends JpaRepository<PowerLotto, Long>, QueryDslPredicateExecutor<PowerLotto> {

}
