package spoon.report.service;

import spoon.bet.domain.BetDto;
import spoon.bet.domain.BetReport;

import java.util.List;

public interface ReportListService {

    List<BetReport> reportList(BetDto.ReportCommand command);

    long reportListTotal(BetDto.ReportCommand command);
}
