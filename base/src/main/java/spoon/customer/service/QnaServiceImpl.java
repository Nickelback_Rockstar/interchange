package spoon.customer.service;

import com.querydsl.core.BooleanBuilder;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import spoon.common.utils.StringUtils;
import spoon.common.utils.WebUtils;
import spoon.customer.domain.QnaDto;
import spoon.customer.entity.QQna;
import spoon.customer.entity.Qna;
import spoon.customer.repository.QnaRepository;
import spoon.member.domain.User;
import spoon.member.service.MemberService;
import spoon.monitor.service.MonitorService;
import spoon.support.web.AjaxResult;

import java.util.Date;
import java.util.List;

@Slf4j
@AllArgsConstructor
@Service
public class QnaServiceImpl implements QnaService {

    private QnaRepository qnaRepository;

    private MemberService memberService;

    private MonitorService monitorService;

    private static QQna q = QQna.qna;

    @Transactional(readOnly = true)
    @Override
    public Page<Qna> page(String userid, Pageable pageable) {
        return qnaRepository.findAll(q.userid.eq(userid).and(q.hidden.isFalse()).and(q.enabled.isTrue()), pageable);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<Qna> page(QnaDto.Command command, Pageable pageable) {
        BooleanBuilder builder = new BooleanBuilder();

        if (StringUtils.notEmpty(command.getUsername())) {
            if (command.isMatch()) {
                builder.and((q.userid.eq(command.getUsername())).or(q.nickname.eq(command.getUsername())));
            } else {
                builder.and((q.userid.like("%" + command.getUsername() + "%")).or(q.nickname.like("%" + command.getUsername() + "%")));
            }
        }

        if (StringUtils.notEmpty(command.getSearchValue())) {
            switch (command.getSearchType()) {
                case "title":
                    builder.and(q.title.like("%" + command.getSearchValue() + "%"));
                    break;
                case "contents":
                    builder.and(q.contents.like("%" + command.getSearchValue() + "%"));
                    break;
                case "agency":
                    builder.and((q.agency1.like("%" + command.getSearchValue() + "%").or(q.agency7.like("%" + command.getSearchValue() + "%"))));
                    break;
            }
        }

        return qnaRepository.findAll(builder, pageable);
    }

    @Transactional
    @Override
    public Qna getOne(Long id) {
        Qna qna = qnaRepository.findOne(id);
        qna.setAlarm(false);
        qnaRepository.saveAndFlush(qna);
        monitorService.checkQna();
        return qna;
    }

    @Transactional
    @Override
    public AjaxResult add(QnaDto.Add add) {
        AjaxResult result = new AjaxResult();
        User user = memberService.getUser(WebUtils.userid());
        if (user == null) {
            result.setMessage("회원을 찾을 수 없습니다.");
            result.setSuccess(false);
            return result;
        }

        long count = qnaRepository.count(q.userid.eq(user.getUserid()).and(q.re.isFalse()).and(q.hidden.isFalse()));

        if(count > 0) {
            result.setMessage("이미 고객센터 문의중인 글이 있습니다..\n\n 답변을 받으신 이후에 다시 문의해주세요.");
            result.setSuccess(false);
            return result;
        }

        Qna qna = new Qna(user);
        qna.setTitle(add.getTitle());
        qna.setContents(add.getContents());
        qnaRepository.saveAndFlush(qna);
        monitorService.checkQna();
        result.setMessage("문의가 정상등록되었습니다.");
        result.setSuccess(true);
        return result;
    }

    @Transactional
    @Override
    public boolean reply(QnaDto.Reply reply) {
        Qna qna = qnaRepository.findOne(reply.getId());
        qna.setReTitle(reply.getReTitle());
        qna.setReply(reply.getReply());
        qna.setRe(true);
        qna.setReDate(new Date());
        qna.setWorker(WebUtils.userid());
        qnaRepository.saveAndFlush(qna);
        monitorService.checkQna();
        return true;
    }

    @Transactional
    @Override
    public boolean deleteChk(Long[] id) {
        if (id != null && id.length > 0) {
            for (int i = 0; i < id.length; i++) {
                Qna qna = qnaRepository.findOne(id[i]);
                qnaRepository.delete(qna);
//                qna.setHidden(true);
//                qnaRepository.saveAndFlush(qna);
            }
        }
        monitorService.checkQna();


        return true;
    }

    @Transactional
    @Override
    public boolean delete(Long id) {
        qnaRepository.delete(id);
        monitorService.checkQna();
        return true;
    }

    @Transactional
    @Override
    public AjaxResult view(Long id) {
        Qna qna = qnaRepository.findOne(id);
        AjaxResult result = new AjaxResult(true);
        if (qna.isRe()) { // 답변이 있는것만
            qna.setChecked(true);
            result.setValue("true");
        }
        return result;
    }

    @Transactional
    @Override
    public AjaxResult hidden(Long id) {
        Qna qna = qnaRepository.findOne(id);
        qna.setHidden(true);
        return new AjaxResult(true);
    }

    @Transactional
    @Override
    public AjaxResult account() {
        AjaxResult result = new AjaxResult();
        User user = memberService.getUser(WebUtils.userid());
        if (user == null) {
            result.setMessage("계좌문의에 실패하였습니다.\n\n잠시 후 다시 이용하세요.");
            return result;
        }

        // 답변하지 않고 삭제하지 않은 계좌번호 요청
        result.setSuccess(true);
        long count = qnaRepository.count(q.userid.eq(user.getUserid()).and(q.hidden.isFalse()).and(q.re.isFalse()).and(q.title.eq("계좌번호 문의 요청이 접수 되었습니다.")));
        if (count > 0) {
            result.setMessage("현재 문의중인 계좌문의 요청이 있습니다.");
            return result;
        }

        Qna qna = new Qna(user);
        qna.setTitle("계좌번호 문의 요청이 접수 되었습니다.");
        qna.setContents("계좌번호를 요청 합니다.");
        qnaRepository.saveAndFlush(qna);
        monitorService.checkQna();

        return result;
    }

    @Override
    public AjaxResult deleteAll() {
        List<Qna> qnaList = qnaRepository.findAll();
        for (Qna qna : qnaList) {
            qnaRepository.deleteAll();
//            qna.setHidden(true);
//            qnaRepository.saveAndFlush(qna);
        }
        monitorService.checkQna();
        return new AjaxResult(true, "처리되었습니다.");
    }
}
