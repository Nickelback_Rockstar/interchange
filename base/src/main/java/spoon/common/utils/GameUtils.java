package spoon.common.utils;

import lombok.extern.slf4j.Slf4j;

import java.util.Calendar;

@Slf4j
public class GameUtils {

    public static boolean notFxGame(Calendar c){
        /*
        평일 07:20 ~ 06:20 (토 05:00 까지), 일요일은 게임 없음
         */
        int dw = c.get(Calendar.DAY_OF_WEEK);
        int hhmm = Integer.parseInt(c.get(Calendar.HOUR_OF_DAY) + preFixZero(c.get(Calendar.MINUTE)));

        if(dw == 1) return true;
        if(dw == 7 && hhmm > 500) return true;
        if(dw != 7 && 720 > hhmm && hhmm > 620) return true;

        return false;
    }

    public static String preFixZero(int i) {
        String p = "000" + i;
        return p.substring(p.length()-2);
    }

}
