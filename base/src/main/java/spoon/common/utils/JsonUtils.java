package spoon.common.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import spoon.bot.sports.best.domain.BotBest;
import spoon.bot.sports.bet365.domain.BotBet365;
import spoon.bot.sports.inGame.domain.BotInGame;
import spoon.common.net.HttpParsing;
import spoon.config.domain.Config;
import spoon.gameZone.dog.Dog;
import spoon.gameZone.soccer.Soccer;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Slf4j
public abstract class JsonUtils {

    private static final ObjectMapper mapper = new ObjectMapper();

    //호출시간과 api 시간 체크
    public static boolean getApiTime(String date1){
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyyMMddHHmmss");
        Date now = new Date();
        String strDate = sdfDate.format(now);

        long d1 = Long.parseLong(date1);
        long d2 = Long.parseLong(strDate);

        if(d1 - d2 > Config.getSysConfig().getSports().getInGameApiTime()){
            return false;
        }else{
            return true;
        }

    }

    /**
     * 객체를 JSON String으로 변환한다.
     *
     * @param clazz Class 객체
     * @return 변환에 성공하면 JSON 스트링을 실패할 경우 배열형태면 빈배열 JSON 을, 클래스 형태의 경우 빈 JSON 을 반환한다.
     */
    public static String toString(Object clazz) {
        try {
            return mapper.writeValueAsString(clazz);
        } catch (JsonProcessingException e) {
            log.error("JSON 객체 변환 실패 - {}", e.getMessage());
            log.warn("{}", ErrorUtils.traceAll(e.getStackTrace()));
        }

        return null;
    }

    /**
     * JSON 객체를 모델로 변환한다.
     *
     * @param json      JSON 형태 문자열
     * @param valueType 변환할 Class 형식
     * @param <T>       변환할 클래스
     * @return 변환에 성공하면 class 객체를 실패하면 null 을 반환한다.
     */
    public static <T> T toModel(String json, Class<T> valueType) {
        try {
            return mapper.readValue(json, valueType);
        } catch (IOException e) {
            log.error("객체 변환 실패 : {}, {}", e.getMessage(), json);
            log.warn("{}", ErrorUtils.traceAll(e.getStackTrace()));
        }
        return null;
    }

    /**
     * 게임봇 List<BotBet365> 반환
     *
     * @param json
     * @return
     */
    public static List<BotBet365> toBet365List(String json) {
        try {
            return mapper.readValue(json, new TypeReference<List<BotBet365>>() {
            });
        } catch (IOException e) {
            log.warn("List<BotBet365> 객체 변환 실패 : {}", e.getMessage());
            log.warn("{}", ErrorUtils.traceAll(e.getStackTrace()));
        }
        return null;
    }

    /**
     * 게임봇 List<BotBest> 반환
     *
     * @param json
     * @return
     */
    public static List<BotBest> toBestList(String json) {
        try {
            return mapper.readValue(json, new TypeReference<List<BotBest>>() {
            });
        } catch (IOException e) {
            log.warn("List<BotBest> 객체 변환 실패 : {}", e.getMessage());
            log.warn("{}", ErrorUtils.traceAll(e.getStackTrace()));
        }
        return null;
    }

    /**
     * 게임봇 List<BotInGame> 반환
     *
     * @param json
     * @return
     */
    public static List<BotInGame> toInGameList(String json) {
        try {
            return mapper.readValue(json, new TypeReference<List<BotInGame>>() {
            });
        } catch (IOException e) {
            log.warn("List<BotInGame> 객체 변환 실패 : {}", e.getMessage());
            log.warn("{}", ErrorUtils.traceAll(e.getStackTrace()));
        }
        return null;
    }

    /**
     * 가상축구 List<Soccer> 반환
     *
     * @param json
     * @return
     */
    public static List<Soccer> toSoccerList(String json) {
        try {
            return mapper.readValue(json, new TypeReference<List<Soccer>>() {
            });
        } catch (IOException e) {
            log.warn("List<Soccer> 객체 변환 실패 : {}", e.getMessage());
            log.warn("{}", ErrorUtils.traceAll(e.getStackTrace()));
        }
        return null;
    }

    /**
     * 개경주 List<Dog> 반환
     *
     * @param json
     * @return
     */
    public static List<Dog> toDogList(String json) {
        try {
            return mapper.readValue(json, new TypeReference<List<Dog>>() {
            });
        } catch (IOException e) {
            log.warn("List<Dog> 객체 변환 실패 : {}", e.getMessage());
            log.warn("{}", ErrorUtils.traceAll(e.getStackTrace()));
        }
        return null;
    }

    public static JSONObject getJson(String url){
        JSONObject jsonObj = new JSONObject();
        String json = HttpParsing.getJson2(url);
        JSONParser parser = new JSONParser();
        if (!StringUtils.empty(json)) {
            try {
                jsonObj = (JSONObject) parser.parse(json);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return jsonObj;
    }
}
