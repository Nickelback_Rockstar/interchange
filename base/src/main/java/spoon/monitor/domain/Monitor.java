package spoon.monitor.domain;

import lombok.Data;
import spoon.gameZone.ZoneConfig;

@Data
public class Monitor {

    private long money;

    private long point;

    private long in;

    private long out;

    private long outAgency;

    // 충전
    private long deposit;

    private long alarmDeposit;

    // 환전
    private long withdraw;

    private long alarmWithdraw;

    // 고객응대
    private long qna;

    private long alarmQna;

    // 블랙회원 베팅
    private long black;

    // 신규회원
    private long member;

    // 대기회원
    private long alarmMember;

    // 게시판
    private long board;

    // 결과처리
    private long cross;

    private long special;

    private long live;

    private long inGame;

    // 정산
    private long sports;
    private long sportsEnd;

    private long inplay;
    private long inplayEnd;

    private long ladder;
    private long ladderEnd;

    private long dari;
    private long dariEnd;

    private long snail;
    private long snailEnd;

    private long newSnail;
    private long newSnailEnd;

    private long power;
    private long powerEnd;

    private long eosPower1;
    private long eosPower1End;

    private long eosPower2;
    private long eosPower2End;

    private long eosPower3;
    private long eosPower3End;

    private long eosPower4;
    private long eosPower4End;

    private long eosPower5;
    private long eosPower5End;

    private long powerFreeKick;
    private long powerFreeKickEnd;

    private long powerLadder;
    private long powerLadderEnd;

    private long aladdin;
    private long aladdinEnd;

    private long lowhi;
    private long lowhiEnd;

    private long oddeven;
    private long oddevenEnd;

    private long baccarat;
    private long baccaratEnd;

    private long lotusOddeven;
    private long lotusOddevenEnd;

    private long lotusBaccarat;
    private long lotusBaccaratEnd;

    private long lotusBaccarat2;
    private long lotusBaccarat2End;

    private long soccer;
    private long soccerEnd;

    private long dog;
    private long dogEnd;

    private long luck;
    private long luckEnd;

    private long kenoLadder;
    private long kenoLadderEnd;

    private long kenoSpeed;
    private long kenoSpeedEnd;

    private long speedHomeRun;
    private long speedHomeRunEnd;

    private long binanceCoin1;
    private long binanceCoin1End;

    private long binanceCoin3;
    private long binanceCoin3End;

    private long fxGame1;
    private long fxGame1End;

    private long fxGame2;
    private long fxGame2End;

    private long fxGame3;
    private long fxGame3End;

    private long fxGame4;
    private long fxGame4End;

    private long fxGame5;
    private long fxGame5End;

    private long bogleLadder;
    private long bogleLadderEnd;

    private long boglePower;
    private long boglePowerEnd;

    // 게임존
    public long getLadderResult() {
        return ZoneConfig.getLadder().getResult();
    }

    public long getDariResult() {
        return ZoneConfig.getDari().getResult();
    }

    public long getSnailResult() {
        return ZoneConfig.getSnail().getResult();
    }

    public long getNewSnailResult() {
        return ZoneConfig.getNewSnail().getResult();
    }

    public long getPowerResult() {
        return ZoneConfig.getPower().getResult();
    }

    public long getEosPower1Result() {
        return ZoneConfig.getEosPower1().getResult();
    }

    public long getEosPower2Result() {
        return ZoneConfig.getEosPower2().getResult();
    }

    public long getEosPower3Result() {
        return ZoneConfig.getEosPower3().getResult();
    }

    public long getEosPower4Result() {
        return ZoneConfig.getEosPower4().getResult();
    }

    public long getEosPower5Result() {
        return ZoneConfig.getEosPower5().getResult();
    }

    public long getPowerFreeKickResult() {
        return ZoneConfig.getPowerFreeKick().getResult();
    }

    public long getPowerLadderResult() {
        return ZoneConfig.getPowerLadder().getResult();
    }

    public long getAladdinResult() {
        return ZoneConfig.getAladdin().getResult();
    }

    public long getLowhiResult() {
        return ZoneConfig.getLowhi().getResult();
    }

    public long getOddevenResult() {
        return ZoneConfig.getOddeven().getResult();
    }

    public long getBaccaratResult() {
        return ZoneConfig.getBaccarat().getResult();
    }

    public long getLotusOddevenResult() {
        return ZoneConfig.getLotusOddeven().getResult();
    }

    public long getLotusBaccaratResult() {
        return ZoneConfig.getLotusBaccarat().getResult();
    }

    public long getSoccerResult() {
        return ZoneConfig.getSoccer().getResult();
    }

    public long getDogResult() {
        return ZoneConfig.getDog().getResult();
    }

    public long getLuckResult() {
        return ZoneConfig.getLuck().getResult();
    }

    public boolean getDariEnabled() {
        return ZoneConfig.getDari().isEnabled();
    }

    public boolean getLadderEnabled() {
        return ZoneConfig.getLadder().isEnabled();
    }

    public boolean getPowerEnabled() {
        return ZoneConfig.getPower().isEnabled();
    }

    public boolean getKenoLadderEnabled() {
        return ZoneConfig.getKenoLadder().isEnabled();
    }

    public boolean getKenoSpeedEnabled() {
        return ZoneConfig.getKenoSpeed().isEnabled();
    }

    public boolean getSpeedHomeRunEnabled() {
        return ZoneConfig.getSpeedHomeRun().isEnabled();
    }

    public boolean getBinanceCoin1Enabled() {
        return ZoneConfig.getBinanceCoin1().isEnabled();
    }

    public boolean getBinanceCoin3Enabled() {
        return ZoneConfig.getBinanceCoin3().isEnabled();
    }

    public boolean getFxGame1Enabled() {
        return ZoneConfig.getFxGame1().isEnabled();
    }
    public boolean getFxGame2Enabled() {
        return ZoneConfig.getFxGame2().isEnabled();
    }
    public boolean getFxGame3Enabled() {
        return ZoneConfig.getFxGame3().isEnabled();
    }
    public boolean getFxGame4Enabled() {
        return ZoneConfig.getFxGame4().isEnabled();
    }
    public boolean getFxGame5Enabled() {
        return ZoneConfig.getFxGame5().isEnabled();
    }

    public boolean getBogleLadderEnabled() {
        return ZoneConfig.getBogleLadder().isEnabled();
    }
    public boolean getBoglePowerEnabled() {
        return ZoneConfig.getBoglePower().isEnabled();
    }
}
