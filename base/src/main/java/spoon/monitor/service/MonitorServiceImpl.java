package spoon.monitor.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import spoon.banking.domain.BankingCode;
import spoon.banking.entity.QBanking;
import spoon.banking.repository.BankingRepository;
import spoon.bet.entity.QBet;
import spoon.bet.repository.BetRepository;
import spoon.board.entity.QBoard;
import spoon.board.repository.BoardRepository;
import spoon.common.utils.DateUtils;
import spoon.customer.entity.QQna;
import spoon.customer.repository.QnaRepository;
import spoon.game.domain.MenuCode;
import spoon.game.entity.QGame;
import spoon.game.repository.GameRepository;
import spoon.mapper.MonitorMapper;
import spoon.member.domain.Role;
import spoon.member.entity.QMember;
import spoon.member.repository.MemberRepository;
import spoon.monitor.domain.Monitor;
import spoon.monitor.domain.MonitorDto;

import java.util.Date;
import java.util.List;

@Slf4j
@AllArgsConstructor
@Service
public class MonitorServiceImpl implements MonitorService {

    private BankingRepository bankingRepository;

    private QnaRepository qnaRepository;

    private BetRepository betRepository;

    private MemberRepository memberRepository;

    private BoardRepository boardRepository;

    private GameRepository gameRepository;

    private MonitorMapper monitorMapper;

    private static boolean action = false;

    private static Monitor monitor = new Monitor();

    @Override
    public Monitor getMonitor() {
        return monitor;
    }

    @Override
    public void initMonitor() {
        checkDeposit();
        checkWithdraw();
        checkQna();
        checkMember();
        checkBlack();
        checkBoard();
        checkSports();
        checkInPlay();
    }

    @Transactional(readOnly = true)
    @Override
    public void checkDeposit() {
        QBanking q = QBanking.banking;
        long cnt = bankingRepository.count((
                q.bankingCode.eq(BankingCode.IN).or(q.bankingCode.eq(BankingCode.CASINO_IN))
        ).and(q.closing.isFalse()));
        long alarm = bankingRepository.count((
                q.bankingCode.eq(BankingCode.IN).or(q.bankingCode.eq(BankingCode.CASINO_IN))
        ).and(q.closing.isFalse()).and(q.alarm.isTrue()));
        monitor.setDeposit(cnt);
        monitor.setAlarmDeposit(alarm);
    }

    @Transactional(readOnly = true)
    @Override
    public void checkWithdraw() {
        QBanking q = QBanking.banking;
        long cnt = bankingRepository.count((
                q.bankingCode.eq(BankingCode.OUT).or(q.bankingCode.eq(BankingCode.CASINO_OUT))
        ).and(q.closing.isFalse()));
        long alarm = bankingRepository.count((
                q.bankingCode.eq(BankingCode.OUT).or(q.bankingCode.eq(BankingCode.CASINO_OUT))
        ).and(q.closing.isFalse()).and(q.alarm.isTrue()));
        monitor.setWithdraw(cnt);
        monitor.setAlarmWithdraw(alarm);
    }

    @Transactional(readOnly = true)
    @Override
    public void checkQna() {
        QQna q = QQna.qna;
        long cnt = qnaRepository.count(q.re.isFalse().and(q.hidden.isFalse()));
        long alarm = qnaRepository.count(q.re.isFalse().and(q.alarm.isTrue()).and(q.hidden.isFalse()));
        monitor.setQna(cnt);
        monitor.setAlarmQna(alarm);
    }

    @Transactional(readOnly = true)
    @Override
    public void checkBlack() {
        QBet q = QBet.bet;
        long cnt = betRepository.count(q.closing.isFalse().and(q.cancel.isFalse()).and(q.black.isTrue()));
        monitor.setBlack(cnt);
    }

    @Transactional(readOnly = true)
    @Override
    public void checkMember() {
        QMember q = QMember.member;
        long alarmCnt = memberRepository.count(q.role.eq(Role.USER).and(q.secession.isFalse()).and(q.enabled.isFalse()).and(q.alarm.isTrue()));
        monitor.setAlarmMember(alarmCnt);
        long cnt = memberRepository.count(q.role.eq(Role.USER).and(q.secession.isFalse()).and(q.enabled.isFalse()));
        monitor.setMember(cnt);
    }

    @Transactional(readOnly = true)
    @Override
    public void checkBoard() {
        QBoard q = QBoard.board;
        long cnt = boardRepository.count(q.alarm.isTrue());
        monitor.setBoard(cnt);
    }

    @Transactional(readOnly = true)
    @Override
    public void checkSports() {
        QGame q = QGame.game;

        long cross = gameRepository.count(q.closing.isFalse().and(q.deleted.isFalse()).and(q.cancel.isFalse()).and(q.menuCode.in(MenuCode.MATCH, MenuCode.HANDICAP, MenuCode.CROSS)).and(q.gameDate.lt(new Date())).and(q.scoreHome.isNotNull()));
        monitor.setCross(cross);

        long special = gameRepository.count(q.closing.isFalse().and(q.deleted.isFalse()).and(q.cancel.isFalse()).and(q.menuCode.eq(MenuCode.SPECIAL)).and(q.gameDate.lt(new Date())).and(q.scoreHome.isNotNull()));
        monitor.setSpecial(special);

        long live = gameRepository.count(q.closing.isFalse().and(q.deleted.isFalse()).and(q.cancel.isFalse()).and(q.menuCode.eq(MenuCode.LIVE)).and(q.gameDate.lt(new Date())).and(q.scoreHome.isNotNull()));
        monitor.setLive(live);

    }

    @Transactional(readOnly = true)
    @Override
    public void checkInPlay() {
        QGame q = QGame.game;

        long inGame = gameRepository.count(q.closing.isFalse().and(q.deleted.isFalse()).and(q.cancel.isFalse()).and(q.menuCode.eq(MenuCode.INGAME)).and(q.gameDate.lt(new Date())).and(q.scoreHome.isNotNull()));
        monitor.setInGame(inGame);
    }

    @Async
    @Transactional(readOnly = true)
    @Override
    public void checkAmount() {
        if (action) return;

        action = true;

        String start = DateUtils.format(new Date(), "yyyy-MM-dd");
        String end = DateUtils.format(DateUtils.beforeDays(-1), "yyyy-MM-dd");

        // 머니 포인트
        MonitorDto.Amount amount = monitorMapper.getAmount();
        monitor.setMoney(amount.getMoney());
        monitor.setPoint(amount.getPoint());

        // 입금 출금
        MonitorDto.Bank bank = monitorMapper.getBank(start, end);
        monitor.setIn(bank.getInAmount());
        monitor.setOut(bank.getOutAmount());
        monitor.setOutAgency(bank.getOutAmountAgency());

        // 진행금액
        List<MonitorDto.Bet> betList = monitorMapper.getBet();
        long sports = 0, inplay = 0, ladder = 0, dari = 0, snail = 0, newSnail = 0, power = 0, powerFreeKick = 0, powerLadder = 0, aladdin = 0, lowhi = 0, kenoLadder = 0, kenoSpeed = 0, speedHomeRun = 0, oddeven = 0, baccarat = 0, lotusOddeven = 0, lotusBaccarat = 0, lotusBaccarat2 = 0, soccer = 0, dog = 0, luck = 0, binanceCoin1 = 0, binanceCoin3 = 0;
        long fxGame1 = 0, fxGame2 = 0, fxGame3 = 0, fxGame4 = 0, fxGame5 = 0, bogleLadder = 0, boglePower = 0;
        long eosPower1 = 0, eosPower2 = 0, eosPower3 = 0, eosPower4 = 0, eosPower5 = 0;

        for (MonitorDto.Bet bet : betList) {
            switch (bet.getMenuCode()) {
                case MATCH:
                case HANDICAP:
                case SPECIAL:
                case LIVE:
                case CROSS:
                    sports += bet.getBetMoney();
                    break;
                case INGAME:
                    inplay += bet.getBetMoney();
                    break;
                case LADDER:
                    ladder += bet.getBetMoney();
                    break;
                case DARI:
                    dari += bet.getBetMoney();
                    break;
                case SNAIL:
                    snail += bet.getBetMoney();
                    break;
                case NEWSNAIL:
                    newSnail += bet.getBetMoney();
                    break;
                case POWER:
                    power += bet.getBetMoney();;
                    break;
                case EOSPOWER1:
                    eosPower1 += bet.getBetMoney();
                    break;
                case EOSPOWER2:
                    eosPower2 += bet.getBetMoney();
                    break;
                case EOSPOWER3:
                    eosPower3 += bet.getBetMoney();
                    break;
                case EOSPOWER4:
                    eosPower4 += bet.getBetMoney();
                    break;
                case EOSPOWER5:
                    eosPower5 += bet.getBetMoney();
                    break;
                case POWERFREEKICK:
                    powerFreeKick += bet.getBetMoney();;
                    break;
                case POWER_LADDER:
                    powerLadder += bet.getBetMoney();
                    break;
                case ALADDIN:
                    aladdin += bet.getBetMoney();;
                    break;
                case LOWHI:
                    lowhi += bet.getBetMoney();
                    break;
                case ODDEVEN:
                    oddeven += bet.getBetMoney();
                    break;
                case BACCARAT:
                    baccarat += bet.getBetMoney();
                    break;
                case LODDEVEN:
                    lotusOddeven += bet.getBetMoney();
                    break;
                case LBACCARAT:
                    lotusBaccarat += bet.getBetMoney();
                    break;
                case LBACCARAT2:
                    lotusBaccarat2 += bet.getBetMoney();
                    break;
                case SOCCER:
                    soccer += bet.getBetMoney();
                    break;
                case DOG:
                    dog += bet.getBetMoney();
                    break;
                case LUCK:
                    luck += bet.getBetMoney();
                    break;
                case KENOLADDER:
                    kenoLadder += bet.getBetMoney();
                    break;
                case KENOSPEED:
                    kenoSpeed += bet.getBetMoney();
                    break;
                case SPEEDHOMERUN:
                    speedHomeRun += bet.getBetMoney();
                    break;
                case BINANCECOIN1:
                    binanceCoin1 += bet.getBetMoney();
                    break;
                case BINANCECOIN3:
                    binanceCoin3 += bet.getBetMoney();
                    break;
                case FXGAME1:
                    fxGame1 += bet.getBetMoney();
                    break;
                case FXGAME2:
                    fxGame2 += bet.getBetMoney();
                    break;
                case FXGAME3:
                    fxGame3 += bet.getBetMoney();
                    break;
                case FXGAME4:
                    fxGame4 += bet.getBetMoney();
                    break;
                case FXGAME5:
                    fxGame5 += bet.getBetMoney();
                    break;
                case BOGLELADDER:
                    bogleLadder += bet.getBetMoney();
                    break;
                case BOGLEPOWER:
                    boglePower += bet.getBetMoney();
                    break;
            }
        }
        monitor.setSports(sports);
        monitor.setInplay(inplay);
        monitor.setLadder(ladder);
        monitor.setDari(dari);
        monitor.setSnail(snail);
        monitor.setNewSnail(newSnail);
        monitor.setEosPower1(eosPower1);
        monitor.setEosPower2(eosPower2);
        monitor.setEosPower3(eosPower3);
        monitor.setEosPower4(eosPower4);
        monitor.setEosPower5(eosPower5);
        monitor.setPower(power);
        monitor.setPowerFreeKick(powerFreeKick);
        monitor.setPowerLadder(powerLadder);
        monitor.setAladdin(aladdin);
        monitor.setLowhi(lowhi);
        monitor.setOddeven(oddeven);
        monitor.setBaccarat(baccarat);
        monitor.setLotusOddeven(lotusOddeven);
        monitor.setLotusBaccarat(lotusBaccarat);
        monitor.setLotusBaccarat2(lotusBaccarat2);
        monitor.setSoccer(soccer);
        monitor.setDog(dog);
        monitor.setLuck(luck);
        monitor.setKenoLadder(kenoLadder);
        monitor.setKenoSpeed(kenoSpeed);
        monitor.setSpeedHomeRun(speedHomeRun);
        monitor.setBinanceCoin1(binanceCoin1);
        monitor.setBinanceCoin3(binanceCoin3);
        monitor.setFxGame1(fxGame1);
        monitor.setFxGame2(fxGame2);
        monitor.setFxGame3(fxGame3);
        monitor.setFxGame4(fxGame4);
        monitor.setFxGame5(fxGame5);
        monitor.setBogleLadder(bogleLadder);
        monitor.setBoglePower(boglePower);

        // 종료금액
        List<MonitorDto.Bet> betEndList = monitorMapper.getBetEnd(start, end);
        sports = 0; inplay = 0; ladder = 0; dari = 0; snail = 0; newSnail = 0; power = 0; powerFreeKick = 0; powerLadder = 0; aladdin = 0; lowhi = 0; kenoLadder = 0; kenoSpeed = 0; speedHomeRun = 0; oddeven = 0; baccarat = 0; lotusOddeven = 0; lotusBaccarat = 0; lotusBaccarat2 = 0; soccer = 0; dog = 0; luck = 0;
        binanceCoin1 = 0; binanceCoin3 = 0;fxGame1 = 0; fxGame2 = 0; fxGame3 = 0; fxGame4 = 0; fxGame5 = 0; bogleLadder =0; boglePower =0;eosPower1 = 0; eosPower2 = 0; eosPower3 = 0; eosPower4 = 0; eosPower5 = 0;

        for (MonitorDto.Bet betEnd : betEndList) {
            switch (betEnd.getMenuCode()) {
                case MATCH:
                case HANDICAP:
                case SPECIAL:
                case LIVE:
                case CROSS:
                    sports += betEnd.getBetMoney() - betEnd.getHitMoney();
                    break;
                case INGAME:
                    inplay += betEnd.getBetMoney() - betEnd.getHitMoney();
                    break;
                case LADDER:
                    ladder += betEnd.getBetMoney() - betEnd.getHitMoney();
                    break;
                case DARI:
                    dari += betEnd.getBetMoney() - betEnd.getHitMoney();
                    break;
                case SNAIL:
                    snail += betEnd.getBetMoney() - betEnd.getHitMoney();
                    break;
                case NEWSNAIL:
                    newSnail += betEnd.getBetMoney() - betEnd.getHitMoney();
                    break;
                case POWER:
                    power += betEnd.getBetMoney() - betEnd.getHitMoney();
                    break;
                case EOSPOWER1:
                    eosPower1 += betEnd.getBetMoney() - betEnd.getHitMoney();
                    break;
                case EOSPOWER2:
                    eosPower2 += betEnd.getBetMoney() - betEnd.getHitMoney();
                    break;
                case EOSPOWER3:
                    eosPower3 += betEnd.getBetMoney() - betEnd.getHitMoney();
                    break;
                case EOSPOWER4:
                    eosPower4 += betEnd.getBetMoney() - betEnd.getHitMoney();
                    break;
                case EOSPOWER5:
                    eosPower5 += betEnd.getBetMoney() - betEnd.getHitMoney();
                    break;
                case POWERFREEKICK:
                    powerFreeKick += betEnd.getBetMoney() - betEnd.getHitMoney();
                    break;
                case POWER_LADDER:
                    powerLadder += betEnd.getBetMoney() - betEnd.getHitMoney();
                    break;
                case ALADDIN:
                    aladdin += betEnd.getBetMoney() - betEnd.getHitMoney();
                    break;
                case LOWHI:
                    lowhi += betEnd.getBetMoney() - betEnd.getHitMoney();
                    break;
                case ODDEVEN:
                    oddeven += betEnd.getBetMoney() - betEnd.getHitMoney();
                    break;
                case BACCARAT:
                    baccarat += betEnd.getBetMoney() - betEnd.getHitMoney();
                    break;
                case LODDEVEN:
                    lotusOddeven += betEnd.getBetMoney() - betEnd.getHitMoney();
                    break;
                case LBACCARAT:
                    lotusBaccarat += betEnd.getBetMoney() - betEnd.getHitMoney();
                    break;
                case LBACCARAT2:
                    lotusBaccarat2 += betEnd.getBetMoney() - betEnd.getHitMoney();
                    break;
                case SOCCER:
                    soccer += betEnd.getBetMoney() - betEnd.getHitMoney();
                    break;
                case DOG:
                    dog += betEnd.getBetMoney() - betEnd.getHitMoney();
                    break;
                case LUCK:
                    luck += betEnd.getBetMoney() - betEnd.getHitMoney();
                    break;
                case KENOLADDER:
                    kenoLadder += betEnd.getBetMoney() - betEnd.getHitMoney();
                    break;
                case KENOSPEED:
                    kenoSpeed += betEnd.getBetMoney() - betEnd.getHitMoney();
                    break;
                case SPEEDHOMERUN:
                    speedHomeRun += betEnd.getBetMoney() - betEnd.getHitMoney();
                    break;
                case BINANCECOIN1:
                    binanceCoin1 += betEnd.getBetMoney() - betEnd.getHitMoney();
                    break;
                case BINANCECOIN3:
                    binanceCoin3 += betEnd.getBetMoney() - betEnd.getHitMoney();
                    break;
                case FXGAME1:
                    fxGame1 += betEnd.getBetMoney() - betEnd.getHitMoney();
                    break;
                case FXGAME2:
                    fxGame2 += betEnd.getBetMoney() - betEnd.getHitMoney();
                    break;
                case FXGAME3:
                    fxGame3 += betEnd.getBetMoney() - betEnd.getHitMoney();
                    break;
                case FXGAME4:
                    fxGame4 += betEnd.getBetMoney() - betEnd.getHitMoney();
                    break;
                case FXGAME5:
                    fxGame5 += betEnd.getBetMoney() - betEnd.getHitMoney();
                    break;
                case BOGLELADDER:
                    bogleLadder += betEnd.getBetMoney() - betEnd.getHitMoney();
                    break;
                case BOGLEPOWER:
                    boglePower += betEnd.getBetMoney() - betEnd.getHitMoney();
                    break;
            }
        }
        monitor.setSportsEnd(sports);
        monitor.setInplayEnd(inplay);
        monitor.setLadderEnd(ladder);
        monitor.setDariEnd(dari);
        monitor.setSnailEnd(snail);
        monitor.setNewSnailEnd(newSnail);
        monitor.setPowerEnd(power);
        monitor.setEosPower1End(eosPower1);
        monitor.setEosPower2End(eosPower2);
        monitor.setEosPower3End(eosPower3);
        monitor.setEosPower4End(eosPower4);
        monitor.setEosPower5End(eosPower5);
        monitor.setPowerFreeKickEnd(powerFreeKick);
        monitor.setPowerLadderEnd(powerLadder);
        monitor.setAladdinEnd(aladdin);
        monitor.setLowhiEnd(lowhi);
        monitor.setOddevenEnd(oddeven);
        monitor.setBaccaratEnd(baccarat);
        monitor.setLotusOddevenEnd(lotusOddeven);
        monitor.setLotusBaccaratEnd(lotusBaccarat);
        monitor.setLotusBaccarat2End(lotusBaccarat2);
        monitor.setSoccerEnd(soccer);
        monitor.setDogEnd(dog);
        monitor.setLuckEnd(luck);
        monitor.setKenoLadderEnd(kenoLadder);
        monitor.setKenoSpeedEnd(kenoSpeed);
        monitor.setSpeedHomeRunEnd(speedHomeRun);
        monitor.setBinanceCoin1End(binanceCoin1);
        monitor.setBinanceCoin3End(binanceCoin3);
        monitor.setFxGame1End(fxGame1);
        monitor.setFxGame2End(fxGame2);
        monitor.setFxGame3End(fxGame3);
        monitor.setFxGame4End(fxGame4);
        monitor.setFxGame5End(fxGame5);
        monitor.setBogleLadder(bogleLadder);
        monitor.setBoglePower(boglePower);

        action = false;
    }

}
