package spoon.member.entity;

import lombok.Data;
import spoon.support.convert.CryptoConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "MVINFO")
public class MvInfo implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(columnDefinition = "NVARCHAR(64)")
    private String code;

    @Convert(converter = CryptoConverter.class)
    private String crypto1 = "";

    @Convert(converter = CryptoConverter.class)
    private String crypto2 = "";

    @Convert(converter = CryptoConverter.class)
    private String crypto3 = "";

    @Convert(converter = CryptoConverter.class)
    private String crypto4 = "";

    @Column(columnDefinition = "nvarchar(max)")
    private String json;

    @Temporal(value = TemporalType.TIMESTAMP)
    private Date regDate;

}
