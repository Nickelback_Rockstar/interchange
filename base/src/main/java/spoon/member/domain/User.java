package spoon.member.domain;

import lombok.Data;

import java.util.Date;

@Data
public class User {

    private String userid;

    private String nickname;

    private int agencyDepth;

    private String agency1;

    private String agency2;

    private String agency3;

    private String agency4;

    private String agency5;

    private String agency6;

    private String agency7;

    private Role role;

    private int level;

    private int memo;

    private int qna;

    private long money;

    private long point;

    private boolean enabled;

    private boolean secession;

    private boolean block;

    private boolean black;

    private boolean ladderBlock = false;
    private boolean dariBlock = false;
    private boolean newSnailBlock = false;
    private boolean powerBlock = false;
    private boolean powerFreeKickBlock = false;
    private boolean powerLadderBlock = false;
    private boolean kenoLadderBlock = false;
    private boolean kenoSpeedBlock = false;
    private boolean speedHomeRunBlock = false;
    private boolean lotusOddEvenBlock = false;
    private boolean lotusBaccaratBlock = false;
    private boolean lotusBaccarat2Block = false;
    private boolean binanceCoin1Block = false;
    private boolean binanceCoin3Block = false;

    private boolean oneFolderBlock;

    private boolean twoFolderBlock;

    private String loginIp;

    private long inMoney;

    private long outMoney;

    private Date loginDate;

    private long lotto;

    public long getMilliseconds() {
        return System.currentTimeMillis();
    }

}
