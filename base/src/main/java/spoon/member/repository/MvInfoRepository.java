package spoon.member.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import spoon.member.entity.MvInfo;

public interface MvInfoRepository extends JpaRepository<MvInfo, Long>, QueryDslPredicateExecutor<MvInfo> {

}
