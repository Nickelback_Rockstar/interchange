package spoon.member.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import spoon.member.entity.MvInfo;
import spoon.member.entity.QMvInfo;
import spoon.member.repository.MvInfoRepository;

@Slf4j
@AllArgsConstructor
@Service
public class MvInfoServiceImpl implements MvInfoService {

    private MvInfoRepository mvInfoRepository;

    private static QMvInfo q = QMvInfo.mvInfo;

    @Transactional
    @Override
    public void update(MvInfo mvInfo) {
        mvInfoRepository.saveAndFlush(mvInfo);
    }
}
