package spoon.member.service;

import spoon.member.entity.MvInfo;

public interface MvInfoService {
    /**
     * 회원 정보를 업데이트 한다.
     */
    void update(MvInfo mvInfo);

}
