package spoon.member.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionInterceptor;
import spoon.common.utils.DateUtils;
import spoon.common.utils.ErrorUtils;
import spoon.common.utils.StringUtils;
import spoon.mapper.ChangeMapper;
import spoon.member.domain.UserChange;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Slf4j
@AllArgsConstructor
@Service
public class MemberChangeServiceImpl implements MemberChangeService {

    private ChangeMapper changeMapper;

    @Transactional
    @Override
    public List<String> addMember(String memberText) {
        String[] lines = memberText.split(System.getProperty("line.separator"));
        List<UserChange> list = new ArrayList<>();
        List<String> error = new ArrayList<>();

        // 0:role, 1:level, 2:userid, 3:password, 4:nickname, 5:phone, 6:agency1, 7:agency2 8:joinDomain
        // 9:money, 10:point, 11:joinDate, 12:joinIp, 13:loginDate, 14:loginIp, 15:enabled, 16:secession
        // 17:bank, 18:depositor, 19:account, 20:bankPassword, 21:recommender, 22:joinCode
        // 23:rateCode, 24:rateShare, 25:rateSports, 26:rateZone, 27:deposit, 28:withdraw
        // 29:betSports, 30:betSportsCnt, 31:loginCnt, 32:memo
        for (String line : lines) {
            if (StringUtils.empty(line)) continue;
            String[] fields = Arrays.copyOf(line.split("\\t"), 34);

            UserChange user = new UserChange();

            try {
                user.setRole(fields[0]);
                user.setLevel(StringUtils.empty(fields[1]) ? 0 : Integer.parseInt(fields[1], 10));
                user.setUserid(fields[2]);
                user.setPassword(fields[3]);
                user.setNickname(fields[4]);
                user.setPhone(fields[5]);
                user.setAgency1(fields[6]);
                user.setAgency2(fields[7]);
                user.setAgency3("");
                user.setAgency4("");
                user.setAgency5("");
                user.setAgency6("");
                user.setAgency7("");
                user.setJoinDomain(fields[8]);
                user.setMoney(Integer.parseInt(fields[9], 10));
                user.setPoint(Integer.parseInt(fields[10], 10));
                user.setJoinDate(StringUtils.empty(fields[11]) ? DateUtils.parse("2017-01-01", "yyyy-MM-dd") : DateUtils.parse(fields[11], "yyyy-MM-dd HH:mm:ss"));
                user.setJoinIp(fields[12]);
                user.setLoginDate(StringUtils.empty(fields[13]) ? DateUtils.parse("2017-01-01", "yyyy-MM-dd") : DateUtils.parse(fields[13], "yyyy-MM-dd HH:mm:ss"));
                user.setLoginIp(fields[14]);
                user.setEnabled("1".equals(fields[15]));
                user.setSecession("1".equals(fields[16]));
                user.setBank(fields[17]);
                user.setDepositor(fields[18]);
                user.setAccount(fields[19]);
                user.setBankPassword(fields[20]);
                user.setRecommender(fields[21]);
                user.setJoinCode(fields[22]);
                user.setRateCode(fields[23]);
                user.setRateShare(Double.parseDouble(fields[24]));
                user.setRateSports(Double.parseDouble(fields[25]));
                user.setRateZone(Double.parseDouble(fields[26]));
                user.setDeposit(Long.parseLong(fields[27]));
                user.setWithdraw(Long.parseLong(fields[28]));
                user.setBetSports(Long.parseLong(fields[29]));
                user.setBetSportsCnt(Long.parseLong(fields[30]));
                user.setLoginCnt(Long.parseLong(fields[31]));
                user.setBalance("0".equals(fields[32]) ? 1 : 0);
                user.setMemo(fields[33]);
                list.add(user);

            } catch (RuntimeException e) {
                log.error("리스트 변경 에러 : {}", e.getMessage());
                error.add(fields[2] + " : " + e.getMessage());
            }

            if (list.size() == 30) {
                addMember(list);
                list.clear();
            }
        }

        if (list.size() > 0) addMember(list);

        return error;
    }

    private boolean addMember(List<UserChange> list) {
        try {
            changeMapper.addMember(list);
            return true;
        } catch (RuntimeException e) {
            log.error("{}", e.getMessage());
            log.error("{}", ErrorUtils.trace(e.getStackTrace()));
            TransactionInterceptor.currentTransactionStatus().setRollbackOnly();
            return false;
        }
    }

}
