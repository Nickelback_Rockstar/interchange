package spoon.support.web;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.tika.Tika;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletContext;
import java.io.File;
import java.io.IOException;

@Slf4j
@AllArgsConstructor
@Component
public class FileUploadHandler {

    private ServletContext servletContext;

    public String saveFile(MultipartFile file, String fileName, UploadType target) {

        String ext = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
        String chkExt = ext.substring(ext.length()-3).toLowerCase();
        if("gif".equals(chkExt) || "jpg".equals(chkExt) || "png".equals(chkExt)){
            // 파일업로드 gif,jpg,png 만 업로드함.
        }else{
            //에러발생
            return "";
        }

        String path = servletContext.getRealPath(servletContext.getContextPath()).replaceAll("\\\\", "/");
        File transferFile = new File(path + getTargetPath(target) + fileName + ext);

        try {
            if (!file.isEmpty()) {
                file.transferTo(transferFile);
                String mimeType = new Tika().detect(transferFile);
                if(!mimeType.startsWith("image/")){
                    transferFile.delete();
                    //이미지 타입이 아니면 패스
                    return "";
                }
            }
        } catch (IllegalStateException | IOException e) {
            e.getStackTrace();
            log.error("파일 업로드에 실패하였습니다. - {}, {}", fileName, target);
        }
        return transferFile.getName();
    }

    private String getTargetPath(UploadType target) {
        switch (target) {
            case SPORTS:
                return "images/sports/";
            case LEAGUE:
                return "images/league/";
            case NOTICE:
                return "images/notice/";
        }
        throw new RuntimeException();
    }

}
