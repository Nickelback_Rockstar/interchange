package spoon.support.web;

import org.springframework.web.util.HtmlUtils;
import spoon.bet.entity.BetItem;
import spoon.common.utils.DateUtils;
import spoon.common.utils.WebUtils;
import spoon.config.domain.Config;
import spoon.game.domain.CasinoCode;
import spoon.member.domain.Role;

import java.math.BigDecimal;
import java.util.Date;

public final class CustomTag {

    // String tag
    public static String onlyBr(String value) {
        if (value == null) return "";
        value = HtmlUtils.htmlEscape(value);
        return value.replaceAll(System.getProperty("line.separator"), "<br/>");
    }

    public static String onlyBr2(String value) {
        if (value == null) return "";
        value = HtmlUtils.htmlEscape(value);
        return value.replaceAll("(\r\n|\r|\n|\n\r)", "<br/>");
    }

    public static String colorBr(String value) {
        if (value == null) return "";
        return color(value)
                .replaceAll(System.getProperty("line.separator"), "<br/>");
    }

    public static String color(String value) {
        if (value == null) return "";
        return value
                .replaceAll("\\{\\{\\{\\{\\{", "<em class=\"color55\">").replaceAll("}}}}}", "</em>")
                .replaceAll("\\{\\{\\{\\{", "<em class=\"color44\">").replaceAll("}}}}", "</em>")
                .replaceAll("\\{\\{\\{", "<em class=\"color33\">").replaceAll("}}}", "</em>")
                .replaceAll("\\{\\{", "<em class=\"color22\">").replaceAll("}}", "</em>")
                .replaceAll("\\{", "<em class=\"color11\">").replaceAll("}", "</em>");
    }

    public static String country(String ip) {
        return WebUtils.country(ip);
    }

    // Flag
    public static String sportsFlag(BetItem item) {
        switch (item.getMenuCode()) {
            case MATCH:
            case HANDICAP:
            case CROSS:
            case SPECIAL:
            case LIVE:
            case INGAME:
            case SPORTS:
                return "/images/sports/" + Config.getSportsMap().get(item.getSports()).getSportsFlag();
            default:
                return "/images/zone/flag-" + item.getMenuCode().toString().toLowerCase() + ".png";
        }
    }

    public static String leagueFlag(BetItem item) {
        switch (item.getMenuCode()) {
            case MATCH:
            case HANDICAP:
            case CROSS:
            case SPECIAL:
            case LIVE:
            case INGAME:
            case SPORTS:
                return Config.getSysConfig().getSports().getImageUrl() + "/league/" + Config.getLeagueMap().get(item.getSports() + "-" + item.getLeague()).getLeagueFlag();
            default:
                return "/images/zone/flag-" + item.getMenuCode().toString().toLowerCase() + ".png";
        }
    }

    // Date tag
    public static String dayWeekTime(Date date) {
        if (date == null) return "-";
        return DateUtils.format(date, "MM/dd(E) ") + "<em class=\"color02\">" + DateUtils.format(date, "HH:mm") + "</em>";
    }

    // Date tag
    public static String dayWeekTime2(Date date) {
        if (date == null) return "-";
        return DateUtils.format(date, "MM/dd(E) ") + "<em class=\"color05\">" + DateUtils.format(date, "HH:mm") + "</em>";
    }

    public static String dayWeekTimes(Date date) {
        if (date == null) return "-";
        return DateUtils.format(date, "MM/dd(E) ") + "<em class=\"color02\">" + DateUtils.format(date, "HH:mm:ss") + "</em>";
    }

    public static String dayWeekTimes2(Date date) {
        if (date == null) return "-";
        return DateUtils.format(date, "MM/dd(E) ") + "<br><em class=\"color02\">" + DateUtils.format(date, "HH:mm:ss") + "</em>";
    }

    public static String dayWeek(Date date) {
        if (date == null) return "-";
        return DateUtils.format(date, "MM/dd(E)");
    }

    public static String dayWeek2(Date date) {
        if (date == null) return "-";
        return DateUtils.format(date, "yyyy/MM/dd(E)");
    }

    public static String dateWeek(Date date) {
        if (date == null) return "-";
        return DateUtils.format(date, "yyyy.MM.dd ") + "<em class=\"color02\">" + DateUtils.format(date, "(E)") + "</em>";
    }

    public static String titleDate(Date date) {
        if (date == null) return "-";
        return DateUtils.format(date, "yyyy.MM.dd(E) HH:mm:ss");
    }

    public static String fullDate(Date date) {
        if (date == null) return "-";
        return "<em class=\"color04\">" + DateUtils.format(date, "yyyy.MM.dd") + "</em><em class=\"color01\">" + DateUtils.format(date, " (E) ") + "</em>" + DateUtils.format(date, "HH:mm:ss");
    }

    public static String betDate(Date date) {
        if (date == null) return "-";
        return DateUtils.format(date, "dd(E) ") + "<em class=\"color02\">" + DateUtils.format(date, "HH:mm:ss") + "</em>";
    }

    // Number Tag
    public static String num(Long num) {
        if (num == null) return "0";
        return String.format("%,d", num);
    }

    public static int numVal(BigDecimal num) {
        return num == null ? 0 : num.intValue();
    }

    public static String handicap(double num) {
        return String.format("%,.1f", num);
    }

    public static String odds(double num) {
        return String.format("%,.2f", num);
    }

    public static String odds(double odds, double handicap) {
        if (odds == 0D && handicap == 0D) {
            return "VS";
        } else if (odds > 0) {
            return odds(odds);
        } else {
            return handicap(handicap);
        }
    }

    public static String getSize(String text, String type) {
        String size = "";
        if("size".equals(type)){
            size = text.replace("대","중");
        }else if("odd_size".equals(type)){
            size = text.replace("홀 + 대","홀 + 중");
        }else if("even_size".equals(type)){
            size = text.replace("짝 + 대","짝 + 중");
        }
        return size;
    }

    public static String inGameBetInfo(String s) {
        String betInfo[] = s.split(" #### ");
        String betTeam = "";
        if(betInfo != null && betInfo.length > 1){
            switch (betInfo[1].trim()) {
                case "1":
                    betTeam = "Home";
                    break;
                case "2":
                    betTeam = "Away";
                    break;
                case "X":
                    betTeam = "Draw";
                    break;
                default:
                    betTeam = betInfo[1];
                    break;
            }
            return betInfo[0] + " - 베팅 : " + betTeam;
        }
        return s;
    }

    public static String round(int round, int size) {
        return String.format("%0" + size + "d", round);
    }

    // role
    public static boolean isAdmin(Role role) {
        return role.getValue() >= Role.ADMIN.getValue();
    }

    public static String casinoName(int code) {
        switch (code) {
            case 1 :
                return CasinoCode.TPC1.getName();
            case 4 :
                return CasinoCode.TPC4.getName();
            case 5 :
                return CasinoCode.TPC5.getName();
            case 10:
                return CasinoCode.TPC10.getName();
            case 11:
                return CasinoCode.TPC11.getName();
            case 15:
                return CasinoCode.TPC15.getName();
            case 16:
                return CasinoCode.TPC16.getName();
            case 19:
                return CasinoCode.TPC19.getName();
            case 21:
                return CasinoCode.TPC21.getName();
            case 22:
                return CasinoCode.TPC22.getName();
            case 23:
                return CasinoCode.TPC23.getName();
            case 24:
                return CasinoCode.TPC24.getName();
            case 28:
                return CasinoCode.TPC28.getName();
            case 29:
                return CasinoCode.TPC29.getName();
            case 30:
                return CasinoCode.TPC30.getName();
            case 32:
                return CasinoCode.TPC32.getName();
            case 33:
                return CasinoCode.TPC33.getName();
            case 34:
                return CasinoCode.TPC34.getName();
            case 37:
                return CasinoCode.TPC37.getName();
            case 39:
                return CasinoCode.TPC39.getName();
            case 40:
                return CasinoCode.TPC40.getName();
            case 41:
                return CasinoCode.TPC41.getName();
            case 44:
                return CasinoCode.TPC44.getName();
            case 45:
                return CasinoCode.TPC45.getName();
            default:
                return CasinoCode.TPC99.getName();
        }
    }

}
