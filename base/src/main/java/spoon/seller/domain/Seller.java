package spoon.seller.domain;

import lombok.Data;
import spoon.member.domain.Role;

@Data
public class Seller {

    private String userid;

    private String nickname;

    private Role role;

    private String joinCode;

    private int agencyDepth;

    private String agency;
    private String agency1;
    private String agency2;
    private String agency3;
    private String agency4;
    private String agency5;
    private String agency6;
    private String agency7;

    private String rateCode;

    private double rateShare;

    private double rateSports;

    private double rateZone;

    private double rateZone1;

    private double rateZone2;

    private double ratePowerLotto;

    private long inMoney;

    private long outMoney;

    private long regMember;

    private long joinMember;

    private long betSports;

    private long betZone;

    private long betZone1;

    private long betZone2;

    private long betPowerLotto;

}
