package spoon.mapper;

import org.apache.ibatis.annotations.Param;
import spoon.sale.domain.SaleDto;
import spoon.sale.entity.PowerLottoSaleItem;
import spoon.sale.entity.SaleItem;
import spoon.seller.domain.Seller;
import spoon.seller.domain.SellerDto;

import java.util.List;

public interface ShareMapper {

    List<Seller> sellerShare(@Param("command") SellerDto.Command command);

    List<Seller> sellerShareNew(@Param("command") SellerDto.Command command);

    void updateRateCode(@Param("update") SellerDto.Update update);

    void updateRateCodeNew(@Param("update") SellerDto.Update update);

    List<SaleItem> currentSale(@Param("command") SaleDto.Command command);

    List<SaleItem> currentSaleNew(@Param("command") SaleDto.Command command);

    List<SaleItem> currentSaleDepth(@Param("command") SaleDto.Command command);

    List<SaleItem> currentSaleDepthNew(@Param("command") SaleDto.Command command);

    List<Seller> sellerList(@Param("command") SellerDto.Command command);

    List<SaleItem> currentListSale(@Param("command") SaleDto.Command command);

    List<Seller> sellerShareNewPbLotto(@Param("command") SellerDto.Command command);

    List<PowerLottoSaleItem> currentSaleNewPbLotto(@Param("command") SaleDto.Command command);

    List<PowerLottoSaleItem> currentSaleDepthPbLotto(@Param("command") SaleDto.Command command);

    List<PowerLottoSaleItem> currentSaleDepthNewPbLotto(@Param("command") SaleDto.Command command);

}
