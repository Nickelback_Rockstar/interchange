package spoon.mapper;

import org.apache.ibatis.annotations.Param;

import java.util.Date;

public interface QnaMapper {

    void deleteBeforeRegDate(@Param("regDate") Date regDate);

}
