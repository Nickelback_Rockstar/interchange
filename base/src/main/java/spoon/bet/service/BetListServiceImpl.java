package spoon.bet.service;

import com.querydsl.core.BooleanBuilder;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import spoon.bet.domain.BetDto;
import spoon.bet.entity.Bet;
import spoon.bet.entity.BetItem;
import spoon.bet.entity.QBet;
import spoon.bet.entity.QBetItem;
import spoon.bet.repository.BetItemRepository;
import spoon.bet.repository.BetRepository;
import spoon.common.utils.DateUtils;
import spoon.common.utils.StringUtils;
import spoon.game.domain.GameCode;
import spoon.game.domain.MenuCode;
import spoon.game.domain.StatusCode;
import spoon.gameZone.PowerLotto.PowerLottoRepository;
import spoon.gameZone.power.PowerLotto;
import spoon.gameZone.power.QPowerLotto;
import spoon.member.domain.Role;
import spoon.member.entity.Member;
import spoon.member.service.MemberService;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Slf4j
@AllArgsConstructor
@Service
public class BetListServiceImpl implements BetListService {

    private BetRepository betRepository;

    private PowerLottoRepository powerLottoRepository;

    private BetItemRepository betItemRepository;

    private MemberService memberService;

    private static QBet q = QBet.bet;

    @Transactional(readOnly = true)
    @Override
    public Page<Bet> adminPage(BetDto.Command command, Pageable pageable) {
        BooleanBuilder builder = new BooleanBuilder();

        // 아이디 닉네임
        if (StringUtils.notEmpty(command.getUsername())) {
            if (command.isMatch()) { // 정확하게 일치
                builder.and(q.userid.eq(command.getUsername()).or(q.nickname.eq(command.getUsername())));
            } else {
                builder.and(q.userid.like("%" + command.getUsername() + "%").or(q.nickname.like("%" + command.getUsername() + "%")));
            }
        }

        if (StringUtils.notEmpty(command.getIp())) {
            builder.and(q.ip.like(command.getIp() + "%"));
        }

        // 메뉴타입
        if (command.getMenuCode() != null) {
            switch (command.getMenuCode()) {
                case SPORTS:
                    builder.and(q.menuCode.in(MenuCode.MATCH, MenuCode.HANDICAP, MenuCode.LIVE, MenuCode.CROSS));
                    break;
                case SPECIAL:
                    builder.and(q.menuCode.eq(MenuCode.SPECIAL));
                    break;
                case INGAME:
                    builder.and(q.menuCode.eq(MenuCode.INGAME));
                    break;
                default:
                    builder.and(q.menuCode.eq(command.getMenuCode()));
            }
        }

        // 회원등급
        if (StringUtils.notEmpty(command.getRole())) {
            if ("USER".equals(command.getRole())) {
                builder.and(q.role.eq(Role.USER));
            } else if("BLACK".equals(command.getRole())){
                builder.and(q.role.eq(Role.USER).and(q.black.isTrue()));
            } else {
                builder.and(q.role.eq(Role.DUMMY));
            }
        }

        switch (command.getResult()) {
            case "ing": // 적중가능
                builder.and(q.closing.isFalse()).and(q.loseCount.eq(0)).and(q.cancel.isFalse());
                break;
            case "ready": // 대기
                builder.and(q.closing.isFalse()).and(q.cancel.isFalse());
                break;
            case "closing": // 종료
                builder.and(q.closing.isTrue()).and(q.cancel.isFalse());
                break;
            case "hit": // 적중
                builder.and(q.closing.isTrue()).and(q.hitMoney.gt(0)).and(q.cancel.isFalse());
                break;
            case "noHit": // 미적중
                builder.and(q.closing.isTrue()).and(q.hitMoney.eq(0L)).and(q.cancel.isFalse());
                break;
            case "cancel": // 취소
                builder.and(q.closing.isTrue()).and(q.cancel.isTrue());
                break;
        }

        // 베팅날짜
        if (StringUtils.notEmpty(command.getBetDate())) {
            builder.and(q.betDate.goe(DateUtils.start(command.getBetDate()))).and(q.betDate.lt(DateUtils.end(command.getBetDate())));
        }

        // 회원아이디
        if (StringUtils.notEmpty(command.getUserid())) {
            builder = builder.and(q.userid.eq(command.getUserid()));
        }

        if (StringUtils.notEmpty(command.getSpecial())) {
            builder = builder.and(q.betInfo.like("%" + command.getSpecial() + "%"));
        }

        if (StringUtils.notEmpty(command.getSports())) {
            builder = builder.and(q.betInfo.like("%" + command.getSports() + "%"));
        }

        if (StringUtils.notEmpty(command.getTeamName())) {
            builder = builder.and(q.betInfo.like("%" + command.getTeamName() + "%"));
        }

        PageRequest pageRequest = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(), getBetOrder(command.getOrderBy()));

        return betRepository.findAll(builder, pageRequest);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<Bet> userPowerBet(BetDto.UserCommand command, Pageable pageable) {
        BooleanBuilder builder = new BooleanBuilder();

        builder.and(q.userid.eq(command.getUserid())
                .and(q.betDate.goe(DateUtils.start(command.getSDate())))
                .and(q.betDate.lt(DateUtils.end(command.getEDate())))
                .and(q.menuCode.eq(MenuCode.POWER))
                .and(q.deleted.isFalse()));

        Sort sort = new Sort(Sort.Direction.DESC, "betDate");
        PageRequest pageRequest = new PageRequest(0,5,sort);

        return betRepository.findAll(builder, pageRequest);
    }

    @Override
    public Page<Bet> userPowerLadderBet(BetDto.UserCommand command, Pageable pageable) {
        BooleanBuilder builder = new BooleanBuilder();

        builder.and(q.userid.eq(command.getUserid())
                .and(q.betDate.goe(DateUtils.start(command.getSDate())))
                .and(q.betDate.lt(DateUtils.end(command.getEDate())))
                .and(q.menuCode.eq(MenuCode.POWER_LADDER))
                .and(q.deleted.isFalse()));

        Sort sort = new Sort(Sort.Direction.DESC, "betDate");
        PageRequest pageRequest = new PageRequest(0,5,sort);

        return betRepository.findAll(builder, pageRequest);
    }

    @Override
    public Page<Bet> userKenoSpeedBet(BetDto.UserCommand command, Pageable pageable) {
        BooleanBuilder builder = new BooleanBuilder();

        builder.and(q.userid.eq(command.getUserid())
                .and(q.betDate.goe(DateUtils.start(command.getSDate())))
                .and(q.betDate.lt(DateUtils.end(command.getEDate())))
                .and(q.menuCode.eq(MenuCode.KENOSPEED))
                .and(q.deleted.isFalse()));

        Sort sort = new Sort(Sort.Direction.DESC, "betDate");
        PageRequest pageRequest = new PageRequest(0,5,sort);

        return betRepository.findAll(builder, pageRequest);
    }

    @Override
    public Page<Bet> userKenoLadderBet(BetDto.UserCommand command, Pageable pageable) {
        BooleanBuilder builder = new BooleanBuilder();

        builder.and(q.userid.eq(command.getUserid())
                .and(q.betDate.goe(DateUtils.start(command.getSDate())))
                .and(q.betDate.lt(DateUtils.end(command.getEDate())))
                .and(q.menuCode.eq(MenuCode.KENOLADDER))
                .and(q.deleted.isFalse()));

        Sort sort = new Sort(Sort.Direction.DESC, "betDate");
        PageRequest pageRequest = new PageRequest(0,5,sort);

        return betRepository.findAll(builder, pageRequest);
    }

    @Override
    public Page<Bet> userBinanceCoin1(BetDto.UserCommand command, Pageable pageable) {
        BooleanBuilder builder = new BooleanBuilder();

        builder.and(q.userid.eq(command.getUserid())
                .and(q.betDate.goe(DateUtils.start(command.getSDate())))
                .and(q.betDate.lt(DateUtils.end(command.getEDate())))
                .and(q.menuCode.eq(MenuCode.BINANCECOIN1))
                .and(q.deleted.isFalse()));

        Sort sort = new Sort(Sort.Direction.DESC, "betDate");
        PageRequest pageRequest = new PageRequest(0,5,sort);

        return betRepository.findAll(builder, pageRequest);
    }

    @Override
    public Page<Bet> userBinanceCoin3(BetDto.UserCommand command, Pageable pageable) {
        BooleanBuilder builder = new BooleanBuilder();

        builder.and(q.userid.eq(command.getUserid())
                .and(q.betDate.goe(DateUtils.start(command.getSDate())))
                .and(q.betDate.lt(DateUtils.end(command.getEDate())))
                .and(q.menuCode.eq(MenuCode.BINANCECOIN3))
                .and(q.deleted.isFalse()));

        Sort sort = new Sort(Sort.Direction.DESC, "betDate");
        PageRequest pageRequest = new PageRequest(0,5,sort);

        return betRepository.findAll(builder, pageRequest);
    }

    @Override
    public Page<Bet> userFxGame1(BetDto.UserCommand command, Pageable pageable) {
        BooleanBuilder builder = new BooleanBuilder();

        builder.and(q.userid.eq(command.getUserid())
                .and(q.betDate.goe(DateUtils.start(command.getSDate())))
                .and(q.betDate.lt(DateUtils.end(command.getEDate())))
                .and(q.menuCode.eq(MenuCode.FXGAME1))
                .and(q.deleted.isFalse()));

        Sort sort = new Sort(Sort.Direction.DESC, "betDate");
        PageRequest pageRequest = new PageRequest(0,5,sort);

        return betRepository.findAll(builder, pageRequest);
    }

    @Override
    public Page<Bet> userFxGame2(BetDto.UserCommand command, Pageable pageable) {
        BooleanBuilder builder = new BooleanBuilder();

        builder.and(q.userid.eq(command.getUserid())
                .and(q.betDate.goe(DateUtils.start(command.getSDate())))
                .and(q.betDate.lt(DateUtils.end(command.getEDate())))
                .and(q.menuCode.eq(MenuCode.FXGAME2))
                .and(q.deleted.isFalse()));

        Sort sort = new Sort(Sort.Direction.DESC, "betDate");
        PageRequest pageRequest = new PageRequest(0,5,sort);

        return betRepository.findAll(builder, pageRequest);
    }

    @Override
    public Page<Bet> userFxGame3(BetDto.UserCommand command, Pageable pageable) {
        BooleanBuilder builder = new BooleanBuilder();

        builder.and(q.userid.eq(command.getUserid())
                .and(q.betDate.goe(DateUtils.start(command.getSDate())))
                .and(q.betDate.lt(DateUtils.end(command.getEDate())))
                .and(q.menuCode.eq(MenuCode.FXGAME3))
                .and(q.deleted.isFalse()));

        Sort sort = new Sort(Sort.Direction.DESC, "betDate");
        PageRequest pageRequest = new PageRequest(0,5,sort);

        return betRepository.findAll(builder, pageRequest);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<Bet> userPage(BetDto.UserCommand command, Pageable pageable) {
        return betRepository.findAll(q.userid.eq(command.getUserid())
                .and(q.betDate.goe(DateUtils.start(command.getSDate())))
                .and(q.betDate.lt(DateUtils.end(command.getEDate())))
                .and(q.deleted.isFalse()), pageable);
    }

    @Transactional(readOnly = true)
    @Override
    public Iterable<Bet> listByBetInGame() {
        return betRepository.findAll(q.menuCode.eq(MenuCode.INGAME)
                .and(q.role.eq(Role.USER))
                .and(q.closing.isFalse())
                .and(q.cancel.isFalse())
                .and(q.status.eq(StatusCode.STAY))
                .and(q.betDate.gt(DateUtils.beforeMinutes(20)))
                .and(q.betDate.lt(DateUtils.beforeMinutes(3)))
        );

    }

    @Transactional(readOnly = true)
    @Override
    public Page<Bet> sellerPage(BetDto.SellerCommand command, Pageable pageable) {
        BooleanBuilder builder = new BooleanBuilder(q.role.eq(Role.USER));

        String agency = "";
        int agencyDepth = 0;
        if (StringUtils.notEmpty(command.getAgency())) {
            Member member = memberService.getMember(command.getAgency());
            if (member != null) {
                agency = member.getUserid();
                agencyDepth = member.getAgencyDepth();
            }
        }

        System.out.println("agency=" + agency);
        System.out.println("agencyDepth=" + agencyDepth);

        if (StringUtils.notEmpty(agency) && agencyDepth > 0) {
            if (agencyDepth == 1) {
                builder.and(q.agency1.eq(agency));
            } else if (agencyDepth == 2) {
                builder.and(q.agency2.eq(agency));
            } else if (agencyDepth == 3) {
                builder.and(q.agency3.eq(agency));
            } else if (agencyDepth == 4) {
                builder.and(q.agency4.eq(agency));
            } else if (agencyDepth == 5) {
                builder.and(q.agency5.eq(agency));
            } else if (agencyDepth == 6) {
                builder.and(q.agency6.eq(agency));
            } else if (agencyDepth == 7) {
                builder.and(q.agency7.eq(agency));
            }
        } else {
            if (StringUtils.notEmpty(command.getAgency1())) {
                builder.and(q.agency1.eq(command.getAgency1()));
            }

            if (StringUtils.notEmpty(command.getAgency2())) {
                builder.and(q.agency2.eq(command.getAgency2()));
            }

            if (StringUtils.notEmpty(command.getAgency3())) {
                builder.and(q.agency3.eq(command.getAgency3()));
            }

            if (StringUtils.notEmpty(command.getAgency4())) {
                builder.and(q.agency4.eq(command.getAgency4()));
            }

            if (StringUtils.notEmpty(command.getAgency5())) {
                builder.and(q.agency5.eq(command.getAgency5()));
            }

            if (StringUtils.notEmpty(command.getAgency6())) {
                builder.and(q.agency6.eq(command.getAgency6()));
            }

            if (StringUtils.notEmpty(command.getAgency7())) {
                builder.and(q.agency7.eq(command.getAgency7()));
            }
        }


        if (StringUtils.notEmpty(command.getUsername())) {
            if (command.isMatch()) {
                builder.and(q.userid.eq(command.getUsername()).or(q.nickname.eq(command.getUsername())));
            } else {
                builder.and(q.userid.like("%" + command.getUsername() + "%").or(q.nickname.eq("%" + command.getUsername() + "%")));
            }
        }

        return betRepository.findAll(builder, pageable);
    }

    @Transactional(readOnly = true)
    @Override
    public List<Bet> listByGame(Long gameId, String betTeam) {
        QBetItem q = QBetItem.betItem;
        Iterable<BetItem> items = betItemRepository.findAll(q.gameId.eq(gameId).and(q.gameCode.in(GameCode.MATCH, GameCode.HANDICAP, GameCode.OVER_UNDER)).and(q.role.eq(Role.USER)).and(q.betTeam.eq(betTeam)));
        return StreamSupport.stream(items.spliterator(), false).map(BetItem::getBet).collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    @Override
    public Iterable<BetItem> listByGame(MenuCode menuCode, String sdate) {
        QBetItem q = QBetItem.betItem;
        return betItemRepository.findAll(q.menuCode.eq(menuCode).and(q.groupId.eq(sdate)).and(q.role.eq(Role.USER)));
    }

    @Transactional(readOnly = true)
    @Override
    public Iterable<Bet> listByBoard(Long[] betIds) {
        return betRepository.findAll(q.id.in(betIds));
    }

    @Transactional(readOnly = true)
    @Override
    public Iterable<Bet> listByBoard(String betId, String userid) {
        String[] ids = betId.split(",");
        Long[] betIds = new Long[ids.length];
        int index = 0;

        for (String id : ids) {
            betIds[index++] = Long.parseLong(id.trim());
        }

        return betRepository.findAll(q.id.in(betIds).and(q.userid.eq(userid)));
    }

    @Override
    public Page userBogleLadderBet(BetDto.UserCommand command, Pageable pageable) {
        BooleanBuilder builder = new BooleanBuilder();

        builder.and(q.userid.eq(command.getUserid())
                .and(q.betDate.goe(DateUtils.start(command.getSDate())))
                .and(q.betDate.lt(DateUtils.end(command.getEDate())))
                .and(q.menuCode.eq(MenuCode.BOGLELADDER))
                .and(q.deleted.isFalse()));

        Sort sort = new Sort(Sort.Direction.DESC, "betDate");
        PageRequest pageRequest = new PageRequest(0,5,sort);

        return betRepository.findAll(builder, pageRequest);
    }

    @Override
    public Page userBoglePowerBet(BetDto.UserCommand command, Pageable pageable) {
        BooleanBuilder builder = new BooleanBuilder();

        builder.and(q.userid.eq(command.getUserid())
                .and(q.betDate.goe(DateUtils.start(command.getSDate())))
                .and(q.betDate.lt(DateUtils.end(command.getEDate())))
                .and(q.menuCode.eq(MenuCode.BOGLEPOWER))
                .and(q.deleted.isFalse()));

        Sort sort = new Sort(Sort.Direction.DESC, "betDate");
        PageRequest pageRequest = new PageRequest(0,5,sort);

        return betRepository.findAll(builder, pageRequest);
    }

    @Override
    public Page<Bet> userEosPower1Bet(BetDto.UserCommand command, Pageable pageable) {
        BooleanBuilder builder = new BooleanBuilder();

        builder.and(q.userid.eq(command.getUserid())
                .and(q.betDate.goe(DateUtils.start(command.getSDate())))
                .and(q.betDate.lt(DateUtils.end(command.getEDate())))
                .and(q.menuCode.eq(MenuCode.EOSPOWER1))
                .and(q.deleted.isFalse()));

        Sort sort = new Sort(Sort.Direction.DESC, "betDate");
        PageRequest pageRequest = new PageRequest(0,5,sort);

        return betRepository.findAll(builder, pageRequest);
    }

    @Override
    public Page<Bet> userEosPower2Bet(BetDto.UserCommand command, Pageable pageable) {
        BooleanBuilder builder = new BooleanBuilder();

        builder.and(q.userid.eq(command.getUserid())
                .and(q.betDate.goe(DateUtils.start(command.getSDate())))
                .and(q.betDate.lt(DateUtils.end(command.getEDate())))
                .and(q.menuCode.eq(MenuCode.EOSPOWER2))
                .and(q.deleted.isFalse()));

        Sort sort = new Sort(Sort.Direction.DESC, "betDate");
        PageRequest pageRequest = new PageRequest(0,5,sort);

        return betRepository.findAll(builder, pageRequest);
    }

    @Override
    public Page<Bet> userEosPower3Bet(BetDto.UserCommand command, Pageable pageable) {
        BooleanBuilder builder = new BooleanBuilder();

        builder.and(q.userid.eq(command.getUserid())
                .and(q.betDate.goe(DateUtils.start(command.getSDate())))
                .and(q.betDate.lt(DateUtils.end(command.getEDate())))
                .and(q.menuCode.eq(MenuCode.EOSPOWER3))
                .and(q.deleted.isFalse()));

        Sort sort = new Sort(Sort.Direction.DESC, "betDate");
        PageRequest pageRequest = new PageRequest(0,5,sort);

        return betRepository.findAll(builder, pageRequest);
    }

    @Override
    public Page<Bet> userEosPower4Bet(BetDto.UserCommand command, Pageable pageable) {
        BooleanBuilder builder = new BooleanBuilder();

        builder.and(q.userid.eq(command.getUserid())
                .and(q.betDate.goe(DateUtils.start(command.getSDate())))
                .and(q.betDate.lt(DateUtils.end(command.getEDate())))
                .and(q.menuCode.eq(MenuCode.EOSPOWER4))
                .and(q.deleted.isFalse()));

        Sort sort = new Sort(Sort.Direction.DESC, "betDate");
        PageRequest pageRequest = new PageRequest(0,5,sort);

        return betRepository.findAll(builder, pageRequest);
    }

    @Override
    public Page<Bet> userEosPower5Bet(BetDto.UserCommand command, Pageable pageable) {
        BooleanBuilder builder = new BooleanBuilder();

        builder.and(q.userid.eq(command.getUserid())
                .and(q.betDate.goe(DateUtils.start(command.getSDate())))
                .and(q.betDate.lt(DateUtils.end(command.getEDate())))
                .and(q.menuCode.eq(MenuCode.EOSPOWER5))
                .and(q.deleted.isFalse()));

        Sort sort = new Sort(Sort.Direction.DESC, "betDate");
        PageRequest pageRequest = new PageRequest(0,5,sort);

        return betRepository.findAll(builder, pageRequest);
    }

    private Sort getBetOrder(String orderBy) {
        switch (orderBy) {
            case "betMoney.asc":
                return new Sort("betMoney");
            case "betMoney.desc":
                return new Sort(Sort.Direction.DESC, "betMoney");
            case "hitMoney.asc":
                return new Sort("hitMoney");
            case "hitMoney.desc":
                return new Sort(Sort.Direction.DESC, "hitMoney");
            case "expMoney.asc":
                return new Sort("expMoney");
            case "expMoney.desc":
                return new Sort(Sort.Direction.DESC, "expMoney");
            default:
                return new Sort(Sort.Direction.DESC, "betDate");
        }
    }

    @Transactional(readOnly = true)
    @Override
    public Page<PowerLotto> adminPagePbLotto(BetDto.Command command, Pageable pageable) {
        BooleanBuilder builder = new BooleanBuilder();

        QPowerLotto qpl = QPowerLotto.powerLotto;
        // 아이디 닉네임
        if (StringUtils.notEmpty(command.getUsername())) {
            if (command.isMatch()) { // 정확하게 일치
                builder.and(qpl.userid.eq(command.getUsername()).or(qpl.nickname.eq(command.getUsername())));
            } else {
                builder.and(qpl.userid.like("%" + command.getUsername() + "%").or(qpl.nickname.like("%" + command.getUsername() + "%")));
            }
        }

        switch (command.getResult()) {
            case "hit": // 적중
                builder.and(qpl.hitMoney.gt(0));
                break;
            case "noHit": // 미적중
                builder.and(qpl.hitMoney.eq(0L));
                break;
        }

        // 베팅날짜
        if (StringUtils.notEmpty(command.getBetDate())) {
            builder.and(qpl.betDate.goe(DateUtils.start(command.getBetDate()))).and(qpl.betDate.lt(DateUtils.end(command.getBetDate())));
        }

        // 회원아이디
        if (StringUtils.notEmpty(command.getUserid())) {
            builder = builder.and(qpl.userid.eq(command.getUserid()));
        }

        PageRequest pageRequest = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(), getBetOrder(command.getOrderBy()));

        return powerLottoRepository.findAll(builder, pageRequest);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<PowerLotto> sellerPagePbLotto(BetDto.SellerCommand command, Pageable pageable) {
        QPowerLotto qpl = QPowerLotto.powerLotto;

        BooleanBuilder builder = new BooleanBuilder();

        if (StringUtils.notEmpty(command.getAgency1())) {
            builder.and(qpl.agency1.eq(command.getAgency1()));
        }

        if (StringUtils.notEmpty(command.getAgency2())) {
            builder.and(qpl.agency2.eq(command.getAgency2()));
        }

        if (StringUtils.notEmpty(command.getAgency3())) {
            builder.and(qpl.agency3.eq(command.getAgency3()));
        }

        if (StringUtils.notEmpty(command.getAgency4())) {
            builder.and(qpl.agency4.eq(command.getAgency4()));
        }

        if (StringUtils.notEmpty(command.getAgency5())) {
            builder.and(qpl.agency5.eq(command.getAgency5()));
        }

        if (StringUtils.notEmpty(command.getAgency6())) {
            builder.and(qpl.agency6.eq(command.getAgency6()));
        }

        if (StringUtils.notEmpty(command.getAgency7())) {
            builder.and(qpl.agency7.eq(command.getAgency7()));
        }

        if (StringUtils.notEmpty(command.getUsername())) {
            if (command.isMatch()) {
                builder.and(qpl.userid.eq(command.getUsername()).or(qpl.nickname.eq(command.getUsername())));
            } else {
                builder.and(qpl.userid.like("%" + command.getUsername() + "%").or(qpl.nickname.eq("%" + command.getUsername() + "%")));
            }
        }

        return powerLottoRepository.findAll(builder, pageable);
    }
}
