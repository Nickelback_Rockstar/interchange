package spoon.bet.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import spoon.bet.domain.BetDto;
import spoon.bet.entity.Bet;
import spoon.bet.entity.BetItem;
import spoon.game.domain.MenuCode;
import spoon.gameZone.power.PowerLotto;

import java.util.List;

public interface BetListService {

    Page<Bet> adminPage(BetDto.Command command, Pageable pageable);

    Page<PowerLotto> adminPagePbLotto(BetDto.Command command, Pageable pageable);

    Page<Bet> userPowerBet(BetDto.UserCommand command, Pageable pageable);

    Page<Bet> userPowerLadderBet(BetDto.UserCommand command, Pageable pageable);

    Page<Bet> userKenoSpeedBet(BetDto.UserCommand command, Pageable pageable);

    Page<Bet> userKenoLadderBet(BetDto.UserCommand command, Pageable pageable);

    Page<Bet> userBinanceCoin1(BetDto.UserCommand command, Pageable pageable);

    Page<Bet> userBinanceCoin3(BetDto.UserCommand command, Pageable pageable);

    Page<Bet> userFxGame1(BetDto.UserCommand command, Pageable pageable);

    Page<Bet> userFxGame2(BetDto.UserCommand command, Pageable pageable);

    Page<Bet> userFxGame3(BetDto.UserCommand command, Pageable pageable);

    Page<Bet> userPage(BetDto.UserCommand command, Pageable pageable);

    Page<Bet> sellerPage(BetDto.SellerCommand command, Pageable pageable);

    Page<PowerLotto> sellerPagePbLotto(BetDto.SellerCommand command, Pageable pageable);

    Iterable<Bet> listByBetInGame();

    List<Bet> listByGame(Long gameId, String betTeam);

    Iterable<BetItem> listByGame(MenuCode menuCode, String sdate);

    Iterable<Bet> listByBoard(Long[] betIds);

    Iterable<Bet> listByBoard(String betId, String userid);

    Page userBogleLadderBet(BetDto.UserCommand command, Pageable pageable);

    Page userBoglePowerBet(BetDto.UserCommand command, Pageable pageable);

    Page<Bet> userEosPower1Bet(BetDto.UserCommand command, Pageable pageable);
    Page<Bet> userEosPower2Bet(BetDto.UserCommand command, Pageable pageable);
    Page<Bet> userEosPower3Bet(BetDto.UserCommand command, Pageable pageable);
    Page<Bet> userEosPower4Bet(BetDto.UserCommand command, Pageable pageable);
    Page<Bet> userEosPower5Bet(BetDto.UserCommand command, Pageable pageable);
}
