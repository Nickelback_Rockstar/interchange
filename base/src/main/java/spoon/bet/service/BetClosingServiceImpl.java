package spoon.bet.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import spoon.bet.entity.Bet;
import spoon.bet.entity.BetItem;
import spoon.bet.entity.QBetItem;
import spoon.bet.repository.BetItemRepository;
import spoon.bet.repository.BetRepository;
import spoon.common.utils.DateUtils;
import spoon.config.domain.Config;
import spoon.game.domain.GameCode;
import spoon.game.domain.GameResult;
import spoon.game.domain.StatusCode;
import spoon.gameZone.ZoneScore;
import spoon.member.domain.Role;
import spoon.member.domain.User;
import spoon.member.service.MemberService;
import spoon.payment.domain.MoneyCode;
import spoon.payment.domain.PointCode;
import spoon.payment.service.PaymentService;

import java.util.Date;

@Slf4j
@AllArgsConstructor
@Service
public class BetClosingServiceImpl implements BetClosingService {

    private BetItemRepository betItemRepository;

    private BetRepository betRepository;

    private MemberService memberService;

    private PaymentService paymentService;

    private static QBetItem q = QBetItem.betItem;

    @Transactional(readOnly = true)
    @Override
    public boolean notBetting(long gameId) {
        QBetItem q = QBetItem.betItem;
        return betItemRepository.count(q.gameId.eq(gameId).and(q.gameCode.ne(GameCode.ZONE)).and(q.cancel.isFalse()).and(q.role.eq(Role.USER))) == 0;
    }

    @Transactional
    @Override
    public void closingGameBetting(Long gameId, Integer scoreHome, Integer scoreAway, boolean cancel) {

        Iterable<BetItem> betItems = betItemRepository.findAll(q.gameId.eq(gameId).and(q.cancel.isFalse()).and(q.gameCode.ne(GameCode.ZONE)));
        for (BetItem item : betItems) {
            Bet bet = item.getBet();

            if (item.isClosing()) continue; // 이미 결과처리 된 베팅아이템이다

            if(bet.getStatus() == StatusCode.STAY) continue;

            boolean closing = item.updateScore(scoreHome, scoreAway, cancel);
            if (!closing) continue;

            bet.setPayment(true);
            bet.setClosing(true);
            bet.setClosingDate(new Date());
            betRepository.saveAndFlush(bet);

            if (bet.getRole() != Role.USER) continue; // 더미는 해 줄것이 없다.

            if (bet.getHitMoney() > 0) {
                paymentWin(bet.getId(), bet.getUserid(), bet.getHitMoney(), makeGameMemo(bet, "적중"));
                // 추천인 지급 폴더보다 폴더가 많고 베팅시 지급대상이라면
                if (bet.getBetCount() >= Config.getGameConfig().getRecommFolder() && Config.getGameConfig().isRecommType()) { // 베팅시 베팅금액 지정되어 있다면
                    paymentRecomm(bet.getId(), bet.getUserid(), bet.getBetMoney(), makeGameMemo(bet, "추천인"), true);
                }
            } else {
                // 미적중시 미적중 포인트 확인
                if (bet.getBetCount() >= Config.getGameConfig().getNoHitFolder()) {
                    paymentLose(bet.getId(), bet.getUserid(), bet.getLevel(), bet.getBetMoney(), makeGameMemo(bet, "미적중"), true);
                }
                // 미적중시 추천인 확인
                if (bet.getBetCount() >= Config.getGameConfig().getRecommFolder()) {
                    paymentRecomm(bet.getId(), bet.getUserid(), bet.getBetMoney(), makeGameMemo(bet, "추천인"), true);
                }
            }
        }
    }

    @Transactional
    @Override
    public void closingGameBetting(Long gameId, Integer scoreHome, Integer scoreAway, boolean cancel, JSONArray jsonArray) {
        //해당 로직은 인게임만 적용
        /*Iterable<BetItem> betItems = betItemRepository.findAll(q.gameId.eq(gameId).and(q.cancel.isFalse()));
        for (BetItem item : betItems) {
            Bet bet = item.getBet();
            if(bet.getStatus() == StatusCode.STAY) continue; //베팅이 대기상태면 패스, 성공베팅만 처리한다.

            if (item.isClosing()) continue; // 이미 결과처리 된 베팅아이템이다

            if(jsonArray == null || jsonArray.size() == 0){ //스코어 정보가 없는경우 패스
                //continue;
            }

            *//*
            //베팅 시간 성공여부는 배치에서 체크하니 여긴 주석처리
            boolean betStatus = false;
            for(int i=0;i<jsonArray.size();i++){
                JSONObject j = (JSONObject) jsonArray.get(i);
                Date d1 = DateUtils.parse(j.get("min").toString(), "yyyyMMddHHmmss");
                Date d2 = DateUtils.parse(j.get("max").toString(), "yyyyMMddHHmmss");

                //베팅한 시간이 해당 스코어 시간에 포함된경우
                if(bet.getBetDate().getTime() >= d1.getTime() && bet.getBetDate().getTime() <= d2.getTime()){
                    //베팅한 시간이 스코어의 마지막 시간에서 90초를 뺀 시간보다 적으면 인정
                    if( (d2.getTime() - 90000) -  bet.getBetDate().getTime() > 0){
                        betStatus = true; //인게임 베팅 성공
                    }
                }
            }
            *//*

            boolean closing;
            //인게임 베팅이 취소된경우
            *//*if(!betStatus){
                closing = item.updateScoreHit(scoreHome, scoreAway, cancel);
            }else{
                closing = item.updateScore(scoreHome, scoreAway, cancel);
            }*//*

            closing = item.updateScore(scoreHome, scoreAway, cancel);

            if (!closing) continue;

            bet.setPayment(true);
            bet.setClosing(true);
            bet.setClosingDate(new Date());
            betRepository.saveAndFlush(bet);

            if (bet.getRole() != Role.USER) continue; // 더미는 해 줄것이 없다.

            if (bet.getHitMoney() > 0) {
                paymentWin(bet.getId(), bet.getUserid(), bet.getHitMoney(), makeGameMemo(bet, "적중"));
                // 추천인 지급 폴더보다 폴더가 많고 베팅시 지급대상이라면
                if (bet.getBetCount() >= Config.getGameConfig().getRecommFolder() && Config.getGameConfig().isRecommType()) { // 베팅시 베팅금액 지정되어 있다면
                    paymentRecomm(bet.getId(), bet.getUserid(), bet.getBetMoney(), makeGameMemo(bet, "추천인"), true);
                }
            } else {
                // 미적중시 미적중 포인트 확인
                if (bet.getBetCount() >= Config.getGameConfig().getNoHitFolder()) {
                    paymentLose(bet.getId(), bet.getUserid(), bet.getLevel(), bet.getBetMoney(), makeGameMemo(bet, "미적중"), true);
                }
                // 미적중시 추천인 확인
                if (bet.getBetCount() >= Config.getGameConfig().getRecommFolder()) {
                    paymentRecomm(bet.getId(), bet.getUserid(), bet.getBetMoney(), makeGameMemo(bet, "추천인"), true);
                }
            }
        }*/
    }

    @Transactional
    @Override
    public void rollbackBetting(Long gameId) {
        QBetItem qi = QBetItem.betItem;
        Iterable<BetItem> betItems = betItemRepository.findAll(qi.gameId.eq(gameId).and(qi.gameCode.ne(GameCode.ZONE)));
        rollbackBetting(betItems);
    }

    @Transactional
    @Override
    public void rollbackBetting(Iterable<BetItem> betItems) {
        for (BetItem item : betItems) {
            Bet bet = item.getBet();
            if (bet.getRole() != Role.USER) continue;

            if (bet.isPayment()) {
                if (bet.getHitMoney() > 0) {
                    paymentService.rollbackMoney(MoneyCode.WIN, item.getBet().getId(), item.getBet().getUserid());
                } else {
                    paymentService.rollbackPoint(PointCode.LOSE, item.getBet().getId(), item.getBet().getUserid());
                }
                paymentService.rollbackRecomm(PointCode.RECOMM, item.getBet().getId(), item.getBet().getUserid());
            }

            item.reset();
            if (bet.isPayment()) bet.setCancel(false);
            bet.setClosing(false);
            bet.setPayment(false);
            bet.setHitMoney(0);
            betRepository.saveAndFlush(bet);
        }
    }

    @Transactional
    @Override
    public void closingZoneBetting(Bet bet, ZoneScore zoneScore) {
        BetItem item = bet.getBetItems().get(0);

        item.updateScore(zoneScore);
        bet.setPayment(true);
        bet.setClosing(true);
        bet.setClosingDate(new Date());

        betRepository.saveAndFlush(bet);

        if (bet.getRole() != Role.USER) return;

        if (bet.getHitMoney() > 0) {
            paymentWin(bet.getId(), bet.getUserid(), bet.getHitMoney(), makeZoneMemo(item, "적중"));
            if (Config.getGameConfig().isRecommType()) { // 베팅시 베팅금액 지정되어 있다면
                paymentRecomm(bet.getId(), bet.getUserid(), bet.getBetMoney(), makeZoneMemo(item, "추천인"), false);
            }
        } else {
            // 미적중시 미적중 포인트 확인
            paymentLose(bet.getId(), bet.getUserid(), bet.getLevel(), bet.getBetMoney(), makeZoneMemo(item, "미적중"), false);
            // 미적중시 추천인 확인
            paymentRecomm(bet.getId(), bet.getUserid(), bet.getBetMoney(), makeZoneMemo(item, "추천인"), false);
        }
    }

    @Transactional
    @Override
    public void closingGameBetting(BetItem bot, int result) {
        //해당 로직은 인게임만 적용
        //같은 베팅 타입 경기들 조회
        Iterable<BetItem> betItems = betItemRepository.findAll(q.gameId.eq(bot.getGameId()).and(q.cancel.isFalse()).and(q.closing.isFalse()));
        for (BetItem item : betItems) {
            Bet bet = item.getBet();
            boolean closing;

            closing = item.updateScore(result);

            if (!closing) continue;

            bet.setPayment(true);
            bet.setClosing(true);
            bet.setClosingDate(new Date());
            betRepository.saveAndFlush(bet);

            if (bet.getRole() != Role.USER) continue; // 더미는 해 줄것이 없다.

            if (bet.getHitMoney() > 0) {
                paymentWin(bet.getId(), bet.getUserid(), bet.getHitMoney(), makeZoneMemo(item, "적중"));
                if (Config.getGameConfig().isRecommType()) { // 베팅시 베팅금액 지정되어 있다면
                    paymentRecomm(bet.getId(), bet.getUserid(), bet.getBetMoney(), makeZoneMemo(item, "추천인"), false);
                }
            } else {
                // 미적중시 미적중 포인트 확인
                paymentLose(bet.getId(), bet.getUserid(), bet.getLevel(), bet.getBetMoney(), makeZoneMemo(item, "미적중"), false);
                // 미적중시 추천인 확인
                paymentRecomm(bet.getId(), bet.getUserid(), bet.getBetMoney(), makeZoneMemo(item, "추천인"), false);
            }
        }
    }

    private void paymentRecomm(Long betId, String userid, long betMoney, String memo, boolean sports) {
        // 추천인 미적중 포인트가 지급이 아니라면
        if (!Config.getGameConfig().isRecommPayment()) return;

        User recomm = memberService.getRecomm(userid);
        if (recomm != null) {
            // 스포츠와 게임존 구분
            double recommRate = sports ? Config.getGameConfig().getNoHitSportsRecommOdds()[recomm.getLevel()] : Config.getGameConfig().getNoHitZoneRecommOdds()[recomm.getLevel()];
            if (recommRate > 0) {
                long recommPoint = (long) (betMoney * recommRate / 100);
                if (paymentService.notPaidPoint(PointCode.RECOMM, betId, recomm.getUserid())) {
                    paymentService.addPoint(PointCode.RECOMM, betId, recomm.getUserid(), recommPoint, memo);
                }
            }
        }
    }

    private void paymentLose(Long betId, String userid, int level, long betMoney, String memo, boolean sports) {
        // 스포츠와 게임존 구분
        double rate = sports ? Config.getGameConfig().getNoHitSportsOdds()[level] : Config.getGameConfig().getNoHitZoneOdds()[level];
        if (rate > 0) {
            long losePoint = (long) (betMoney * rate / 100);
            if (paymentService.notPaidPoint(PointCode.LOSE, betId, userid)) {
                paymentService.addPoint(PointCode.LOSE, betId, userid, losePoint, memo);
            }
        }
    }

    private void paymentWin(Long betId, String userid, long hitMoney, String memo) {
        if (paymentService.notPaidMoney(MoneyCode.WIN, betId, userid)) {
            paymentService.addMoney(MoneyCode.WIN, betId, userid, hitMoney, memo);
        }
    }

    private String makeGameMemo(Bet bet, String result) {
        return bet.getMenuCode().getName() + " " + bet.getBetCount() + "폴더 " + result + " " + bet.getBetMoney() + "원베팅";
    }

    private String makeZoneMemo(BetItem item, String result) {
        return item.getMenuCode().getName() + " " + item.getLeague() + " " + item.getSpecial() + " " + result;
    }
}
