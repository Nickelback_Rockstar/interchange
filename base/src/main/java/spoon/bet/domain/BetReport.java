package spoon.bet.domain;

import lombok.Data;

@Data
public class BetReport {

    private int betCount;
    private int totalCount;
    private long betMoney;
    private long hitMoney;
    private int level;
    private String nickname;
    private String userid;

}
